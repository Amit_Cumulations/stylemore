package com.cumulations.stylemore

import android.content.Context
import android.content.Intent
import android.support.multidex.MultiDexApplication
import android.support.v7.app.AppCompatDelegate
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.cumulations.stylemore.base.firebase.FirebaseSyncService
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper.get
import com.cumulations.stylemore.base.utils.Utils
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.google.firebase.database.FirebaseDatabase
import io.fabric.sdk.android.Fabric


/**
 * Created by Amit Tumkur on 29-05-2018.
 */
class StyleMoreApp: MultiDexApplication() {

    companion object {
        private lateinit var context: StyleMoreApp
        lateinit var auth_key:String
        lateinit var user_id:String
        lateinit var IMAGE_BASE_URL:String
        var fcmAuthKey:String = ""

        fun getContext(): Context {
            return context
        }
    }

    override fun onCreate() {
        super.onCreate()
        context = this
        /* Enable disk persistence  */
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        FacebookSdk.sdkInitialize(this)
        AppEventsLogger.activateApp(context)

        Fabric.with(this, Crashlytics())

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        val userPreferences = SharedPreferenceHelper.customPrefs(this, Constants.USER_PREF)
        auth_key = userPreferences[Constants.AUTH_KEY,""]!!
        user_id = userPreferences[Constants.USER_ID,""]!!
        IMAGE_BASE_URL = userPreferences[Constants.USER_IMAGE_BASE_URL,""]!!

        /*start firebase sync service*/
        /*fix for Android 8.0: java.lang.IllegalStateException: Not allowed to start service Intent*/
//        ContextCompat.startForegroundService(this,Intent(this,FirebaseSyncService::class.java))
        if (user_id.isNotEmpty() && user_id.toInt()>0) {
            try {
                startService(Intent(applicationContext, FirebaseSyncService::class.java))
            } catch (e:Exception){
                e.printStackTrace()
            }

            fcmAuthKey = "key=${BuildConfig.FCM_SERVER_KEY}"
        }

        /*val key = FacebookSdk.getApplicationSignature(this)
        Log.e("facebook hashkey",key)*/
        if (BuildConfig.DEBUG)
            Utils.logFbHashKey(this)
    }
}