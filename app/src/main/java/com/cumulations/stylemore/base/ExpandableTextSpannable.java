package com.cumulations.stylemore.base;

import android.graphics.Color;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

public class ExpandableTextSpannable extends ClickableSpan {

    private boolean isUnderline = false;

    /**
     * Constructor
     */
    public ExpandableTextSpannable(boolean isUnderline) {
        this.isUnderline = isUnderline;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setUnderlineText(isUnderline);
        ds.setColor(Color.parseColor("#FF4081"));
    }

    @Override
    public void onClick(View widget) {

    }
}