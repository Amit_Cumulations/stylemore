package com.cumulations.stylemore.base

/**
 * Created by Amit Tumkur on 31-05-2018.
 */
data class HttpErrorResponse(val status: String, val description: String)