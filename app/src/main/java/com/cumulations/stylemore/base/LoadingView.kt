package com.cumulations.stylemore.base

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

interface LoadingView {
    fun showLoading()
    fun hideLoading()
}
