package com.cumulations.stylemore.base

import rx.Single

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

abstract class UseCase<RequestObj, ResponseOnj> {
    abstract fun buildUseCase(requestObj: RequestObj): Single<ResponseOnj>

    fun execute(requestObj: RequestObj): Single<ResponseOnj> {
        return buildUseCase(requestObj)
    }
}
