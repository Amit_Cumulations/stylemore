package com.cumulations.stylemore.base.fcm

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.support.v4.app.TaskStackBuilder
import android.support.v4.content.ContextCompat
import android.util.Log
import com.cumulations.stylemore.R
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.firebase.FbActionUser
import com.cumulations.stylemore.base.firebase.FbNotification
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.notifications.NotificationsActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONException
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL


class FbMessagingService : FirebaseMessagingService() {
    companion object {
        private val TAG = FbMessagingService::class.java.simpleName
        private var REQUEST_NOTIFICATIONS = 1001
        private const val SUMMARY_ID = 21
        private const val GROUP_KEY = "StyleMoreGroup"
    }

    private var notificationId:Int = 0
    private val smallIcon: Int
        get() = if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP)
            R.mipmap.ic_stat_launcher
        else
            R.mipmap.ic_launcher

    private var notificationManager: NotificationManager? = null

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage!!.from!!)

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)
            // Also if you intend on generating your own notifications as a result of a received FCM
            // message, here is where that should be initiated. See sendNotification method below.
            createNotification(parseFbNotification(remoteMessage.data))
//            parseFbNotification(remoteMessage.data)
        }

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.notification!!.body!!)
        }
    }

    private fun getNotificationManager(): NotificationManager {
        if (notificationManager == null) {
            notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }
        return notificationManager!!
    }

    private fun parseFbNotification(remoteMessageData: Map<String, String>): FbNotification? {
        val fbActionUser = FbActionUser()
        val fbActionUserString = remoteMessageData["fbActionUser"]
        try {
            val fbActionUserJsonObject = JSONObject(fbActionUserString)
            fbActionUser.name = fbActionUserJsonObject.getString("name")
            fbActionUser.profPicUrl = fbActionUserJsonObject.getString("profPicUrl")
            fbActionUser.userId = java.lang.Long.parseLong(fbActionUserJsonObject.getString("userId"))
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        var seen = false
        if (remoteMessageData["seen"].equals("true", false)) {
            seen = true
        }

        var clicked = false
        if (remoteMessageData["clicked"].equals("true", false)) {
            clicked = true
        }

        if(!StyleMoreApp.user_id.equals(remoteMessageData["actionToUserId"],true)){
            return null
        }

        return FbNotification(
                remoteMessageData["actionId"]!!.toLong(),
                remoteMessageData["actionName"]!!,
                fbActionUser,
                remoteMessageData["actionFeedId"]!!.toLong(),
                remoteMessageData["actionFeedPic"],
                remoteMessageData["timestamp"]!!.toLong(),
                seen,
                clicked
        )
    }
    // [END receive_message]

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String?) {
        Log.d(TAG, "Refreshed token: " + token!!)
        sendRegistrationToServer(token)
    }

    private fun sendRegistrationToServer(token: String?) {
        // TODO: Implement this method to send token to your app server.
        Utils.updateTokenInFirebaseDb(token)
        val userSharedPreferences = SharedPreferenceHelper.customPrefs(this, Constants.USER_PREF)
        userSharedPreferences.edit().putString(Constants.FCM_TOKEN, token).apply()
    }

    private fun createNotification(fbNotification: FbNotification?) {

        if(fbNotification == null){
            return
        }

        if (Utils.appInForeground(this)){
            Log.d("createNotification","app foreground")
            return
        }

        val content = StringBuilder()
        val title = "StyleMore"
        when(fbNotification.actionName){
            "liked"->{
                content.append("${fbNotification.fbActionUser.name} liked your feed")
            }

            "commented"->{
                content.append("${fbNotification.fbActionUser.name} commented on your feed")
            }

            "followed"->{
                content.append("${fbNotification.fbActionUser.name} started following you")
            }
        }

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupFeedsNotificationChannel()
        }


        val intent = Intent(this, NotificationsActivity::class.java)
        val stackBuilder = TaskStackBuilder.create(this)
        stackBuilder.addNextIntentWithParentStack(intent)
        val resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        val channelId = getString(R.string.feeds_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val singleNotificationBuilder = NotificationCompat.Builder(this, channelId)
        singleNotificationBuilder.color = ContextCompat.getColor(this,R.color.colorAccent)
        singleNotificationBuilder.setSmallIcon(smallIcon)
        /*singleNotificationBuilder.setLargeIcon(BitmapFactory.decodeResource(resources,R.mipmap.ic_notification_big))*/
        singleNotificationBuilder.setContentTitle(title)
        //set content text to support devices running API level < 24
        singleNotificationBuilder.setContentText(content.toString())
        singleNotificationBuilder.setStyle(NotificationCompat.BigTextStyle()
                        .setBigContentTitle(title)
                        .bigText(content.toString()))
        singleNotificationBuilder.setAutoCancel(true)
        singleNotificationBuilder.setSound(defaultSoundUri)
        singleNotificationBuilder.setContentIntent(resultPendingIntent)
        singleNotificationBuilder.setShowWhen(true)

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.N) {
            //specify which group this notification belongs to
            singleNotificationBuilder.setGroup(GROUP_KEY)
        }

        if (fbNotification.actionFeedPic.isNotEmpty()) {
            val imageBitmap = getBitmapFromUrl(fbNotification.actionFeedPic) //obtain the image
            if (imageBitmap != null) {
                singleNotificationBuilder.setStyle(NotificationCompat.BigPictureStyle()
                        .setBigContentTitle(title)
                        .setSummaryText(content.toString())
                        .bigPicture(imageBitmap)
                        /*.bigLargeIcon(BitmapFactory.decodeResource(resources,
                                R.mipmap.ic_notification_big))*/)
            }
        }

        var notificId = -1
        when(fbNotification.actionName){
            "liked"->{
                try {
                    notificId = fbNotification.actionFeedId.toInt()+fbNotification?.fbActionUser.userId.toInt()
                } catch (e:Exception){
                    e.printStackTrace()
                }
            }

            "followed"->{
                try {
                    notificId = fbNotification?.fbActionUser.userId.toInt() + 11
                } catch (e:Exception){
                    e.printStackTrace()
                }
            }

            else -> notificId = notificationId++
        }

        val singleNotification = singleNotificationBuilder.build()
        getNotificationManager().notify(notificId /* ID of notification */, singleNotification)
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.N) {
            val summaryNotification = buildSummaryNotification(resultPendingIntent)
            getNotificationManager().notify(SUMMARY_ID, summaryNotification)
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupFeedsNotificationChannel() {
        val channelId = getString(R.string.feeds_notification_channel_id)

        if (Utils.isNotificationChannelEnabled(this,channelId))
            return

        val channelName = "StyleMore feeds"
        val channelDescription = "Push notifications of users activities on feeds and follows from StyleMore"
        val notificationChannel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)
        notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        notificationChannel.description = channelDescription
        notificationChannel.enableLights(true)
        notificationChannel.lightColor = R.color.colorAccent
        notificationChannel.enableVibration(false)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val audioAttributes = AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION_EVENT)
                .build()
        notificationChannel.setSound(defaultSoundUri, audioAttributes)
        getNotificationManager().createNotificationChannel(notificationChannel)
    }

    private fun getBitmapFromUrl(imageUrl: String): Bitmap? {
        try {
            val url = URL(StyleMoreApp.IMAGE_BASE_URL+imageUrl)
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input = connection.inputStream
            return BitmapFactory.decodeStream(input)
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }

    }

    private fun buildSummaryNotification(resultPendingIntent: PendingIntent?): Notification {
        return NotificationCompat.Builder(this,getString(R.string.feeds_notification_channel_id))
                .setContentTitle("StyleMore feeds")
                .setContentText("You have unread notifications of your feeds")
                .setSmallIcon(smallIcon)
                .setShowWhen(true)
                .setGroup(GROUP_KEY)
                .setGroupSummary(true)
                /*to open when grouped notifications are clicked*/
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true)
                .build()
    }
}