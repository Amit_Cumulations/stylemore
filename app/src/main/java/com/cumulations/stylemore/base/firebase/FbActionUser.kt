package com.cumulations.stylemore.base.firebase

import java.io.Serializable

class FbActionUser : Serializable {
    lateinit var name: String
    lateinit var profPicUrl: String
    var userId: Long = 0

    constructor() {
        // Default constructor required for calls to DataSnapshot.getValue(FbActionUser.class)
    }

    constructor(name: String, profPicUrl: String, userId: Long) {
        this.name = name
        this.profPicUrl = profPicUrl
        this.userId = userId
    }
}