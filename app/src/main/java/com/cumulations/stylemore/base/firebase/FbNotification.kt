package com.cumulations.stylemore.base.firebase

import java.io.Serializable

class FbNotification : Serializable {
    var actionId: Long = 0
    lateinit var actionName: String
    lateinit var fbActionUser: FbActionUser
    var actionFeedId: Long = 0
    var actionFeedPic:String = "NIL"
    var timestamp: Long = 0
    var seen:Boolean = false
    var fbKey:String = ""
    var actionToUserId = ""
    var clicked = false

    constructor() {
        // Default constructor required for calls to DataSnapshot.getValue(FbActionUser.class)
    }

    constructor(actionId: Long, actionName: String, fbActionUser: FbActionUser, actionFeedId: Long, actionFeedPic: String?,
                timestamp: Long, seen: Boolean, clicked: Boolean) {
        this.actionId = actionId
        this.actionName = actionName
        this.fbActionUser = fbActionUser
        this.actionFeedId = actionFeedId
        this.actionFeedPic = actionFeedPic!!
        this.timestamp = timestamp
        this.seen = seen
        this.clicked = clicked
    }
}