package com.cumulations.stylemore.base.firebase

import java.util.*

/**
 * Created by Amit Tumkur on 30-07-2018.
 */
interface FirebaseSyncListener {
    fun provideSyncedList(fbNotificationList: LinkedList<FbNotification?>)
    fun provideNotificationCount(notificationCount: Int)
    fun showLoader(loading: Boolean)
}