package com.cumulations.stylemore.base.firebase

import android.app.AlarmManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.util.Log
import com.cumulations.stylemore.StyleMoreApp
import com.google.firebase.database.*
import java.lang.Exception
import java.util.*


/**
 * Created by Amit Tumkur on 30-07-2018.
 */
class FirebaseSyncService : Service() {
    private lateinit var userIdListenerDbReference: DatabaseReference
    private lateinit var likedDbReference: DatabaseReference
    private lateinit var commentedDbReference: DatabaseReference
    private lateinit var followedDbReference: DatabaseReference

    private lateinit var userIdDbChildEventListener: ChildEventListener
    //    private lateinit var userIdDbValueEventListener: ValueEventListener
    var totalNotificationCount = 0
    var commentCount = 0
    var likeCount = 0
    var followCount = 0
    lateinit var fbNotificationList: LinkedList<FbNotification?>
    lateinit var likeList: LinkedList<FbNotification?>
    lateinit var commentList: LinkedList<FbNotification?>
    lateinit var followList: LinkedList<FbNotification?>

    /*Listener to provide data to UI*/
    private var firebaseSyncListener: FirebaseSyncListener? = null

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    inner class FirebaseSyncBinder : Binder() {
        fun getService(): FirebaseSyncService {
            return this@FirebaseSyncService
        }
    }

    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private val mBinder = FirebaseSyncBinder()

    private val REMOVE_LISTENERS_TIMER:Long = 2*60*1000
    private var removeListenersHandler:Handler? = null
    private var removeListenersRunnable:Runnable? = null

    override fun onCreate() {
        super.onCreate()
        Log.v("FirebaseSyncService","onCreate")
        setRecurringAlarmForService()
        fbNotificationList = LinkedList()
        likeList = LinkedList()
        commentList = LinkedList()
        followList = LinkedList()
        removeListenersHandler = Handler()
        FirebaseDatabase.getInstance().goOnline()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        /*Make or open connection to firebase db server*/
        if (intent != null) {
            Log.d("onStartCommand", "" + System.currentTimeMillis())
            fbNotificationList.clear()
            likeList.clear()
            commentList.clear()
            followList.clear()
            totalNotificationCount = 0
            likeCount = 0
            commentCount = 0
            followCount = 0

            FirebaseDatabase.getInstance().goOnline()
            userIdListenerDbReference = FirebaseDatabase.getInstance().reference
                    .child("Notifications")
                    .child(StyleMoreApp.user_id)
            likedDbReference = userIdListenerDbReference.child("liked")
            commentedDbReference = userIdListenerDbReference.child("commented")
            followedDbReference = userIdListenerDbReference.child("followed")

            /*orderByChild -> ascending order of mentioned child key, take last 10 values from sorted list
            * giving highest to lowest of time*/
            val likedQuery = likedDbReference.orderByChild("timestamp").limitToLast(10)
            val commentedQuery = commentedDbReference.limitToFirst(10)/*.orderByKey()*/
            val followedQuery = followedDbReference.orderByChild("timestamp").limitToLast(10)

            likedQuery.addValueEventListener(likedDbValueEventListener)
            commentedQuery.addValueEventListener(commentedDbValueEventListener)
            followedQuery.addValueEventListener(followedDbValueEventListener)

            removeListenersRunnable = Runnable {
                Log.i("removeListenersRunnable","${System.currentTimeMillis()}")
                likedQuery.removeEventListener(likedDbValueEventListener)
                commentedQuery.removeEventListener(commentedDbValueEventListener)
                followedQuery.removeEventListener(followedDbValueEventListener)
                FirebaseDatabase.getInstance().goOffline()
                return@Runnable
            }

            removeListenersHandler?.removeCallbacks(removeListenersRunnable)
            removeListenersHandler?.postDelayed(removeListenersRunnable,REMOVE_LISTENERS_TIMER)
        }

        return START_NOT_STICKY
    }

    override fun onBind(p0: Intent?): IBinder? {
        return /*null*/mBinder
    }

    private val likedDbValueEventListener = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) {
            Log.e("onCancelled", "{${p0.details}}")
        }

        override fun onDataChange(p0: DataSnapshot) {
//            firebaseSyncListener?.showLoader(true)
            likeCount = 0
            likeList.clear()
            for (uniqueKeydataSnapShot in p0.children) {
                val fbNotification = uniqueKeydataSnapShot.getValue(FbNotification::class.java)
//                if (!fbNotificationList.contains(fbNotification)) {
                try {
                    Log.i("onDataChange", "${fbNotification?.fbActionUser?.name}, ${fbNotification?.actionName}," +
                            " ${fbNotification?.seen}")
                    likeList.add(fbNotification)

                    if (!fbNotification?.seen!!)
                        likeCount++
                } catch (e:Exception){
                    e.printStackTrace()
                }
//                }
            }
            updateListAndCount()
        }

    }

    private val commentedDbValueEventListener = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) {
            Log.e("onCancelled", "{${p0.details}}")
        }

        override fun onDataChange(p0: DataSnapshot) {
//            firebaseSyncListener?.showLoader(true)
            commentCount = 0
            commentList.clear()
            for (feedIdKeydataSnapShot in p0.children) {
                for (fbNotificationSnapShot in feedIdKeydataSnapShot.children) {
                    val fbKey = fbNotificationSnapShot.key
                    val fbNotification = fbNotificationSnapShot.getValue(FbNotification::class.java)
                    fbNotification?.fbKey = fbKey!!
                    try {
                        Log.i("onDataChange", "${fbNotification?.fbActionUser?.name}, ${fbNotification?.actionName}," +
                                " ${fbNotification?.seen}")
                        commentList.add(fbNotification)

                        if (!fbNotification?.seen!!)
                            commentCount++
                    } catch (e:Exception){
                        e.printStackTrace()
                    }
                }
            }
            updateListAndCount()
        }

    }

    private val followedDbValueEventListener = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) {
            Log.e("onCancelled", "{${p0.details}}")
        }

        override fun onDataChange(p0: DataSnapshot) {
//            firebaseSyncListener?.showLoader(true)
            followCount = 0
            followList.clear()
            for (uniqueKeydataSnapShot in p0.children) {
                val fbNotification = uniqueKeydataSnapShot.getValue(FbNotification::class.java)
//                if (!fbNotificationList.contains(fbNotification)) {
                try {
                    Log.i("onDataChange", "${fbNotification?.fbActionUser?.name}, ${fbNotification?.actionName}," +
                            " ${fbNotification?.seen}")
                    followList.add(fbNotification)

                    if (!fbNotification?.seen!!)
                        followCount++
                } catch (e:Exception){
                    e.printStackTrace()
                }
//                }
            }

            updateListAndCount()
        }

    }

    private fun updateListAndCount() {
        totalNotificationCount = likeCount + commentCount + followCount

        fbNotificationList.clear()
        fbNotificationList.addAll(likeList)
        fbNotificationList.addAll(commentList)
        fbNotificationList.addAll(followList)

        firebaseSyncListener?.provideSyncedList(fbNotificationList)
        firebaseSyncListener?.provideNotificationCount(totalNotificationCount)
//        firebaseSyncListener?.showLoader(false)
    }

    fun setFbSyncListener(firebaseSyncListener: FirebaseSyncListener) {
        this.firebaseSyncListener = firebaseSyncListener
    }

    fun removeFbSyncListener() {
        firebaseSyncListener = null
    }

    private fun setRecurringAlarmForService() {
        val fbSyncService = Intent(this, FirebaseSyncService::class.java)
        fbSyncService.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val pendingIntent = PendingIntent.getService(this, 0, fbSyncService, PendingIntent.FLAG_UPDATE_CURRENT)
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        // should be AlarmManager.INTERVAL_DAY (but changed to 15min for testing)
        /*after 2 mins event listener is removed, so start service after 30 secs ie 2min 30sec */
        val triggerAtMillis = System.currentTimeMillis() + 2 * (60 + 15) * 1000
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, triggerAtMillis, 2 * (60 + 15) * 1000, pendingIntent)
        Log.d("FirebaseSync", "alarm Repeating to: $triggerAtMillis")
    }

    fun refreshNotificatons(){
        try {
            startService(Intent(this, FirebaseSyncService::class.java))
        } catch (e:Exception){
            e.printStackTrace()
        }
    }
}