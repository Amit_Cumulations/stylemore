package com.cumulations.stylemore.base.nocropper;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Environment;

import java.io.File;

/**
 * Created by Jay Rambhia on 11/2/2015.
 */
public class BitmapUtils {
    private static final String DIRECTORY_SEPARATOR = "/";

    public static Bitmap getCroppedBitmap(Bitmap bitmap, CropInfo cropInfo) throws OutOfMemoryError, IllegalArgumentException {
        if (!cropInfo.addPadding) {
            return Bitmap.createBitmap(bitmap, cropInfo.x, cropInfo.y, cropInfo.width, cropInfo.height);
        }

        return BitmapUtils.addPadding(bitmap, cropInfo, cropInfo.paddingColor);
    }

    public static Bitmap addPadding(Bitmap bmp, CropInfo info, int color) throws OutOfMemoryError {

        if (bmp == null) {
            return null;
        }

        Bitmap bitmap = null;

        try {

            int biggerParam = Math.max(info.width + 2*info.horizontalPadding, info.height + 2*info.verticalPadding);
            bitmap = Bitmap.createBitmap(biggerParam, biggerParam, bmp.getConfig());
            Canvas canvas = new Canvas(bitmap);
            canvas.drawColor(color);

            Rect dest = new Rect(info.horizontalPadding, info.verticalPadding, info.horizontalPadding + info.width, info.verticalPadding + info.height);
            Rect src = new Rect(info.x, info.y, info.x + info.width, info.y + info.height);

            canvas.drawBitmap(bmp, src, dest, null);
            return bitmap;

        } catch (OutOfMemoryError e) {
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
                bitmap = null;
            }

            throw e;
        }
    }

    public static File getFile(String uri) {
        final String filename = new File(uri).getName();

//		final File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        File cropFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath()
                +DIRECTORY_SEPARATOR+"StyleMore"+DIRECTORY_SEPARATOR+"Cropped");
        if (!cropFolder.exists())
            cropFolder.mkdirs();
        return new File(cropFolder, /*generateFilename()*/filename);
    }

}
