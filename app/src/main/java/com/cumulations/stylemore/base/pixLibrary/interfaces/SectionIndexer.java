package com.cumulations.stylemore.base.pixLibrary.interfaces;

/**
 * Created by akshay on 07/05/18.
 */

public interface SectionIndexer {
    String getSectionText(int position);
}

