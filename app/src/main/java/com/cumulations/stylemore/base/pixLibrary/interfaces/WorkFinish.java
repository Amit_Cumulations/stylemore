package com.cumulations.stylemore.base.pixLibrary.interfaces;


/**
 * Created by akshay on 05/07/18.
 */
public interface WorkFinish {
    void onWorkFinish(Boolean check);
}
