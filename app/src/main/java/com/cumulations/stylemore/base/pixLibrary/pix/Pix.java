package com.cumulations.stylemore.base.pixLibrary.pix;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cumulations.stylemore.R;
import com.cumulations.stylemore.base.pixLibrary.adapters.InstantImageAdapter;
import com.cumulations.stylemore.base.pixLibrary.adapters.MainImageAdapter;
import com.cumulations.stylemore.base.pixLibrary.interfaces.OnSelectionListner;
import com.cumulations.stylemore.base.pixLibrary.interfaces.WorkFinish;
import com.cumulations.stylemore.base.pixLibrary.modals.Img;
import com.cumulations.stylemore.base.pixLibrary.utility.HeaderItemDecoration;
import com.cumulations.stylemore.base.pixLibrary.utility.ImageFetcher;
import com.cumulations.stylemore.base.pixLibrary.utility.PermUtil;
import com.cumulations.stylemore.base.pixLibrary.utility.PixConstants;
import com.cumulations.stylemore.base.pixLibrary.utility.Utility;
import com.cumulations.stylemore.base.pixLibrary.utility.ui.FastScrollStateChangeListener;
import com.cumulations.stylemore.base.utils.Constants;
import com.cumulations.stylemore.uploadFeed.data.model.CroppedPic;
import com.cumulations.stylemore.uploadFeed.presentation.CropSelectedPicsActivity;
import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraKitEventCallback;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraView;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Pix extends AppCompatActivity implements View.OnTouchListener {

    private static final int sBubbleAnimDuration = 1000;
    private static final int sScrollbarHideDelay = 1000;
    private static final String SELECTION = "selection";
    private static final int sTrackSnapRange = 5;
    public static String IMAGE_RESULTS = "image_results";
    public static float TOPBAR_HEIGHT;
    int BottomBarHeight = 0;
    int colorPrimaryDark;
    private Handler handler = new Handler();
    private FastScrollStateChangeListener mFastScrollStateChangeListener;
    private CameraView mCamera;
    private RecyclerView recyclerView, instantRecyclerView;
    private BottomSheetBehavior mBottomSheetBehavior;
    private InstantImageAdapter initaliseadapter;
    private GridLayoutManager mLayoutManager;
    private View status_bar_bg, mScrollbar, topbar, mainFrameLayout, bottomButtons, sendButton;
    private TextView mBubbleView, selection_ok, img_count;
    private ImageView mHandleView, clickme, selection_back, selection_check;
    private ViewPropertyAnimator mScrollbarAnimator;
    private ViewPropertyAnimator mBubbleAnimator;
    private Set<Img> selectionList = new HashSet<Img>();
    private Runnable mScrollbarHider = new Runnable() {
        @Override
        public void run() {
            hideScrollbar();
        }
    };
    private MainImageAdapter mainImageAdapter;
    private float mViewHeight;
    private boolean mHideScrollbar = true;
    private boolean LongSelection = false;
    private int SelectionCount = 1;

    private HashMap<String, String> bucketNamesMap = new HashMap<>();
    private List<String> bucketIds = new ArrayList<>();
    private AppCompatSpinner imageFoldersSpinner;
    /*int colorPrimary = ResourcesCompat.getColor(getResources(), R.color.colorPrimary, getTheme());
    int colorAccent = ResourcesCompat.getColor(getResources(), R.color.colorAccent, getTheme());*/

    private int cameraMethod = CameraKit.Constants.METHOD_STANDARD;
    private boolean cropOutput = false;

    private boolean spinnerSetup = false;

    private RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (!mHandleView.isSelected() && recyclerView.isEnabled()) {
                setViewPositions(getScrollProportion(recyclerView));
            }
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (recyclerView.isEnabled()) {
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        handler.removeCallbacks(mScrollbarHider);
                        Utility.cancelAnimation(mScrollbarAnimator);
                        if (!Utility.isViewVisible(mScrollbar) && (recyclerView.computeVerticalScrollRange() - mViewHeight > 0)) {
                            mScrollbarAnimator = Utility.showScrollbar(mScrollbar, Pix.this);
                        }
                        break;
                    case RecyclerView.SCROLL_STATE_IDLE:
                        if (mHideScrollbar && !mHandleView.isSelected()) {
                            handler.postDelayed(mScrollbarHider, sScrollbarHideDelay);
                        }
                        break;
                }
            }
        }
    };
    private TextView selection_count;
    private FrameLayout flash;
    private ImageView front;
    private ProgressDialog dialog;
    private OnSelectionListner onSelectionListner = new OnSelectionListner() {
        @Override
        public void OnClick(Img img, View view, int position) {
            //Log.e("OnClick", "OnClick");
            if (LongSelection) {
                if (selectionList.contains(img)) {
                    selectionList.remove(img);
                    initaliseadapter.Select(false, position);
                    mainImageAdapter.Select(false, position);
                } else {
                    if (SelectionCount <= selectionList.size()) {
                        Toast.makeText(Pix.this, String.format(getResources().getString(R.string.selection_limiter_pix), selectionList.size()), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    img.setPosition(position);
                    selectionList.add(img);
                    initaliseadapter.Select(true, position);
                    mainImageAdapter.Select(true, position);
                }
                if (selectionList.size() == 0) {
                    LongSelection = false;
                    imageFoldersSpinner.setVisibility(View.VISIBLE);
                    selection_check.setVisibility(View.VISIBLE);
                    selection_count.setVisibility(View.GONE);
                    selection_ok.setVisibility(View.GONE);
                    DrawableCompat.setTint(selection_back.getDrawable(), colorPrimaryDark);
                    topbar.setBackgroundColor(Color.parseColor("#ffffff"));
                    Animation anim = new ScaleAnimation(
                            1f, 0f, // Start and end values for the X axis scaling
                            1f, 0f, // Start and end values for the Y axis scaling
                            Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                            Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
                    anim.setFillAfter(true); // Needed to keep the result of the animation
                    anim.setDuration(300);
                    anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            sendButton.setVisibility(View.GONE);
                            sendButton.clearAnimation();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    sendButton.startAnimation(anim);

                } else {
                    selection_count.setVisibility(View.VISIBLE);
                    selection_count.setText("Selected Pictures " + selectionList.size());
                }
                img_count.setText("" + selectionList.size());

                //DrawableCompat.setTint(selection_back.getDrawable(), Color.parseColor("#ffffff "));
            } else {
                img.setPosition(position);
                selectionList.add(img);
                returnObjects();
                DrawableCompat.setTint(selection_back.getDrawable(), colorPrimaryDark);
                topbar.setBackgroundColor(Color.parseColor("#ffffff"));
            }
        }

        @Override
        public void OnLongClick(Img img, View view, int position) {
            if (SelectionCount > 1) {
                Utility.vibe(Pix.this, 50);
                //Log.e("OnLongClick", "OnLongClick");
                LongSelection = true;
                if (selectionList.size() == 0) {
                    if (mBottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                        sendButton.setVisibility(View.VISIBLE);
                        Animation anim = new ScaleAnimation(
                                0f, 1f, // Start and end values for the X axis scaling
                                0f, 1f, // Start and end values for the Y axis scaling
                                Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                                Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
                        anim.setFillAfter(true); // Needed to keep the result of the animation
                        anim.setDuration(300);
                        sendButton.startAnimation(anim);
                    }
                    //sendButton.animate().scaleX(1).scaleY(1).setDuration(800).start();
                }
                if (selectionList.contains(img)) {
                    selectionList.remove(img);
                    initaliseadapter.Select(false, position);
                    mainImageAdapter.Select(false, position);
                } else {
                    img.setPosition(position);
                    selectionList.add(img);
                    initaliseadapter.Select(true, position);
                    mainImageAdapter.Select(true, position);
                }
                selection_check.setVisibility(View.GONE);
                topbar.setBackgroundColor(/*colorPrimaryDark*/Color.parseColor("#ffffff"));
                selection_count.setVisibility(View.VISIBLE);
                imageFoldersSpinner.setVisibility(View.GONE);
                selection_count.setText("Selected Pictures " + selectionList.size());
                selection_ok.setVisibility(View.VISIBLE);
                img_count.setText("" + selectionList.size());
                DrawableCompat.setTint(selection_back.getDrawable(), colorPrimaryDark);
            }

        }
    };

    public static void start(final Fragment context, final int requestCode, final int selectionCount) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermUtil.checkForCamara_WritePermissions(context, new WorkFinish() {
                @Override
                public void onWorkFinish(Boolean check) {
                    Intent i = new Intent(context.getActivity(), Pix.class);
                    i.putExtra(SELECTION, selectionCount);
                    context.startActivityForResult(i, requestCode);
                }
            });
        } else {
            Intent i = new Intent(context.getActivity(), Pix.class);
            i.putExtra(SELECTION, selectionCount);
            context.startActivityForResult(i, requestCode);
        }

    }

    public static void start(Fragment context, int requestCode) {
        start(context, requestCode, 1);
    }

    public static void start(final FragmentActivity context, final int requestCode, final int selectionCount) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermUtil.checkForCamara_WritePermissions(context, new WorkFinish() {
                @Override
                public void onWorkFinish(Boolean check) {
                    Intent i = new Intent(context, Pix.class);
                    i.putExtra(SELECTION, selectionCount);
                    context.startActivityForResult(i, requestCode);
                }
            });
        } else {
            Intent i = new Intent(context, Pix.class);
            i.putExtra(SELECTION, selectionCount);
            context.startActivityForResult(i, requestCode);
//            context.startActivity(i);
        }
    }

    public static void start(final FragmentActivity context, int requestCode) {
        start(context, requestCode, 1);
    }

    private void hideScrollbar() {
        float transX = getResources().getDimensionPixelSize(R.dimen.fastscroll_scrollbar_padding_end);
        mScrollbarAnimator = mScrollbar.animate().translationX(transX).alpha(0f)
                .setDuration(PixConstants.sScrollbarAnimDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        mScrollbar.setVisibility(View.GONE);
                        mScrollbarAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        super.onAnimationCancel(animation);
                        mScrollbar.setVisibility(View.GONE);
                        mScrollbarAnimator = null;
                    }
                });
    }

    public void returnObjects() {
        sendPicsForCrop();
    }

    public void sendPicsForCrop() {
        hideProgressBar();
        if (selectionList == null || selectionList.size() == 0) {
            Toast.makeText(Pix.this, "Select images to proceed!", Toast.LENGTH_SHORT).show();
            return;
        }
        ArrayList<CroppedPic> list = new ArrayList<>();
        for (Img i : selectionList) {
            list.add(new CroppedPic(
                    i.getUrl(),
                    false,
                    null,
                    null,
                    null,
                    null,
                    null));
        }
        Intent sendIntent = new Intent(this, CropSelectedPicsActivity.class);
        sendIntent.putExtra(Constants.SELECTED_PICS, list);
//        startActivityForResult(resultIntent,200);
        /*forwarding startActivityForResult from Pix to CropSelectedPicsActivity*/
        sendIntent.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        startActivity(sendIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.SetupStatusBarHiden(this);
        Utility.hideStatusBar(this);
        setContentView(R.layout.activity_pix);
        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCamera.start();
    }

    @Override
    protected void onPause() {
        mCamera.stop();
        super.onPause();
    }

    private void initialize() {
        Utility.getScreensize(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        try {
            SelectionCount = getIntent().getIntExtra(SELECTION, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        colorPrimaryDark = ContextCompat.getColor(this, R.color.slight_black);
        mCamera = findViewById(R.id.camera);
        mCamera.setMethod(cameraMethod);
        mCamera.setCropOutput(cropOutput);
        mCamera.start();
        mCamera.setFocus(CameraKit.Constants.FOCUS_TAP_WITH_MARKER);

        clickme = findViewById(R.id.clickme);
        flash = findViewById(R.id.flash);
        front = findViewById(R.id.front);
        topbar = findViewById(R.id.topbar);
        selection_count = findViewById(R.id.selection_count);
        selection_ok = findViewById(R.id.selection_ok);
        selection_back = findViewById(R.id.selection_back);
        selection_check = findViewById(R.id.selection_check);
        selection_check.setVisibility((SelectionCount > 1) ? View.VISIBLE : View.GONE);
        sendButton = findViewById(R.id.sendButton);
        img_count = findViewById(R.id.img_count);
        mBubbleView = findViewById(R.id.fastscroll_bubble);
        mHandleView = findViewById(R.id.fastscroll_handle);
        mScrollbar = findViewById(R.id.fastscroll_scrollbar);
        mScrollbar.setVisibility(View.GONE);
        mBubbleView.setVisibility(View.GONE);
        bottomButtons = findViewById(R.id.bottomButtons);
        TOPBAR_HEIGHT = Utility.convertDpToPixel(56, Pix.this);
        status_bar_bg = findViewById(R.id.status_bar_bg);
        instantRecyclerView = findViewById(R.id.instantRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        instantRecyclerView.setLayoutManager(linearLayoutManager);
        initaliseadapter = new InstantImageAdapter(this);
        initaliseadapter.AddOnSelectionListner(onSelectionListner);
        instantRecyclerView.setAdapter(initaliseadapter);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.addOnScrollListener(mScrollListener);
        mainFrameLayout = findViewById(R.id.mainFrameLayout);
        BottomBarHeight = Utility.getSoftButtonsBarSizePort(this);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(0, 0, 0, BottomBarHeight);
        mainFrameLayout.setLayoutParams(lp);
        FrameLayout.LayoutParams fllp = (FrameLayout.LayoutParams) sendButton.getLayoutParams();
        fllp.setMargins(0, 0, (int) (Utility.convertDpToPixel(16, this)),
                (int) (Utility.convertDpToPixel(174, this)));
        sendButton.setLayoutParams(fllp);
        mainImageAdapter = new MainImageAdapter(this);
        mLayoutManager = new GridLayoutManager(this, MainImageAdapter.spanCount);
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (mainImageAdapter.getItemViewType(position)) {
                    case MainImageAdapter.HEADER:
                        return MainImageAdapter.spanCount;
                    case MainImageAdapter.ITEM:
                        return 1;
                    default:
                        return 1;
                }
            }
        });
        recyclerView.setLayoutManager(mLayoutManager);
        mainImageAdapter.AddOnSelectionListner(onSelectionListner);
        recyclerView.setAdapter(mainImageAdapter);
        recyclerView.addItemDecoration(new HeaderItemDecoration(this, recyclerView, mainImageAdapter));
        mHandleView.setOnTouchListener(this);
        clickme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressBar("Capturing image...");
                mCamera.captureImage(new CameraKitEventCallback<CameraKitImage>() {
                    @Override
                    public void callback(CameraKitImage cameraKitImage) {
                        if (cameraKitImage.getJpeg() != null) {
                            synchronized (cameraKitImage) {
                                File photo = Utility.writeImage(cameraKitImage.getJpeg());
                                selectionList.clear();
                                selectionList.add(new Img("", "", photo.getAbsolutePath(), ""));
                                returnObjects();
                            }
                        } else {
                            hideProgressBar();
                            Toast.makeText(Pix.this, "Unable to Get The Image", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                mCamera.captureImage();
                // Toast.makeText(Pix.this, "fin", Toast.LENGTH_SHORT).show();
                //Log.e("Hello", "onclick");

            }
        });
        selection_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toast.makeText(Pix.this, "fin", Toast.LENGTH_SHORT).show();
                //Log.e("Hello", "onclick");
                returnObjects();
            }
        });
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(Pix.this, "fin", Toast.LENGTH_SHORT).show();
                //Log.e("Hello", "onclick");
                returnObjects();
            }
        });
        selection_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
        selection_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                topbar.setBackgroundColor(/*colorPrimaryDark*/Color.parseColor("#ffffff"));
                selection_count.setVisibility(View.VISIBLE);
                selection_count.setText("Tap photo to select");
                selection_ok.setVisibility(View.VISIBLE);
                img_count.setText("" + selectionList.size());
                DrawableCompat.setTint(selection_back.getDrawable(), colorPrimaryDark);
                LongSelection = true;
                selection_check.setVisibility(View.GONE);
                imageFoldersSpinner.setVisibility(View.GONE);
            }
        });
        final ImageView iv = (ImageView) flash.getChildAt(0);
        mCamera.setFlash(CameraKit.Constants.FLASH_AUTO);
        flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int height = flash.getHeight();
                iv.animate().translationY(height).setDuration(100).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        iv.setTranslationY(-(height / 2));
                        int image_id = 0;
                        switch (mCamera.getFlash()) {
                            case CameraKit.Constants.FLASH_ON: {
                                iv.setImageResource(R.drawable.ic_flash_auto_black_24dp);
                                mCamera.setFlash(CameraKit.Constants.FLASH_AUTO);
                            }
                            break;
                            case CameraKit.Constants.FLASH_AUTO: {
                                iv.setImageResource(R.drawable.ic_flash_off_black_24dp);
                                mCamera.setFlash(CameraKit.Constants.FLASH_OFF);
                            }
                            break;
                            default: {
                                iv.setImageResource(R.drawable.ic_flash_on_black_24dp);
                                mCamera.setFlash(CameraKit.Constants.FLASH_ON);
                            }
                            break;

                        }

                        iv.animate().translationY(0).setDuration(50).setListener(null).start();
                    }
                }).start();
            }
        });

        front.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ObjectAnimator oa1 = ObjectAnimator.ofFloat(front, "scaleX", 1f, 0f).setDuration(150);
                final ObjectAnimator oa2 = ObjectAnimator.ofFloat(front, "scaleX", 0f, 1f).setDuration(150);
                oa1.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        front.setImageResource(R.drawable.ic_photo_camera);
                        oa2.start();
                    }
                });
                oa1.start();
                mCamera.setFacing((mCamera.isFacingFront() ? CameraKit.Constants.FACING_BACK : CameraKit.Constants.FACING_FRONT));
            }
        });
        /*flash.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        flash.setBackgroundResource(R.drawable.bubble);
                    }
                    break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_OUTSIDE:
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_HOVER_EXIT:
                        flash.setBackgroundResource(0);
                        break;
                }
                return false;
            }
        });*/

        imageFoldersSpinner = findViewById(R.id.spinner_image_folders);

        DrawableCompat.setTint(selection_back.getDrawable(), colorPrimaryDark);

        /*initially pass null*/
        updateImages(null);
//        setImageFoldersSpinner();
    }

    private void showProgressBar(String msg) {
        if (dialog == null) {
            dialog = new ProgressDialog(this);
            dialog.setCancelable(false);
        }
        dialog.setMessage(msg);
        dialog.show();
    }

    private void hideProgressBar() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        dialog = null;
    }


    private void updateImages(final String bucketId) {
        mainImageAdapter.ClearList();
        Cursor cursor;
        /*if (bucketId == null)
        cursor = Utility.getCursor(Pix.this);
        else cursor = Utility.getCursor(this,bucketId);*/

        cursor = Utility.getCursor(this, bucketId);

        ArrayList<Img> ImagesDataList = new ArrayList<>();
        String header = "";
        int limit;
        if (cursor.getCount() >= 100) {
            limit = 100;
        } else {
            limit = cursor.getCount();
        }
        int date = cursor.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN);
        int data = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
        int contenturl = cursor.getColumnIndex(MediaStore.Images.Media._ID);
        int bucketIdIndex = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_ID);
        int bucketNameIndex = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        for (int i = 0; i < limit; i++) {
            cursor.moveToNext();
            Uri path = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + cursor.getInt(contenturl));
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(cursor.getLong(date));
            String mydate = Utility.getDateDifference(calendar);

            if (bucketId == null && !bucketNamesMap.containsKey(cursor.getString(bucketNameIndex))) {
                bucketNamesMap.put(cursor.getString(bucketNameIndex), cursor.getString(bucketIdIndex));
                Log.d("updateImages", "bucketname = " + cursor.getString(bucketNameIndex) + ", Id = " + cursor.getString(bucketIdIndex));
            }

            if (!header.equalsIgnoreCase("" + mydate)) {
                header = "" + mydate;
                ImagesDataList.add(new Img("" + mydate, "", "", ""));
            }
            ImagesDataList.add(new Img("" + header, "" + path, cursor.getString(data), ""));
        }
        cursor.close();

        if (bucketId == null)
            initaliseadapter.addImageList(ImagesDataList);

        mainImageAdapter.addImageList(ImagesDataList);

        new ImageFetcher(bucketNamesMap) {
            @Override
            protected void onPostExecute(ArrayList<Img> imgs) {
                super.onPostExecute(imgs);
                mainImageAdapter.addImageList(imgs);
                if (!spinnerSetup) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setImageFoldersSpinner();
                        }
                    });
                }
            }
        }.execute(Utility.getCursor(Pix.this, bucketId));
        setBottomSheetBehavior();
    }

    private void setBottomSheetBehavior() {
        View bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setPeekHeight((int) (Utility.convertDpToPixel(194, this)));
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {
            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {
                Utility.manupulateVisibility(Pix.this, slideOffset,
                        instantRecyclerView, recyclerView, status_bar_bg,
                        topbar, bottomButtons, sendButton, LongSelection);
                if (slideOffset == 1) {
                    Utility.showScrollbar(mScrollbar, Pix.this);
                    mainImageAdapter.notifyDataSetChanged();
                    mViewHeight = mScrollbar.getMeasuredHeight();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            setViewPositions(getScrollProportion(recyclerView));
                        }
                    });
                    sendButton.setVisibility(View.GONE);
                } else if (slideOffset == 0) {

                    initaliseadapter.notifyDataSetChanged();
                    hideScrollbar();
                    img_count.setText("" + selectionList.size());
                }
            }
        });
    }

    //
    private float getScrollProportion(RecyclerView recyclerView) {
        final int verticalScrollOffset = recyclerView.computeVerticalScrollOffset();
        final int verticalScrollRange = recyclerView.computeVerticalScrollRange();
        final float rangeDiff = verticalScrollRange - mViewHeight;
        float proportion = (float) verticalScrollOffset / (rangeDiff > 0 ? rangeDiff : 1f);
        return mViewHeight * proportion;
    }

    private void setViewPositions(float y) {
        int handleY = Utility.getValueInRange(0, (int) (mViewHeight - mHandleView.getHeight()), (int) (y - mHandleView.getHeight() / 2));
        mBubbleView.setY(handleY + Utility.convertDpToPixel((56), Pix.this));
        mHandleView.setY(handleY);
    }


    private void setRecyclerViewPosition(float y) {
        if (recyclerView != null && recyclerView.getAdapter() != null) {
            int itemCount = recyclerView.getAdapter().getItemCount();
            float proportion;

            if (mHandleView.getY() == 0) {
                proportion = 0f;
            } else if (mHandleView.getY() + mHandleView.getHeight() >= mViewHeight - sTrackSnapRange) {
                proportion = 1f;
            } else {
                proportion = y / mViewHeight;
            }

            int scrolledItemCount = Math.round(proportion * itemCount);
            int targetPos = Utility.getValueInRange(0, itemCount - 1, scrolledItemCount);
            recyclerView.getLayoutManager().scrollToPosition(targetPos);

            if (mainImageAdapter != null) {
                mBubbleView.setText(mainImageAdapter.getSectionMonthYearText(targetPos));
            }
        }
    }

    private void showBubble() {
        if (!Utility.isViewVisible(mBubbleView)) {
            mBubbleView.setVisibility(View.VISIBLE);
            mBubbleView.setAlpha(0f);
            mBubbleAnimator = mBubbleView.animate().alpha(1f)
                    .setDuration(sBubbleAnimDuration)
                    .setListener(new AnimatorListenerAdapter() {
                        // adapter required for new alpha value to stick
                    });
            mBubbleAnimator.start();
        }
    }

    private void hideBubble() {
        if (Utility.isViewVisible(mBubbleView)) {
            mBubbleAnimator = mBubbleView.animate().alpha(0f)
                    .setDuration(sBubbleAnimDuration)
                    .setListener(new AnimatorListenerAdapter() {

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            mBubbleView.setVisibility(View.GONE);
                            mBubbleAnimator = null;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            super.onAnimationCancel(animation);
                            mBubbleView.setVisibility(View.GONE);
                            mBubbleAnimator = null;
                        }
                    });
            mBubbleAnimator.start();
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (event.getX() < mHandleView.getX() - ViewCompat.getPaddingStart(mHandleView)) {
                    return false;
                }
                mHandleView.setSelected(true);
                handler.removeCallbacks(mScrollbarHider);
                Utility.cancelAnimation(mScrollbarAnimator);
                Utility.cancelAnimation(mBubbleAnimator);

                if (!Utility.isViewVisible(mScrollbar) && (recyclerView.computeVerticalScrollRange() - mViewHeight > 0)) {
                    mScrollbarAnimator = Utility.showScrollbar(mScrollbar, Pix.this);
                }

                if (mainImageAdapter != null) {
                    showBubble();
                }

                if (mFastScrollStateChangeListener != null) {
                    mFastScrollStateChangeListener.onFastScrollStart(this);
                }
            case MotionEvent.ACTION_MOVE:
                final float y = event.getRawY();
                mBubbleView.setText(mainImageAdapter.getSectionText(recyclerView.getVerticalScrollbarPosition()));
                setViewPositions(y - TOPBAR_HEIGHT);
                setRecyclerViewPosition(y);
                return true;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                mHandleView.setSelected(false);
                if (mHideScrollbar) {
                    handler.postDelayed(mScrollbarHider, sScrollbarHideDelay);
                }
                hideBubble();
                if (mFastScrollStateChangeListener != null) {
                    mFastScrollStateChangeListener.onFastScrollStop(this);
                }
                return true;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onBackPressed() {
        if (selectionList.size() > 0) {
            for (Img img : selectionList) {
                mainImageAdapter.getItemList().get(img.getPosition()).setSelected(false);
                mainImageAdapter.notifyItemChanged(img.getPosition());
                initaliseadapter.getItemList().get(img.getPosition()).setSelected(false);
                initaliseadapter.notifyItemChanged(img.getPosition());
            }
            LongSelection = false;
            if (SelectionCount > 1) {
                selection_check.setVisibility(View.VISIBLE);
                selection_count.setVisibility(View.GONE);
                selection_ok.setVisibility(View.GONE);
                imageFoldersSpinner.setVisibility(View.VISIBLE);
            }
            DrawableCompat.setTint(selection_back.getDrawable(), colorPrimaryDark);
            topbar.setBackgroundColor(Color.parseColor("#ffffff"));
            Animation anim = new ScaleAnimation(
                    1f, 0f, // Start and end values for the X axis scaling
                    1f, 0f, // Start and end values for the Y axis scaling
                    Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                    Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
            anim.setFillAfter(true); // Needed to keep the result of the animation
            anim.setDuration(300);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    sendButton.setVisibility(View.GONE);
                    sendButton.clearAnimation();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            sendButton.startAnimation(anim);
            selectionList.clear();
        } else if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else {
            super.onBackPressed();
        }

    }

    private void setImageFoldersSpinner() {
        List<String> folders_list = new ArrayList<>();
        folders_list.add("All");
        for (Map.Entry<String, String> entry : bucketNamesMap.entrySet()) {
            folders_list.add(entry.getKey());
        }
        final ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, folders_list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        imageFoldersSpinner.setAdapter(adapter);

        imageFoldersSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String clickedBucketFolder = adapterView.getItemAtPosition(i).toString();

                /*gets called initially*/
                if (clickedBucketFolder.equalsIgnoreCase("All")) {
                    updateImages(null);
                    return;
                }

                String clickedBcktId = bucketNamesMap.get(clickedBucketFolder);
                if (clickedBcktId != null) {
                    updateImages(clickedBcktId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        try {

            /*View view = LayoutInflater.from(this).inflate(android.R.layout.simple_spinner_item,null);
            TextView selectedTextview = view.findViewById(android.R.id.text1);
            Log.v("EMS1","Found the textview:"+selectedTextview);
            selectedTextview.setHorizontallyScrolling(true);
            selectedTextview.setMarqueeRepeatLimit(-1);
            selectedTextview.setSelected(true);*/


            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(imageFoldersSpinner);

            // Set popupWindow height to 500px
            popupWindow.setHeight((int) Utility.convertDpToPixel(185, this));
            spinnerSetup = true;
        } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 200:
                if (resultCode == Activity.RESULT_OK) {
                    ArrayList<String> list = data.getStringArrayListExtra(IMAGE_RESULTS);
                    if (list == null)
                        return;
                    Intent resultIntent = new Intent();
                    resultIntent.putStringArrayListExtra(IMAGE_RESULTS, list);
                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();
                }
                break;
        }
    }
}
