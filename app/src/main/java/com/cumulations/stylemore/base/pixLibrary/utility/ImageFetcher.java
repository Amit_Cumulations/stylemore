package com.cumulations.stylemore.base.pixLibrary.utility;

import android.database.Cursor;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;

import com.cumulations.stylemore.base.pixLibrary.modals.Img;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by akshay on 06/04/18.
 */

public class ImageFetcher extends AsyncTask<Cursor, Void, ArrayList<Img>> {
    ArrayList<Img> LIST = new ArrayList<>();
    private HashMap<String,String> bucketNamesMap;

    public ImageFetcher(HashMap<String,String> bucketNamesMap){
        this.bucketNamesMap = bucketNamesMap;
    }

    @Override
    protected ArrayList<Img> doInBackground(Cursor... cursors) {
        Cursor cursor = cursors[0];
        if (cursor != null) {

            int date = cursor.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN);
            int data = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            int contenturl = cursor.getColumnIndex(MediaStore.Images.Media._ID);
            int bucketIdIndex = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_ID);
            int bucketNameIndex = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
            String header = "";
            int limit = 100;
            if (cursor.getCount() >= 100) {
                limit = 100;
            } else {
                limit = cursor.getCount();
            }
            cursor.move(limit - 1);
            for (int i = limit; i < cursor.getCount(); i++) {
                cursor.moveToNext();
                Uri curl = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + cursor.getInt(contenturl));
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(cursor.getLong(date));
                String mydate = Utility.getDateDifference(calendar);

                if (!bucketNamesMap.containsKey(cursor.getString(bucketNameIndex))) {
                    bucketNamesMap.put(cursor.getString(bucketNameIndex), cursor.getString(bucketIdIndex));
                    Log.d("ImageFetcher","bucketname = "+cursor.getString(bucketNameIndex)+", Id = "+cursor.getString(bucketIdIndex));
                }

                if (!header.equalsIgnoreCase("" + mydate)) {
                    header = "" + mydate;
                    LIST.add(new Img("" + mydate, "", "", new SimpleDateFormat("MMMM yyyy").format(calendar.getTime())));
                }
                LIST.add(new Img("" + header, "" + curl, "" + cursor.getString(data), new SimpleDateFormat("MMMM yyyy").format(calendar.getTime())));
            }
        }
        cursor.close();
        return LIST;
    }

}
