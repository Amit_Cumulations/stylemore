package com.cumulations.stylemore.base.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import android.view.View

class AspectRatioImageView : AppCompatImageView {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {}

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        /*int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = width * getDrawable().getIntrinsicHeight() / getDrawable().getIntrinsicWidth();
        setMeasuredDimension(width, height);*/
        if (drawable != null && drawable.intrinsicWidth > 0) {
            var width = View.MeasureSpec.getSize(widthMeasureSpec)
            if (width <= 0)
                width = layoutParams.width

            val height = width * drawable.intrinsicHeight / drawable.intrinsicWidth
            setMeasuredDimension(width, (height /2f).toInt())
//            setMeasuredDimension(width, height)
        } else
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }
}