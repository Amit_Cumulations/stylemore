package com.cumulations.stylemore.base.utils

import android.graphics.Bitmap

import com.squareup.picasso.Transformation

class ScaleToFitWidthHeightTransform(private val mSize: Int, private val isHeightScale: Boolean) : Transformation {

    override fun transform(source: Bitmap): Bitmap {
        val scale: Float
        val newSize: Int
        var scaleBitmap: Bitmap? = null
        if (isHeightScale) {
            scale = mSize.toFloat() / source.height
            newSize = Math.round(source.width * scale)
            scaleBitmap = Bitmap.createScaledBitmap(source, newSize, mSize, true)
        } else {
            scale = mSize.toFloat() / source.width
            newSize = Math.round(source.height * scale)
            scaleBitmap = Bitmap.createScaledBitmap(source, mSize, newSize, true)
        }

        /*val diffWidth:Int
        val screenWidth = mSize
        val percentage:Int
        if (screenWidth>source.width){
            diffWidth = screenWidth - source.width
            percentage = diffWidth/source.width
            val newHeight = (source.height*percentage)+source.height
            scaleBitmap = Bitmap.createScaledBitmap(source, screenWidth,newHeight,true)
        } else if (source.width>screenWidth){
            diffWidth = source.width - screenWidth
            percentage = diffWidth/source.width
            val newHeight = source.height-(source.height*percentage)
            scaleBitmap = Bitmap.createScaledBitmap(source, screenWidth,newHeight, true)
        } else {
            scaleBitmap = Bitmap.createScaledBitmap(source, screenWidth, source.height, true)
        }*/

        if (scaleBitmap != source) {
            source.recycle()
        }

        return scaleBitmap!!

    }

    override fun key(): String {
        return "scaleRespectRatio$mSize$isHeightScale"
    }
}