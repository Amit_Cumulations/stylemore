package com.cumulations.stylemore.base.utils;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class SquareViewPager extends ViewPager {
    private OnClickListener mOnClickListener;
    private View.OnClickListener mOnClickListener2;

    public SquareViewPager(Context context) {
        super(context);
        setup();
    }

    public SquareViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);

//        int width = MeasureSpec.getSize(widthMeasureSpec);
//        int height = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
//        setMeasuredDimension(width, height);
    }

    private void setup() {
        final GestureDetector tapGestureDetector = new GestureDetector(getContext(), new TapGestureListener());

        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                tapGestureDetector.onTouchEvent(event);

                return false;
            }
        });
    }

    public void setOnViewPagerClickListener(OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    public void setOnViewPagerClickListener(View.OnClickListener onClickListener) {
        mOnClickListener2 = onClickListener;
    }

    public interface OnClickListener {
        void onViewPagerClick(ViewPager viewPager);
    }

    private class TapGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (mOnClickListener != null) {
                mOnClickListener.onViewPagerClick(SquareViewPager.this);
            }
            if (mOnClickListener2 != null) {
                mOnClickListener2.onClick(SquareViewPager.this);
            }
            return true;
        }
    }
}