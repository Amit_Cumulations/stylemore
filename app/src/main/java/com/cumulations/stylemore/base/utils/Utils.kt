package com.cumulations.stylemore.base.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.app.NotificationManager
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Point
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.support.annotation.NonNull
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatImageView
import android.text.TextUtils
import android.text.format.DateUtils
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.webkit.MimeTypeMap
import android.widget.TextView
import com.cumulations.stylemore.R
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.firebase.FbActionUser
import com.cumulations.stylemore.base.firebase.FbNotification
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper.get
import com.cumulations.stylemore.fcmPushNotificationSender.data.model.FcmPushPostRequest
import com.cumulations.stylemore.fcmPushNotificationSender.domain.FcmPushUseCase
import com.cumulations.stylemore.fcmPushNotificationSender.presentation.FcmPushPresenterImpl
import com.cumulations.stylemore.feeds.data.model.TagsItem
import com.cumulations.stylemore.feeds.presentation.FeedListActivity
import com.google.firebase.database.*
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap


/**
 * Created by Amit Tumkur on 29-05-2018.
 */
object Utils {
    @SuppressLint("PackageManagerGetSignatures")
    fun logFbHashKey(context: Context) {
        val info: PackageInfo
        try {
            info = context.packageManager.getPackageInfo("com.stylemore", PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val something = String(Base64.encode(md.digest(), 0))
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("fb hashkey", something)
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e("no such an algorithm", e.toString())
        } catch (e: Exception) {
            Log.e("exception", e.toString())
        }

    }

    fun isOnline(): Boolean {
        val cm = StyleMoreApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    fun showLoader(loadingBar: AppCompatImageView, loadingText: TextView?, context: Context, blockUi:Boolean) {
        if (blockUi){
            showLoader(loadingBar,loadingText,context)
            return
        }

        loadingBar.visibility = View.VISIBLE
        if (loadingText != null)
            loadingText.visibility = View.VISIBLE
        val rotation = AnimationUtils.loadAnimation(context, R.anim.rotate)
        rotation.fillAfter = true
        loadingBar.startAnimation(rotation)
    }

    fun showLoader(loadingBar: AppCompatImageView, loadingText: TextView?, context: Context) {
        loadingBar.visibility = View.VISIBLE
        if (loadingText != null)
            loadingText.visibility = View.VISIBLE
        val rotation = AnimationUtils.loadAnimation(context, R.anim.rotate)
        rotation.fillAfter = true
        loadingBar.startAnimation(rotation)
        val appCompatActivity = context as AppCompatActivity
        appCompatActivity.window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    fun closeLoader(loadingBar: AppCompatImageView, loadingText: TextView?, context: Context) {
        loadingBar.visibility = View.GONE
        if (loadingText != null)
            loadingText.visibility = View.GONE
        loadingBar.clearAnimation()
        val appCompatActivity = context as AppCompatActivity
        appCompatActivity.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    fun getDateInFormat(date: String, inFormat: String, outFormat: String): String {
        try {
            val initialDateFormat = SimpleDateFormat(inFormat, Locale.getDefault())
            val dateObj = initialDateFormat.parse(date)
            val updatedDateFormat = SimpleDateFormat(outFormat, Locale.getDefault())
            return updatedDateFormat.format(dateObj)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return ""
    }

    fun getDateInRelativeTime(dateString: String, dateFormat: String): String {
        try {
            val utcSdf = SimpleDateFormat(dateFormat, Locale.ENGLISH)
            utcSdf.timeZone = TimeZone.getTimeZone("UTC")
            val utcDateObj = utcSdf.parse(dateString)

            utcSdf.timeZone = TimeZone.getDefault()
            val localDateObj = utcSdf.parse(dateString)
            var formattedString = DateUtils.getRelativeTimeSpanString(utcDateObj.time, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS).toString()
//            return DateUtils.getRelativeTimeSpanString(StyleMoreApp.getContext(),utcDateObj.time).toString()
            if (formattedString.startsWith("0", true) && formattedString.endsWith("ago", true)) {
                formattedString = "Just now"
            }
            return formattedString
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return ""
    }

    fun getDateInRelativeTime(timestamp: Long): String {
        try {
            return DateUtils.getRelativeTimeSpanString(timestamp, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS).toString()
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return ""
    }

    fun getMimeType(uri: Uri): String? {
        var mimeType: String? = null
        if (uri.scheme == ContentResolver.SCHEME_CONTENT) {
            val cr = StyleMoreApp.getContext().contentResolver
            mimeType = cr.getType(uri)
        } else {
            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString())
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase())
        }
        return mimeType
    }

    fun getGridTagsMap(): LinkedHashMap<String, TagsItem> {
        val tagsMap = LinkedHashMap<String, TagsItem>()

        tagsMap["Top"] = TagsItem("Top", "", false)
        tagsMap["Pants"] = TagsItem("Pants", "", false)
        tagsMap["Dress"] = TagsItem("Dress", "", false)
        tagsMap["Accessory"] = TagsItem("Accessory", "", false)
        tagsMap["Shoes"] = TagsItem("Shoes", "", false)
        tagsMap["Bag"] = TagsItem("Bag", "", false)
        tagsMap["Skirt"] = TagsItem("Skirt", "", false)
        tagsMap["Watch"] = TagsItem("Watch", "", false)
        tagsMap["Eyewear"] = TagsItem("Eyewear", "", false)
        tagsMap["Other"] = TagsItem("Other", "", false)

        return tagsMap
    }

    fun getTagsListFromMap(tagsMap: LinkedHashMap<String, TagsItem>): ArrayList<TagsItem> {
        val list = ArrayList<TagsItem>()

        tagsMap.forEach { (key, value) ->
            println("$key = $value")
            list.add(value)
        }

        return list
    }

    fun getSelectedTagsListFromMap(tagsMap: LinkedHashMap<String, TagsItem>): LinkedList<TagsItem> {
        val list = LinkedList<TagsItem>()

        tagsMap.forEach { (key, value) ->
            println("$key = $value")
            if (value.isSelected)
                list.add(value)
        }

        return list
    }

    fun showNotification(notificationManager: NotificationManager?,
                         notificationBuilder: NotificationCompat.Builder?,
                         title: String, content: String, notificId: Int) {
        notificationBuilder?.setContentTitle(title)
        notificationBuilder?.setContentText(content)
        notificationBuilder?.setStyle(NotificationCompat.BigTextStyle()
                .bigText(content))
        notificationBuilder?.setAutoCancel(true)
        notificationManager?.notify(notificId, notificationBuilder?.build())
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    fun isNotificationChannelEnabled(context: Context, @NonNull channelId: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (!TextUtils.isEmpty(channelId)) {
                val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                val notificationChannel = notificationManager.getNotificationChannel(channelId)
                return notificationChannel != null && notificationChannel.importance != NotificationManager.IMPORTANCE_NONE
            }
            return false
        } else {
            return NotificationManagerCompat.from(context).areNotificationsEnabled()
        }
    }

    fun closeKeyboard(context: Context, view: View?) {
        try {
            val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if (view != null) {
                inputManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.RESULT_UNCHANGED_SHOWN)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun openKeyboard(context: Context, view: View?) {
        try {
            val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if (view != null) {
                inputManager.toggleSoftInputFromWindow(view.applicationWindowToken, InputMethodManager.SHOW_FORCED, 0)
                view.requestFocus()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getScreenWidthInDp(activity: AppCompatActivity): Int {
        val display = activity.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size);
        val screen_width = size.x
        val screen_height = size.y
        return screen_width
    }

    fun getScreenHeightInPixel(activity: Activity): Int {
        val display = activity.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size);
        val screenHeight = size.y
        return screenHeight
    }

    fun goToHomeScreen(context: Context) {
        context.startActivity(Intent(context, FeedListActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
        (context as AppCompatActivity).finish()
    }

    fun goToHomeScreen(context: Context,tabType:Int) {
        context.startActivity(Intent(context, FeedListActivity::class.java)
                .putExtra(Constants.TAB_TYPE,tabType)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
        (context as AppCompatActivity).finish()
    }

    fun pushActionToFirebaseDb(userIdDatabaseReference: DatabaseReference,
                               fbNotification: FbNotification) {

        val actionNameDbReference = userIdDatabaseReference.child(fbNotification.actionName)
        val feedIdDbReference = actionNameDbReference.child(fbNotification.actionFeedId.toString())
        val uniqueLikedDatabaseReference = actionNameDbReference.child("${fbNotification.actionFeedId}_${fbNotification.fbActionUser.userId}")

        when (fbNotification.actionName.toLowerCase()) {
            "liked" -> {
                uniqueLikedDatabaseReference.setValue(fbNotification)
                        .addOnSuccessListener {
                            sendFcmPushNotification(fbNotification)
                            Log.d("liked", "success")
                        }
                        .addOnFailureListener {
                            Log.d("liked", "failure  = ${it.message}")
                        }
            }

            "disliked" -> {
                val likedDbReference = userIdDatabaseReference.child("liked")
//                val disLikedfeedIdDbReference = likedDbReference.child(fbNotification.actionFeedId.toString())
                val uniqueDislikedDbRef = likedDbReference.child("${fbNotification.actionFeedId}_${fbNotification.fbActionUser.userId}")
                uniqueDislikedDbRef.removeValue()
                        .addOnSuccessListener {
                            Log.d("disliked", "removed")
                        }
                        .addOnFailureListener {
                            Log.d("disliked", "failure  = ${it.message}")
                        }
            }

            "commented" -> {
                feedIdDbReference.push().setValue(fbNotification)
                        .addOnSuccessListener {
                            sendFcmPushNotification(fbNotification)
                            Log.d("commented", "success")
                        }
                        .addOnFailureListener {
                            Log.d("commented", "failure  = ${it.message}")
                        }
            }

            "followed" -> {
                actionNameDbReference.child(fbNotification.fbActionUser.userId.toString()).setValue(fbNotification)
                        .addOnSuccessListener {
                            sendFcmPushNotification(fbNotification)
                            Log.d("followed", "success")
                        }
                        .addOnFailureListener {
                            Log.d("followed", "failure  = ${it.message}")
                        }
            }

            "unfollowed" -> {
                val followedDbReference = userIdDatabaseReference.child("followed")
                val unfollowedUserIdDbReference = followedDbReference.child(fbNotification.fbActionUser.userId.toString())
                unfollowedUserIdDbReference.removeValue()
                        .addOnSuccessListener {
                            Log.d("unfollowed", "removed")
                        }
                        .addOnFailureListener {
                            Log.d("unfollowed", "failure  = ${it.message}")
                        }
            }

        }
    }

    fun updateSeenStatusToFirebaseDb(userIdDatabaseReference: DatabaseReference,
                                     fbNotification: FbNotification) {

        val actionNameDbReference = userIdDatabaseReference.child(fbNotification.actionName)
        val feedIdDbReference = actionNameDbReference.child(fbNotification.actionFeedId.toString())
        val uniqueLikedDatabaseReference = actionNameDbReference.child("${fbNotification.actionFeedId}_${fbNotification.fbActionUser.userId}")

        when (fbNotification.actionName.toLowerCase()) {
            "liked" -> {
                uniqueLikedDatabaseReference.child("seen").setValue(true)
                        .addOnSuccessListener {
                            Log.d("liked seen", "true")
                        }
                        .addOnFailureListener {
                            Log.d("liked seen", "failure  = ${it.message}")
                        }
            }

            "commented" -> {
                feedIdDbReference.child(fbNotification.fbKey)
                        .child("seen")
                        .setValue(true)
                        .addOnSuccessListener {
                            Log.d("commented seen", "true")
                        }
                        .addOnFailureListener {
                            Log.d("commented seen", "failure  = ${it.message}")
                        }
            }

            "followed" -> {
                actionNameDbReference.child(fbNotification.fbActionUser.userId.toString())
                        .child("seen")
                        .setValue(true)
                        .addOnSuccessListener {
                            Log.d("followed seen", "true")
                        }
                        .addOnFailureListener {
                            Log.d("followed seen", "failure  = ${it.message}")
                        }
            }
        }
    }

    fun removeNotificationForDeletedPost(userIdDatabaseReference: DatabaseReference,
                                         fbNotification: FbNotification) {
        val actionNameDbReference = userIdDatabaseReference.child(fbNotification.actionName)
        val feedIdDbReference = actionNameDbReference.child(fbNotification.actionFeedId.toString())

        when (fbNotification.actionName.toLowerCase()) {
            "liked" -> {
                feedIdDbReference.removeValue()
                        .addOnSuccessListener {
                            Log.d("liked removed", "true")
                        }
                        .addOnFailureListener {
                            Log.d("liked removed", "failure  = ${it.message}")
                        }
            }

            "commented" -> {
                feedIdDbReference.child(fbNotification.fbKey)
                        .removeValue()
                        .addOnSuccessListener {
                            Log.d("commented removed", "true")
                        }
                        .addOnFailureListener {
                            Log.d("commented removed", "failure  = ${it.message}")
                        }
            }
        }
    }

    fun updateTokenInFirebaseDb(token: String?) {
        if (StyleMoreApp.user_id.isEmpty())
            return

        val tokenIdDatabaseReference = FirebaseDatabase.getInstance().reference
                .child("Tokens")
                .child(StyleMoreApp.user_id)
        tokenIdDatabaseReference.child("id").setValue(token)
                .addOnSuccessListener {
                    Log.d("${StyleMoreApp.user_id} token update", "true")
                }
                .addOnFailureListener {
                    Log.d("${StyleMoreApp.user_id} token update", "failure  = ${it.message}")
                }
    }

    fun getLoggedInActionUser(context: Context): FbActionUser {
        val usrPref = SharedPreferenceHelper.customPrefs(context, Constants.USER_PREF)
        return FbActionUser(
                usrPref[Constants.USER_FULLNAME, ""]!!,
                usrPref[Constants.USER_PROF_PIC, ""]!!,
                usrPref[Constants.USER_ID, ""]?.toLong()!!
        )
    }

    private fun sendFcmPushNotification(fbNotification: FbNotification) {
        var sendToUserToken: String? = ""
        val toUserTokenIdDatabaseReference = FirebaseDatabase.getInstance().reference
                .child("Tokens")
                .child(fbNotification.actionToUserId)
                .child("id")

        toUserTokenIdDatabaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.e("user token", p0.message)
            }

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    sendToUserToken = p0.getValue(String::class.java)
                    Log.v("user token", sendToUserToken)
                    if (sendToUserToken == null) {
                        Log.v("user token", "not found")
                        return
                    }
                    val fcmPushPostRequest = FcmPushPostRequest(fbNotification, true, sendToUserToken!!, "high")
                    val fcmPushPresenter = FcmPushPresenterImpl()
                    fcmPushPresenter.getFcmPushResponse(FcmPushUseCase(), fcmPushPostRequest)
                }
            }

        })
    }

    fun removeAllNotificationsOfDeletedFeedOfThisUser(userId: String, deletedFeedId: String) {
        /*user id will be logged in user id*/
        if (StyleMoreApp.user_id.isNullOrEmpty()) {
            Log.e("removeFeedNotfi", "user id null")
            return
        }

        val userTokenIdDatabaseReference = FirebaseDatabase.getInstance().reference
                .child("Notifications")
                .child(userId)

        /*remove comments notifications of deleted feed*/
        userTokenIdDatabaseReference
                .child("commented")
                .child(deletedFeedId)
                .removeValue()
                .addOnSuccessListener {
                    if (it == null) {
                        Log.i("removeFeedNotific", "$deletedFeedId not found")
                        return@addOnSuccessListener
                    }
                    Log.i("removeFeedNotific", "$deletedFeedId commented success")
                }
                .addOnFailureListener {
                    Log.e("removeFeedNotific", "$deletedFeedId commented failure")
                }

        /*remove likes notifications of deleted feed*/
        userTokenIdDatabaseReference
                .child("liked")
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (uniqueKeydataSnapShot in p0.children) {
                            if (uniqueKeydataSnapShot.exists() && uniqueKeydataSnapShot.key.toString().contains(deletedFeedId)) {
                                userTokenIdDatabaseReference
                                        .child("liked")
                                        .child("${uniqueKeydataSnapShot.key}")
                                        .removeValue()
                                        .addOnSuccessListener {
                                            Log.i("removeFeedNotific", "$deletedFeedId liked success")
                                        }
                                        .addOnFailureListener {
                                            Log.e("removeFeedNotific", "$deletedFeedId liked failure")
                                        }
                            }
                        }
                    }

                })
    }

    fun updateFeedPicForNotification(fbNotification: FbNotification, updatePicUrl:String){
        val userIdDatabaseReference = FirebaseDatabase.getInstance().reference.child(fbNotification.actionToUserId)
        val actionNameDbReference = userIdDatabaseReference.child(fbNotification.actionName)
        val feedIdDbReference = actionNameDbReference.child(fbNotification.actionFeedId.toString())

        when(fbNotification.actionName.toLowerCase()){
            "liked"->{
                feedIdDbReference
                        .child("actionFeedPic")
                        .setValue(updatePicUrl)
                        .addOnSuccessListener {
                            Log.d("like pic updated", "true")
                        }
                        .addOnFailureListener {
                            Log.d("like pic updated", "failure  = ${it.message}")
                        }
            }

            "commented"->{
                feedIdDbReference.child(fbNotification.fbKey)
                        .child("actionFeedPic")
                        .setValue(updatePicUrl)
                        .addOnSuccessListener {
                            Log.d("commented pic updated", "true")
                        }
                        .addOnFailureListener {
                            Log.d("commented pic updated", "failure  = ${it.message}")
                        }
            }
        }
    }

    fun updateClickedStatusToFirebaseDb(userIdDatabaseReference: DatabaseReference,
                                     fbNotification: FbNotification) {

        val actionNameDbReference = userIdDatabaseReference.child(fbNotification.actionName)
        val feedIdDbReference = actionNameDbReference.child(fbNotification.actionFeedId.toString())
        val uniqueLikedDatabaseReference = actionNameDbReference.child("${fbNotification.actionFeedId}_${fbNotification.fbActionUser.userId}")

        when (fbNotification.actionName.toLowerCase()) {
            "liked" -> {
                uniqueLikedDatabaseReference.child("clicked").setValue(true)
                        .addOnSuccessListener {
                            Log.d("liked clicked", "true")
                        }
                        .addOnFailureListener {
                            Log.d("liked clicked", "failure  = ${it.message}")
                        }
            }

            "commented" -> {
                feedIdDbReference.child(fbNotification.fbKey)
                        .child("clicked")
                        .setValue(true)
                        .addOnSuccessListener {
                            Log.d("commented clicked", "true")
                        }
                        .addOnFailureListener {
                            Log.d("commented clicked", "failure  = ${it.message}")
                        }
            }

            "followed" -> {
                actionNameDbReference.child(fbNotification.fbActionUser.userId.toString())
                        .child("clicked")
                        .setValue(true)
                        .addOnSuccessListener {
                            Log.d("followed clicked", "true")
                        }
                        .addOnFailureListener {
                            Log.d("followed clicked", "failure  = ${it.message}")
                        }
            }
        }
    }

    fun appInForeground(context: Context): Boolean {
        val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val runningAppProcesses = activityManager.runningAppProcesses ?: return false
        return runningAppProcesses
                .any {
                    it.processName == context.packageName
                            && it.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND }
    }

    fun deleteCommentNotification(){

    }
}