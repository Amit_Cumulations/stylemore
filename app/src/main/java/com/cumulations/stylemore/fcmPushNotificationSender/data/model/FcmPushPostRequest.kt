package com.cumulations.stylemore.fcmPushNotificationSender.data.model

import com.cumulations.stylemore.base.firebase.FbNotification
import com.google.gson.annotations.SerializedName

data class FcmPushPostRequest(@SerializedName("data")
                              val data: FbNotification,
                              @SerializedName("mutable_content")
                              val mutableContent: Boolean = false,
                              @SerializedName("to")
                              val to: String = "",
                              @SerializedName("priority")
                              val priority: String = "")