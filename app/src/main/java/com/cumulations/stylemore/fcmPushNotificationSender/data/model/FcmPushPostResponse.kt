package com.cumulations.stylemore.fcmPushNotificationSender.data.model

import com.cumulations.stylemore.base.firebase.FbActionUser
import com.google.gson.annotations.SerializedName

data class FcmPushPostResponse(@SerializedName("canonical_ids")
                               val canonicalIds: Int = 0,
                               @SerializedName("success")
                               val success: Int = 0,
                               @SerializedName("failure")
                               val failure: Int = 0,
                               @SerializedName("results")
                               val results: List<FcmPushResultsItem>?,
                               @SerializedName("multicast_id")
                               val multicastId: Long = 0)