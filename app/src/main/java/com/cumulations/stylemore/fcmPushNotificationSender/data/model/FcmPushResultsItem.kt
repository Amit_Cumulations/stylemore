package com.cumulations.stylemore.fcmPushNotificationSender.data.model

import com.google.gson.annotations.SerializedName

data class FcmPushResultsItem(@SerializedName("message_id")
                              val messageId: String = "")