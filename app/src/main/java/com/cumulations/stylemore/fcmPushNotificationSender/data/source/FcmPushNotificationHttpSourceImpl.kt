package com.cumulations.stylemore.fcmPushNotificationSender.data.source

import com.cumulations.stylemore.fcmPushNotificationSender.data.model.FcmPushPostRequest
import com.cumulations.stylemore.fcmPushNotificationSender.data.model.FcmPushPostResponse
import com.cumulations.stylemore.fcmPushNotificationSender.domain.FcmPushDataInterface
import rx.Single
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by Amit Tumkur on 29-05-2018.
 */
class FcmPushNotificationHttpSourceImpl: FcmPushDataInterface {

    override fun sendFcmNotification(fcmPushPostRequest: FcmPushPostRequest): Single<FcmPushPostResponse> {
        return FcmRestClient.getApiService().createAndSendPushNotification(FcmRestClient.getFcmAuthHeaders(),fcmPushPostRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }
}