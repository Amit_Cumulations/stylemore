package com.cumulations.stylemore.fcmPushNotificationSender.data.source

import android.util.Log
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.HttpErrorResponse
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.instagramLogin.data.model.InstaLoginError
import com.cumulations.stylemore.rest.ApiService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit


class FcmRestClient {
    companion object {
        private var retrofit: Retrofit? = null

        val gson:Gson = GsonBuilder().setLenient().create()

        private val logging = HttpLoggingInterceptor()

        val client = OkHttpClient.Builder()
                .addInterceptor(logging)
                .readTimeout(1,TimeUnit.MINUTES)
                .writeTimeout(2,TimeUnit.MINUTES)
                .connectTimeout(1,TimeUnit.MINUTES)
                .build()

        fun getApiService(): ApiService {
            logging.level = HttpLoggingInterceptor.Level.BODY

            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                        .baseUrl(Constants.FCM_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .client(client)
                        .build()
            }
            return retrofit!!.create<ApiService>(ApiService::class.java)
        }

        fun getFcmAuthHeaders() : Map<String,String>{
            val map = HashMap<String, String>()
            map[Constants.AUTHORIZATION] = StyleMoreApp.fcmAuthKey
            return map
        }
    }

}