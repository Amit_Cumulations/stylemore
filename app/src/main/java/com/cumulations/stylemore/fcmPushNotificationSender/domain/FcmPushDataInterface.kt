package com.cumulations.stylemore.fcmPushNotificationSender.domain

import com.cumulations.stylemore.fcmPushNotificationSender.data.model.FcmPushPostRequest
import com.cumulations.stylemore.fcmPushNotificationSender.data.model.FcmPushPostResponse
import com.cumulations.stylemore.userProfile.data.model.UserFollowersResponse
import com.cumulations.stylemore.userProfile.data.model.UserFollowingResponse
import com.cumulations.stylemore.userProfile.data.model.UserLikedResponse
import com.cumulations.stylemore.userProfile.data.model.UserTabRequest
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

interface FcmPushDataInterface {
    fun sendFcmNotification(fcmPushPostRequest: FcmPushPostRequest): Single<FcmPushPostResponse>
}
