package com.cumulations.stylemore.fcmPushNotificationSender.domain

import com.cumulations.stylemore.base.UseCase
import com.cumulations.stylemore.fcmPushNotificationSender.data.model.FcmPushPostRequest
import com.cumulations.stylemore.fcmPushNotificationSender.data.model.FcmPushPostResponse
import com.cumulations.stylemore.fcmPushNotificationSender.data.source.FcmPushNotificationHttpSourceImpl
import com.cumulations.stylemore.feeds.data.model.EarlierPostResponse
import com.cumulations.stylemore.feeds.data.model.MockEarlierFeedsResponse
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest
import com.cumulations.stylemore.userProfile.data.model.UserLikedResponse
import com.cumulations.stylemore.userProfile.data.model.UserTabRequest
import com.cumulations.stylemore.userProfile.data.source.UserProfileSourceImpl
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class FcmPushUseCase : UseCase<FcmPushPostRequest, FcmPushPostResponse>() {
    private val fcmPushDataInterface = FcmPushNotificationHttpSourceImpl()

    override fun buildUseCase(requestObj: FcmPushPostRequest): Single<FcmPushPostResponse> {
        return fcmPushDataInterface.sendFcmNotification(requestObj)
    }
}
