package com.cumulations.stylemore.fcmPushNotificationSender.presentation

import com.cumulations.stylemore.fcmPushNotificationSender.data.model.FcmPushPostRequest
import com.cumulations.stylemore.fcmPushNotificationSender.domain.FcmPushUseCase

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

interface FcmPushPresenter {
    fun getFcmPushResponse(fcmPushUseCase: FcmPushUseCase, fcmPushPostRequest: FcmPushPostRequest)
}
