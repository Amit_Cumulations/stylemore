package com.cumulations.stylemore.fcmPushNotificationSender.presentation

import android.util.Log
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.fcmPushNotificationSender.data.model.FcmPushPostRequest
import com.cumulations.stylemore.fcmPushNotificationSender.domain.FcmPushUseCase
import retrofit2.adapter.rxjava.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class FcmPushPresenterImpl() : FcmPushPresenter {

    override fun getFcmPushResponse(fcmPushUseCase: FcmPushUseCase, fcmPushPostRequest: FcmPushPostRequest) {
        if (Utils.isOnline()) {
            fcmPushUseCase.execute(fcmPushPostRequest)
                    .subscribe({

                        if (it.failure == 0) {
                            Log.v("FcmPushResponse", "success")
                        }

                    }, {
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                Log.e("FcmPushResponse", "${it.message}")
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                Log.e("FcmPushResponse", Constants.NETWORK_ERROR)
                            } else {
                                Log.e("FcmPushResponse", "${it.message}")
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            Log.e("FcmPushResponse", "${e.message}")
                        }
                    })

        } else
            Log.e("FcmPushResponse", Constants.NO_INTERNET)
    }
}
