package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName

data class EarlierPostResponse(@SerializedName("result")
                               val result: List</*EarlierPostResultItem*/ResultItem>?,
                               @SerializedName("status")
                               val status: String = "",
                               @SerializedName("description")
                               val description: String = "")