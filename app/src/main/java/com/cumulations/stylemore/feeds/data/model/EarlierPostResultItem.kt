package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName

data class EarlierPostResultItem(@SerializedName("user_id")
                                 val userId: Int = 0,
                                 @SerializedName("l_count")
                                 val lCount: Int = 0,
                                 @SerializedName("profile_pic")
                                 val profilePic: String = "",
                                 @SerializedName("name")
                                 val name: String = "",
                                 @SerializedName("description")
                                 val description: String = "",
                                 @SerializedName("added_on")
                                 val addedOn: String = "",
                                 @SerializedName("pictures")
                                 val pictures: List<String>?,
                                 @SerializedName("old_feeds")
                                 val oldFeeds: List<OldFeedsItem>?,
                                 @SerializedName("feed_id")
                                 val feedId: Int = 0,
                                 @SerializedName("c_count")
                                 val cCount: Int = 0,
                                 @SerializedName("tags")
                                 val tags: List<TagsItem>?)