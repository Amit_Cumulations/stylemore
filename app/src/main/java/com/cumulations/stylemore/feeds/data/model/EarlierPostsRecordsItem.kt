package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName

data class EarlierPostsRecordsItem(@SerializedName("comments")
                                   val comments: Int = 0,
                                   @SerializedName("user_id")
                                   val userId: String = "",
                                   @SerializedName("feed_pic")
                                   val feedPic: String = "",
                                   @SerializedName("feed_id")
                                   val feedId: Int = 0,
                                   @SerializedName("likes")
                                   val likes: Int = 0)