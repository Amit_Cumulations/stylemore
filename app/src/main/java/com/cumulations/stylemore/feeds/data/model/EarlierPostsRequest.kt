package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName

data class EarlierPostsRequest(@SerializedName("user_id")
                               val userId: String,
                               @SerializedName("feed_id")
                               val feedId: String)