package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName

data class FeedCommentsRecordsItem(@SerializedName("user_id")
                                   val userId: String = "",
                                   @SerializedName("name")
                                   val name: String = "",
                                   @SerializedName("comment")
                                   val comment: String = "",
                                   @SerializedName("prof_pic")
                                   val profPic: String = "",
                                   @SerializedName("added_on")
                                   val addedOn: String = "")