package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName

data class FeedCommentsResponse(@SerializedName("result")
                                val result: List<FeedCommentsResultItem>?,
                                @SerializedName("status")
                                val status: String = "",
                                @SerializedName("description")
                                val description: String = "")