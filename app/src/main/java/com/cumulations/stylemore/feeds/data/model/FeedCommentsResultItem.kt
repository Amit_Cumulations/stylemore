package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName

data class FeedCommentsResultItem(@SerializedName("user_id")
                                  val userId: Int = 0,
                                  @SerializedName("name")
                                  val name: String = "",
                                  @SerializedName("profile_pic")
                                  val profilePic: String = "",
                                  @SerializedName("comment")
                                  val comment: String = "",
                                  @SerializedName("comment_id")
                                  val commentId: Int = 0,
                                  @SerializedName("added_on")
                                  val addedOn: String = "")