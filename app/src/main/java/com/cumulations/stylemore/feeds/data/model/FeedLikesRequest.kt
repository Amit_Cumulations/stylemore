package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Amit Tumkur on 12-06-2018.
 */
data class FeedLikesRequest(@SerializedName("feed_id")
                            val feedId: String? = "")