package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName

data class FeedLikesResponse(@SerializedName("result")
                             val result: List<FeedLikesResultItem>?,
                             @SerializedName("status")
                             val status: String = "",
                             @SerializedName("description")
                             val description: String = "")