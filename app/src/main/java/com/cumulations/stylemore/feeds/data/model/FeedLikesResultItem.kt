package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName

data class FeedLikesResultItem(@SerializedName("user_id")
                                val userId: String = "",
                               @SerializedName("following")
                                val following: Boolean = false,
                               @SerializedName("name")
                                val name: String = "",
                               @SerializedName("profile_pic")
                                val profilePic: String = "")