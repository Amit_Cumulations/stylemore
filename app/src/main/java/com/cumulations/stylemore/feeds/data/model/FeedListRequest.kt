package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName

data class FeedListRequest(@SerializedName("item_count")
                           val itemCount: Int = 0,
                           @SerializedName("index")
                           val index: Int = 0)