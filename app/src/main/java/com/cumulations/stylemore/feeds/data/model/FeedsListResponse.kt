package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName

data class FeedsListResponse(@SerializedName("result")
                             val result: List<ResultItem>?,
                             @SerializedName("status")
                             val status: String = "",
                             @SerializedName("description")
                             val description: String = "")