package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Amit Tumkur on 14-06-2018.
 */
data class LikeFeedResponse(@SerializedName("result")
                            val result: String = "",
                            @SerializedName("status")
                            val status: String = "",
                            @SerializedName("description")
                            val description: String = "")