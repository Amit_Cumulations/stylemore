package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName

data class MockEarlierFeedsResponse(@SerializedName("records")
                                    val records: List<MockFeedsRecordsItem>?,
                                    @SerializedName("status")
                                    val status: String = "",
                                    @SerializedName("description")
                                    val description: String = "")