package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class OldFeedsItem(@SerializedName("l_count")
                        val lCount: Int = 0,
                        @SerializedName("added_on")
                        val addedOn: String = "",
                        @SerializedName("pictures")
                        val pictures: List<String>?,
                        @SerializedName("feed_id")
                        val feedId: Int = 0,
                        @SerializedName("c_count")
                        val cCount: Int = 0):Serializable