package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

data class ResultItem(@SerializedName("user_id")
                      val userId: Int = 0,
                      @SerializedName("l_count")
                      var lCount: Int = 0,
                      @SerializedName("profile_pic")
                      val profilePic: String = "",
                      @SerializedName("name")
                      val name: String = "",
                      @SerializedName("description")
                      val description: String = "",
                      @SerializedName("liked_by_me")
                      var likedByMe: Boolean = false,
                      @SerializedName("following")
                      var following: Boolean = false,
                      @SerializedName("added_on")
                      val addedOn: String = "",
                      @SerializedName("pictures")
                      val pictures: List<String>?,
                      @SerializedName("old_feeds")
                      var oldFeeds: List<OldFeedsItem>?,
                      @SerializedName("feed_id")
                      val feedId: Int = 0,
                      @SerializedName("c_count")
                      var cCount: Int = 0,
                      @SerializedName("tags")
                      val tags: LinkedList<TagsItem>?) : Serializable