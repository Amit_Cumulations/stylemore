package com.cumulations.stylemore.feeds.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class TagsItem(
        @SerializedName("tag")
        val tag: String = "",
        @SerializedName("brand")
        var brand: String = "",
        @SerializedName("isSelected")
        var isSelected: Boolean = false) : Serializable