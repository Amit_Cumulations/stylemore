package com.cumulations.stylemore.feeds.data.source

import com.cumulations.stylemore.feeds.data.model.*
import com.cumulations.stylemore.feeds.domain.FeedListDataInterface
import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.data.model.LoginResponse
import com.cumulations.stylemore.login.domain.LoginDataInterface
import com.cumulations.stylemore.rest.RestClient
import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest
import retrofit2.http.HeaderMap
import rx.Single
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by Amit Tumkur on 29-05-2018.
 */
class FeedListDataSourceImpl:FeedListDataInterface {

    override fun getHotFeeds(feedListRequest: FeedListRequest): Single<FeedsListResponse> {
        return RestClient.getApiService().getHotFeeds(RestClient.getAuthHeaders(),feedListRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun getNewFeeds(feedListRequest: FeedListRequest): Single<FeedsListResponse> {
        return RestClient.getApiService().getNewFeeds(RestClient.getAuthHeaders(),feedListRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun getFollowingFeeds(feedListRequest: FeedListRequest): Single<FeedsListResponse> {
        return RestClient.getApiService().getFollowingFeeds(RestClient.getAuthHeaders(),feedListRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun getStyleTipFeeds(feedListRequest: FeedListRequest): Single<FeedsListResponse> {
        return RestClient.getApiService().getStyleTipFeeds(RestClient.getAuthHeaders(),feedListRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun getEarlierFeeds(earlierPostsRequest: EarlierPostsRequest): Single<EarlierPostResponse> {
        /*return RestClient.getJSONGeneratorApiService().getUserEarlierPosts(RestClient.getAuthHeaders(),earlierPostsRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())*/

        return RestClient.getApiService().getEarlierFeeds(RestClient.getAuthHeaders(),earlierPostsRequest.feedId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun getFeedLikes(feedLikesRequest: FeedLikesRequest): Single<FeedLikesResponse> {
        return RestClient./*getJSONGeneratorApiService()*/getApiService().getFeedLikes(RestClient.getAuthHeaders(),feedLikesRequest.feedId!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun getFeedComments(feedLikesRequest: FeedLikesRequest): Single<FeedCommentsResponse> {
        return RestClient./*getJSONGeneratorApiService()*/getApiService().getFeedComments(RestClient.getAuthHeaders(),feedLikesRequest.feedId!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun addCommentToFeed(addCommentRequest: AddCommentRequest): Single<LikeFeedResponse> {
        return RestClient.getApiService().addCommentToFeed(RestClient.getAuthHeaders(),addCommentRequest.feed_id!!,addCommentRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun addLikeToFeed(userDetailRequest: UserDetailRequest): Single<LikeFeedResponse> {
        return RestClient.getApiService().addLikeToFeed(RestClient.getAuthHeaders(),userDetailRequest.feed_id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun deleteFeedFromServer(userDetailRequest: UserDetailRequest): Single<BasicResponse> {
        return RestClient.getApiService().deleteFeed(RestClient.getAuthHeaders(),userDetailRequest.feed_id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun followUnfollowUser(userDetailRequest: UserDetailRequest): Single<BasicResponse> {
        return RestClient.getApiService().followUnfollowUser(RestClient.getAuthHeaders(),userDetailRequest.user_id!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun deleteCommentForFeed(userDetailRequest: UserDetailRequest): Single<BasicResponse> {
        return RestClient.getApiService().deleteComment(RestClient.getAuthHeaders(), userDetailRequest.feed_id, userDetailRequest.comment_id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }
}