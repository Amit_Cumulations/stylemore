package com.cumulations.stylemore.feeds.domain

import com.cumulations.stylemore.base.UseCase
import com.cumulations.stylemore.feeds.data.model.*
import com.cumulations.stylemore.feeds.data.source.FeedListDataSourceImpl
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class AddLikeUseCase : UseCase<UserDetailRequest, LikeFeedResponse>() {

    private val feedListDataInterface = FeedListDataSourceImpl()

    override fun buildUseCase(requestObj: UserDetailRequest): Single<LikeFeedResponse> {
        return feedListDataInterface.addLikeToFeed(requestObj)
    }

}
