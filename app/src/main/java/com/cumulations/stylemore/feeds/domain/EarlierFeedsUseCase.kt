package com.cumulations.stylemore.feeds.domain

import com.cumulations.stylemore.base.UseCase
import com.cumulations.stylemore.feeds.data.model.EarlierPostResponse
import com.cumulations.stylemore.feeds.data.model.EarlierPostsRequest
import com.cumulations.stylemore.feeds.data.source.FeedListDataSourceImpl
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class EarlierFeedsUseCase : UseCase<EarlierPostsRequest, EarlierPostResponse>() {

    private val feedListDataSourceImpl = FeedListDataSourceImpl()

    override fun buildUseCase(requestObj: EarlierPostsRequest): Single<EarlierPostResponse> {
        return feedListDataSourceImpl.getEarlierFeeds(requestObj)
    }

}
