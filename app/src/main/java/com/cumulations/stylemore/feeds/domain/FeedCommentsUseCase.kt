package com.cumulations.stylemore.feeds.domain

import com.cumulations.stylemore.base.UseCase
import com.cumulations.stylemore.feeds.data.model.*
import com.cumulations.stylemore.feeds.data.source.FeedListDataSourceImpl
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class FeedCommentsUseCase : UseCase<FeedLikesRequest, FeedCommentsResponse>() {

    private val feedListDataSourceImpl = FeedListDataSourceImpl()

    override fun buildUseCase(requestObj: FeedLikesRequest): Single<FeedCommentsResponse> {
        return feedListDataSourceImpl.getFeedComments(requestObj)
    }

}
