package com.cumulations.stylemore.feeds.domain

import com.cumulations.stylemore.base.UseCase
import com.cumulations.stylemore.feeds.data.model.EarlierPostResponse
import com.cumulations.stylemore.feeds.data.model.EarlierPostsRequest
import com.cumulations.stylemore.feeds.data.model.FeedLikesRequest
import com.cumulations.stylemore.feeds.data.model.FeedLikesResponse
import com.cumulations.stylemore.feeds.data.source.FeedListDataSourceImpl
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class FeedLikesUseCase : UseCase<FeedLikesRequest, FeedLikesResponse>() {

    private val feedListDataSourceImpl = FeedListDataSourceImpl()

    override fun buildUseCase(requestObj: FeedLikesRequest): Single<FeedLikesResponse> {
        return feedListDataSourceImpl.getFeedLikes(requestObj)
    }

}
