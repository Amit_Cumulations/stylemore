package com.cumulations.stylemore.feeds.domain

import com.cumulations.stylemore.feeds.data.model.*
import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.data.model.LoginResponse
import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest
import retrofit2.http.HeaderMap
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

interface FeedListDataInterface {
    fun getHotFeeds(feedListRequest: FeedListRequest): Single<FeedsListResponse>
    fun getNewFeeds(feedListRequest: FeedListRequest): Single<FeedsListResponse>
    fun getFollowingFeeds(feedListRequest: FeedListRequest): Single<FeedsListResponse>
    fun getStyleTipFeeds(feedListRequest: FeedListRequest): Single<FeedsListResponse>
    fun getEarlierFeeds(earlierPostsRequest: EarlierPostsRequest): Single<EarlierPostResponse>
    fun getFeedLikes(feedLikesRequest: FeedLikesRequest): Single<FeedLikesResponse>
    fun getFeedComments(feedLikesRequest: FeedLikesRequest): Single<FeedCommentsResponse>
    fun addCommentToFeed(addCommentRequest: AddCommentRequest): Single<LikeFeedResponse>
    fun addLikeToFeed(userDetailRequest: UserDetailRequest): Single<LikeFeedResponse>
    fun deleteFeedFromServer(userDetailRequest: UserDetailRequest): Single<BasicResponse>
    fun followUnfollowUser(userDetailRequest: UserDetailRequest): Single<BasicResponse>
    fun deleteCommentForFeed(userDetailRequest: UserDetailRequest): Single<BasicResponse>
}
