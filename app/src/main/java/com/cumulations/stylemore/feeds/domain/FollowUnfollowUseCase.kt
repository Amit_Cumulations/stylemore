package com.cumulations.stylemore.feeds.domain

import com.cumulations.stylemore.base.UseCase
import com.cumulations.stylemore.feeds.data.model.*
import com.cumulations.stylemore.feeds.data.source.FeedListDataSourceImpl
import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class FollowUnfollowUseCase : UseCase<UserDetailRequest, BasicResponse>() {

    private val feedListDataInterface = FeedListDataSourceImpl()

    override fun buildUseCase(requestObj: UserDetailRequest): Single<BasicResponse> {
        return feedListDataInterface.followUnfollowUser(requestObj)
    }

}
