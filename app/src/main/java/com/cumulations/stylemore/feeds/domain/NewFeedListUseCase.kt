package com.cumulations.stylemore.feeds.domain

import com.cumulations.stylemore.base.UseCase
import com.cumulations.stylemore.feeds.data.model.FeedListRequest
import com.cumulations.stylemore.feeds.data.model.FeedsListResponse
import com.cumulations.stylemore.feeds.data.source.FeedListDataSourceImpl
import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.data.model.LoginResponse
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class NewFeedListUseCase : UseCase<FeedListRequest, FeedsListResponse>() {
    private val feedListDataSourceImpl = FeedListDataSourceImpl()

    override fun buildUseCase(requestObj: FeedListRequest): Single<FeedsListResponse> {
        return feedListDataSourceImpl.getNewFeeds(requestObj)
    }
}
