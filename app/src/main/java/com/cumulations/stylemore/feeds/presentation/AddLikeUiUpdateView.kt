package com.cumulations.stylemore.feeds.presentation

import com.cumulations.stylemore.base.LoadingView
import com.cumulations.stylemore.feeds.data.model.LikeFeedResponse

/**
 * Created by Amit Tumkur on 15-06-2018.
 */
interface AddLikeUiUpdateView : LoadingView {
    fun updateLikeResponse(likeFeedResponse: LikeFeedResponse)
    fun showError(error: String)
}