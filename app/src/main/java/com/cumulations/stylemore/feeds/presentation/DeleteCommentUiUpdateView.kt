package com.cumulations.stylemore.feeds.presentation

import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse

/**
 * Created by Amit Tumkur on 15-06-2018.
 */
interface DeleteCommentUiUpdateView {
    fun updateDeleteCommentResponse(basicResponse: BasicResponse)
    fun errorMessage(error: String)
}