package com.cumulations.stylemore.feeds.presentation

import android.view.View
import com.cumulations.stylemore.feeds.data.model.TagsItem
import java.util.*

/**
 * Created by Amit Tumkur on 21-06-2018.
 */
interface DialogBtnsListener {
    fun onPositiveBtnClick(view: View)
    fun onNegativeBtnClick(view: View)
}