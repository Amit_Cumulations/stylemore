package com.cumulations.stylemore.feeds.presentation

import android.content.Context
import android.content.Intent
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cumulations.stylemore.R
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.data.model.FeedCommentsResultItem
import com.cumulations.stylemore.userProfile.presentation.UserProfileActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_comments.view.*
import org.apache.commons.text.StringEscapeUtils
import kotlin.collections.ArrayList


class DialogFeedCommentsAdapter(val context: Context,
                                var feedCommentsList: ArrayList<FeedCommentsResultItem>?,
                                val feedCommentsDialogFragment: FeedCommentsDialogFragment) : RecyclerView.Adapter<DialogFeedCommentsAdapter.FeedCommentsItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): FeedCommentsItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_comments, parent, false)
        return FeedCommentsItemViewHolder(view)
    }


    override fun onBindViewHolder(feedCommentsItemViewHolder: FeedCommentsItemViewHolder, position: Int) {
        val feedCommentsRecordsItem = feedCommentsList?.get(position)
        feedCommentsItemViewHolder.bindFeedComments(feedCommentsRecordsItem,position)
    }

    override fun getItemCount(): Int {
        return feedCommentsList?.size!!
    }

    inner class FeedCommentsItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindFeedComments(feedCommentsResultItem: FeedCommentsResultItem?, position: Int){
            Picasso.get().load(feedCommentsResultItem?.profilePic)
                    .resize(80,80)
                    .placeholder(R.drawable.user_placeholder_square)
                    .into(itemView.civ_user_icon)

            itemView.tv_user_name.text = feedCommentsResultItem?.name

            val unescapedComment = StringEscapeUtils.unescapeJava(feedCommentsResultItem?.comment!!)
            itemView.tv_user_comment.text = unescapedComment

            val timeAgo = Utils.getDateInRelativeTime(feedCommentsResultItem?.addedOn!!,"yyyy-MM-dd HH:mm:ss")
            itemView.tv_comment_time.text = timeAgo

            itemView.civ_user_icon.setOnClickListener {
                context.startActivity(Intent(context, UserProfileActivity::class.java)
                        .putExtra(Constants.USER_ID, feedCommentsResultItem?.userId)
                        .putExtra(Constants.USER_FULLNAME,feedCommentsResultItem?.name)
                        .putExtra(Constants.USER_PROF_PIC,feedCommentsResultItem?.profilePic))
            }

            if (StyleMoreApp.user_id == feedCommentsResultItem.userId.toString()){
                itemView.iv_more.visibility = View.VISIBLE
            } else itemView.iv_more.visibility = View.GONE

            itemView.iv_more.setOnClickListener{
                //Creating the instance of PopupMenu
                val popup = PopupMenu(context, it)
                //Inflating the Popup using xml file
                popup.menuInflater.inflate(R.menu.comment_popup_menu, popup.getMenu())

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener {
                    when(it.itemId){
                        R.id.delete ->{
                            feedCommentsDialogFragment.deleteCommentFromList(feedCommentsResultItem,position)
                        }

                        R.id.cancel->{
                            popup.dismiss()
                        }
                    }
                    true
                }

                popup.show()//showing popup menu
            }
        }
    }

    fun updateList(feedCommentsList: ArrayList<FeedCommentsResultItem>?){
        this.feedCommentsList = feedCommentsList
        notifyDataSetChanged()
    }
}



