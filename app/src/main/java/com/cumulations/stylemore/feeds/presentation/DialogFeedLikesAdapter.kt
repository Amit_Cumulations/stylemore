package com.cumulations.stylemore.feeds.presentation

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cumulations.stylemore.R
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper.get
import com.cumulations.stylemore.feeds.data.model.FeedLikesResultItem
import com.cumulations.stylemore.userProfile.presentation.UserProfileActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_likes.view.*

class DialogFeedLikesAdapter(val context: Context,
                             var feedLikesList: List<FeedLikesResultItem>?,
                             val feedLikesDialogFragment: FeedLikesDialogFragment) : RecyclerView.Adapter<DialogFeedLikesAdapter.FeedLikesItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): FeedLikesItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_likes, parent, false)
        return FeedLikesItemViewHolder(view)
    }


    override fun onBindViewHolder(feedLikesItemViewHolder: FeedLikesItemViewHolder, position: Int) {
        val feedLikesRecordsItem = feedLikesList?.get(position)
        feedLikesItemViewHolder.bindFeedLikes(feedLikesRecordsItem)
    }

    override fun getItemCount(): Int {
        return feedLikesList!!.size
    }

    inner class FeedLikesItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindFeedLikes(feedLikesResultItem: FeedLikesResultItem?){
            Picasso.get().load(feedLikesResultItem?.profilePic)
                    .resize(80,80)
                    .placeholder(R.drawable.user_placeholder_square)
                    .into(itemView.civ_user_icon)

            itemView.tv_user_name.text = feedLikesResultItem?.name

            val userPref = SharedPreferenceHelper.customPrefs(context,Constants.USER_PREF)
            val userId = userPref[Constants.USER_ID,""]

            if (userId.equals(feedLikesResultItem?.userId)){
                itemView.iv_following.visibility = View.GONE
            } else {
                itemView.iv_following.visibility = View.VISIBLE
                if (feedLikesResultItem?.following!!){
                    itemView.iv_following.setImageResource(R.drawable.ic_following_red)
                } else
                    itemView.iv_following.setImageResource(R.drawable.ic_follow_black)
            }

            itemView.iv_following.setOnClickListener {
                feedLikesDialogFragment.followUnfollowUser(feedLikesResultItem)
            }

            itemView.ll_user_name.setOnClickListener {
                context.startActivity(Intent(context, UserProfileActivity::class.java)
                        .putExtra(Constants.USER_ID, feedLikesResultItem?.userId?.toInt())
                        .putExtra(Constants.USER_FULLNAME,feedLikesResultItem?.name)
                        .putExtra(Constants.USER_FOLLOWING,feedLikesResultItem?.following)
                        .putExtra(Constants.USER_PROF_PIC,feedLikesResultItem?.profilePic)
                        /*.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)*/)
            }
        }
    }

    fun updateList(feedLikesList: List<FeedLikesResultItem>?){
        this.feedLikesList = feedLikesList
        notifyDataSetChanged()
    }

}



