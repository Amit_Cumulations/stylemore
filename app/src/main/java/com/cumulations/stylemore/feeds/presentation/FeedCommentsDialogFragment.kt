package com.cumulations.stylemore.feeds.presentation

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.cumulations.stylemore.R
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.firebase.FbNotification
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper.get
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.data.model.*
import com.cumulations.stylemore.feeds.domain.AddCommentUseCase
import com.cumulations.stylemore.feeds.domain.DeleteCommentUseCase
import com.cumulations.stylemore.feeds.domain.FeedCommentsUseCase
import com.cumulations.stylemore.login.presentation.SplashScreenActivity
import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_feed_comments.*
import kotlinx.android.synthetic.main.layout_full_screen_loader.*
import org.apache.commons.text.StringEscapeUtils
import org.jetbrains.anko.toast
import kotlin.collections.ArrayList


/**
 * Created by Amit Tumkur on 05-06-2018.
 */
class FeedCommentsDialogFragment: DialogFragment(),FeedDetailsUiUpdateView,AddCommentUiUpdateView,DeleteCommentUiUpdateView {

    private lateinit var dialogFeedCommentsAdapter: DialogFeedCommentsAdapter
    private var feedId: String? = null
    private lateinit var feedListPresenter: FeedListPresenterImpl
    private lateinit var updateCommentCountListener: UpdateCommentCountListener
    private var updateItemPosition = -1
    private var deletePos = -1

    interface UpdateCommentCountListener{
        fun incrementCommentCount(updateItemPosition: Int)
        fun decrementCommentCount(updateItemPosition: Int)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        val dialog = Dialog(activity, R.style.CustomDialogTheme)
        dialog.setContentView(R.layout.dialog_feed_comments)
        dialog.setCanceledOnTouchOutside(true)
        dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        feedId = arguments?.getString(Constants.FEED_ID)
        dialogFeedCommentsAdapter = DialogFeedCommentsAdapter(this.activity!!, ArrayList(),this)

        val mLayoutManager = LinearLayoutManager(this.activity!!)
        dialog.rv_feed_comments.layoutManager = mLayoutManager
        dialog.rv_feed_comments.itemAnimator = DefaultItemAnimator()
        dialog.rv_feed_comments.adapter = dialogFeedCommentsAdapter
        val userPref = SharedPreferenceHelper.customPrefs(activity!!, Constants.USER_PREF)
        val profPicUrl = userPref[Constants.USER_PROF_PIC,""]

        Picasso.get().load(profPicUrl)
                .resize(64,64)
                .placeholder(R.drawable.user_placeholder_square)
                .into(dialog.civ_user_icon)


        dialog.iv_add_comment.isEnabled = !dialog.et_user_add_comment.text.isNullOrEmpty()
        dialog.iv_add_comment.isClickable = dialog.iv_add_comment.isEnabled
        if (dialog.iv_add_comment.isEnabled){
            dialog.iv_add_comment.alpha = 1f
        } else dialog.iv_add_comment.alpha = .5f

        dialog.et_user_add_comment.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                dialog.iv_add_comment.isEnabled = !p0.isNullOrEmpty()
                dialog.iv_add_comment.isClickable = !p0.isNullOrEmpty()
                if (dialog.iv_add_comment.isEnabled){
                    dialog.iv_add_comment.alpha = 1f
                } else dialog.iv_add_comment.alpha = .5f
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })

        dialog.iv_add_comment.setOnClickListener {
            if (dialog.et_user_add_comment.text.trim().isEmpty()){
                activity?.toast("Empty spaces not allowed")
                return@setOnClickListener
            }

            Utils.closeKeyboard(activity!!,it)
            addCommentToFeed()
        }

        return dialog
    }

    private fun addCommentToFeed() {
        feedListPresenter.setCommentResponseListener(this)
        val etString = dialog.et_user_add_comment.text.toString()
        val unicodeEtString = StringEscapeUtils.escapeJava(etString)
        val addCommentRequest = AddCommentRequest(StyleMoreApp.user_id,feedId,unicodeEtString)
        feedListPresenter.getAddCommentResponse(AddCommentUseCase(),addCommentRequest)
        dialog.et_user_add_comment.text.clear()
    }

    override fun onStart() {
        super.onStart()
        getFeedComments()
    }

    private fun getFeedComments() {
        feedListPresenter = FeedListPresenterImpl(this)
        val feedLikesRequest = FeedLikesRequest(feedId)
        feedListPresenter.getFeedCommentsResponse(FeedCommentsUseCase(),feedLikesRequest)
    }

    override fun provideUserEarlierPostsResponse(earlierPostResponse: EarlierPostResponse) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun provideFeedLikes(feedLikesResponse: FeedLikesResponse) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showLoading() {
        if (dialog == null)
            return
        dialog.full_screen_loader_layout.visibility = View.VISIBLE
        Utils.showLoader(dialog.loader,null, this.activity!!,false)
    }

    override fun hideLoading() {
        if (dialog == null)
            return
        dialog.full_screen_loader_layout.visibility = View.GONE
        Utils.closeLoader(dialog.loader,null,this.activity!!)
    }

    override fun provideFeedComments(feedCommentsResponse: FeedCommentsResponse) {
        if (dialog == null)
            return
        if (feedCommentsResponse.result!!.isNotEmpty()){
            dialog.tv_no_data.visibility = View.GONE
            dialog.rv_feed_comments.visibility = View.VISIBLE
            dialogFeedCommentsAdapter.updateList(feedCommentsResponse.result as ArrayList<FeedCommentsResultItem>?)
            if (activity is FeedDetailsActivity) {
                (activity as FeedDetailsActivity).updateLikeCommentsCount(false, feedCommentsResponse.result.size)
            }
        }
    }

    override fun showError(error: String) {
//        activity?.toast(error)
        if (error == Constants.INVALID_AUTH_KEY){
            SharedPreferenceHelper.clearCustomPrefs(activity!!, Constants.USER_PREF)
            activity!!.startActivity(Intent(activity, SplashScreenActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
            activity!!.finish()
        }

        if (dialog == null)
            return
        if (error == Constants.NO_DATA_FOUND){
            dialog.tv_no_data.text = "No comments yet. Be the first to comment"
            dialog.tv_no_data.visibility = View.VISIBLE
            dialog.rv_feed_comments.visibility = View.INVISIBLE
        }
    }

    override fun updateCommentResponse(likeFeedResponse: LikeFeedResponse) {
        getFeedComments()

        /*push to fb db*/
        val feedUserId = arguments?.getInt(Constants.USER_ID)
        val feedPic = arguments?.getString(Constants.FEED_PIC)
        if (!StyleMoreApp.user_id.equals(feedUserId.toString())) {
            val userIdDbReference = FirebaseDatabase.getInstance().reference
                    .child("Notifications")
                    .child("$feedUserId")
            val timestamp = System.currentTimeMillis()
            val fbNotification = FbNotification(timestamp,"commented",Utils.getLoggedInActionUser(activity!!),
                    feedId!!.toLong(),feedPic,timestamp,false, false)
            fbNotification.actionToUserId = feedUserId.toString()
            Utils.pushActionToFirebaseDb(userIdDbReference, fbNotification)
        }

        /*updating comments count in Feed List screen for clicked feed position*/
        updateItemPosition = arguments?.getInt(Constants.POSITION,-1)!!
        if (updateItemPosition!=-1){
            updateCommentCountListener.incrementCommentCount(updateItemPosition)
        }
    }

    override fun provideDeleteFeedResponse(basicResponse: BasicResponse) {
    }

    fun setUpdateCommentCountListener(updateCommentCountListener: UpdateCommentCountListener){
        this.updateCommentCountListener = updateCommentCountListener
    }

    fun deleteCommentFromList(feedCommentsResultItem: FeedCommentsResultItem?, position: Int){
        deletePos = position
        feedListPresenter.setDeleteCommentUiUpdateListener(this)
        val userDetailRequest = UserDetailRequest("",feedId!!, feedCommentsResultItem?.commentId.toString())
        feedListPresenter.getDeleteCommentResponse(DeleteCommentUseCase(),userDetailRequest)
    }

    override fun updateDeleteCommentResponse(basicResponse: BasicResponse) {
        if (basicResponse.result.equals("success",true)){
            updateItemPosition = arguments?.getInt(Constants.POSITION,-1)!!
            if (deletePos!=-1){
                dialogFeedCommentsAdapter.feedCommentsList?.removeAt(deletePos)
                if (dialogFeedCommentsAdapter.feedCommentsList?.size!!>0) {
                    dialogFeedCommentsAdapter.notifyItemRemoved(deletePos)
                    dialogFeedCommentsAdapter.notifyItemRangeChanged(deletePos, dialogFeedCommentsAdapter.feedCommentsList?.size!!)
                } else {
                    dialog.tv_no_data.text = "No comments yet.Be the first to comment"
                    dialog.tv_no_data.visibility = View.VISIBLE
                    dialog.rv_feed_comments.visibility = View.INVISIBLE
                }

                updateCommentCountListener.decrementCommentCount(updateItemPosition)
            }
        }
    }

    override fun errorMessage(error: String) {
        activity?.toast(error)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (dialog!=null && activity!=null){
            Utils.closeLoader(dialog.loader,null,activity!!)
        }
    }
}