package com.cumulations.stylemore.feeds.presentation

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.cumulations.stylemore.R
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.firebase.FbNotification
import com.cumulations.stylemore.base.hashTagHelper.HashTagHelper
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper.get
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.data.model.*
import com.cumulations.stylemore.feeds.domain.AddLikeUseCase
import com.cumulations.stylemore.feeds.domain.DeleteFeedUseCase
import com.cumulations.stylemore.feeds.domain.EarlierFeedsUseCase
import com.cumulations.stylemore.feeds.domain.FollowUnfollowUseCase
import com.cumulations.stylemore.search.presentation.SearchActivity
import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse
import com.cumulations.stylemore.uploadFeed.presentation.UploadFeedActivity
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest
import com.cumulations.stylemore.userProfile.presentation.UserProfileActivity
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_feed_details.*
import kotlinx.android.synthetic.main.layout_feed_details_bottom.*
import org.apache.commons.text.StringEscapeUtils
import org.jetbrains.anko.toast
import java.util.*


class FeedDetailsActivity : AppCompatActivity(), View.OnClickListener,
        FeedDetailsUiUpdateView,
        FollowUserUiUpdateView,
        AddLikeUiUpdateView,
        HashTagHelper.OnHashTagClickListener,
        FeedCommentsDialogFragment.UpdateCommentCountListener {

    private lateinit var resultItem: ResultItem
    private lateinit var feedEarlierPostsAdapter: FeedEarlierPostsAdapter
    private var isFavClicked = false
    private var feedId: Int = 0
    private var screenWidth = 0
    private var notificationDbReference: DatabaseReference = FirebaseDatabase.getInstance().reference.child("Notifications")
    private lateinit var userIdDatabaseReference: DatabaseReference
    var additionalSymbols = charArrayOf('_'/*, '$'*/)
    private var toUserId = ""
    private var loadingFeed = false
    private var isLoading = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!Utils.isOnline()){
            toast(getString(R.string.no_internet_connection))
            finish()
            return
        }

        setContentView(R.layout.activity_feed_details)

        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.elevation = 5.0f

        screenWidth = Utils.getScreenWidthInDp(this)
        initClickListeners()
        initGridAdapter()
    }

    override fun onStart() {
        super.onStart()
        feedId = intent.getIntExtra(Constants.FEED_ID, -1)
        if (feedId != -1) {
            getFeedDetails()
        }
    }

    private fun initViews() {

        val userPref = SharedPreferenceHelper.customPrefs(this, Constants.USER_PREF)
        val userId = userPref[Constants.USER_ID, ""]

        if (!userId.isNullOrEmpty() && userId!! == resultItem.userId.toString()) {
            ib_edit_feed.visibility = View.VISIBLE
            ib_delete_feed.visibility = View.VISIBLE
            ib_add_user.visibility = View.GONE
        }

        if (resultItem.following) {
            ib_add_user.setImageResource(R.drawable.ic_following_red)
        } else {
            ib_add_user.setImageResource(R.drawable.ic_follow_black)
        }


        Picasso.get().load(resultItem.profilePic)
                .placeholder(R.drawable.user_placeholder_square)
                .into(civ_profile_icon)

        tv_user_name.text = resultItem.name

        try {
            val timeAgo = Utils.getDateInRelativeTime(resultItem.addedOn, "yyyy-MM-dd HH:mm:ss")
            tv_feed_posted_time.text = timeAgo
        } catch (e: Exception) {
            e.printStackTrace()
            tv_feed_posted_time.text = ""
        }

        val hashTagHelper = HashTagHelper.Creator.create(ContextCompat.getColor(this, R.color.colorAccent), this, additionalSymbols)
        hashTagHelper.handle(tv_brand_tags_more)

        val unescapedDescription = StringEscapeUtils.unescapeJava(resultItem?.description!!)
        tv_brand_tags_more.text = unescapedDescription
//        TextViewUtil.makeTextViewResizable(tv_brand_tags_more,3,"Show More", true)

        tv_likes_count.text = "${resultItem.lCount} Likes"
        tv_comments_count.text = "${resultItem.cCount} Comments"

        initTagItems()

        tv_earlier_posts_label.text = "${resultItem.name} earlier posts"
    }

    private fun initClickListeners() {
        linear_likes.setOnClickListener(this)
        linear_comments.setOnClickListener(this)
        ib_back.setOnClickListener(this)
        civ_profile_icon.setOnClickListener(this)
        tv_user_name.setOnClickListener(this)
        ib_edit_feed.setOnClickListener(this)
        ib_delete_feed.setOnClickListener(this)
        ib_add_user.setOnClickListener(this)
        iv_heart.setOnClickListener(this)
//        initAppBarListener()
    }

    private fun initAppBarListener() {
        appbar.addOnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (collapsing_toolbar.getHeight() + verticalOffset < 2 * ViewCompat.getMinimumHeight(collapsing_toolbar)) {
                DrawableCompat.setTint(ib_back.drawable, ContextCompat.getColor(this, android.R.color.black))
                DrawableCompat.setTint(ib_edit_feed.drawable, ContextCompat.getColor(this, android.R.color.black))
                DrawableCompat.setTint(ib_add_user.drawable, ContextCompat.getColor(this, android.R.color.black))
            } else {
                DrawableCompat.setTint(ib_back.drawable, ContextCompat.getColor(this, android.R.color.white))
                DrawableCompat.setTint(ib_edit_feed.drawable, ContextCompat.getColor(this, android.R.color.white))
                DrawableCompat.setTint(ib_add_user.drawable, ContextCompat.getColor(this, android.R.color.white))
            }
        }
    }

    private fun initTagItems() {

        linear_tags_container.removeAllViews()

        for (tagItem in resultItem.tags!!) {

            val view = View.inflate(this, R.layout.layout_details_tag_item, null)

            val tagItemTextView = view.findViewById<AppCompatTextView>(R.id.tv_tag_item)

            tagItemTextView.text = tagItem.brand

            if (resultItem.likedByMe) {
                iv_heart.setImageResource(R.drawable.ic_heart_red_96)
            } else {
                iv_heart.setImageResource(R.drawable.ic_heart_grey_96)
            }

            when (tagItem.tag.toLowerCase()) {
                "top" -> {
                    tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_top, 0, 0, 0)
                }

                "pants" -> {
                    tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_pants, 0, 0, 0)
                }

                "dress" -> {
                    tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_dress, 0, 0, 0)
                }

                "accessory" -> {
                    tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_accessory, 0, 0, 0)
                }

                "shoes" -> {
                    tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_shoes, 0, 0, 0)
                }

                "bag" -> {
                    tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_bag, 0, 0, 0)
                }

                "skirt" -> {
                    tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_skirt, 0, 0, 0)
                }

                "watch" -> {
                    tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_watch, 0, 0, 0)
                }

                "eyewear" -> {
                    tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_eyewear, 0, 0, 0)
                }

                "other" -> {
                    tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_others, 0, 0, 0)
                }
            }

            tagItemTextView.setOnClickListener(tagItemOnClickListener)
            linear_tags_container.addView(view)
        }
    }

    private fun initPicsViewPagerAdapter() {
        images_viewpager.adapter = FeedPicsViewPagerAdapter(resultItem.pictures as ArrayList<String>)
        pageIndicatorView.setViewPager(images_viewpager)

        /*images_viewpager.setOnViewPagerClickListener {
            openZoom(it,it.currentItem)
        }*/

        /*check for 1st image pic url change to update in Firebase for notifications*/
        if (feedId !=-1){
            val fbNotification = intent?.getSerializableExtra(Constants.FB_NOTIFICATION) as FbNotification?
            if (fbNotification != null) {
                if (!fbNotification.actionFeedPic?.isNullOrEmpty() && fbNotification.actionFeedPic != resultItem.pictures!!.get(0)){
                    Utils.updateFeedPicForNotification(fbNotification,resultItem.pictures!!.get(0))
                }
            }
        }
    }

    private fun openZoom(viewPager: ViewPager, position: Int) {
        val intent = Intent(this@FeedDetailsActivity, ImageZoomActivity::class.java)
        intent.putStringArrayListExtra(Constants.SELECTED_PICS, resultItem.pictures as ArrayList<String>?)
        intent.putExtra(Constants.POSITION, position)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this@FeedDetailsActivity,
                    viewPager,
                    getString(R.string.zoomActivityTransitionName))
            startActivity(intent, options.toBundle())
        } else {
            startActivity(intent)
        }
    }

    private fun initGridAdapter() {
        val numberOfColumns = 2
        feedEarlierPostsAdapter = FeedEarlierPostsAdapter(this, ArrayList<OldFeedsItem>())
        rv_earlier_posts_list.layoutManager = GridLayoutManager(this, numberOfColumns)
        rv_earlier_posts_list.isNestedScrollingEnabled = false
        rv_earlier_posts_list.adapter = feedEarlierPostsAdapter
    }

    private fun getFeedDetails() {
        loadingFeed = true
        val feedListPresenter = FeedListPresenterImpl(this)
        val earlierPostsRequest = EarlierPostsRequest(StyleMoreApp.user_id, feedId.toString())
        feedListPresenter.getUserEarlierPostsResponse(EarlierFeedsUseCase(), earlierPostsRequest)
    }

    inner class FeedPicsViewPagerAdapter(private val picsUrls: ArrayList<String>) : PagerAdapter() {
        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view == `object`
        }


        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            // Get the view from pager page layout
            val view = LayoutInflater.from(container.context).inflate(R.layout.viewpager_image_layout, container, false)
            val backgroundIv: ImageView = view.findViewById(R.id.iv_background)
            Picasso.get().load(StyleMoreApp.IMAGE_BASE_URL + picsUrls.get(position))
                    .placeholder(R.drawable.placeholder_large)
                    .resize(screenWidth / 2, screenWidth / 2)
                    .onlyScaleDown()
                    .centerInside()
                    .into(backgroundIv)
            container.addView(view)
            return view
        }

        override fun getCount(): Int {
            return picsUrls.size
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
        }

    }

    override fun onClick(p0: View?) {

        if (!::resultItem.isInitialized) {
            Log.e("onclick", "!resultItem.isInitialized")
            return
        }

        when (p0?.id) {
            R.id.linear_likes -> {
                if (!Utils.isOnline()){
                    showError(getString(R.string.no_internet_connection))
                    return
                }
                val feedLikesDialogFragment = FeedLikesDialogFragment()
                val bundle = Bundle()
                bundle.putString(Constants.FEED_ID, resultItem.feedId.toString())
                bundle.putString(Constants.FEED_PIC, resultItem.pictures?.get(0))
                bundle.putInt(Constants.USER_ID, resultItem.userId)
                feedLikesDialogFragment.arguments = bundle
                feedLikesDialogFragment.show(supportFragmentManager, FeedLikesDialogFragment::class.java.simpleName)
            }

            R.id.linear_comments -> {
                if (!Utils.isOnline()){
                    showError(getString(R.string.no_internet_connection))
                    return
                }
                val feedCommentsDialogFragment = FeedCommentsDialogFragment()
                val bundle = Bundle()
                bundle.putString(Constants.FEED_ID, resultItem.feedId.toString())
                bundle.putString(Constants.FEED_PIC, resultItem.pictures?.get(0))
                bundle.putInt(Constants.USER_ID, resultItem.userId)
                feedCommentsDialogFragment.arguments = bundle
                feedCommentsDialogFragment.show(supportFragmentManager, FeedCommentsDialogFragment::class.java.simpleName)
                feedCommentsDialogFragment.setUpdateCommentCountListener(this@FeedDetailsActivity)
            }

            R.id.ib_back -> {
                onBackPressed()
            }

            R.id.tv_user_name, R.id.civ_profile_icon -> {
                startActivity(Intent(this, UserProfileActivity::class.java)
                        .putExtra(Constants.USER_ID, resultItem.userId)
                        .putExtra(Constants.USER_FULLNAME, resultItem.name)
                        .putExtra(Constants.USER_FOLLOWING, resultItem.following)
                        .putExtra(Constants.USER_PROF_PIC, resultItem.profilePic))
            }

            R.id.ib_edit_feed -> {
                startActivity(Intent(this, UploadFeedActivity::class.java)
                        .putExtra(Constants.RESULT_ITEM, resultItem))
            }

            R.id.ib_delete_feed -> {
                val twoBtnDialogFragment = TwoBtnDialogFragment()
                val bundle = Bundle()
                bundle.putString(Constants.DLG_TITLE, "Please confirm to delete the feed")
                bundle.putString(Constants.DLG_POS_BTN, "Delete")
                bundle.putString(Constants.DLG_NEG_BTN, "Cancel")
                twoBtnDialogFragment.arguments = bundle

                twoBtnDialogFragment.show(supportFragmentManager, TwoBtnDialogFragment::class.java.simpleName)

                twoBtnDialogFragment.setDialogBtnsListener(object : DialogBtnsListener {
                    override fun onPositiveBtnClick(view: View) {
                        deleteFeed()
                        twoBtnDialogFragment.dismiss()
                    }

                    override fun onNegativeBtnClick(view: View) {
                        twoBtnDialogFragment.dismiss()
                    }

                })
            }

            R.id.iv_heart -> {
//                isFavClicked = true
                val feedListPresenter = FeedListPresenterImpl()
                feedListPresenter.setLikesResponseListener(this@FeedDetailsActivity)

                val userDetailRequest = UserDetailRequest(StyleMoreApp.user_id, resultItem.feedId.toString())
                feedListPresenter.getAddLikeResponse(AddLikeUseCase(), userDetailRequest)
            }

            R.id.ib_add_user -> {
                val twoBtnDialogFragment = TwoBtnDialogFragment()
                val bundle = Bundle()
                if (resultItem!!.following) {
                    bundle.putString(Constants.DLG_TITLE, "Unfollow user ${resultItem.name}?")
                    bundle.putString(Constants.DLG_POS_BTN, "Unfollow")
                } else {
                    bundle.putString(Constants.DLG_TITLE, "Follow user ${resultItem.name}?")
                    bundle.putString(Constants.DLG_POS_BTN, "Follow")
                }

                bundle.putString(Constants.DLG_NEG_BTN, "Cancel")
                twoBtnDialogFragment.arguments = bundle

                twoBtnDialogFragment.show(supportFragmentManager, TwoBtnDialogFragment::class.java.simpleName)

                twoBtnDialogFragment.setDialogBtnsListener(object : DialogBtnsListener {
                    override fun onPositiveBtnClick(view: View) {
                        followUnfollowUser()
                        twoBtnDialogFragment.dismiss()
                    }

                    override fun onNegativeBtnClick(view: View) {
                        twoBtnDialogFragment.dismiss()
                    }

                })
            }
        }
    }

    private fun deleteFeed() {
        val feedListPresenter = FeedListPresenterImpl(this)
        val userDetailRequest = UserDetailRequest(StyleMoreApp.user_id, resultItem.feedId.toString())
        feedListPresenter.getDeleteFeedResponse(DeleteFeedUseCase(), userDetailRequest)
    }

    private fun followUnfollowUser() {
        val feedListPresenter = FeedListPresenterImpl(this)
        val userDetailRequest = UserDetailRequest(resultItem.userId.toString(), resultItem.feedId.toString())
        feedListPresenter.setFollowUserUiUpdateListener(this)
        feedListPresenter.getFollowResponse(FollowUnfollowUseCase(), userDetailRequest)
    }

    override fun showLoading() {
        isLoading = true
        if (this.isFinishing)
            return
        if (!loadingFeed) {
            main_loader_container_layout.visibility = View.VISIBLE
            Utils.showLoader(full_screen_loader, null, this)
        } else {
            rv_earlier_posts_list.visibility = View.GONE
            loader.visibility = View.VISIBLE
            val rotation = AnimationUtils.loadAnimation(this, R.anim.rotate)
            rotation.fillAfter = true
            loader.startAnimation(rotation)
        }
    }

    override fun hideLoading() {
        isLoading = false
        if (this.isFinishing)
            return
        if (!loadingFeed) {
            main_loader_container_layout.visibility = View.GONE
            Utils.closeLoader(full_screen_loader, null, this)
        } else {
            rv_earlier_posts_list.visibility = View.VISIBLE
            loader.visibility = View.GONE
            loader.clearAnimation()
        }
    }

    override fun provideUserEarlierPostsResponse(earlierPostResponse: EarlierPostResponse) {
        /*if (feedId != -1) {
            resultItem = earlierPostResponse.result!![0]
            initPicsViewPagerAdapter()
            initViews()

            userIdDatabaseReference = notificationDbReference.child("${resultItem.userId}")
            toUserId = resultItem.userId.toString()
        }*/

        resultItem = earlierPostResponse.result!![0]
        initPicsViewPagerAdapter()
        initViews()

        userIdDatabaseReference = notificationDbReference.child("${resultItem.userId}")
        toUserId = resultItem.userId.toString()
        resultItem.oldFeeds = earlierPostResponse.result?.get(0)?.oldFeeds
        if (resultItem.oldFeeds != null && resultItem.oldFeeds!!.isNotEmpty()) {
            feedEarlierPostsAdapter.updateRecords(resultItem.oldFeeds)
        } else {
            tv_no_data.visibility = View.VISIBLE
        }
        loadingFeed = false
    }

    override fun showError(error: String) {
        loadingFeed = false
//        toast(error)
        /*if (isFavClicked) {
            updateFav()
            isFavClicked = false
        }*/

        if (error.equals(Constants.NO_FEED_FOUND, true)) {
            toast("Feed no longer exists / removed by user")
            val fbNotification = intent?.getSerializableExtra(Constants.FB_NOTIFICATION) as FbNotification?
            if (fbNotification != null) {
                userIdDatabaseReference = notificationDbReference.child(StyleMoreApp.user_id)
                Utils.removeNotificationForDeletedPost(userIdDatabaseReference, fbNotification)
            }
            Handler().postDelayed({
                finish()
//                Utils.goToHomeScreen(this)
            }, 500)
        } else
            toast(error)
    }

    override fun provideFeedLikes(feedLikesResponse: FeedLikesResponse) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun provideFeedComments(feedCommentsResponse: FeedCommentsResponse) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun provideDeleteFeedResponse(basicResponse: BasicResponse) {
//        Utils.goToHomeScreen(this)
        if (basicResponse.result.equals("success", true)) {
            if (resultItem.userId != -1) {
                Utils.removeAllNotificationsOfDeletedFeedOfThisUser(resultItem.userId.toString(), resultItem.feedId.toString())
            }
            finish()
        }
    }

    override fun provideFollowUserResult(basicResponse: BasicResponse) {
//        toast(basicResponse.result)
        if (basicResponse.result.equals("success", true)) {
            resultItem.following = !resultItem.following

            /*val userIdDbReference = FirebaseDatabase.getInstance().reference
                    .child("Notifications")
                    .child("${resultItem.userId}")*/
            val timestamp = System.currentTimeMillis()
            if (resultItem.following) {
                ib_add_user.setImageResource(R.drawable.ic_following_red)
                /*push to fb db*/
                if (!StyleMoreApp.user_id.equals(resultItem.userId.toString())) {
                    val fbNotification = FbNotification(timestamp, "followed", Utils.getLoggedInActionUser(this),
                            -1, "NIL", timestamp, false, false)
                    fbNotification.actionToUserId = toUserId
                    Utils.pushActionToFirebaseDb(userIdDatabaseReference, fbNotification)
                }
            } else {
                ib_add_user.setImageResource(R.drawable.ic_follow_black)
                if (!StyleMoreApp.user_id.equals(resultItem.userId.toString())) {
                    val fbNotification = FbNotification(timestamp, "unfollowed", Utils.getLoggedInActionUser(this),
                            -1, "NIL", timestamp, false, false)
                    fbNotification.actionToUserId = toUserId
                    Utils.pushActionToFirebaseDb(userIdDatabaseReference, fbNotification)
                }
            }
        }
    }

    fun updateLikeCommentsCount(isLikeUpdate: Boolean, count: Int) {
        if (isLikeUpdate) {
            tv_likes_count.text = "${count} Likes"
        } else {
            tv_comments_count.text = "${count} Comments"
        }
    }

    override fun updateLikeResponse(likeFeedResponse: LikeFeedResponse) {
        val userIdDbReference = FirebaseDatabase.getInstance().reference
                .child("Notifications")
                .child("${resultItem.userId}")
        val timestamp = System.currentTimeMillis()
        if (likeFeedResponse.result.equals("liked",true) && !resultItem?.likedByMe) {
            resultItem?.likedByMe = true
            updateFav()
            /*updating firebase db*/
            if (!StyleMoreApp.user_id.equals(resultItem.userId.toString())) {
                val fbNotification = FbNotification(timestamp, "liked", Utils.getLoggedInActionUser(this),
                        resultItem.feedId.toLong(), resultItem.pictures?.get(0), timestamp, false, false)
                fbNotification.actionToUserId = toUserId
                Utils.pushActionToFirebaseDb(userIdDbReference, fbNotification)
            }
        } else if (likeFeedResponse.result.equals("disliked",true) && resultItem?.likedByMe) {
            resultItem?.likedByMe = false
            updateFav()
            if (!StyleMoreApp.user_id.equals(resultItem.userId.toString())) {
                val fbNotification = FbNotification(timestamp, "disliked", Utils.getLoggedInActionUser(this),
                        resultItem.feedId.toLong(), resultItem.pictures?.get(0), timestamp, false, false)
                fbNotification.actionToUserId = toUserId
                Utils.pushActionToFirebaseDb(userIdDbReference, fbNotification)
            }
        }
    }

    private fun updateFav() {
        if (resultItem.likedByMe) {
            iv_heart.setImageResource(R.drawable.ic_heart_red_96)
            resultItem.lCount = (resultItem.lCount + 1)
            tv_likes_count.text = "${resultItem.lCount} Likes"
        } else {
            iv_heart.setImageResource(R.drawable.ic_heart_grey_96)
            if (resultItem.lCount > 0) {
                resultItem.lCount = (resultItem.lCount - 1)
            }
            tv_likes_count.text = "${resultItem.lCount} Likes"
        }
    }

    override fun errorOccured(errorMsg: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val tagItemOnClickListener = View.OnClickListener {
        val brandTagName = (it as AppCompatTextView).text
        if (brandTagName.isNullOrEmpty())
            return@OnClickListener
        startActivity(Intent(this, SearchActivity::class.java)
                .putExtra(Constants.SEARCH_TYPE, SearchActivity.SEARCH_TAG)
                .putExtra(Constants.SEARCH_KEYWORD, brandTagName)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
    }

    override fun onHashTagClicked(hashTag: String?) {
        Log.v("onHashTagClicked", "$hashTag")
        startActivity(Intent(this, SearchActivity::class.java)
                .putExtra(Constants.SEARCH_TYPE, SearchActivity.SEARCH_DESCR)
                .putExtra(Constants.SEARCH_KEYWORD, "#$hashTag")
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
    }

    override fun incrementCommentCount(updateItemPosition: Int) {
        if (resultItem != null) {
            resultItem.cCount += 1
            tv_comments_count.text = "${resultItem.cCount} Comments"
        }
    }

    override fun decrementCommentCount(updateItemPosition: Int) {
        if (resultItem != null) {
            if (resultItem.cCount > 0) {
                resultItem.cCount -= 1
                tv_comments_count.text = "${resultItem.cCount} Comments"
            }
        }
    }

    /*disable back function while showing loader*/
    override fun onBackPressed() {
        if (!isLoading)
            super.onBackPressed()
    }

}
