package com.cumulations.stylemore.feeds.presentation

import com.cumulations.stylemore.base.LoadingView
import com.cumulations.stylemore.feeds.data.model.EarlierPostResponse
import com.cumulations.stylemore.feeds.data.model.FeedCommentsResponse
import com.cumulations.stylemore.feeds.data.model.FeedLikesResponse
import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse

/**
 * Created by Amit Tumkur on 11-06-2018.
 */
interface FeedDetailsUiUpdateView : LoadingView{
    fun provideUserEarlierPostsResponse(earlierPostResponse: EarlierPostResponse)
    fun provideFeedLikes(feedLikesResponse: FeedLikesResponse)
    fun provideFeedComments(feedCommentsResponse: FeedCommentsResponse)
    fun provideDeleteFeedResponse(basicResponse: BasicResponse)
    fun showError(error: String)
}