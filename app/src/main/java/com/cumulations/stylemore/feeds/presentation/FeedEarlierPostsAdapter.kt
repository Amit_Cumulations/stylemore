package com.cumulations.stylemore.feeds.presentation

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.cumulations.stylemore.R
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.data.model.OldFeedsItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_feed_earlier_post.view.*

class FeedEarlierPostsAdapter(val context: Context,
                              var oldFeedsList: List<OldFeedsItem>?) : RecyclerView.Adapter<FeedEarlierPostsAdapter.FeedEarlierPostsItemViewHolder>() {

    internal var cardLayoutParams: FrameLayout.LayoutParams? = null
    internal var size = (Utils.getScreenWidthInDp(context as AppCompatActivity) / 2)

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): FeedEarlierPostsItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_feed_earlier_post, parent, false)
        return FeedEarlierPostsItemViewHolder(view)
    }


    override fun onBindViewHolder(feedEarlierPostsItemViewHolder: FeedEarlierPostsItemViewHolder, position: Int) {
        val earlierPostsRecordsItem = oldFeedsList?.get(position)
        feedEarlierPostsItemViewHolder.bindFeedRecord(earlierPostsRecordsItem)
    }

    override fun getItemCount(): Int {
        return oldFeedsList?.size ?: 4
    }

    inner class FeedEarlierPostsItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindFeedRecord(oldFeedsItem: OldFeedsItem?) {
            cardLayoutParams = FrameLayout.LayoutParams(size, size)

            if (oldFeedsItem?.pictures != null && oldFeedsItem.pictures.isNotEmpty()) {
                Picasso.get().load(StyleMoreApp.IMAGE_BASE_URL + oldFeedsItem.pictures.get(0))
//                    .memoryPolicy(MemoryPolicy.NO_CACHE)
//                    .networkPolicy(NetworkPolicy.NO_CACHE)
                        .placeholder(R.drawable.placeholder_large)
                        .resize((size / 1.5f).toInt(), (size / 1.5f).toInt())
//                        .fit()
                        .centerCrop()
                        .into(itemView.iv_post_image)
            }

            itemView.cv_parent_layout.layoutParams = cardLayoutParams

            itemView.tv_post_likes.text = oldFeedsItem?.lCount.toString()
            itemView.tv_post_comments.text = oldFeedsItem?.cCount.toString()

            itemView.cv_parent_layout.setOnClickListener {
                val intent = Intent(context, FeedDetailsActivity::class.java)
                intent.putExtra(Constants.FEED_ID, oldFeedsItem?.feedId)
                context.startActivity(intent)
//                (context as AppCompatActivity).finish()
            }

        }
    }

    fun updateRecords(oldFeedsList: List<OldFeedsItem>?) {
        this.oldFeedsList = oldFeedsList
        notifyDataSetChanged()
    }

}



