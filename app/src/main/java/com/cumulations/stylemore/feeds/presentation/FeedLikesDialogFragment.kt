package com.cumulations.stylemore.feeds.presentation

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.ViewGroup
import com.cumulations.stylemore.R
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.firebase.FbNotification
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.data.model.*
import com.cumulations.stylemore.feeds.domain.FeedLikesUseCase
import com.cumulations.stylemore.feeds.domain.FollowUnfollowUseCase
import com.cumulations.stylemore.login.presentation.SplashScreenActivity
import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.dialog_feed_likes.*
import kotlinx.android.synthetic.main.layout_full_screen_loader.*
import org.jetbrains.anko.toast


/**
 * Created by Amit Tumkur on 05-06-2018.
 */
class FeedLikesDialogFragment: DialogFragment(),FeedDetailsUiUpdateView,FollowUserUiUpdateView {

    private lateinit var dialogFeedLikesAdapter: DialogFeedLikesAdapter
    private var feedId: String? = null
    private var following:Boolean = false

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        val customDialog = Dialog(activity, R.style.CustomDialogTheme)
        customDialog.setContentView(R.layout.dialog_feed_likes)
        customDialog.setCanceledOnTouchOutside(true)
        customDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        dialogFeedLikesAdapter = DialogFeedLikesAdapter(this.activity!!,ArrayList(),this)

        val mLayoutManager = LinearLayoutManager(this.activity!!)
        customDialog.rv_feed_likes.setLayoutManager(mLayoutManager)
        customDialog.rv_feed_likes.setItemAnimator(DefaultItemAnimator())
        customDialog.rv_feed_likes.setAdapter(dialogFeedLikesAdapter)

        return customDialog
    }

    override fun onStart() {
        super.onStart()
        getFeedLikes()
    }

    private fun getFeedLikes() {
        feedId = arguments?.getString(Constants.FEED_ID)
        val feedListPresenter = FeedListPresenterImpl(this)
        val feedLikesRequest = FeedLikesRequest(feedId)
        feedListPresenter.getFeedLikesResponse(FeedLikesUseCase(),feedLikesRequest)
    }

    fun followUnfollowUser(feedLikesResultItem: FeedLikesResultItem?){

        val twoBtnDialogFragment = TwoBtnDialogFragment()
        val bundle = Bundle()
        if (feedLikesResultItem!!.following) {
            following = false
            bundle.putString(Constants.DLG_TITLE, "Unfollow user ${feedLikesResultItem.name}?")
            bundle.putString(Constants.DLG_POS_BTN, "Unfollow")
        } else{
            following = true
            bundle.putString(Constants.DLG_TITLE, "Follow user ${feedLikesResultItem.name}?")
            bundle.putString(Constants.DLG_POS_BTN, "Follow")
        }
        bundle.putString(Constants.DLG_NEG_BTN,"Cancel")
        twoBtnDialogFragment.arguments = bundle

        twoBtnDialogFragment.show(activity?.supportFragmentManager,TwoBtnDialogFragment::class.java.simpleName)

        twoBtnDialogFragment.setDialogBtnsListener(object : DialogBtnsListener{
            override fun onPositiveBtnClick(view: View) {
                val feedListPresenter =FeedListPresenterImpl(this@FeedLikesDialogFragment)
                val userDetailRequest = UserDetailRequest(feedLikesResultItem.userId,"")
                feedListPresenter.setFollowUserUiUpdateListener(this@FeedLikesDialogFragment)
                feedListPresenter.getFollowResponse(FollowUnfollowUseCase(),userDetailRequest)
                twoBtnDialogFragment.dismiss()
            }

            override fun onNegativeBtnClick(view: View) {
                twoBtnDialogFragment.dismiss()
            }

        })
    }

    override fun showLoading() {
        if (dialog == null)
            return
        dialog.full_screen_loader_layout.visibility = View.VISIBLE
        Utils.showLoader(dialog.loader,null, this.activity!!,false)
    }

    override fun hideLoading() {
        if (dialog == null)
            return
        dialog.full_screen_loader_layout.visibility = View.GONE
        Utils.closeLoader(dialog.loader,null,this.activity!!)
    }

    override fun provideUserEarlierPostsResponse(earlierPostResponse: EarlierPostResponse) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun provideFeedLikes(feedLikesResponse: FeedLikesResponse) {
        if (dialog == null)
            return
        if (feedLikesResponse.result!!.isNotEmpty()) {
            dialog.tv_no_data.visibility = View.GONE
            dialog.rv_feed_likes.visibility = View.VISIBLE
            dialogFeedLikesAdapter.updateList(feedLikesResponse.result)
        }
    }

    override fun provideFeedComments(feedCommentsResponse: FeedCommentsResponse) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showError(error: String) {
//        activity?.toast(error)
        if (error == Constants.INVALID_AUTH_KEY){
            SharedPreferenceHelper.clearCustomPrefs(activity!!, Constants.USER_PREF)
            activity!!.startActivity(Intent(activity, SplashScreenActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
            activity!!.finish()
        }

        if (dialog == null)
            return
        if (error == Constants.NO_DATA_FOUND){
            dialog.tv_no_data.text = "No likes. Be the first one to like"
            dialog.tv_no_data.visibility = View.VISIBLE
            dialog.rv_feed_likes.visibility = View.GONE
        }
    }

    override fun provideDeleteFeedResponse(basicResponse: BasicResponse) {
    }

    override fun provideFollowUserResult(basicResponse: BasicResponse) {
//        activity?.toast(basicResponse.result)
        val feedUserId = arguments?.getInt(Constants.USER_ID)
        val userIdDbReference = FirebaseDatabase.getInstance().reference
                .child("Notifications")
                .child("${feedUserId}")
        val timestamp = System.currentTimeMillis()
        if (basicResponse.result.equals("success",true) && following){
            /*push to fb db*/
            if (!StyleMoreApp.user_id.equals(feedUserId.toString())) {
                val fbNotification = FbNotification(timestamp,"followed",Utils.getLoggedInActionUser(activity!!),
                        -1,"NIL",timestamp,false, false)
                fbNotification.actionToUserId = feedUserId.toString()
                Utils.pushActionToFirebaseDb(userIdDbReference, fbNotification)
            }
        } else if (basicResponse.result.equals("success",true) && !following){
            /*push to fb db*/
            if (!StyleMoreApp.user_id.equals(feedUserId.toString())) {
                val fbNotification = FbNotification(timestamp,"unfollowed",Utils.getLoggedInActionUser(activity!!),
                        -1,"NIL",timestamp,false, false)
                Utils.pushActionToFirebaseDb(userIdDbReference, fbNotification)
            }
        }

        getFeedLikes()
    }

    override fun errorOccured(errorMsg: String) {
        activity?.toast(errorMsg)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (dialog!=null && activity!=null){
            Utils.closeLoader(dialog.loader,null,activity!!)
        }
    }
}