package com.cumulations.stylemore.feeds.presentation

import android.animation.Animator
import android.animation.ObjectAnimator
import android.app.NotificationManager
import android.content.*
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.support.annotation.RequiresApi
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.ViewAnimationUtils
import android.view.animation.AccelerateInterpolator
import android.widget.Toast
import com.cumulations.stylemore.R
import com.cumulations.stylemore.base.firebase.FbNotification
import com.cumulations.stylemore.base.firebase.FirebaseSyncListener
import com.cumulations.stylemore.base.firebase.FirebaseSyncService
import com.cumulations.stylemore.base.utils.BottomNavigationViewHelper
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper.get
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper.set
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.notifications.NotificationsActivity
import com.cumulations.stylemore.search.presentation.SearchActivity
import com.cumulations.stylemore.uploadFeed.presentation.UploadFeedActivity
import com.cumulations.stylemore.userProfile.presentation.UserProfileActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.firebase.iid.FirebaseInstanceId
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_feed_list.*
import kotlinx.android.synthetic.main.layout_create_fashion.*
import kotlinx.android.synthetic.main.layout_notification_counter.*
import java.util.*

class FeedListActivity : AppCompatActivity(), View.OnClickListener, FirebaseSyncListener {

    private lateinit var feedListFragment: FeedListFragment
    private var tabSelected: Int = 0

    private var mBoundSyncService: FirebaseSyncService? = null
    private var isServiceBound = false
    private var fbNotificationList: LinkedList<FbNotification?> = LinkedList()

    private val TIME_INTERVAL = 2000 // # milliseconds, desired time passed between two back presses.
    private var mBackPressedInterval: Long = 0
    private var backToast: Toast? = null
    var tabTotalItems = -1
    var clickedFeedPos = -1

    /*private val refreshNewTabReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action?.equals(BROADCAST_REFRESH_TAB)!!){
                refreshNewTab()
            }
        }
    }

    companion object {
        const val BROADCAST_REFRESH_TAB: String = "3456"
    }*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed_list)

        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        fl_parent_create_feed.setOnClickListener {
            Log.d("TAG", "empty click")
        }

        backToast = Toast.makeText(this, R.string.close_app_alert, Toast.LENGTH_SHORT);

        /*Making bottom naivgation always show text and icon as fixed*/
        BottomNavigationViewHelper.removeShiftMode(bottom_navigation)

        val bundle: Bundle? = Bundle()
        bottom_navigation.setOnNavigationItemSelectedListener {

            when (it.itemId) {
                R.id.action_hot -> {
                    feedListFragment = FeedListFragment()
                    bundle?.putInt(Constants.FEED_TYPE, FeedListFragment.FEED_TYPE_HOT)
                    feedListFragment.arguments = bundle
                    tabSelected = FeedListFragment.FEED_TYPE_HOT
                }

                R.id.action_new -> {
                    feedListFragment = FeedListFragment()
                    bundle?.putInt(Constants.FEED_TYPE, FeedListFragment.FEED_TYPE_NEW)
                    feedListFragment.arguments = bundle
                    tabSelected = FeedListFragment.FEED_TYPE_NEW
                }

                R.id.action_following -> {
                    feedListFragment = FeedListFragment()
                    bundle?.putInt(Constants.FEED_TYPE, FeedListFragment.FEED_TYPE_FOLLOWING)
                    feedListFragment.arguments = bundle
                    tabSelected = FeedListFragment.FEED_TYPE_FOLLOWING
                }

                R.id.action_style_tip -> {
                    feedListFragment = FeedListFragment()
                    bundle?.putInt(Constants.FEED_TYPE, FeedListFragment.FEED_TYPE_STIP)
                    feedListFragment.arguments = bundle
                    tabSelected = FeedListFragment.FEED_TYPE_STIP
                }
            }

            clickedFeedPos = -1
            Log.d("bottom nav clicked","clicked pos reset")
            loadFragment(feedListFragment)
        }

        civ_profile_icon.setOnClickListener {
            startActivity(Intent(this, UserProfileActivity::class.java))
        }

        val userPref = SharedPreferenceHelper.customPrefs(this, Constants.USER_PREF)
        val prof_pic_url = userPref[Constants.USER_PROF_PIC, ""]

        Picasso.get().load(prof_pic_url)
//                .resize(28,28)
                .placeholder(R.drawable.user_placeholder_square)
                .into(civ_profile_icon)

        iv_camera.setOnClickListener(this)
        ll_create_fashion.setOnClickListener(this)
        ll_create_beauty.setOnClickListener(this)
        ll_create_style_tip.setOnClickListener(this)
        iv_close_create_fashion.setOnClickListener(this)
        fl_notifications.setOnClickListener(this)
        iv_search.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        checkPlayServices()
        sendFCMTokenFirstTime()
        bindService()
//        LocalBroadcastManager.getInstance(this).registerReceiver(refreshNewTabReceiver, IntentFilter(BROADCAST_REFRESH_TAB))
    }

    private fun refreshNewTab(){
        val bundle = Bundle()
        bottom_navigation.menu.getItem(1).isChecked = true
        clickedFeedPos = -1
        tabTotalItems = -1
        feedListFragment = FeedListFragment()
        bundle.putInt(Constants.FEED_TYPE, FeedListFragment.FEED_TYPE_NEW)
        feedListFragment.arguments = bundle
        loadFragment(feedListFragment)
    }

    override fun onResume() {
        super.onResume()
        Log.d("onResume", "tabType frm intent = $tabSelected")
        val bundle = Bundle()
        if (intent?.getIntExtra(Constants.TAB_TYPE,0) == FeedListFragment.FEED_TYPE_NEW){
            tabSelected = FeedListFragment.FEED_TYPE_NEW
            intent?.removeExtra(Constants.TAB_TYPE)

            refreshNewTab()
        } else {
            if (tabSelected == 0) {
                feedListFragment = FeedListFragment()
                bundle?.putInt(Constants.FEED_TYPE, FeedListFragment.FEED_TYPE_HOT)
                feedListFragment.arguments = bundle
                loadFragment(feedListFragment)
            } else {
                when (tabSelected) {
                    FeedListFragment.FEED_TYPE_HOT -> {
                        feedListFragment = FeedListFragment()
                        bundle?.putInt(Constants.FEED_TYPE, FeedListFragment.FEED_TYPE_HOT)
                        feedListFragment.arguments = bundle
                    }

                    FeedListFragment.FEED_TYPE_NEW -> {
                        feedListFragment = FeedListFragment()
                        bundle?.putInt(Constants.FEED_TYPE, FeedListFragment.FEED_TYPE_NEW)
                        feedListFragment.arguments = bundle
                    }

                    FeedListFragment.FEED_TYPE_FOLLOWING -> {
                        feedListFragment = FeedListFragment()
                        bundle?.putInt(Constants.FEED_TYPE, FeedListFragment.FEED_TYPE_FOLLOWING)
                        feedListFragment.arguments = bundle
                    }

                    FeedListFragment.FEED_TYPE_STIP -> {
                        feedListFragment = FeedListFragment()
                        bundle?.putInt(Constants.FEED_TYPE, FeedListFragment.FEED_TYPE_STIP)
                        feedListFragment.arguments = bundle
                    }
                }
                loadFragment(feedListFragment)
            }
        }

        // Clear all notification
        val nMgr = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nMgr.cancelAll()

//        println(bottom_navigation.selectedItemId)
        if (mBoundSyncService != null) {
            updateNotificationIcon(mBoundSyncService?.totalNotificationCount!!)
        }
    }

    private fun updateNotificationIcon(count: Int) {
        if (count > 0) {
            tv_notification_badge.visibility = View.VISIBLE
            tv_notification_badge.text = "$count"
            val flip = ObjectAnimator.ofFloat(tv_notification_badge, "rotationY", 0f, 360f)
            flip.duration = 1500
            flip.start()
        } else
            tv_notification_badge.visibility = View.GONE
    }

    private fun loadFragment(feedListFragment: FeedListFragment?): Boolean {
        //switching fragment
        if (feedListFragment != null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fl_container, feedListFragment)
                    .commit()
            return true
        }
        return false
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.iv_camera -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    revealCreateFashion(false)
                    return
                }

                if (fl_parent_create_feed.visibility == View.VISIBLE) {
                    fl_parent_create_feed.visibility = View.GONE
                } else {
                    fl_parent_create_feed.visibility = View.VISIBLE
                }
            }

            R.id.ll_create_fashion -> {
                startActivity(Intent(this, UploadFeedActivity::class.java)
                        .putExtra(Constants.MP_FEED_TYPE, "fashion"))
                fl_parent_create_feed.visibility = View.GONE
            }

            R.id.ll_create_beauty -> {
                startActivity(Intent(this, UploadFeedActivity::class.java)
                        .putExtra(Constants.MP_FEED_TYPE, "beauty"))
                fl_parent_create_feed.visibility = View.GONE
            }

            R.id.ll_create_style_tip -> {
                startActivity(Intent(this, UploadFeedActivity::class.java)
                        .putExtra(Constants.MP_FEED_TYPE, "style_tip"))
                fl_parent_create_feed.visibility = View.GONE
            }

            R.id.iv_close_create_fashion -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    revealCreateFashion(true)
                    return
                }
                fl_parent_create_feed.visibility = View.GONE
            }

            R.id.fl_notifications -> {
                startActivity(Intent(this, NotificationsActivity::class.java)
//                        .putExtra(Constants.NOTIFICATIONS_LIST, fbNotificationList)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
                if (mBoundSyncService != null) {
                    mBoundSyncService?.totalNotificationCount = 0
//                    mBoundSyncService?.fbNotificationList?.clear()
                }
            }

            R.id.iv_search -> {
                startActivity(Intent(this, SearchActivity::class.java)
                        .putExtra(Constants.SEARCH_TYPE, SearchActivity.SEARCH_DESCR)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
            }
        }
    }

    override fun onStop() {
        super.onStop()
        unbindService()
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(refreshNewTabReceiver)
    }

    override fun onBackPressed() {
        if (fl_parent_create_feed.visibility == View.VISIBLE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                revealCreateFashion(true)
                return
            }
            fl_parent_create_feed.visibility = View.GONE
        } else {
            if (mBackPressedInterval + TIME_INTERVAL > System.currentTimeMillis()) {
                backToast?.cancel()
                super.onBackPressed()
                return
            } else {
                backToast?.show()
            }
            mBackPressedInterval = System.currentTimeMillis()
        }

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun revealCreateFashion(isOpen: Boolean) {

        if (!isOpen) {

            /*reveal/grow from camera icon position*/
            val cameraCentreX = (iv_camera.left + iv_camera.right) / 2
            val x = cameraCentreX
            val y = iv_camera.bottom + 16

            val startRadius = 0
            val endRadius = Math.hypot(fl_parent_create_feed.width.toDouble(), fl_parent_create_feed.height.toDouble()).toInt()

            /*fab.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(resources, android.R.color.white, null)))
            fab.setImageResource(R.drawable.ic_close_grey)*/

            val anim = ViewAnimationUtils.createCircularReveal(fl_parent_create_feed, x, y, startRadius.toFloat(), endRadius.toFloat())
            anim.duration = 500
            anim.interpolator = AccelerateInterpolator()
            fl_parent_create_feed.visibility = View.VISIBLE
            anim.start()
//            isOpen = true

        } else {

            val cameraCentre = (iv_close_create_fashion.left + iv_close_create_fashion.right) / 2
            val x = cameraCentre
            val y = iv_close_create_fashion.bottom + 16

            val startRadius = Math.max(fl_parent_create_feed.width, fl_parent_create_feed.height)
            val endRadius = 0

            /*fab.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(resources, R.color.colorAccent, null)))
            fab.setImageResource(R.drawable.ic_plus_white)*/

            val anim = ViewAnimationUtils.createCircularReveal(fl_parent_create_feed, x, y, startRadius.toFloat(), endRadius.toFloat())
            anim.duration = 500
            anim.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animator: Animator) {

                }

                override fun onAnimationEnd(animator: Animator) {
                    fl_parent_create_feed.visibility = View.GONE
                }

                override fun onAnimationCancel(animator: Animator) {

                }

                override fun onAnimationRepeat(animator: Animator) {

                }
            })
            anim.start()
//            isOpen = false
        }
    }

    private val mServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            mBoundSyncService = (service as FirebaseSyncService.FirebaseSyncBinder).getService()
            if (mBoundSyncService != null) {
                isServiceBound = true
                mBoundSyncService?.setFbSyncListener(this@FeedListActivity)
                fbNotificationList = mBoundSyncService!!.fbNotificationList
                updateNotificationIcon(mBoundSyncService!!.totalNotificationCount)
            } else Log.e("mServiceConnection", "service null")
        }

        override fun onServiceDisconnected(className: ComponentName) {
            Log.e("mServiceConnection", "disconnected")
            isServiceBound = false
            mBoundSyncService = null
        }
    }

    override fun provideSyncedList(fbNotificationList: LinkedList<FbNotification?>) {
        this.fbNotificationList = fbNotificationList
    }

    override fun provideNotificationCount(notificationCount: Int) {
        updateNotificationIcon(notificationCount)
    }

    override fun showLoader(loading: Boolean) {
    }

    private fun bindService() {
        bindService(Intent(this, FirebaseSyncService::class.java),
                mServiceConnection, Context.BIND_AUTO_CREATE)
    }

    private fun unbindService() {
        if (isServiceBound) {
            if (mBoundSyncService != null) {
                mBoundSyncService!!.removeFbSyncListener()
            }
            unbindService(mServiceConnection)
            isServiceBound = false
        }
    }

    private fun checkPlayServices() {
        val googleApiAvailability = GoogleApiAvailability.getInstance()
        val success = googleApiAvailability.isGooglePlayServicesAvailable(this)
        if (success != ConnectionResult.SUCCESS) {
            googleApiAvailability.makeGooglePlayServicesAvailable(this)
        }
    }

    private fun sendFCMTokenFirstTime() {
        val userSharedPreferences = SharedPreferenceHelper.customPrefs(this, Constants.USER_PREF)
        var token = ""
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
            token = it.token
        }
        Log.d("token", "" + token)
        if (token.isNotEmpty()) {
            val savedToken = userSharedPreferences[Constants.FCM_TOKEN, ""]
            if (savedToken!!.isEmpty() || savedToken != token) {
                Utils.updateTokenInFirebaseDb(token)
                userSharedPreferences[Constants.FCM_TOKEN] = token
            }
        }
    }
}
