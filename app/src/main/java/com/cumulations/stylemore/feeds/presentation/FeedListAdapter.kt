package com.cumulations.stylemore.feeds.presentation

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatDrawableManager
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.cumulations.stylemore.R
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.firebase.FbNotification
import com.cumulations.stylemore.base.hashTagHelper.ClickableForegroundColorSpan
import com.cumulations.stylemore.base.hashTagHelper.HashTagHelper
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.data.model.LikeFeedResponse
import com.cumulations.stylemore.feeds.data.model.ResultItem
import com.cumulations.stylemore.feeds.domain.AddLikeUseCase
import com.cumulations.stylemore.search.presentation.SearchActivity
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest
import com.cumulations.stylemore.userProfile.presentation.UserProfileActivity
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_feed_list_v2.view.*
import org.apache.commons.text.StringEscapeUtils
import org.jetbrains.anko.toast


class FeedListAdapter(val context: Context,
                      var feedRecordList: MutableList<ResultItem>?,
                      var feedListFragment: FeedListFragment?) :
        RecyclerView.Adapter<FeedListAdapter.FeedRecordItemViewHolder>(),
        AddLikeUiUpdateView {

    var likedPos: Int = -1
    var isFavClicked = false
    lateinit var lastFbNotification: FbNotification
    internal var screenWidth = (Utils.getScreenWidthInDp(context as AppCompatActivity))
    internal lateinit var updateCommentCountListener: FeedCommentsDialogFragment.UpdateCommentCountListener
    var additionalSymbols = charArrayOf('_'/*, '$'*/)

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): FeedRecordItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_feed_list_v2, parent, false)
        return FeedRecordItemViewHolder(view)
    }


    override fun onBindViewHolder(feedRecordItemViewHolder: FeedRecordItemViewHolder, position: Int) {
        val recordsItem = feedRecordList!![position]
        feedRecordItemViewHolder.bindFeedRecord(position, recordsItem)
    }

    override fun getItemCount(): Int {
        return feedRecordList!!.size
    }

    inner class FeedRecordItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("RestrictedApi")
        fun bindFeedRecord(clickedPos: Int, resultItem: ResultItem) {

            val activity = itemView.context as AppCompatActivity

            if (resultItem.pictures!!.isNotEmpty()) {
                itemView.square_pics_viewpager.adapter = FeedListItemViewPagerAdapter(resultItem.pictures as ArrayList<String>)
                itemView.pageIndicatorView.setViewPager(itemView.square_pics_viewpager)

                itemView.tv_selected_pics_count.text = "1/${resultItem.pictures.size}"
                itemView.square_pics_viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                    override fun onPageScrollStateChanged(state: Int) {

                    }

                    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                    }

                    override fun onPageSelected(position: Int) {
                        itemView.tv_selected_pics_count.text = "${position + 1}/${resultItem.pictures.size}"
                    }

                })
            }

            Picasso.get().load(resultItem.profilePic)
                    .placeholder(R.drawable.user_placeholder_square)
                    .into(itemView.civ_profile_icon)

            itemView.tv_user_name.text = resultItem.name

            try {
                val timeAgo = Utils.getDateInRelativeTime(resultItem.addedOn, "yyyy-MM-dd HH:mm:ss")
                itemView.tv_feed_posted_time.text = timeAgo
            } catch (e: Exception) {
                e.printStackTrace()
                itemView.tv_feed_posted_time.text = ""
            }

            val hashTagClickListener = HashTagHelper.OnHashTagClickListener {
                Log.v("onHashTagClicked", "it")
                if (!Utils.isOnline()) {
                    showError(context.getString(R.string.no_internet_connection))
                    return@OnHashTagClickListener
                }

                feedListFragment?.updatePositionAndSize(clickedPos)
                if (activity is SearchActivity){
                    activity.prevClickedFeedPos = clickedPos
                    activity.totalLoadedFeeds = itemCount
                }

                context.startActivity(Intent(context, SearchActivity::class.java)
                        .putExtra(Constants.SEARCH_TYPE, SearchActivity.SEARCH_DESCR)
                        .putExtra(Constants.SEARCH_KEYWORD, "#$it")
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
            }
            val hashTagHelper = HashTagHelper.Creator.create(ContextCompat.getColor(context, R.color.colorAccent),
                    hashTagClickListener, additionalSymbols)
            hashTagHelper.handle(itemView.tv_brand_tags_more)

            val unescapedDescription = StringEscapeUtils.unescapeJava(resultItem?.description!!)
            itemView.tv_brand_tags_more.text = unescapedDescription

            itemView.tv_likes_count.text = "${resultItem.lCount} Likes"
            itemView.tv_comments_count.text = "${resultItem.cCount} Comments"

            if (resultItem.likedByMe) {
                itemView.iv_heart.setImageResource(R.drawable.ic_heart_red_96)
            } else {
                itemView.iv_heart.setImageResource(R.drawable.ic_heart_grey_96)
            }

            itemView.linear_tags_container.removeAllViews()
            for (tagItem in resultItem.tags!!) {
                val view = View.inflate(itemView.context, R.layout.layout_tag_item, null)

                val tagItemTextView = view.findViewById<AppCompatTextView>(R.id.tv_tag_item)

                tagItemTextView.text = tagItem.brand

                when (tagItem.tag.toLowerCase()) {
                    "top" -> {
                        val drawable =
                        tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(
                                AppCompatDrawableManager.get().getDrawable(context,R.drawable.ic_red_top), null, null, null)
                    }

                    "pants" -> {
                        tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(
                                AppCompatDrawableManager.get().getDrawable(context,R.drawable.ic_red_pants), null, null, null)
                    }

                    "dress" -> {
                        tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(
                                AppCompatDrawableManager.get().getDrawable(context,R.drawable.ic_red_dress), null, null, null)
                    }

                    "accessory" -> {
                        tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(
                                AppCompatDrawableManager.get().getDrawable(context,R.drawable.ic_red_accessory), null, null, null)
                    }

                    "shoes" -> {
                        tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(
                                AppCompatDrawableManager.get().getDrawable(context,R.drawable.ic_red_shoes), null, null, null)
                    }

                    "bag" -> {
                        tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(
                                AppCompatDrawableManager.get().getDrawable(context,R.drawable.ic_red_bag), null, null, null)
                    }

                    "skirt" -> {
                        tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(
                                AppCompatDrawableManager.get().getDrawable(context,R.drawable.ic_red_skirt), null, null, null)
                    }

                    "watch" -> {
                        tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(
                                AppCompatDrawableManager.get().getDrawable(context,R.drawable.ic_red_watch), null, null, null)
                    }

                    "eyewear" -> {
                        tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(
                                AppCompatDrawableManager.get().getDrawable(context,R.drawable.ic_red_eyewear), null, null, null)
                    }

                    "other" -> {
                        tagItemTextView.setCompoundDrawablesWithIntrinsicBounds(
                                AppCompatDrawableManager.get().getDrawable(context,R.drawable.ic_red_others), null, null, null)
                    }
                }

                tagItemTextView.setOnClickListener {
                    val brandTagName = (it as AppCompatTextView).text
                    if (brandTagName.isNullOrEmpty())
                        return@setOnClickListener

                    if (!Utils.isOnline()) {
                        showError(context.getString(R.string.no_internet_connection))
                        return@setOnClickListener
                    }

                    feedListFragment?.updatePositionAndSize(clickedPos)
                    if (activity is SearchActivity){
                        activity.prevClickedFeedPos = clickedPos
                        activity.totalLoadedFeeds = itemCount
                    }

                    context.startActivity(Intent(context, SearchActivity::class.java)
                            .putExtra(Constants.SEARCH_TYPE, SearchActivity.SEARCH_TAG)
                            .putExtra(Constants.SEARCH_KEYWORD, brandTagName)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                }

                itemView.linear_tags_container.addView(view)
            }

            itemView.iv_brand_tag.setOnClickListener {
                if (itemView.linear_tags_container.visibility == View.VISIBLE)
                    itemView.linear_tags_container.visibility = View.GONE
                else itemView.linear_tags_container.visibility = View.VISIBLE
            }

            itemView.iv_heart.setOnClickListener {
                if (!Utils.isOnline()){
                    showError(context.getString(R.string.no_internet_connection))
                    return@setOnClickListener
                }

                likedPos = adapterPosition
                isFavClicked = true
                val feedListPresenter = FeedListPresenterImpl()
                feedListPresenter.setLikesResponseListener(this@FeedListAdapter)

                val userDetailRequest = UserDetailRequest(StyleMoreApp.user_id, resultItem.feedId.toString())
                feedListPresenter.getAddLikeResponse(AddLikeUseCase(), userDetailRequest)
//                updateFavorite(resultItem, itemView)
            }

            itemView.linear_likes.setOnClickListener {
                if (!Utils.isOnline()){
                    showError(context.getString(R.string.no_internet_connection))
                    return@setOnClickListener
                }

                val feedLikesDialogFragment = FeedLikesDialogFragment()
                val bundle = Bundle()
                bundle.putString(Constants.FEED_ID, resultItem.feedId.toString())
                bundle.putString(Constants.FEED_PIC, resultItem.pictures.get(0))
                bundle.putInt(Constants.USER_ID, resultItem.userId)
                feedLikesDialogFragment.arguments = bundle
                feedLikesDialogFragment.show(activity.supportFragmentManager, FeedLikesDialogFragment::class.java.simpleName)
            }

            itemView.linear_comments.setOnClickListener {
                if (!Utils.isOnline()){
                    showError(context.getString(R.string.no_internet_connection))
                    return@setOnClickListener
                }
                val feedCommentsDialogFragment = FeedCommentsDialogFragment()
                val bundle = Bundle()
                bundle.putString(Constants.FEED_ID, resultItem.feedId.toString())
                bundle.putString(Constants.FEED_PIC, resultItem.pictures[0])
                bundle.putInt(Constants.USER_ID, resultItem.userId)
                bundle.putInt(Constants.POSITION, clickedPos)
                feedCommentsDialogFragment.arguments = bundle
                feedCommentsDialogFragment.show(activity.supportFragmentManager, FeedCommentsDialogFragment::class.java.simpleName)
                feedCommentsDialogFragment.setUpdateCommentCountListener(updateCommentCountListener)
            }

            val gotoProfileScreen: View.OnClickListener = View.OnClickListener {
                if (!Utils.isOnline()) {
                    showError(context.getString(R.string.no_internet_connection))
                    return@OnClickListener
                }

                feedListFragment?.updatePositionAndSize(clickedPos)
                if (activity is SearchActivity){
                    activity.prevClickedFeedPos = clickedPos
                    activity.totalLoadedFeeds = itemCount
                }

                context.startActivity(Intent(context, UserProfileActivity::class.java)
                        .putExtra(Constants.USER_ID, resultItem.userId)
                        .putExtra(Constants.USER_FULLNAME, resultItem.name)
                        .putExtra(Constants.USER_FOLLOWING, resultItem.following)
                        .putExtra(Constants.USER_PROF_PIC, resultItem.profilePic))
            }

            itemView.civ_profile_icon.setOnClickListener(gotoProfileScreen)
            itemView.tv_user_name.setOnClickListener(gotoProfileScreen)

            val gotoDetailsScreen: View.OnClickListener = View.OnClickListener {
                if (!Utils.isOnline()) {
                    showError(context.getString(R.string.no_internet_connection))
                    return@OnClickListener
                }
//               TODO: @amit dude it's better to send feed_id
                feedListFragment?.updatePositionAndSize(clickedPos)
                if (activity is SearchActivity){
                    activity.prevClickedFeedPos = clickedPos
                    activity.totalLoadedFeeds = itemCount
                }

                val intent = Intent(context, FeedDetailsActivity::class.java)
                intent.putExtra(Constants.FEED_ID, resultItem.feedId)
                context.startActivity(intent)
            }

//            itemView.cv_parent_layout.setOnClickListener(gotoDetailsScreen)
            itemView.square_pics_viewpager.setOnViewPagerClickListener(gotoDetailsScreen)

        }
    }

    fun setCommentCountListener(updateCommentCountListener: FeedCommentsDialogFragment.UpdateCommentCountListener) {
        this.updateCommentCountListener = updateCommentCountListener
    }

    fun updateList(feedRecordList: List<ResultItem>?) {
        this.feedRecordList?.addAll(feedRecordList!!)
        notifyDataSetChanged()
    }

    override fun updateLikeResponse(likeFeedResponse: LikeFeedResponse) {

        val item = feedRecordList?.get(likedPos)
        val userIdDbReference = FirebaseDatabase.getInstance().reference
                .child("Notifications")
                .child("${item?.userId}")
        if (likeFeedResponse.result.equals("liked",true) && !item?.likedByMe!!) {
            /*updating firebase db*/
            item?.likedByMe = true
            updateFavorite(item,likedPos)
            if (!StyleMoreApp.user_id.equals(item?.userId.toString())) {
                Utils.pushActionToFirebaseDb(userIdDbReference, lastFbNotification)
            }
        } else if (likeFeedResponse.result.equals("disliked",true) && item?.likedByMe!!) {
            /*updating firebase db*/
            item?.likedByMe = false
            updateFavorite(item,likedPos)
            if (!StyleMoreApp.user_id.equals(item?.userId.toString())) {
                Utils.pushActionToFirebaseDb(userIdDbReference, lastFbNotification)
            }
        }
    }

    override fun showError(error: String) {
        context.toast(error)
        if (isFavClicked) {
            /*updating item value manually*/
            if (feedRecordList != null && likedPos < feedRecordList?.size!!) {
                /*val item = feedRecordList?.get(likedPos)
                item?.likedByMe = !item?.likedByMe!!
                notifyItemChanged(likedPos)*/
            }
            likedPos = -1;
            isFavClicked = false
        }
    }

    private fun updateFavorite(resultItem: ResultItem?, clickedPos: Int) {
        val timestamp = System.currentTimeMillis()
        if (resultItem?.likedByMe!!) {
            resultItem?.lCount = (resultItem?.lCount!! + 1)
            lastFbNotification = FbNotification(timestamp, "liked", Utils.getLoggedInActionUser(context),
                    resultItem.feedId.toLong(), resultItem.pictures?.get(0), timestamp, false, false)
        } else {
            if (resultItem?.lCount!! > 0) {
                resultItem?.lCount = (resultItem?.lCount!! - 1)
            }
            lastFbNotification = FbNotification(timestamp, "disliked", Utils.getLoggedInActionUser(context),
                    resultItem.feedId.toLong(), resultItem.pictures?.get(0), timestamp, false, false)
        }
        lastFbNotification.actionToUserId = resultItem?.userId.toString()
        /*update particular row UI*/
        notifyItemChanged(clickedPos)

    }

    inner class FeedListItemViewPagerAdapter(private val picsUrls: ArrayList<String>) : PagerAdapter() {
        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view == `object`
        }


        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            // Get the view from pager page layout
            val view = LayoutInflater.from(container.context)
                    .inflate(R.layout.viewpager_image_layout, container, false)
            val backgroundIv: ImageView = view.findViewById(R.id.iv_background)
//            view.fl_parent_viewpager.setPadding(0,0,20,0)
            Picasso.get().load(StyleMoreApp.IMAGE_BASE_URL + picsUrls.get(position))
                    .placeholder(R.drawable.placeholder_large)
                    .resize(screenWidth / 2, screenWidth / 2)
                    .onlyScaleDown()
//                    .fit()
                    .centerInside()
                    .into(backgroundIv)
            container.addView(view)
            return view
        }

        override fun getCount(): Int {
            return picsUrls.size
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
        }

    }

    override fun showLoading() {
        feedListFragment?.showLoading()
        if ((context as AppCompatActivity)is SearchActivity){
            (context as SearchActivity).showLoading()
        }
    }

    override fun hideLoading() {
        feedListFragment?.hideLoading()
        if ((context as AppCompatActivity)is SearchActivity){
            (context as SearchActivity).hideLoading()
        }
    }
}



