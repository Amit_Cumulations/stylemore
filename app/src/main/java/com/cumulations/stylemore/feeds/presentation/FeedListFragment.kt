package com.cumulations.stylemore.feeds.presentation

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cumulations.stylemore.R
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.data.model.*
import com.cumulations.stylemore.feeds.domain.FollowingFeedListUseCase
import com.cumulations.stylemore.feeds.domain.HotFeedListUseCase
import com.cumulations.stylemore.feeds.domain.NewFeedListUseCase
import com.cumulations.stylemore.feeds.domain.STipFeedListUseCase
import kotlinx.android.synthetic.main.fragment_feed_list.*
import kotlinx.android.synthetic.main.layout_full_screen_loader.*
import org.jetbrains.anko.toast
import android.content.Intent
import android.support.v4.content.ContextCompat
import com.cumulations.stylemore.login.presentation.SplashScreenActivity
import android.support.v7.widget.RecyclerView


/**
 * Created by Amit Tumkur on 04-06-2018.
 */
class FeedListFragment: Fragment(),FeedListUiUpdateView,FeedCommentsDialogFragment.UpdateCommentCountListener {

    companion object {
        const val FEED_TYPE_HOT = 11
        const val FEED_TYPE_NEW = 12
        const val FEED_TYPE_FOLLOWING = 13
        const val FEED_TYPE_STIP = 14
    }

    private val TAG = FeedListFragment::class.java.simpleName
    private var feedType: Int = FEED_TYPE_NEW
    private lateinit var feedListAdapter: FeedListAdapter
    private var currentPageCount = 0
    private var isLastPage = false
    private var isLoading = false
    private lateinit var mLayoutManager: LinearLayoutManager
    private var lastFeedClickedPos = -1
    private var PAGE_SIZE = 10

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_feed_list,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        feedType = arguments?.getInt(Constants.FEED_TYPE) ?: FEED_TYPE_HOT

        initAdapter()
        loadNextFeeds()
    }

    private fun loadNextFeeds() {
        /*Check if previously loaded items count present*/
        val lastTotalItemCount = (activity as FeedListActivity).tabTotalItems
        lastFeedClickedPos = (activity as FeedListActivity).clickedFeedPos
        if (lastTotalItemCount > 0 && lastFeedClickedPos >= 0) {

            if (lastTotalItemCount in 1..10) {
                PAGE_SIZE = 10
            } else if (lastTotalItemCount in 11..20) {
                PAGE_SIZE = 20
            } else if (lastTotalItemCount in 21..30) {
                PAGE_SIZE = 30
            } else if (lastTotalItemCount in 31..40) {
                PAGE_SIZE = 40
            } else if (lastTotalItemCount in 41..50) {
                PAGE_SIZE = 50
            } else if (lastTotalItemCount in 51..60) {
                PAGE_SIZE = 60
            } else if (lastTotalItemCount in 61..70) {
                PAGE_SIZE = 70
            } else if (lastTotalItemCount in 71..80) {
                PAGE_SIZE = 80
            }

            currentPageCount = 0

        }

        currentPageCount++

        val feedListPresenter = FeedListPresenterImpl(this)
        val feedListRequest = FeedListRequest(PAGE_SIZE,currentPageCount)
        when(feedType){
            FEED_TYPE_NEW -> {
                feedListPresenter.getNewFeedsResponse(NewFeedListUseCase(),feedListRequest)
            }

            FEED_TYPE_HOT -> {
                feedListPresenter.getHotFeedsResponse(HotFeedListUseCase(),feedListRequest)
            }

            FEED_TYPE_FOLLOWING -> {
                feedListPresenter.getFollowingFeedsResponse(FollowingFeedListUseCase(),feedListRequest)
            }

            FEED_TYPE_STIP -> {
                feedListPresenter.getSTipFeedsResponse(STipFeedListUseCase(),feedListRequest)
            }
        }
    }

    private fun initAdapter() {
        feedListAdapter = FeedListAdapter(this.activity!!,ArrayList(), this)
        feedListAdapter.setCommentCountListener(this)

        mLayoutManager = LinearLayoutManager(this.activity!!)
        rv_feed_list.layoutManager = mLayoutManager
        rv_feed_list.itemAnimator = DefaultItemAnimator()
        rv_feed_list.adapter = feedListAdapter

        rv_feed_list.addOnScrollListener(recyclerViewOnScrollListener)

        swipe_refresh_layout.setOnRefreshListener {
            if (!Utils.isOnline()){
                activity?.toast(Constants.NO_INTERNET)
                swipe_refresh_layout.isRefreshing = false
                return@setOnRefreshListener
            }
            /*reload feed*/
            currentPageCount = 0
            isLastPage = false
            PAGE_SIZE = 10

            (activity as FeedListActivity).clickedFeedPos = -1
            (activity as FeedListActivity).tabTotalItems = -1

            feedListAdapter.feedRecordList?.clear()
            feedListAdapter.notifyDataSetChanged()
            loadNextFeeds()
        }

        swipe_refresh_layout.setColorSchemeColors(
                ContextCompat.getColor(activity!!, R.color.insta_light_red),
                ContextCompat.getColor(activity!!, R.color.insta_purple),
                ContextCompat.getColor(activity!!, R.color.insta_orange))
    }

    override fun showLoading() {
        if (activity!=null && !activity!!.isFinishing) {
            isLoading = true
            full_screen_loader_layout.visibility = View.VISIBLE
            Utils.showLoader(loader, null, this.activity!!)
        }
    }

    override fun hideLoading() {
        if (activity!=null && !activity!!.isFinishing) {
            isLoading = false
            full_screen_loader_layout.visibility = View.GONE
            Utils.closeLoader(loader, null, this.activity!!)
        }
    }

    override fun provideFeedListResponse(feedsListResponse: FeedsListResponse) {
        if (activity == null || activity?.isFinishing!!)
            return
        val recordsItemList:List<ResultItem> = feedsListResponse.result!!
        swipe_refresh_layout.isRefreshing = false
        feedListAdapter.updateList(recordsItemList)
        if (recordsItemList.size < PAGE_SIZE) {
            isLastPage = true
        }

//        (activity as FeedListActivity).tabTotalItems = feedListAdapter.feedRecordList?.size!!

        if (lastFeedClickedPos>=0){
            rv_feed_list.scrollToPosition(lastFeedClickedPos)
            currentPageCount = (PAGE_SIZE/10) /*- 1*/
            PAGE_SIZE = 10
            (activity as FeedListActivity).tabTotalItems = -1
            (activity as FeedListActivity).clickedFeedPos = -1
            lastFeedClickedPos = -1
        }
    }

    override fun showError(error: String) {
        if (activity?.isFinishing!!)
            return
        if (error == Constants.INVALID_AUTH_KEY){
            activity?.toast(error)
            SharedPreferenceHelper.clearCustomPrefs(activity!!,Constants.USER_PREF)
            activity!!.startActivity(Intent(activity, SplashScreenActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
            activity!!.finish()
        }

        if (error == Constants.NO_DATA_FOUND){
            if (currentPageCount == 1) {
                when(feedType){
                    FEED_TYPE_HOT->{
                        tv_no_data.text = "No hot feeds added. Be the 1st one to add"
                    }

                    FEED_TYPE_NEW->{
                        tv_no_data.text = "No new feeds added. Be the 1st one to add"
                    }

                    FEED_TYPE_FOLLOWING->{
                        tv_no_data.text = "No feeds added from your followers"
                    }

                    FEED_TYPE_STIP->{
                        tv_no_data.text = "No style tip feeds added. Be the 1st one to add"
                    }
                }
                tv_no_data.visibility = View.VISIBLE
                rv_feed_list.visibility = View.GONE
            } else if (!isLoading){
                isLastPage = true
                activity?.toast("No further feeds")
            }
        }
    }

    private val recyclerViewOnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val visibleItemCount = mLayoutManager.childCount
            val totalItemCount = mLayoutManager.itemCount
            val firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition()
            val lastVisibleItemPosition = mLayoutManager.findLastVisibleItemPosition()

            if (isLoading || isLastPage || swipe_refresh_layout.isRefreshing){
                /*if (isLastPage && dy > 0 *//*scrolled bottom*//*) {
                    if ((lastVisibleItemPosition + 1) == totalItemCount) {
                        if (currentPageCount > 1)
                            activity?.toast("No further feeds")
                    }
                }*/
                return
            }

            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                    && totalItemCount >= PAGE_SIZE) {
                loadNextFeeds()
            }
        }
    }

    override fun incrementCommentCount(updateItemPosition: Int) {
        val resultItem = feedListAdapter?.feedRecordList?.get(updateItemPosition)
        if (resultItem!=null){
            resultItem.cCount+=1
            feedListAdapter.notifyItemChanged(updateItemPosition)
        }
    }

    override fun decrementCommentCount(updateItemPosition: Int) {
        val resultItem = feedListAdapter?.feedRecordList?.get(updateItemPosition)
        if (resultItem!=null){
            if (resultItem.cCount>0) {
                resultItem.cCount -= 1
                feedListAdapter.notifyItemChanged(updateItemPosition)
            }
        }
    }

    /*override fun onStop() {
        super.onStop()
        rv_feed_list.removeOnScrollListener(recyclerViewOnScrollListener)
        currentPageCount = 0
    }*/

    override fun onDestroyView() {
        super.onDestroyView()
        rv_feed_list.removeOnScrollListener(recyclerViewOnScrollListener)
        currentPageCount = 0
    }

    fun updatePositionAndSize(position: Int){
        (activity as FeedListActivity).clickedFeedPos = position
        (activity as FeedListActivity).tabTotalItems = feedListAdapter?.feedRecordList?.size!!
    }
}