package com.cumulations.stylemore.feeds.presentation

import com.cumulations.stylemore.feeds.data.model.AddCommentRequest
import com.cumulations.stylemore.feeds.data.model.EarlierPostsRequest
import com.cumulations.stylemore.feeds.data.model.FeedLikesRequest
import com.cumulations.stylemore.feeds.data.model.FeedListRequest
import com.cumulations.stylemore.feeds.domain.*
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

interface FeedListPresenter {
    fun getNewFeedsResponse(newFeedListUseCase: NewFeedListUseCase, feedListRequest: FeedListRequest)
    fun getHotFeedsResponse(hotFeedListUseCase: HotFeedListUseCase, feedListRequest: FeedListRequest)
    fun getFollowingFeedsResponse(followingFeedListUseCase: FollowingFeedListUseCase, feedListRequest: FeedListRequest)
    fun getSTipFeedsResponse(sTipFeedListUseCase: STipFeedListUseCase, feedListRequest: FeedListRequest)
    fun getUserEarlierPostsResponse(earlierFeedsUseCase: EarlierFeedsUseCase, earlierPostsRequest: EarlierPostsRequest)
    fun getFeedLikesResponse(feedLikesUseCase: FeedLikesUseCase, feedLikesRequest: FeedLikesRequest)
    fun getFeedCommentsResponse(feedCommentsUseCase: FeedCommentsUseCase, feedLikesRequest: FeedLikesRequest)
    fun getAddLikeResponse(addLikeUseCase: AddLikeUseCase, userDetailRequest: UserDetailRequest)
    fun getAddCommentResponse(addCommentUseCase: AddCommentUseCase, addCommentRequest: AddCommentRequest)
    fun getDeleteFeedResponse(deleteFeedUseCase: DeleteFeedUseCase, userDetailRequest: UserDetailRequest)
    fun getFollowResponse(followUnfollowUseCase: FollowUnfollowUseCase, userDetailRequest: UserDetailRequest)
    fun getDeleteCommentResponse(deleteCommentUseCase: DeleteCommentUseCase, userDetailRequest: UserDetailRequest)
}
