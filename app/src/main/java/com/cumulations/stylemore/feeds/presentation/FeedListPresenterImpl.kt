package com.cumulations.stylemore.feeds.presentation

import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.data.model.AddCommentRequest
import com.cumulations.stylemore.feeds.data.model.EarlierPostsRequest
import com.cumulations.stylemore.feeds.data.model.FeedLikesRequest
import com.cumulations.stylemore.feeds.data.model.FeedListRequest
import com.cumulations.stylemore.feeds.domain.*
import com.cumulations.stylemore.rest.RestClient
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest
import retrofit2.adapter.rxjava.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class FeedListPresenterImpl : FeedListPresenter {

    private lateinit var feedListUiUpdateView:FeedListUiUpdateView
    private lateinit var feedDetailsUiUpdateView:FeedDetailsUiUpdateView
    private lateinit var addLikeUiUpdateView: AddLikeUiUpdateView
    private lateinit var addCommentUiUpdateView: AddCommentUiUpdateView
    private lateinit var followUserUiUpdateView: FollowUserUiUpdateView
    private lateinit var deleteCommentUiUpdateView: DeleteCommentUiUpdateView

    constructor()

    constructor(feedListUiUpdateView: FeedListUiUpdateView){
        this.feedListUiUpdateView = feedListUiUpdateView
    }

    constructor(feedDetailsUiUpdateView: FeedDetailsUiUpdateView){
        this.feedDetailsUiUpdateView = feedDetailsUiUpdateView
    }

    fun setLikesResponseListener(addLikeUiUpdateView: AddLikeUiUpdateView){
        this.addLikeUiUpdateView = addLikeUiUpdateView
    }

    fun setCommentResponseListener(addCommentUiUpdateView: AddCommentUiUpdateView){
        this.addCommentUiUpdateView = addCommentUiUpdateView
    }

    fun setFollowUserUiUpdateListener(followUserUiUpdateView: FollowUserUiUpdateView){
        this.followUserUiUpdateView = followUserUiUpdateView
    }

    fun setDeleteCommentUiUpdateListener(deleteCommentUiUpdateView: DeleteCommentUiUpdateView){
        this.deleteCommentUiUpdateView = deleteCommentUiUpdateView
    }

    override fun getNewFeedsResponse(newFeedListUseCase: NewFeedListUseCase, feedListRequest: FeedListRequest) {
        if (Utils.isOnline()){
            feedListUiUpdateView.showLoading()
            newFeedListUseCase.execute(feedListRequest)
                    .subscribe({
                        feedListUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            feedListUiUpdateView.provideFeedListResponse(it!!)
                        } else if (it?.status.equals("error", true))
                            feedListUiUpdateView.showError(it?.description!!)
                    }, {
                        feedListUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                feedListUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                feedListUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                feedListUiUpdateView.showError(it.message!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            feedListUiUpdateView.showError(e.message!!)
                        }
                    })

        } else
            feedListUiUpdateView.showError(Constants.NO_INTERNET)
    }

    override fun getHotFeedsResponse(hotFeedListUseCase: HotFeedListUseCase, feedListRequest: FeedListRequest) {
        if (Utils.isOnline()){
            feedListUiUpdateView.showLoading()
            hotFeedListUseCase.execute(feedListRequest)
                    .subscribe({
                        feedListUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            feedListUiUpdateView.provideFeedListResponse(it!!)
                        } else if (it?.status.equals("error", true))
                            feedListUiUpdateView.showError(it?.description!!)
                    }, {
                        feedListUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                feedListUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                feedListUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                if (it.localizedMessage != null)
                                    feedListUiUpdateView.showError(it.localizedMessage!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (e.localizedMessage!=null)
                                feedListUiUpdateView.showError(e.localizedMessage!!)
                        }
                    })

        } else
            feedListUiUpdateView.showError(Constants.NO_INTERNET)
    }

    override fun getFollowingFeedsResponse(followingFeedListUseCase: FollowingFeedListUseCase, feedListRequest: FeedListRequest) {
        if (Utils.isOnline()){
            feedListUiUpdateView.showLoading()
            followingFeedListUseCase.execute(feedListRequest)
                    .subscribe({
                        feedListUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            feedListUiUpdateView.provideFeedListResponse(it!!)
                        } else if (it?.status.equals("error", true))
                            feedListUiUpdateView.showError(it?.description!!)
                    }, {
                        feedListUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                feedListUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                feedListUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                if (it.localizedMessage != null)
                                    feedListUiUpdateView.showError(it.localizedMessage!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (e.localizedMessage!=null)
                                feedListUiUpdateView.showError(e.localizedMessage!!)
                        }
                    })

        } else
            feedListUiUpdateView.showError(Constants.NO_INTERNET)
    }

    override fun getSTipFeedsResponse(sTipFeedListUseCase: STipFeedListUseCase, feedListRequest: FeedListRequest) {
        if (Utils.isOnline()){
            feedListUiUpdateView.showLoading()
            sTipFeedListUseCase.execute(feedListRequest)
                    .subscribe({
                        feedListUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            feedListUiUpdateView.provideFeedListResponse(it!!)
                        } else if (it?.status.equals("error", true))
                            feedListUiUpdateView.showError(it?.description!!)
                    }, {
                        feedListUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                feedListUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                feedListUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                if (it.localizedMessage != null)
                                    feedListUiUpdateView.showError(it.localizedMessage!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (e.localizedMessage!=null)
                                feedListUiUpdateView.showError(e.localizedMessage!!)
                        }
                    })

        } else
            feedListUiUpdateView.showError(Constants.NO_INTERNET)
    }

    override fun getUserEarlierPostsResponse(earlierFeedsUseCase: EarlierFeedsUseCase, earlierPostsRequest: EarlierPostsRequest) {
        if (Utils.isOnline()){
            feedDetailsUiUpdateView.showLoading()
            earlierFeedsUseCase.execute(earlierPostsRequest)
                    .subscribe({
                        feedDetailsUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            feedDetailsUiUpdateView.provideUserEarlierPostsResponse(it)
                        } else if (it?.status.equals("error", true))
                            feedDetailsUiUpdateView.showError(it?.description!!)
                    }, {
                        feedDetailsUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                feedDetailsUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                feedDetailsUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                if (it.localizedMessage != null)
                                    feedDetailsUiUpdateView.showError(it.localizedMessage!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (e.localizedMessage!=null)
                                feedDetailsUiUpdateView.showError(e.localizedMessage!!)
                        }
                    })

        } else
            feedDetailsUiUpdateView.showError(Constants.NO_INTERNET)
    }

    override fun getFeedLikesResponse(feedLikesUseCase: FeedLikesUseCase, feedLikesRequest: FeedLikesRequest) {
        if (Utils.isOnline()){
            feedDetailsUiUpdateView.showLoading()
            feedLikesUseCase.execute(feedLikesRequest)
                    .subscribe({
                        feedDetailsUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            feedDetailsUiUpdateView.provideFeedLikes(it)
                        } else if (it?.status.equals("error", true))
                            feedDetailsUiUpdateView.showError(it?.description!!)
                    }, {
                        feedDetailsUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                feedDetailsUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                feedDetailsUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                if (it.localizedMessage != null)
                                    feedDetailsUiUpdateView.showError(it.localizedMessage!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (e.localizedMessage!=null)
                                feedDetailsUiUpdateView.showError(e.localizedMessage!!)
                        }
                    })

        } else
            feedDetailsUiUpdateView.showError(Constants.NO_INTERNET)
    }

    override fun getFeedCommentsResponse(feedCommentsUseCase: FeedCommentsUseCase, feedLikesRequest: FeedLikesRequest) {
        if (Utils.isOnline()){
            feedDetailsUiUpdateView.showLoading()
            feedCommentsUseCase.execute(feedLikesRequest)
                    .subscribe({
                        feedDetailsUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            feedDetailsUiUpdateView.provideFeedComments(it)
                        } else if (it?.status.equals("error", true))
                            feedDetailsUiUpdateView.showError(it?.description!!)
                    }, {
                        feedDetailsUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                feedDetailsUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                feedDetailsUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                if (it.localizedMessage != null)
                                    feedDetailsUiUpdateView.showError(it.localizedMessage!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (e.localizedMessage!=null)
                                feedDetailsUiUpdateView.showError(e.localizedMessage!!)
                        }
                    })

        } else
            feedDetailsUiUpdateView.showError(Constants.NO_INTERNET)
    }

    override fun getAddLikeResponse(addLikeUseCase: AddLikeUseCase, userDetailRequest: UserDetailRequest) {
        if (Utils.isOnline()){
            addLikeUiUpdateView.showLoading()
            addLikeUseCase.execute(userDetailRequest)
                    .subscribe({
                        addLikeUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            addLikeUiUpdateView.updateLikeResponse(it)
                        } else if (it?.status.equals("error", true))
                            addLikeUiUpdateView.showError(it?.description!!)
                    }, {
                        addLikeUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                addLikeUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                addLikeUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                if (it.localizedMessage != null)
                                    addLikeUiUpdateView.showError(it.localizedMessage!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (e.localizedMessage!=null)
                                addLikeUiUpdateView.showError(e.localizedMessage!!)
                        }
                    })

        } else
            addLikeUiUpdateView.showError(Constants.NO_INTERNET)
    }

    override fun getAddCommentResponse(addCommentUseCase: AddCommentUseCase, addCommentRequest: AddCommentRequest) {
        if (Utils.isOnline()){
            addCommentUiUpdateView.showLoading()
            addCommentUseCase.execute(addCommentRequest)
                    .subscribe({
                        addCommentUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            addCommentUiUpdateView.updateCommentResponse(it)
                        } else if (it?.status.equals("error", true))
                            addCommentUiUpdateView.showError(it?.description!!)
                    }, {
                        addCommentUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                addCommentUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                addCommentUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                if (it.localizedMessage != null)
                                    addCommentUiUpdateView.showError(it.localizedMessage!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (e.localizedMessage!=null)
                                addCommentUiUpdateView.showError(e.localizedMessage!!)
                        }
                    })

        } else
            addCommentUiUpdateView.showError(Constants.NO_INTERNET)
    }

    override fun getDeleteFeedResponse(deleteFeedUseCase: DeleteFeedUseCase, userDetailRequest: UserDetailRequest) {
        if (Utils.isOnline()){
            feedDetailsUiUpdateView.showLoading()
            deleteFeedUseCase.execute(userDetailRequest)
                    .subscribe({
                        feedDetailsUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            feedDetailsUiUpdateView.provideDeleteFeedResponse(it)
                        } else if (it?.status.equals("error", true))
                            feedDetailsUiUpdateView.showError(it?.description!!)
                    }, {
                        feedDetailsUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                feedDetailsUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                feedDetailsUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                feedDetailsUiUpdateView.showError(it.message!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            feedDetailsUiUpdateView.showError(e.message!!)
                        }
                    })

        } else
            feedDetailsUiUpdateView.showError(Constants.NO_INTERNET)
    }

    override fun getFollowResponse(followUnfollowUseCase: FollowUnfollowUseCase, userDetailRequest: UserDetailRequest) {
        if (Utils.isOnline()){
            feedDetailsUiUpdateView.showLoading()
            followUnfollowUseCase.execute(userDetailRequest)
                    .subscribe({
                        feedDetailsUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            followUserUiUpdateView.provideFollowUserResult(it)
                        } else if (it?.status.equals("error", true))
                            feedDetailsUiUpdateView.showError(it?.description!!)
                    }, {
                        feedDetailsUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                feedDetailsUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                feedDetailsUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                if (it.localizedMessage != null)
                                    feedDetailsUiUpdateView.showError(it.localizedMessage!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (e.localizedMessage!=null)
                                feedDetailsUiUpdateView.showError(e.localizedMessage!!)
                        }
                    })

        } else
            feedDetailsUiUpdateView.showError(Constants.NO_INTERNET)
    }

    override fun getDeleteCommentResponse(deleteCommentUseCase: DeleteCommentUseCase, userDetailRequest: UserDetailRequest) {
        if (Utils.isOnline()){
//            deleteCommentUiUpdateView.showLoading()
            deleteCommentUseCase.execute(userDetailRequest)
                    .subscribe({
//                        deleteCommentUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            deleteCommentUiUpdateView.updateDeleteCommentResponse(it)
                        } else if (it?.status.equals("error", true))
                            deleteCommentUiUpdateView.errorMessage(it?.description!!)
                    }, {
//                        deleteCommentUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                deleteCommentUiUpdateView.errorMessage(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                deleteCommentUiUpdateView.errorMessage(Constants.NETWORK_ERROR)
                            } else {
                                if (it.localizedMessage != null)
                                    deleteCommentUiUpdateView.errorMessage(it.localizedMessage!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (e.localizedMessage!=null)
                                deleteCommentUiUpdateView.errorMessage(e.localizedMessage!!)
                        }
                    })

        } else
            deleteCommentUiUpdateView.errorMessage(Constants.NO_INTERNET)
    }
}
