package com.cumulations.stylemore.feeds.presentation

import com.cumulations.stylemore.base.LoadingView
import com.cumulations.stylemore.feeds.data.model.FeedLikesResponse
import com.cumulations.stylemore.feeds.data.model.FeedsListResponse
import com.cumulations.stylemore.feeds.data.model.LikeFeedResponse
import com.cumulations.stylemore.login.data.model.LoginResponse

/**
 * Created by Amit Tumkur on 16-02-2018.
 */

interface FeedListUiUpdateView : LoadingView {
    fun provideFeedListResponse(feedsListResponse: FeedsListResponse)
    fun showError(error: String)
}
