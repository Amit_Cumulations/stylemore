package com.cumulations.stylemore.feeds.presentation

import com.cumulations.stylemore.feeds.data.model.AddCommentRequest
import com.cumulations.stylemore.feeds.data.model.EarlierPostsRequest
import com.cumulations.stylemore.feeds.data.model.FeedLikesRequest
import com.cumulations.stylemore.feeds.data.model.FeedListRequest
import com.cumulations.stylemore.feeds.domain.*
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

interface FollowUserPresenter {
    fun getFollowResponse(followUnfollowUseCase: FollowUnfollowUseCase, userDetailRequest: UserDetailRequest)
}
