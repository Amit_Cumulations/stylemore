package com.cumulations.stylemore.feeds.presentation

import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.data.model.AddCommentRequest
import com.cumulations.stylemore.feeds.data.model.EarlierPostsRequest
import com.cumulations.stylemore.feeds.data.model.FeedLikesRequest
import com.cumulations.stylemore.feeds.data.model.FeedListRequest
import com.cumulations.stylemore.feeds.domain.*
import com.cumulations.stylemore.rest.RestClient
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest
import retrofit2.adapter.rxjava.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class FollowUserPresenterImpl(val followUserUiUpdateView: FollowUserUiUpdateView) : FollowUserPresenter {
    override fun getFollowResponse(followUnfollowUseCase: FollowUnfollowUseCase, userDetailRequest: UserDetailRequest) {
        if (Utils.isOnline()){
            followUserUiUpdateView.showLoading()
            followUnfollowUseCase.execute(userDetailRequest)
                    .subscribe({
                        followUserUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            followUserUiUpdateView.provideFollowUserResult(it)
                        } else if (it?.status.equals("error", true))
                            followUserUiUpdateView.errorOccured(it?.description!!)
                    }, {
                        followUserUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                followUserUiUpdateView.errorOccured(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                followUserUiUpdateView.errorOccured(Constants.NETWORK_ERROR)
                            } else {
                                followUserUiUpdateView.errorOccured(it.message!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            followUserUiUpdateView.errorOccured(e.message!!)
                        }
                    })

        } else
            followUserUiUpdateView.errorOccured(Constants.NO_INTERNET)
    }
}
