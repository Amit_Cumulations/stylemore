package com.cumulations.stylemore.feeds.presentation

import com.cumulations.stylemore.base.LoadingView
import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse

/**
 * Created by Amit Tumkur on 04-07-2018.
 */
interface FollowUserUiUpdateView:LoadingView{
    fun provideFollowUserResult(basicResponse: BasicResponse)
    fun errorOccured(errorMsg : String)
}