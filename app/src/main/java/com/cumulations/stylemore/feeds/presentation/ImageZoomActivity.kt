package com.cumulations.stylemore.feeds.presentation

import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import com.cumulations.stylemore.R
import com.cumulations.stylemore.base.utils.Constants
import kotlinx.android.synthetic.main.activity_image_zoom.*

class ImageZoomActivity : AppCompatActivity() {
    private var images = ArrayList<String>()
    private var clickedPos = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_zoom)
        images = intent?.getStringArrayListExtra(Constants.SELECTED_PICS) as ArrayList<String>
        clickedPos = intent?.getIntExtra(Constants.POSITION,-1)!!

        view_pager.adapter = SamplePagerAdapter(images)
        view_pager.currentItem = clickedPos

        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                tv_title_toolbar.text = "${position+1} / ${images.size}"
            }

        })
    }

    internal class SamplePagerAdapter(private val pics:ArrayList<String>) : PagerAdapter() {

        override fun getCount(): Int {
            return pics.size
        }

        override fun instantiateItem(container: ViewGroup, position: Int): View {
            /*val photoView = PhotoView(container.context)

            Picasso.get().load(StyleMoreApp.IMAGE_BASE_URL+ pics[position])
                    .placeholder(R.drawable.placeholder_large)
                    .into(photoView)
            // Now just add PhotoView to ViewPager and return it
            container.addView(photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

            return photoView*/
            return container
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object`
        }

    }
}
