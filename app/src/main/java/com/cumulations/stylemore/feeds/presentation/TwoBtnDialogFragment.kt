package com.cumulations.stylemore.feeds.presentation

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.ViewGroup
import android.view.WindowManager
import com.cumulations.stylemore.R
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper.get
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.data.model.TagsItem
import kotlinx.android.synthetic.main.dialog_add_tags_v2.*
import kotlinx.android.synthetic.main.dialog_enter_brand.*
import kotlinx.android.synthetic.main.dialog_two_button.*
import org.jetbrains.anko.toast
import java.util.*


/**
 * Created by Amit Tumkur on 05-06-2018.
 */
class TwoBtnDialogFragment: DialogFragment() {
    private lateinit var dialogBtnsListener: DialogBtnsListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        val dialog = Dialog(activity!!, R.style.EditDialogTheme)
        dialog.setContentView(R.layout.dialog_two_button)
        dialog.setCanceledOnTouchOutside(false)
        isCancelable = false
        dialog.window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        val dlgTitle = arguments?.getString(Constants.DLG_TITLE)
        val dlgPosBtn = arguments?.getString(Constants.DLG_POS_BTN)
        val dlgNegBtn = arguments?.getString(Constants.DLG_NEG_BTN)

        dialog.dlg_tv_title.text = dlgTitle
        dialog.dlg_btn_positive.text = dlgPosBtn
        dialog.dlg_btn_negative.text = dlgNegBtn

        dialog.dlg_btn_positive.setOnClickListener{
            dialogBtnsListener.onPositiveBtnClick(it)
        }

        dialog.dlg_btn_negative.setOnClickListener {
            dialogBtnsListener.onNegativeBtnClick(it)
        }

        return dialog
    }

    fun setDialogBtnsListener(dialogBtnsListener: DialogBtnsListener){
        this.dialogBtnsListener = dialogBtnsListener
    }
}