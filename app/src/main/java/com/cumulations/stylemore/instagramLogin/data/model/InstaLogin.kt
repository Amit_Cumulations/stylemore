package com.cumulations.stylemore.instagramLogin.data.model

import com.cumulations.stylemore.base.utils.Constants
import java.io.Serializable

/**
 * Created by Amit Tumkur on 09-07-2018.
 */
data class InstaLogin(
        var clientId: String?,
        var clientSecret: String?,
        var grantType: String?,
        var instaAuthUrl: String?,
        var redirectUri: String?,
        var code: String?) : Serializable {

    fun getWebInstaAuthUrl(): String {
        return Constants.INSTAGRAM_AUTH_URL + "client_id=" + clientId + "&redirect_uri=" + redirectUri + "&response_type=code";
    }
}