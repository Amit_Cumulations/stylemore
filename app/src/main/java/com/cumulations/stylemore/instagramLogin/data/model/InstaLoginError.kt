package com.cumulations.stylemore.instagramLogin.data.model

import com.google.gson.annotations.SerializedName

data class InstaLoginError(@SerializedName("error_message")
                           val errorMessage: String = "",
                           @SerializedName("code")
                           val code: Int = 0,
                           @SerializedName("error_type")
                           val errorType: String = "")