package com.cumulations.stylemore.instagramLogin.data.model

import com.google.gson.annotations.SerializedName

data class InstaLoginResponse(@SerializedName("access_token")
                              val accessToken: String = "",
                              @SerializedName("user")
                              val user: InstaUser)