package com.cumulations.stylemore.instagramLogin.data.model

import com.google.gson.annotations.SerializedName

data class InstaUser(@SerializedName("is_business")
                     val isBusiness: Boolean = false,
                     @SerializedName("website")
                     val website: String = "",
                     @SerializedName("full_name")
                     val fullName: String = "",
                     @SerializedName("bio")
                     val bio: String = "",
                     @SerializedName("profile_picture")
                     val profilePicture: String = "",
                     @SerializedName("id")
                     val id: String = "",
                     @SerializedName("username")
                     val username: String = "")