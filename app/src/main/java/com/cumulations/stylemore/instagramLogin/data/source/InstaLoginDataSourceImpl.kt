package com.cumulations.stylemore.instagramLogin.data.source

import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.instagramLogin.data.model.InstaLogin
import com.cumulations.stylemore.instagramLogin.data.model.InstaLoginResponse
import com.cumulations.stylemore.instagramLogin.domain.InstaLoginDataInterface
import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.data.model.LoginResponse
import com.cumulations.stylemore.login.domain.LoginDataInterface
import com.cumulations.stylemore.rest.RestClient
import rx.Single
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by Amit Tumkur on 29-05-2018.
 */
class InstaLoginDataSourceImpl:InstaLoginDataInterface {
    override fun loginInstagram(instaLogin: InstaLogin): Single<InstaLoginResponse> {
        return RestClient.getApiService(Constants.INSTA_TOKEN_BASE_URL)
                .getInstagramProfile(
                        instaLogin.clientId,
                        instaLogin.clientSecret,
                        instaLogin.grantType,
                        instaLogin.redirectUri,
                        instaLogin.code)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

}