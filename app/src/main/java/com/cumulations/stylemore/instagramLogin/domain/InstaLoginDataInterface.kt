package com.cumulations.stylemore.instagramLogin.domain

import com.cumulations.stylemore.instagramLogin.data.model.InstaLogin
import com.cumulations.stylemore.instagramLogin.data.model.InstaLoginResponse
import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.data.model.LoginResponse
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

interface InstaLoginDataInterface {
    fun loginInstagram(instaLogin: InstaLogin): Single<InstaLoginResponse>
}
