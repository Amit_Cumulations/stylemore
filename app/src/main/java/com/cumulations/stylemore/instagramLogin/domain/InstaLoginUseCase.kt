package com.cumulations.stylemore.instagramLogin.domain

import com.cumulations.stylemore.base.UseCase
import com.cumulations.stylemore.instagramLogin.data.model.InstaLogin
import com.cumulations.stylemore.instagramLogin.data.model.InstaLoginResponse
import com.cumulations.stylemore.instagramLogin.data.source.InstaLoginDataSourceImpl
import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.data.model.LoginResponse
import com.cumulations.stylemore.login.data.source.LoginDataSourceImpl
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class InstaLoginUseCase : UseCase<InstaLogin, InstaLoginResponse>() {
    private val instaLoginDataInterface = InstaLoginDataSourceImpl()

    override fun buildUseCase(requestObj: InstaLogin): Single<InstaLoginResponse> {
        return instaLoginDataInterface.loginInstagram(requestObj)
    }
}
