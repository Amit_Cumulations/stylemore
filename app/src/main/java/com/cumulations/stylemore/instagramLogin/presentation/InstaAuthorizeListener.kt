package com.cumulations.stylemore.instagramLogin.presentation

import com.cumulations.stylemore.instagramLogin.data.model.InstaLoginResponse

/**
 * Created by Amit Tumkur on 09-07-2018.
 */
interface InstaAuthorizeListener {
    fun onRetrieveCode(code:String,instaLoginDialog: InstaLoginDialog)
    fun onAuthError(error:String)
    fun onInstagramUser(instaLoginResponse: InstaLoginResponse)
    fun onCancel()
}