package com.cumulations.stylemore.instagramLogin.presentation

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.webkit.WebIconDatabase.getInstance
import android.webkit.WebView
import android.webkit.WebViewClient
import com.cumulations.stylemore.R
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.instagramLogin.data.model.InstaLogin
import com.cumulations.stylemore.instagramLogin.data.model.InstaLoginResponse
import com.cumulations.stylemore.instagramLogin.domain.InstaLoginUseCase
import kotlinx.android.synthetic.main.dialog_insta_login_webview.*
import kotlinx.android.synthetic.main.layout_full_screen_loader.*
import org.jetbrains.anko.toast


/**
 * Created by Amit Tumkur on 05-06-2018.
 */
class InstaLoginDialog: DialogFragment(),InstagramApiUiUpdateView {
    companion object {
        const val TAG = "InstaLoginDialog"
    }

    private var instaLogin: InstaLogin? = null
    private lateinit var instaAuthorizeListener: InstaAuthorizeListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        val dialog = Dialog(activity!!, R.style.InstaDialogTheme)
        dialog.setContentView(R.layout.dialog_insta_login_webview)
        dialog.setCanceledOnTouchOutside(false)
        isCancelable = false
        dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
//        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

        instaLogin = arguments?.get(Constants.INSTA_LOGIN) as InstaLogin?

        setUpWebView(dialog.webView)

        dialog.iv_close_dialog.setOnClickListener {
            instaAuthorizeListener.onCancel()
            dismiss()
        }

        return dialog
    }

    fun authorizeUser(context: Context,instaLogin: InstaLogin,instaAuthorizeListener: InstaAuthorizeListener){
        val instaLoginDialog = InstaLoginDialog()
        val bundle = Bundle()
        bundle.putSerializable(Constants.INSTA_LOGIN,instaLogin)
        instaLoginDialog.arguments = bundle
        instaLoginDialog.show((context as AppCompatActivity).supportFragmentManager, TAG)
        instaLoginDialog.setAuthListener(instaAuthorizeListener)
    }

    fun setAuthListener(instaAuthorizeListener: InstaAuthorizeListener){
        this.instaAuthorizeListener = instaAuthorizeListener
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setUpWebView(webView: WebView) {
//        mWebView = WebView(context)

        getInstance().open(activity?.getDir("icons", MODE_PRIVATE)?.path);
        webView.isVerticalScrollBarEnabled = false
        webView.isHorizontalScrollBarEnabled = false
        webView.webViewClient = InstagramWebViewClient()
        webView.settings.javaScriptEnabled = true
        webView.loadUrl(instaLogin?.getWebInstaAuthUrl())
//        webView.setLayoutParams(FILL)
        webView.clearCache(true)
        webView.clearHistory()

        val webSettings = webView.settings

        webSettings.savePassword = false
        webSettings.saveFormData = false
//        mContent.addView(mWebView)
    }

    fun clearCache() {
        dialog.webView.clearCache(true)
        dialog.webView.clearHistory()
        dialog.webView.clearFormData()
    }

    /*override fun onStop() {
        super.onStop()
        instaAuthorizeListener.onCancel()
    }*/

    private inner class InstagramWebViewClient : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            Log.d(TAG, "Redirecting URL $url")

            if (url.startsWith(instaLogin?.redirectUri!!)) {
                if (url.contains("code")) {
                    val temp = url.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    instaAuthorizeListener.onRetrieveCode(temp[1],this@InstaLoginDialog)
                } else if (url.contains("error")) {
                    val temp = url.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    instaAuthorizeListener.onAuthError(temp[temp.size - 1])
                    dismiss()
                }
                return true
            }
            return false
        }

        override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
            super.onReceivedError(view, errorCode, description, failingUrl)
            instaAuthorizeListener.onAuthError(description)
            dismiss()
            Log.d(TAG, "Page error: $description")
        }

        override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            showLoading()
            Log.d(TAG, "Loading URL: $url")
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            if (dialog == null)
                return
            val title = dialog.webView.title
            if (title != null && title.isNotEmpty()) {
//                mTitle.setText(title)
            }
            hideLoading()
        }
    }

    fun getInstagramProfile(code:String){
        val instaLoginPresenter = InstaLoginPresenterImpl(this)
        instaLogin?.grantType = "authorization_code"
        instaLogin?.code = code
        instaLoginPresenter.getInstaLoginResponse(InstaLoginUseCase(),instaLogin!!)
    }

    override fun showLoading() {
        if (activity?.isFinishing!!)
            return
        dialog.full_screen_loader_layout.visibility = View.VISIBLE
        Utils.showLoader(dialog.loader,null, this.activity!!,false)
    }

    override fun hideLoading() {
        if (activity?.isFinishing!!)
            return
        dialog.full_screen_loader_layout.visibility = View.GONE
        Utils.closeLoader(dialog.loader,null,this.activity!!)
    }

    override fun onInstagramProfile(instaLoginResponse: InstaLoginResponse) {
        instaAuthorizeListener.onInstagramUser(instaLoginResponse)
        dialog?.dismiss()
    }

    override fun showError(error: String) {
        activity?.toast(error)
        dialog?.dismiss()
    }
}