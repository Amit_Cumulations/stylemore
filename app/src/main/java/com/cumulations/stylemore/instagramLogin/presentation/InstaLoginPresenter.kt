package com.cumulations.stylemore.instagramLogin.presentation

import com.cumulations.stylemore.instagramLogin.data.model.InstaLogin
import com.cumulations.stylemore.instagramLogin.domain.InstaLoginUseCase
import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.domain.LoginUseCase

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

interface InstaLoginPresenter {
    fun getInstaLoginResponse(instaLoginUseCase: InstaLoginUseCase, instaLogin: InstaLogin)
}
