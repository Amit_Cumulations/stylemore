package com.cumulations.stylemore.instagramLogin.presentation

import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.instagramLogin.data.model.InstaLogin
import com.cumulations.stylemore.instagramLogin.domain.InstaLoginUseCase
import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.domain.LoginUseCase
import com.cumulations.stylemore.rest.RestClient
import retrofit2.adapter.rxjava.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class InstaLoginPresenterImpl(var instagramApiUiUpdateView: InstagramApiUiUpdateView) : InstaLoginPresenter {

    override fun getInstaLoginResponse(instaLoginUseCase: InstaLoginUseCase, instaLogin: InstaLogin) {
        if (Utils.isOnline()){
            instagramApiUiUpdateView.showLoading()
            instaLoginUseCase.execute(instaLogin)
                    .subscribe({
                        instagramApiUiUpdateView.hideLoading()
                        if (it?.accessToken!=null) {
                            instagramApiUiUpdateView.onInstagramProfile(it)
                        }
                    }, {
                        instagramApiUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                instagramApiUiUpdateView.showError(RestClient.getInstagramHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                instagramApiUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                instagramApiUiUpdateView.showError(it.message!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            instagramApiUiUpdateView.showError(e.message!!)
                        }
                    })

        } else
            instagramApiUiUpdateView.showError(Constants.NO_INTERNET)
    }
}
