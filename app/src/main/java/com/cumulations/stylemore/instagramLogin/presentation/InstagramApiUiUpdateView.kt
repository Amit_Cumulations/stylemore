package com.cumulations.stylemore.instagramLogin.presentation

import com.cumulations.stylemore.base.LoadingView
import com.cumulations.stylemore.instagramLogin.data.model.InstaLoginResponse
import com.cumulations.stylemore.login.data.model.LoginResponse

/**
 * Created by Amit Tumkur on 16-02-2018.
 */

interface InstagramApiUiUpdateView : LoadingView {
    fun onInstagramProfile(instaLoginResponse: InstaLoginResponse)
    fun showError(error: String)
}
