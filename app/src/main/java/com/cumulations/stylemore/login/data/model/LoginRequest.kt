package com.cumulations.stylemore.login.data.model

/**
 * Created by Anirudh Uppunda on 17/11/17.
 */
data class LoginRequest(
        val name: String,
        val email: String,
        val fb_id: String,
        val insta_id: String,
        val profile_pic: String,
        var dob: String? = null,
        var gender: String? = null,
        val timezone: String)