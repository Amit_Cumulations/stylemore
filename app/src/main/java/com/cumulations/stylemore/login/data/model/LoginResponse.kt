package com.cumulations.stylemore.login.data.model

import com.google.gson.annotations.SerializedName

data class LoginResponse(@SerializedName("result")
                         val loginResponseResult: LoginResponseResult,
                         @SerializedName("status")
                         val status: String = "",
                         @SerializedName("description")
                         val description: String = "")