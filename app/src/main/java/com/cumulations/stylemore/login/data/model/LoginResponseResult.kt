package com.cumulations.stylemore.login.data.model

import com.google.gson.annotations.SerializedName

data class LoginResponseResult(@SerializedName("gender")
                               val gender: String = "",
                               @SerializedName("fb_id")
                               val fbId: String = "",
                               @SerializedName("user_id")
                               val userId: Int = 0,
                               @SerializedName("dob")
                               val dob: String = "",
                               @SerializedName("timezone")
                               val timezone: String = "",
                               @SerializedName("profile_pic")
                               val profilePic: String = "",
                               @SerializedName("name")
                               val name: String = "",
                               @SerializedName("insta_id")
                               val instaId: String = "",
                               @SerializedName("auth_key")
                               val authKey: String = "",
                               @SerializedName("email")
                               val email: String = "",
                               @SerializedName("img_url")
                               val img_url: String = "")