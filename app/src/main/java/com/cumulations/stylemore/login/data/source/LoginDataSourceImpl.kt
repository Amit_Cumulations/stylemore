package com.cumulations.stylemore.login.data.source

import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.data.model.LoginResponse
import com.cumulations.stylemore.login.domain.LoginDataInterface
import com.cumulations.stylemore.rest.RestClient
import rx.Single
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by Amit Tumkur on 29-05-2018.
 */
class LoginDataSourceImpl:LoginDataInterface {
    override fun loginUser(loginRequest: LoginRequest): Single<LoginResponse> {
        return RestClient.getApiService().login(loginRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }
}