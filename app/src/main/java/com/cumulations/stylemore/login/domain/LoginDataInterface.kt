package com.cumulations.stylemore.login.domain

import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.data.model.LoginResponse
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

interface LoginDataInterface {
    fun loginUser(loginRequest: LoginRequest): Single<LoginResponse>
}
