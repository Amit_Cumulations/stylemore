package com.cumulations.stylemore.login.domain

import com.cumulations.stylemore.base.UseCase
import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.data.model.LoginResponse
import com.cumulations.stylemore.login.data.source.LoginDataSourceImpl
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class LoginUseCase : UseCase<LoginRequest, LoginResponse>() {
    private val loginDataInterface = LoginDataSourceImpl()

    override fun buildUseCase(requestObj: LoginRequest): Single<LoginResponse> {
        return loginDataInterface.loginUser(requestObj)
    }
}
