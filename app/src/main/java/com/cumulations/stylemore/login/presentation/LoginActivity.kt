package com.cumulations.stylemore.login.presentation

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.PagerAdapter
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.cumulations.stylemore.BuildConfig
import com.cumulations.stylemore.R
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.firebase.FirebaseSyncService
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper.set
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.presentation.FeedListActivity
import com.cumulations.stylemore.instagramLogin.data.model.InstaLogin
import com.cumulations.stylemore.instagramLogin.data.model.InstaLoginResponse
import com.cumulations.stylemore.instagramLogin.presentation.InstaAuthorizeListener
import com.cumulations.stylemore.instagramLogin.presentation.InstaLoginDialog
import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.data.model.LoginResponse
import com.cumulations.stylemore.login.domain.LoginUseCase
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.iid.FirebaseInstanceId
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fb_login_layout.*
import kotlinx.android.synthetic.main.layout_full_screen_loader.*
import kotlinx.android.synthetic.main.instagram_login_layout.*
import org.jetbrains.anko.toast
import java.util.*


class LoginActivity : AppCompatActivity(),View.OnClickListener, LoginUiUpdateView {

    private lateinit var callbackManager: CallbackManager
    private lateinit var fbLoginManager: LoginManager
    //get default userPreferences
//    private lateinit var userPreferences:SharedPreferences
    private var fbId: String = ""
    private var email: String = ""
    private var name: String = ""
    private var bday: String? = null
    private var gender: String? = null
    private var profPicUrl: String = ""
    private var insta_id: String = ""

    private val drawables: IntArray = intArrayOf(
            R.drawable.login_bg_1,
            R.drawable.login_bg_2,
            R.drawable.login_bg_3
    )

    companion object {
       const val INSTA_LOGIN_REQUEST = 1001
    }

    private val CLIENT_ID = "de8c16100ddd48eda56efba4060c7902"
    private val CLIENT_SECRET = "cb29ae3a007a4399bc1e5e351c7a0d0c"
    private val REDIRECT_URI = "https://www.cumulations.com"

    var currentPage = 0
    val DELAY_MS: Long = 1000//delay in milliseconds before task is to be executed
    val PERIOD_MS: Long = 3000 // time in milliseconds between successive task executions.

    private val NUM_PAGES: Int = drawables.size

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

//        val userPreferences = SharedPreferenceHelper.customPrefs(this,Constants.USER_PREF)

        viewpager.adapter = LoginScreenViewPagerAdapter()
        pageIndicatorView.setViewPager(viewpager)

        fb_btn_layout.setOnClickListener(this)
        instagram_btn_layout.setOnClickListener(this)

//        initialiseFacebookSdk()
        initFbCallbacks();

        /*After setting the adapter use the timer */
        val handler = Handler()
        val Update = Runnable {
            if (currentPage == NUM_PAGES) {
                currentPage = 0
            }
            viewpager.setCurrentItem(currentPage++, true)
        }

        val timer = Timer() // This will create a new Thread
        timer.schedule(object : TimerTask() { // task to be scheduled

            override fun run() {
                handler.post(Update)
            }
        }, DELAY_MS, PERIOD_MS)
    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.fb_btn_layout -> {
                if (!Utils.isOnline()) {
                    toast(getString(R.string.no_internet_connection))
                } else {
                    facebookLogin()
                }
            }

            R.id.instagram_btn_layout -> {
                if (!Utils.isOnline()) {
                    toast(getString(R.string.no_internet_connection))
                } else {
                    instaLogin()
                }
            }
        }
    }

    private fun instaLogin() {
        val instaLogin = InstaLogin(CLIENT_ID,CLIENT_SECRET,"",Constants.INSTAGRAM_AUTH_URL,REDIRECT_URI,"")
        InstaLoginDialog().authorizeUser(this,instaLogin,object : InstaAuthorizeListener{
            override fun onRetrieveCode(code: String,instaLoginDialog:InstaLoginDialog) {
                instaLoginDialog.getInstagramProfile(code)
            }

            override fun onAuthError(error: String) {
                toast(error)
            }

            override fun onInstagramUser(instaLoginResponse: InstaLoginResponse) {
                val instagramUser = instaLoginResponse.user
                name = instagramUser.fullName
                email = "${instagramUser.username}@instagram.com"
                insta_id = instagramUser.id
                profPicUrl = instagramUser.profilePicture
                gender = "Male"
                bday = "1993-01-01"
                loginUser()
            }

            override fun onCancel() {
                toast("Login cancelled")
            }

        })
    }

    inner class LoginScreenViewPagerAdapter: PagerAdapter() {
        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view == `object`
        }


        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            // Get the view from pager page layout
            val view = LayoutInflater.from(container.context)
                    .inflate(R.layout.login_viewpager_layout,container,false)
            val backgroundIv:ImageView = view.findViewById(R.id.iv_background)
//            backgroundIv.setImageResource(drawables[position])
            Picasso.get().load(drawables[position])
                    .resize(0,720)
//                    .onlyScaleDown()
//                    .centerCrop()
                    .into(backgroundIv)
            container.addView(view)
            return view
        }

        override fun getCount(): Int {
            return drawables.size
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
        }

    }

    private fun initFbCallbacks() {
        fbLoginManager = LoginManager.getInstance()
        callbackManager = CallbackManager.Factory.create()
        fbLoginManager.registerCallback(callbackManager, getFbcallBackInstance())
//        Utils.logFbHashKey(this)
    }

    private fun getFbcallBackInstance(): FacebookCallback<LoginResult> {
        return object : FacebookCallback<LoginResult> {

            override fun onSuccess(loginResult: LoginResult) {
                val accessToken = loginResult.accessToken
                fetchFbData(accessToken)
            }

            override fun onCancel() {
                Log.d("FbcallBack","cancelled")
            }

            override fun onError(error: FacebookException) {
                Log.d("FbcallBack","error = "+error.toString())
            }
        }
    }

    private fun fetchFbData(accessToken: AccessToken) {
        val graphRequest = GraphRequest.newMeRequest(accessToken, getFacbookResponseCallbackInstance())
        val parameters = Bundle()
        parameters.putString("fields", "id,name,first_name,last_name,email,picture.type(large)")
        graphRequest.parameters = parameters
        graphRequest.executeAsync()
    }

    private fun getFacbookResponseCallbackInstance(): GraphRequest.GraphJSONObjectCallback {
        return GraphRequest.GraphJSONObjectCallback { `object`, response ->
            val jsonobject = response?.jsonObject
            val fbUserId = jsonobject?.optString("id")
            val fbFName = jsonobject?.optString("first_name")
            val fbName = jsonobject?.optString("name")
            val fbEmail = jsonobject?.optString("email")
            val fbLastName = jsonobject?.optString("last_name")
            val fbBirthday = jsonobject?.optString("birthday")
            val fbGender = jsonobject?.optString("gender")
            val fbProfPicUrl = jsonobject?.optJSONObject("picture")?.optJSONObject("data")?.optString("url")

            name = fbName!!
            email = fbEmail!!
            fbId = fbUserId!!
            gender = fbGender?.capitalize()
            if (fbProfPicUrl!=null)
                profPicUrl = fbProfPicUrl
            bday = /*fbBirthday!!*/Utils.getDateInFormat(fbBirthday!!,"MM/dd/yyyy","yyyy-MM-dd")

            Log.e("fb response", "$fbId,$email,$name,$gender,$bday,$profPicUrl,$fbFName")

            loginUser()
        }
    }

    private fun loginUser() {
        val loginPresenter:LoginPresenter = LoginPresenterImpl(this)
        val loginRequest = LoginRequest(name = name,email = email,fb_id = fbId,insta_id = insta_id,profile_pic = profPicUrl,
                /*dob = "1993-01-01",gender = "Male",*/ timezone = "asia/kolkata")
        loginPresenter.getLoginResponse(LoginUseCase(),loginRequest)
    }

    private fun facebookLogin() {
        val permissionList = ArrayList<String>()
        permissionList.add("email")
        permissionList.add("public_profile")
//        permissionList.add("user_birthday")
//        permissionList.add("user_gender")

        fbLoginManager.logInWithReadPermissions(this, permissionList)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode,resultCode,data)
    }

    override fun showLoading() {
        full_screen_loader_layout.visibility = View.VISIBLE
        Utils.showLoader(loader,null,this)
    }

    override fun hideLoading() {
        full_screen_loader_layout.visibility = View.GONE
        Utils.closeLoader(loader,null,this)
    }

    override fun provideLoginResponse(loginResponse: LoginResponse) {
        val loginResponseResult = loginResponse.loginResponseResult
        val userPreferences = SharedPreferenceHelper.customPrefs(this,Constants.USER_PREF)
        StyleMoreApp.auth_key = loginResponseResult.authKey
        StyleMoreApp.user_id = loginResponseResult.userId.toString()

        /*saving to pref*/
        userPreferences[Constants.USER_ID] = loginResponseResult.userId.toString()
        userPreferences[Constants.AUTH_KEY] = loginResponseResult.authKey
        userPreferences[Constants.USER_FULLNAME] = loginResponseResult.name
        userPreferences[Constants.USER_EMAIL] = loginResponseResult.email
        userPreferences[Constants.USER_GENDER] = loginResponseResult.gender
        userPreferences[Constants.USER_DOB] = loginResponseResult.dob
        userPreferences[Constants.USER_FB_ID] = loginResponseResult.fbId
        userPreferences[Constants.USER_PROF_PIC] = loginResponseResult.profilePic
        userPreferences[Constants.USER_LOGGED_IN] = true
        userPreferences[Constants.USER_IMAGE_BASE_URL] = loginResponseResult.img_url
        StyleMoreApp.IMAGE_BASE_URL = loginResponseResult.img_url

        if (StyleMoreApp.user_id.toInt()>0) {
            startService(Intent(applicationContext, FirebaseSyncService::class.java))
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
                Utils.updateTokenInFirebaseDb(it.token)
                userPreferences[Constants.FCM_TOKEN] = it.token
            }
            StyleMoreApp.fcmAuthKey = "key=${BuildConfig.FCM_SERVER_KEY}"
        }

        startActivity(Intent(this, FeedListActivity::class.java))
        finish()
    }

    override fun showError(error: String) {
        toast(error)
    }


}
