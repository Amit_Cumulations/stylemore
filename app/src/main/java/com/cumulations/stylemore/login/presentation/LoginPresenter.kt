package com.cumulations.stylemore.login.presentation

import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.domain.LoginUseCase

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

interface LoginPresenter {
    fun getLoginResponse(loginUseCase: LoginUseCase, loginRequest: LoginRequest)
}
