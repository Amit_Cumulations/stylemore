package com.cumulations.stylemore.login.presentation

import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.domain.LoginUseCase
import com.cumulations.stylemore.rest.RestClient
import retrofit2.adapter.rxjava.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class LoginPresenterImpl(var loginUiUpdateView: LoginUiUpdateView) : LoginPresenter {

    override fun getLoginResponse(loginUseCase: LoginUseCase, loginRequest: LoginRequest) {
        if (Utils.isOnline()){
            loginUiUpdateView.showLoading()
            loginUseCase.execute(loginRequest)
                    /*.subscribe { object : SingleSubscriber<LoginResponse>(){
                        override fun onSuccess(loginResponse: LoginResponse?) {
                            loginUiUpdateView.hideLoading()
                            if (loginResponse?.status.equals("success",true)){
                                loginUiUpdateView.provideLoginResponse(loginResponse!!)
                            } else if (loginResponse?.status.equals("error",true))
                                loginUiUpdateView.showError(loginResponse?.description!!)
                        }

                        override fun onError(error: Throwable) {
                            loginUiUpdateView.hideLoading()
                            try {
                                error.printStackTrace()
                                if (error is HttpException) {
                                    *//*val responseBody = error.response().errorBody()
                                    loginUiUpdateView.showError(RestClient.getErrorMessage(responseBody))*//*
                                    loginUiUpdateView.showError(error.message())
                                } else if (error is ConnectException || error is SocketTimeoutException
                                        || error is UnknownHostException) {
                                    loginUiUpdateView.showError(Constants.NETWORK_ERROR)
                                } else {
                                    loginUiUpdateView.showError(error.message!!)
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                                loginUiUpdateView.showError(*//*Constants.UNKNOWN_ERROR*//*e.message!!)
                            }
                        }

                    } }*/
                    .subscribe({
                        loginUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            loginUiUpdateView.provideLoginResponse(it!!)
                        } else if (it?.status.equals("error", true))
                            loginUiUpdateView.showError(it?.description!!)
                    }, {
                        loginUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                loginUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                loginUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                loginUiUpdateView.showError(it.message!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            loginUiUpdateView.showError(e.message!!)
                        }
                    })

        } else
            loginUiUpdateView.showError(Constants.NO_INTERNET)
    }

}
