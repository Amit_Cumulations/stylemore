package com.cumulations.stylemore.login.presentation

import com.cumulations.stylemore.base.LoadingView
import com.cumulations.stylemore.login.data.model.LoginResponse

/**
 * Created by Amit Tumkur on 16-02-2018.
 */

interface LoginUiUpdateView : LoadingView {
    fun provideLoginResponse(loginResponse: LoginResponse)
    fun showError(error: String)
}
