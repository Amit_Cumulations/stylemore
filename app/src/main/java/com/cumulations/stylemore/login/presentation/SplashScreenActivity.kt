package com.cumulations.stylemore.login.presentation

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper.get
import com.cumulations.stylemore.feeds.presentation.FeedListActivity

class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val userPref = SharedPreferenceHelper.customPrefs(this, Constants.USER_PREF)
        val isLoggedIn: Boolean? = userPref[Constants.USER_LOGGED_IN, false]
        if (!isLoggedIn!!) {
            startActivity(Intent(this, LoginActivity::class.java))
        } else
            startActivity(Intent(this, FeedListActivity::class.java))
        finish()
    }
}
