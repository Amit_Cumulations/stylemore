package com.cumulations.stylemore.notifications

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import com.cumulations.stylemore.R
import com.cumulations.stylemore.base.firebase.FbNotification
import com.cumulations.stylemore.base.firebase.FirebaseSyncListener
import com.cumulations.stylemore.base.firebase.FirebaseSyncService
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper.get
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.userProfile.presentation.UserProfileActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_notifications.*
import kotlinx.android.synthetic.main.layout_full_screen_loader.*
import org.jetbrains.anko.toast
import java.util.*

class NotificationsActivity : AppCompatActivity(), FirebaseSyncListener {
    private lateinit var notificationsAdapter: NotificationsAdapter
    private lateinit var fbNotificationList: LinkedList<FbNotification?>
    private var mBoundSyncService: FirebaseSyncService? = null
    private var isServiceBound = false
    private lateinit var dismissLoaderHandler: Handler
    private val dismissLoaderRunnable = Runnable {
        showLoader(false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!Utils.isOnline()){
            toast(getString(R.string.no_internet_connection))
            finish()
            return
        }

        setContentView(R.layout.activity_notifications)

        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        notificationsAdapter = NotificationsAdapter(this, LinkedList())
        val mLayoutManager = LinearLayoutManager(this)
        rv_notifications_list.layoutManager = mLayoutManager
        rv_notifications_list.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?
        rv_notifications_list.adapter = notificationsAdapter

        civ_profile_icon.setOnClickListener {
            startActivity(Intent(this, UserProfileActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK))
        }
    }

    override fun onStart() {
        super.onStart()
        bindService()
//        val notificationList = intent?.getSerializableExtra(Constants.NOTIFICATIONS_LIST) as ArrayList<FbNotification?>

        val userPref = SharedPreferenceHelper.customPrefs(this, Constants.USER_PREF)
        val profPicUrl = userPref[Constants.USER_PROF_PIC, ""]

        if (profPicUrl?.isNotEmpty()!!) {
            Picasso.get().load(profPicUrl)
                    .placeholder(R.drawable.user_placeholder_square)
                    .into(civ_profile_icon)
        }
        dismissLoaderHandler = Handler()
    }

    private fun updateList() {
        showLoader(true)
        dismissLoaderHandler.postDelayed(dismissLoaderRunnable,3000)
        fbNotificationList = LinkedList(getSortedNotificationList(fbNotificationList))
        if (fbNotificationList.size > 0) {
            notificationsAdapter.updateList(fbNotificationList)
            rv_notifications_list.visibility = View.VISIBLE
            tv_no_data.visibility = View.GONE
        } else {
            rv_notifications_list.visibility = View.GONE
            tv_no_data.text = "No notifications"
            tv_no_data.visibility = View.VISIBLE
        }
    }

    private fun getSortedNotificationList(fbNotificationList: LinkedList<FbNotification?>): List<FbNotification?> {
        return fbNotificationList.sortedWith(
                compareByDescending<FbNotification?> {
                    it?.timestamp
                }.thenByDescending {
                    it?.timestamp
                }
        )
    }

    private val mServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            mBoundSyncService = (service as FirebaseSyncService.FirebaseSyncBinder).getService()
            if (mBoundSyncService != null) {
                isServiceBound = true
                Log.e("mServiceConnection", "onServiceConnected-->service !null")
                mBoundSyncService?.setFbSyncListener(this@NotificationsActivity)
                fbNotificationList = mBoundSyncService!!.fbNotificationList
                updateList()
                mBoundSyncService?.refreshNotificatons()
            } else Log.e("mServiceConnection", "service null")
        }

        override fun onServiceDisconnected(className: ComponentName) {
            Log.e("mServiceConnection", "disconnected")
            isServiceBound = false
            mBoundSyncService = null
        }
    }

    override fun provideSyncedList(fbNotificationList: LinkedList<FbNotification?>) {
        if (!isFinishing) {
            this.fbNotificationList = fbNotificationList
            updateList()
        }
    }

    override fun provideNotificationCount(notificationCount: Int) {
    }

    override fun showLoader(loading: Boolean) {
        if (loading){
            full_screen_loader_layout.visibility = View.VISIBLE
            Utils.showLoader(loader, null, this)
        } else{
            full_screen_loader_layout.visibility = View.GONE
            Utils.closeLoader(loader, null, this)
        }
        Log.v("showLoader","$loading")
    }

    private fun bindService() {
        bindService(Intent(this, FirebaseSyncService::class.java),
                mServiceConnection, Context.BIND_AUTO_CREATE)
    }

    private fun unbindService() {
        if (isServiceBound) {
            if (mBoundSyncService != null) {
                mBoundSyncService!!.removeFbSyncListener()
            }
            unbindService(mServiceConnection)
            isServiceBound = false
        }
    }

    override fun onStop() {
        super.onStop()
        unbindService()
        dismissLoaderHandler.removeCallbacks(dismissLoaderRunnable)
    }
}
