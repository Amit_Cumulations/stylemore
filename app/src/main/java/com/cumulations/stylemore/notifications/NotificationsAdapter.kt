package com.cumulations.stylemore.notifications

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cumulations.stylemore.R
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.firebase.FbNotification
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.presentation.FeedDetailsActivity
import com.cumulations.stylemore.userProfile.presentation.UserProfileActivity
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_notifications.view.*
import java.util.*

class   NotificationsAdapter(val context: Context,
                           var notificationsList: LinkedList<FbNotification?>) : RecyclerView.Adapter<NotificationsAdapter.NotificationsItemViewHolder>() {

    private var following = false
    private val userIdListenerDbReference = FirebaseDatabase.getInstance().reference
            .child("Notifications")
            .child(StyleMoreApp.user_id)

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): NotificationsItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_notifications, parent, false)
        return NotificationsItemViewHolder(view)
    }


    override fun onBindViewHolder(notificationsItemViewHolder: NotificationsItemViewHolder, position: Int) {
        val fbNotification = notificationsList?.get(position)
        notificationsItemViewHolder.bindNotifications(fbNotification)
    }

    override fun getItemCount(): Int {
        return notificationsList?.size!!
    }

    inner class NotificationsItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindNotifications(fbNotification: FbNotification?) {
            Picasso.get().load(fbNotification?.fbActionUser?.profPicUrl)
                    .resize(64, 64)
                    .centerCrop()
                    .placeholder(R.drawable.user_placeholder_square)
                    .into(itemView.civ_user_icon)

            val timeAgo = Utils.getDateInRelativeTime(fbNotification?.timestamp!!)
            itemView.tv_notification_time.text = timeAgo

            var msg = ""
            when (fbNotification.actionName.toLowerCase()) {
                "liked" -> {
                    msg = "${fbNotification?.fbActionUser?.name} liked your feed."
                    if (fbNotification?.actionFeedPic?.isEmpty()) {
                        itemView.iv_feed_pic.visibility = View.GONE
                        return
                    }
                    itemView.iv_feed_pic.visibility = View.VISIBLE
                    Picasso.get().load(StyleMoreApp.IMAGE_BASE_URL + fbNotification.actionFeedPic)
                            .resize(64, 64)
                            .centerCrop()
                            .placeholder(R.drawable.placeholder_large)
                            .into(itemView.iv_feed_pic)
                }

                "commented" -> {
                    msg = "${fbNotification?.fbActionUser?.name} commented on your feed."
                    if (fbNotification?.actionFeedPic?.isEmpty()) {
                        itemView.iv_feed_pic.visibility = View.GONE
                        return
                    }
                    itemView.iv_feed_pic.visibility = View.VISIBLE
                    Picasso.get().load(StyleMoreApp.IMAGE_BASE_URL + fbNotification.actionFeedPic)
                            .resize(64, 64)
                            .centerCrop()
                            .placeholder(R.drawable.placeholder_large)
                            .into(itemView.iv_feed_pic)
                }

                "followed" -> {
                    msg = "${fbNotification?.fbActionUser?.name} started following you."
                    itemView.iv_feed_pic.visibility = View.GONE
                    following = true
                }
            }

            itemView.tv_notification_msg.text = msg

            /*Make notifications read/unread*/
            if (!fbNotification?.clicked) {
                itemView.ll_parent_notifications.setBackgroundColor(ContextCompat.getColor(context, R.color.border_color))
            } else {
                itemView.ll_parent_notifications.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent))
            }

            itemView.ll_notification_msg.setOnClickListener {

                /*update clicked value to firebase*/
                if (!fbNotification.clicked) {
                    fbNotification.clicked = true
                    Utils.updateClickedStatusToFirebaseDb(userIdListenerDbReference, fbNotification)
                }

                when (fbNotification.actionName.toLowerCase()) {
                    "liked", "commented" -> {
                        val intent = Intent(context, FeedDetailsActivity::class.java)
                        intent.putExtra(Constants.FEED_ID, fbNotification.actionFeedId.toInt())
                        intent.putExtra(Constants.FB_NOTIFICATION, fbNotification)
                        context.startActivity(intent)
                    }

                    "followed" -> {
                        itemView.civ_user_icon.performClick()
                    }
                }

            }

            itemView.civ_user_icon.setOnClickListener {
                context.startActivity(Intent(context, UserProfileActivity::class.java)
                        .putExtra(Constants.USER_ID, fbNotification?.fbActionUser.userId.toInt())
                        .putExtra(Constants.USER_FULLNAME, fbNotification?.fbActionUser.name)
                        .putExtra(Constants.USER_FOLLOWING, following)
                        .putExtra(Constants.USER_PROF_PIC, fbNotification?.fbActionUser.profPicUrl))
            }

            /*update seen status*/
            fbNotification?.seen = true
            Utils.updateSeenStatusToFirebaseDb(userIdListenerDbReference,fbNotification)
        }
    }

    fun updateList(notificationsList: LinkedList<FbNotification?>) {
        this.notificationsList = notificationsList
        notifyDataSetChanged()
    }

    fun addNotification(fbNotification: FbNotification?) {
        notificationsList?.add(fbNotification!!)
        notifyItemChanged(notificationsList?.indexOf(fbNotification)!!)
//        notifyDataSetChanged()
    }

}



