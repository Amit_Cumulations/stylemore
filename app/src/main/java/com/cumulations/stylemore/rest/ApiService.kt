package com.cumulations.stylemore.rest

import com.cumulations.stylemore.fcmPushNotificationSender.data.model.FcmPushPostRequest
import com.cumulations.stylemore.fcmPushNotificationSender.data.model.FcmPushPostResponse
import com.cumulations.stylemore.feeds.data.model.*
import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.data.model.LoginResponse
import com.cumulations.stylemore.feeds.data.model.AddCommentRequest
import com.cumulations.stylemore.feeds.data.model.LikeFeedResponse
import com.cumulations.stylemore.instagramLogin.data.model.InstaLoginResponse
import com.cumulations.stylemore.search.data.model.SearchRequest
import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse
import com.cumulations.stylemore.userProfile.data.model.*
import okhttp3.MultipartBody
import retrofit2.http.*
import rx.Single

interface ApiService {

    @POST("user/login/")
    fun login(@Body loginRequest: LoginRequest): Single<LoginResponse>

    @POST("feeds/new-feeds/")
    fun getNewFeeds(@HeaderMap headers: Map<String, String>,
                    @Body feedListRequest: FeedListRequest): Single<FeedsListResponse>

    @POST("feeds/hot-feeds/")
    fun getHotFeeds(@HeaderMap headers: Map<String, String>,
                    @Body feedListRequest: FeedListRequest): Single<FeedsListResponse>

    @POST("feeds/following-feeds/")
    fun getFollowingFeeds(@HeaderMap headers: Map<String, String>,
                          @Body feedListRequest: FeedListRequest): Single<FeedsListResponse>

    @POST("feeds/style-tip-feeds/")
    fun getStyleTipFeeds(@HeaderMap headers: Map<String, String>,
                         @Body feedListRequest: FeedListRequest): Single<FeedsListResponse>

    @POST("bVuSbZUDzC?indent=2")
    fun getUserEarlierPosts(@HeaderMap headers: Map<String, String>,
                            @Body earlierPostsRequest: EarlierPostsRequest): Single<MockEarlierFeedsResponse>

//    @POST("bVPqllrzLS?indent=2")
    /*@GET*/@POST("feeds/{feed_id}/likes/")
    fun getFeedLikes(@HeaderMap headers: Map<String, String>,
                     @Path("feed_id") feed_id:String): Single<FeedLikesResponse>

//    @POST("bVjyvxlcEi?indent=2")
    /*@GET*/@POST("feeds/{feed_id}/comments/")
    fun getFeedComments(@HeaderMap headers: Map<String, String>,
                        @Path("feed_id") feed_id:String): Single<FeedCommentsResponse>

    @POST("bVuSbZUDzC?indent=2")
    fun getUserActivities(@HeaderMap headers: Map<String, String>,
                          @Body userDetailRequest: UserDetailRequest): Single<MockEarlierFeedsResponse>

    @POST("bVuSbZUDzC?indent=2")
    fun getUserLikes(@HeaderMap headers: Map<String, String>,
                     @Body userDetailRequest: UserDetailRequest): Single<MockEarlierFeedsResponse>

    @POST("bVUERkGSgi?indent=2")
    fun getUserFollowers(@HeaderMap headers: Map<String, String>,
                         @Body userDetailRequest: UserDetailRequest): Single<FeedLikesResponse>

    @POST("bVUERkGSgi?indent=2")
    fun getUserFollowing(@HeaderMap headers: Map<String, String>,
                         @Body userDetailRequest: UserDetailRequest): Single<FeedLikesResponse>

    /*@GET*/@POST("feeds/{feed_id}/like/")
    fun addLikeToFeed(@HeaderMap headers: Map<String, String>,
                      @Path("feed_id") feed_id:String): Single<LikeFeedResponse>

    @POST("feeds/{feed_id}/add-comment/")
    fun addCommentToFeed(@HeaderMap headers: Map<String, String>,
                         @Path("feed_id") feed_id:String,
                         @Body addCommentRequest: AddCommentRequest): Single<LikeFeedResponse>

    //    @Streaming
    /*@Multipart
    @POST("feeds/add-feed/")
    fun uploadFeed(
            @HeaderMap headers: Map<String, String>,
            @PartMap multiPartList: Map<String, RequestBody>): Single<BasicResponse>*/

    @Multipart
    @POST("feeds/add-feed/")
    fun uploadFeed(
            @HeaderMap headers: Map<String, String>,
            @Part uploadPics: List<MultipartBody.Part>,
            @Part description: MultipartBody.Part,
            @Part emailId: MultipartBody.Part,
            @Part tags: MultipartBody.Part,
            @Part userId: MultipartBody.Part,
            @Part feedType: MultipartBody.Part): Single<BasicResponse>

    @Multipart
    @POST("feeds/update-feed/")
    fun updateFeed(
            @HeaderMap headers: Map<String, String>,
            @Part uploadPics: List<MultipartBody.Part>,
            @Part description: MultipartBody.Part,
            @Part emailId: MultipartBody.Part,
            @Part tags: MultipartBody.Part,
            @Part userId: MultipartBody.Part,
            @Part feedType: MultipartBody.Part?,
            @Part deleteImgs: MultipartBody.Part,
            @Part feedId: MultipartBody.Part): Single<BasicResponse>

    /*@GET*/@POST("feeds/{feed_id}/")
    fun getEarlierFeeds(@HeaderMap headers: Map<String, String>,
                      @Path("feed_id") feed_id:String): Single<EarlierPostResponse>

    /*@GET*/@POST("feeds/{feed_id}/delete/")
    fun deleteFeed(@HeaderMap headers: Map<String, String>,
                        @Path("feed_id") feed_id:String): Single<BasicResponse>

    @POST("user/profile/")
    fun getUserTab12(@HeaderMap headers: Map<String, String>,
                   @Body userTabRequest: UserTabRequest): Single<UserLikedResponse>

    @POST("user/profile/")
    fun getUserFollowers(@HeaderMap headers: Map<String, String>,
                         @Body userTabRequest: UserTabRequest): Single<UserFollowersResponse>

    @POST("user/profile/")
    fun getUserFollowing(@HeaderMap headers: Map<String, String>,
                         @Body userTabRequest: UserTabRequest): Single<UserFollowingResponse>

    /*@GET*/@POST("user/{follow_id}/follow/")
    fun followUnfollowUser(@HeaderMap headers: Map<String, String>,
                   @Path("follow_id") feed_id:String): Single<BasicResponse>

    @FormUrlEncoded
    @POST("access_token")
    fun getInstagramProfile(@Field("client_id") client_id: String?,
                            @Field("client_secret") client_secret: String?,
                            @Field("grant_type") grant_type: String?,
                            @Field("redirect_uri") redirect_uri: String?,
                            @Field("code") code: String?): Single<InstaLoginResponse>

    @POST("search/description/")
    fun searchDescription(@HeaderMap headers: Map<String, String>,
                          @Body searchRequest: SearchRequest): Single<FeedsListResponse>

    @POST("search/tags/")
    fun searchTags(@HeaderMap headers: Map<String, String>,
                   @Body searchRequest: SearchRequest): Single<FeedsListResponse>

    /*@GET*/@POST("feeds/{feed_id}/{comment_id}/delete-comment/")
    fun deleteComment(@HeaderMap headers: Map<String, String>,
                      @Path("feed_id") feed_id:String,
                      @Path("comment_id") comment_id:String): Single<BasicResponse>

    @POST("send")
    fun createAndSendPushNotification(@HeaderMap headers: Map<String, String>,
                          @Body fcmPushPostRequest: FcmPushPostRequest): Single<FcmPushPostResponse>
}