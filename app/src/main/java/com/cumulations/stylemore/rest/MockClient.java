package com.cumulations.stylemore.rest;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.CopyOnWriteArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by praveenkumar on 13/12/17.
 */

public class MockClient extends OkHttpClient {

    Context context;
    public MockClient(Context context){
        this.context=context;

    }
    @Override
    public Call newCall(Request request) {
        //super.newCall(request)
        return new TestCall(this,request,context);
    }



    class TestCall implements   Call{

        Request request;
        OkHttpClient client;
        Context mContext;

        TestCall(OkHttpClient client,Request request,Context context){
        this.request=request;
        this.client=client;
        mContext=context;

        }
        @Override
        public Request request() {
            return request;
        }

        @Override
        public Response execute() throws IOException {


            Gson gson = new GsonBuilder().setLenient().create();
            HttpUrl url = request().url();
            String sym = "";
            String query = url.encodedQuery() == null ? "" : url.encodedQuery();
            if (!query.equals(""))
                sym = "/";
            String path = url.encodedPath() + sym + query;
            path = path.substring(1).split("/")[1];
            String mBasePath="";
            String responseString = loadAssetTextAsString(mContext,
                    mBasePath + path + ".txt");


            return new Response.Builder()
                    .request(request)
                    .code(200)
                    .message("nothing")
                    .protocol(Protocol.HTTP_1_0)
                    .body(ResponseBody.create(MediaType.parse("application/json"), responseString))
                    .addHeader("content-type", "application/json")
                    .build();

        }

        @Override
        public void enqueue(Callback responseCallback) {

        }

        @Override
        public void cancel() {

        }

        @Override
        public boolean isExecuted() {
            return false;
        }

        @Override
        public boolean isCanceled() {
            return false;
        }

        @Override
        public Call clone() {
            return null;
        }



        public  String loadAssetTextAsString(Context context, String name) {
            BufferedReader in = null;
            try {
                StringBuilder buf = new StringBuilder();

                InputStream is = context.getAssets().open(name);
                in = new BufferedReader(new InputStreamReader(is));

                String str;
                boolean isFirst = true;
                while ((str = in.readLine()) != null) {
                    if (isFirst)
                        isFirst = false;
                    else
                        buf.append('\n');
                    buf.append(str);
                }
                return buf.toString();
            } catch (IOException e) {
                Log.e(context.getPackageName(), "Error opening asset " + name);
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        Log.e(context.getPackageName(), "Error closing asset " + name);
                    }
                }
            }
            return null;
        }
    }

}
