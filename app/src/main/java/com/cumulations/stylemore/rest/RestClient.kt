package com.cumulations.stylemore.rest

import android.util.Log
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.HttpErrorResponse
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.instagramLogin.data.model.InstaLoginError
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit


class RestClient {
    companion object {
        private var retrofit: Retrofit? = null

        val gson:Gson = GsonBuilder().setLenient().create()

        val logging = HttpLoggingInterceptor()

        val client = OkHttpClient.Builder()
//                .addInterceptor(provideOfflineCacheInterceptor())
//                .addNetworkInterceptor(provideCacheInterceptor())
                .addInterceptor(logging)
                .readTimeout(1,TimeUnit.MINUTES)
                .writeTimeout(1,TimeUnit.MINUTES)
                .connectTimeout(1,TimeUnit.MINUTES)
//                .connectionPool(ConnectionPool(100,5,TimeUnit.MINUTES))
//                .cache(provideCache())
                .build()

        fun getInstance(): Retrofit? {
            return retrofit
        }

        fun getApiService(): ApiService {
            logging.level = HttpLoggingInterceptor.Level.BODY

//            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                        .baseUrl(Constants.DJANGO_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .client(client)
                        .build()
//            }
            return retrofit!!.create<ApiService>(ApiService::class.java)
        }

        fun getApiService(baseUrl:String): ApiService {
            logging.level = HttpLoggingInterceptor.Level.BODY

            retrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(client)
                    .build()
            return retrofit!!.create<ApiService>(ApiService::class.java)
        }

        fun getJSONGeneratorApiService(): ApiService {
            logging.level = HttpLoggingInterceptor.Level.BODY

            retrofit = Retrofit.Builder()
                    .baseUrl(Constants.JSON_GENERATOR_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(client)
                    .build()
            return retrofit!!.create<ApiService>(ApiService::class.java)
        }

        fun getMockApiService(): ApiService {
            logging.level = HttpLoggingInterceptor.Level.BODY
            retrofit = Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(MockClient(StyleMoreApp.getContext()))
                    .build()
            return retrofit!!.create<ApiService>(ApiService::class.java)
        }

        private fun provideCache(): Cache? {
            var cache: Cache? = null
            try {
                cache = Cache(File(StyleMoreApp.getContext().cacheDir, "http-cache"),
                        (10 * 1024 * 1024).toLong()) // 10 MB
            } catch (e: Exception) {
                Log.e("OKHTTP", "Could not create Cache!")
            }

            return cache
        }

        private fun provideCacheInterceptor(): Interceptor {
            return Interceptor { chain ->
                val response = chain.proceed(chain.request())

                // re-write response header to force use of cache
                val cacheControl = CacheControl.Builder()
                        .maxAge(2, TimeUnit.MINUTES)
                        .build()

                response.newBuilder()
                        .header("Cache-Control", cacheControl.toString())
                        .removeHeader("Pragma")
                        .build()
            }
        }

        private fun provideOfflineCacheInterceptor(): Interceptor {
            return Interceptor { chain ->
                var request = chain.request()

                //if (!Utils.isOnline()) {
                val cacheControl = CacheControl.Builder()
                        .maxStale(7, TimeUnit.DAYS)
                        .build()

                request = request.newBuilder()
                        .cacheControl(cacheControl)
                        .removeHeader("Pragma")
                        .build()
                //}

                chain.proceed(request)
            }
        }

        fun getHttpErrorMessage(responseBody: ResponseBody?): String {
            try {
                val jsonObject = JSONObject(responseBody?.string())
                val errorBody = Gson().fromJson<HttpErrorResponse>(jsonObject.toString(), HttpErrorResponse::class.java)
                return errorBody.description
            } catch (e: Exception) {
                e.printStackTrace()
                return Constants.UNKNOWN_ERROR
            }
        }

        fun getInstagramHttpErrorMessage(responseBody: ResponseBody?): String {
            try {
                val jsonObject = JSONObject(responseBody?.string())
                val errorBody = Gson().fromJson<InstaLoginError>(jsonObject.toString(), InstaLoginError::class.java)
                return errorBody.errorMessage
            } catch (e: Exception) {
                e.printStackTrace()
                return Constants.UNKNOWN_ERROR
            }
        }

        fun getAuthHeaders() : Map<String,String>{
            val map = HashMap<String, String>()
            map[Constants.AUTH_KEY] = StyleMoreApp.auth_key
            map[Constants.USER_ID] = StyleMoreApp.user_id
//            map["Cache-Control"] = "no-cache"
//            map["Connection"] = "keep-alive"
//            map["timeStamp"] = System.currentTimeMillis().toString()
//            map["Keep-Alive"] = "timeout=5, max=100"
            return map
        }

        fun createRequestBody(uploadFile: File): RequestBody {
            return RequestBody.create(MediaType.parse("image/*"), uploadFile)
        }

        fun createRequestBody(paramValue: String): RequestBody {
            return RequestBody.create(MultipartBody.FORM, paramValue)
        }
    }

}