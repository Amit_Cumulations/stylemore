package com.cumulations.stylemore.search.data.model

import com.google.gson.annotations.SerializedName

data class SearchRequest(
        val description: String?,
        val tags: String?,
        @SerializedName("item_count")
        val itemCount: Int = 0,
        @SerializedName("index")
        val index: Int = 0)