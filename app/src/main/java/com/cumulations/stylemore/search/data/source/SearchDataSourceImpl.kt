package com.cumulations.stylemore.search.data.source

import com.cumulations.stylemore.feeds.data.model.FeedsListResponse
import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.data.model.LoginResponse
import com.cumulations.stylemore.login.domain.LoginDataInterface
import com.cumulations.stylemore.rest.RestClient
import com.cumulations.stylemore.search.data.model.SearchRequest
import com.cumulations.stylemore.search.domain.SearchDataInterface
import rx.Single
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by Amit Tumkur on 29-05-2018.
 */
class SearchDataSourceImpl:SearchDataInterface {
    override fun searchDescription(searchRequest: SearchRequest): Single<FeedsListResponse> {
        return RestClient.getApiService().searchDescription(RestClient.getAuthHeaders(),searchRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun searchTags(searchRequest: SearchRequest): Single<FeedsListResponse> {
        return RestClient.getApiService().searchTags(RestClient.getAuthHeaders(),searchRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }
}