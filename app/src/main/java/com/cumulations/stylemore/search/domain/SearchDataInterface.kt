package com.cumulations.stylemore.search.domain

import com.cumulations.stylemore.feeds.data.model.FeedsListResponse
import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.data.model.LoginResponse
import com.cumulations.stylemore.search.data.model.SearchRequest
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

interface SearchDataInterface {
    fun searchDescription(searchRequest: SearchRequest): Single<FeedsListResponse>
    fun searchTags(searchRequest: SearchRequest): Single<FeedsListResponse>
}
