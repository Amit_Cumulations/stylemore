package com.cumulations.stylemore.search.domain

import com.cumulations.stylemore.base.UseCase
import com.cumulations.stylemore.feeds.data.model.FeedsListResponse
import com.cumulations.stylemore.login.data.model.LoginRequest
import com.cumulations.stylemore.login.data.model.LoginResponse
import com.cumulations.stylemore.login.data.source.LoginDataSourceImpl
import com.cumulations.stylemore.search.data.model.SearchRequest
import com.cumulations.stylemore.search.data.source.SearchDataSourceImpl
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class SearchDescriptionUseCase : UseCase<SearchRequest, FeedsListResponse>() {
    private val searchDataInterface = SearchDataSourceImpl()

    override fun buildUseCase(requestObj: SearchRequest): Single<FeedsListResponse> {
        return searchDataInterface.searchDescription(requestObj)
    }
}
