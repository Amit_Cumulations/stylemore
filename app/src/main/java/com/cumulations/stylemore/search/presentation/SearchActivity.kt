package com.cumulations.stylemore.search.presentation

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import com.cumulations.stylemore.R
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.data.model.FeedsListResponse
import com.cumulations.stylemore.feeds.presentation.FeedCommentsDialogFragment
import com.cumulations.stylemore.feeds.presentation.FeedListAdapter
import com.cumulations.stylemore.login.presentation.SplashScreenActivity
import com.cumulations.stylemore.search.data.model.SearchRequest
import com.cumulations.stylemore.search.domain.SearchDescriptionUseCase
import com.cumulations.stylemore.search.domain.SearchTagsUseCase
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.layout_full_screen_loader.*
import org.jetbrains.anko.toast

class SearchActivity : AppCompatActivity(), SearchUiUpdateView, FeedCommentsDialogFragment.UpdateCommentCountListener {

    companion object {
        const val SEARCH_TAG = 125
        const val SEARCH_DESCR = 126
    }

    private lateinit var feedListAdapter: FeedListAdapter

    private var searchKey: String? = ""
    private var searchType: Int? = -1

    private var currentPageCount = 0
    private var isLastPage = false
    private var isPageLoading = false

    private lateinit var mLayoutManager: LinearLayoutManager
    private var pageSize = 10
    var totalLoadedFeeds = -1
    var prevClickedFeedPos = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        iv_close.setOnClickListener {
            et_search.setText("")
            et_search.post {
                et_search.setSelection(et_search.text.length)
            }
        }
    }

    private fun initAdapter(){
        feedListAdapter = FeedListAdapter(this, ArrayList(),null)
        feedListAdapter.setCommentCountListener(this)

        mLayoutManager = LinearLayoutManager(this)
        rv_search_results_list.layoutManager = mLayoutManager
        rv_search_results_list.itemAnimator = DefaultItemAnimator()
        rv_search_results_list.adapter = feedListAdapter

        rv_search_results_list.addOnScrollListener(recyclerViewOnScrollListener)

        swipe_refresh_layout.setOnRefreshListener {
            if (!Utils.isOnline()){
                toast(Constants.NO_INTERNET)
                swipe_refresh_layout.isRefreshing = false
                return@setOnRefreshListener
            }
            /*reload feed*/
            currentPageCount = 0
            isLastPage = false

            prevClickedFeedPos = -1
            totalLoadedFeeds = -1

            feedListAdapter.feedRecordList?.clear()
            feedListAdapter.notifyDataSetChanged()
            getSearchResults(searchType!!, et_search.text.toString())
        }
        swipe_refresh_layout.setColorSchemeColors(
                ContextCompat.getColor(this, R.color.insta_light_red),
                ContextCompat.getColor(this, R.color.insta_purple),
                ContextCompat.getColor(this, R.color.insta_orange))
    }

    private fun initSearchListeners(){
        searchType = intent?.getIntExtra(Constants.SEARCH_TYPE, -1)
        searchKey = intent?.getStringExtra(Constants.SEARCH_KEYWORD)

        if (searchKey != null && searchKey?.isNotEmpty()!!) {
            /*Search Screen opened by clicking on hashtags or BrandTags*/
            et_search.setText(searchKey)
            Utils.closeKeyboard(this, et_search)
            iv_close.visibility = View.VISIBLE
            et_search.post {
                et_search.setSelection(et_search.text.length)
                Handler().postDelayed({
                    getSearchResults(searchType!!, et_search.text.toString())
                }, 500)
            }
        } else if (et_search.text.toString().trim().isNotEmpty()){
            /*Search screen refreshed with user's manual input clearing intent extras any previously*/
            searchType = SEARCH_DESCR
            isLastPage = false
            feedListAdapter.feedRecordList?.clear()
            feedListAdapter.notifyDataSetChanged()
            getSearchResults(searchType!!, et_search.text.toString())
        } else {
            et_search.postDelayed({
                Utils.openKeyboard(this, et_search)
            }, 500)
        }

        et_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null && !p0.isEmpty()) {
                    iv_close.visibility = View.VISIBLE
                } else
                    iv_close.visibility = View.GONE
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })

        et_search.setOnEditorActionListener { textView, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                /*Clear intent extra values so when we come back to search screen so its Descr search and
                    intent doesn't give 1st time intent search type*/
                intent?.removeExtra(Constants.SEARCH_TYPE)
                intent?.removeExtra(Constants.SEARCH_KEYWORD)
                if (!et_search.text.isNullOrEmpty()) {
                    searchType = SEARCH_DESCR

                    currentPageCount = 0
                    isLastPage = false
                    totalLoadedFeeds = -1
                    prevClickedFeedPos = -1

                    feedListAdapter.feedRecordList?.clear()
                    feedListAdapter.notifyDataSetChanged()
                    getSearchResults(searchType!!, et_search.text.toString())
                } else {
                    toast("Search field cannot be empty")
                    swipe_refresh_layout.isRefreshing = false
                }

                true
            } else
                false
        }
    }

    override fun onStart() {
        super.onStart()
        /*To refresh when we come back to screen*/
        initAdapter()
        initSearchListeners()
    }

    override fun onPause() {
        super.onPause()
        Utils.closeKeyboard(this, et_search)
    }

    private fun getSearchResults(searchType: Int, keywords: String) {
        if (keywords.isEmpty()){
            toast("Search field cannot be empty")
            swipe_refresh_layout.isRefreshing = false
            return
        }

        Utils.closeKeyboard(this, et_search)

        searchKey = keywords
        val searchPresenter = SearchPresenterImpl(this)

        /*Check if previously loaded items count present*/
        if (totalLoadedFeeds>0 && prevClickedFeedPos>=0){

            if (totalLoadedFeeds in 1..10){
                pageSize = 10
            } else if (totalLoadedFeeds in 11..20){
                pageSize = 20
            } else if (totalLoadedFeeds in 21..30){
                pageSize = 30
            } else if (totalLoadedFeeds in 31..40){
                pageSize = 40
            } else if (totalLoadedFeeds in 41..50){
                pageSize = 50
            } else if (totalLoadedFeeds in 51..60){
                pageSize = 60
            } else if (totalLoadedFeeds in 61..70){
                pageSize = 70
            } else if (totalLoadedFeeds in 71..80){
                pageSize = 80
            }

            currentPageCount = 0

        }
        currentPageCount++
        when (searchType) {
            SEARCH_TAG -> {
                val searchRequest = SearchRequest(null, keywords/*.toLowerCase()*/,pageSize,currentPageCount)
                searchPresenter.getSearchTagsResponse(SearchTagsUseCase(), searchRequest)
            }

            SEARCH_DESCR -> {
                val searchRequest = SearchRequest(keywords/*.toLowerCase()*/, null,pageSize,currentPageCount)
                searchPresenter.getSearchDescriptionResponse(SearchDescriptionUseCase(), searchRequest)
            }
        }
    }

    override fun showLoading() {
        if (this.isFinishing)
            return
        if (feedListAdapter == null || feedListAdapter.itemCount == 0) {
            full_screen_loader_layout.visibility = View.VISIBLE
        }
        isPageLoading = true
        Utils.showLoader(loader, null, this)
    }

    override fun hideLoading() {
        if (this.isFinishing)
            return
        isPageLoading = false
        full_screen_loader_layout.visibility = View.GONE
        Utils.closeLoader(loader, null, this)
    }

    override fun provideSearchResponse(feedsListResponse: FeedsListResponse) {
        if (isFinishing)
            return
        swipe_refresh_layout.isRefreshing = false
        tv_no_feed.visibility = View.GONE
        rv_search_results_list.visibility = View.VISIBLE

        val itemList = feedsListResponse.result!!
        feedListAdapter.updateList(itemList)

        if (feedsListResponse.result.size < pageSize) {
            isLastPage = true
//            toast("No further feeds")
        }

//        totalLoadedFeeds = feedListAdapter.feedRecordList?.size!!
        if (prevClickedFeedPos>=0){
            rv_search_results_list.scrollToPosition(prevClickedFeedPos)
            /*prevPos is not -1 it means page size is altered so reset*/
            currentPageCount = (pageSize/10)
            pageSize = 10
            totalLoadedFeeds = -1
            prevClickedFeedPos = -1
        }
    }

    override fun showError(error: String) {
        if (isFinishing)
            return
        toast(error)
        if (error == Constants.INVALID_AUTH_KEY) {
            SharedPreferenceHelper.clearCustomPrefs(this, Constants.USER_PREF)
            startActivity(Intent(this, SplashScreenActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
            finish()
        }

        if (error == Constants.NO_DATA_FOUND) {
            if (currentPageCount == 1) {
                tv_no_feed.visibility = View.VISIBLE
                rv_search_results_list.visibility = View.GONE
            } else if (!isPageLoading){
                isLastPage = true
                toast("No further feeds")
            }
        }
    }

    override fun incrementCommentCount(updateItemPosition: Int) {
        val resultItem = feedListAdapter?.feedRecordList?.get(updateItemPosition)
        if (resultItem != null) {
            resultItem.cCount += 1
            feedListAdapter.notifyItemChanged(updateItemPosition)
        }
    }

    override fun decrementCommentCount(updateItemPosition: Int) {
        val resultItem = feedListAdapter?.feedRecordList?.get(updateItemPosition)
        if (resultItem != null) {
            if (resultItem.cCount > 0) {
                resultItem.cCount -= 1
                feedListAdapter.notifyItemChanged(updateItemPosition)
            }
        }
    }

    private val recyclerViewOnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val visibleItemCount = mLayoutManager.childCount
            val totalItemCount = mLayoutManager.itemCount
            val firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition()

            if (isPageLoading || isLastPage || swipe_refresh_layout.isRefreshing)
                return

            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                    && totalItemCount >= pageSize) {
                getSearchResults(searchType!!,et_search.text.toString())
            }
        }
    }

    override fun onStop() {
        super.onStop()
        currentPageCount = 0
        isLastPage = false
//        totalLoadedFeeds = -1
//        prevClickedFeedPos = -1
    }
}
