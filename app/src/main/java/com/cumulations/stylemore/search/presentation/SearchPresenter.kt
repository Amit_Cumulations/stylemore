package com.cumulations.stylemore.search.presentation

import com.cumulations.stylemore.search.data.model.SearchRequest
import com.cumulations.stylemore.search.domain.SearchDescriptionUseCase
import com.cumulations.stylemore.search.domain.SearchTagsUseCase

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

interface SearchPresenter {
    fun getSearchDescriptionResponse(searchDescriptionUseCase: SearchDescriptionUseCase, searchRequest: SearchRequest)
    fun getSearchTagsResponse(searchTagsUseCase: SearchTagsUseCase, searchRequest: SearchRequest)
}
