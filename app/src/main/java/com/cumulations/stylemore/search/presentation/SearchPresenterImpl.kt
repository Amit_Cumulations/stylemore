package com.cumulations.stylemore.search.presentation

import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.rest.RestClient
import com.cumulations.stylemore.search.data.model.SearchRequest
import com.cumulations.stylemore.search.domain.SearchDescriptionUseCase
import com.cumulations.stylemore.search.domain.SearchTagsUseCase
import retrofit2.adapter.rxjava.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class SearchPresenterImpl(var searchUiUpdateView: SearchUiUpdateView) : SearchPresenter {

    override fun getSearchDescriptionResponse(searchDescriptionUseCase: SearchDescriptionUseCase, searchRequest: SearchRequest) {
        if (Utils.isOnline()){
            searchUiUpdateView.showLoading()
            searchDescriptionUseCase.execute(searchRequest)
                    .subscribe({
                        searchUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            searchUiUpdateView.provideSearchResponse(it!!)
                        } else if (it?.status.equals("error", true))
                            searchUiUpdateView.showError(it?.description!!)
                    }, {
                        searchUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                searchUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                searchUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                searchUiUpdateView.showError(it.message!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            searchUiUpdateView.showError(e.message!!)
                        }
                    })

        } else
            searchUiUpdateView.showError(Constants.NO_INTERNET)
    }

    override fun getSearchTagsResponse(searchTagsUseCase: SearchTagsUseCase, searchRequest: SearchRequest) {
        if (Utils.isOnline()){
            searchUiUpdateView.showLoading()
            searchTagsUseCase.execute(searchRequest)
                    .subscribe({
                        searchUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            searchUiUpdateView.provideSearchResponse(it!!)
                        } else if (it?.status.equals("error", true))
                            searchUiUpdateView.showError(it?.description!!)
                    }, {
                        searchUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                searchUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                searchUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                searchUiUpdateView.showError(it.message!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            searchUiUpdateView.showError(e.message!!)
                        }
                    })

        } else
            searchUiUpdateView.showError(Constants.NO_INTERNET)
    }
}
