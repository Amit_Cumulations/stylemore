package com.cumulations.stylemore.search.presentation

import com.cumulations.stylemore.base.LoadingView
import com.cumulations.stylemore.feeds.data.model.FeedsListResponse
import com.cumulations.stylemore.login.data.model.LoginResponse

/**
 * Created by Amit Tumkur on 16-02-2018.
 */

interface SearchUiUpdateView : LoadingView {
    fun provideSearchResponse(feedsListResponse: FeedsListResponse)
    fun showError(error: String)
}
