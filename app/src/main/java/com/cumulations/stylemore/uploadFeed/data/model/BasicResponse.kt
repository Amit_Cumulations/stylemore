package com.cumulations.stylemore.uploadFeed.data.model

import com.google.gson.annotations.SerializedName

data class BasicResponse(@SerializedName("result")
                         val result: String = "",
                         @SerializedName("status")
                         val status: String = "",
                         @SerializedName("description")
                         val description: String = "")