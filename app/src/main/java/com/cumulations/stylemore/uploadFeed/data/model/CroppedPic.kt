package com.cumulations.stylemore.uploadFeed.data.model

import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.drawable.Drawable
import android.view.ScaleGestureDetector
import java.io.Serializable

/**
 * Created by Amit Tumkur on 20-07-2018.
 */
data class CroppedPic(var fileUri:String?,
                      var snappedToCentre:Boolean = false,
                      var originalBitmap:Bitmap?,
                      var scaledBitmap:Bitmap?,
                      var croppedBitmap:Bitmap?,
                      var matrix: Matrix?,
                      var croppedPicUri:String?):Serializable