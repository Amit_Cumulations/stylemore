package com.cumulations.stylemore.uploadFeed.data.model

import java.io.Serializable

/**
 * Created by Amit Tumkur on 25-06-2018.
 */
data class FeedData(val images:ArrayList<String>,
                    val description: String,
                    val tags: String,
                    val feed_type:String?,
                    val feed_id:String?,
                    val delete_img: String?):Serializable