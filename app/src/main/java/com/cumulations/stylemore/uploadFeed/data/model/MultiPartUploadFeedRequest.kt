package com.cumulations.stylemore.uploadFeed.data.model

import com.google.gson.annotations.SerializedName
import okhttp3.MultipartBody
import okhttp3.RequestBody

/**
 * Created by Amit Tumkur on 19-06-2018.
 */
data class MultiPartUploadFeedRequest(val multiPartList: List<MultipartBody.Part>,
                                      val description: MultipartBody.Part,
                                      val emailId: MultipartBody.Part,
                                      val tags: MultipartBody.Part,
                                      val user_id: MultipartBody.Part,
                                      val feed_type: MultipartBody.Part)