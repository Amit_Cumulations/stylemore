package com.cumulations.stylemore.uploadFeed.data.source

import com.cumulations.stylemore.rest.RestClient
import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse
import com.cumulations.stylemore.uploadFeed.data.model.MultiPartUpdateFeedRequest
import com.cumulations.stylemore.uploadFeed.data.model.MultiPartUploadFeedRequest
import com.cumulations.stylemore.uploadFeed.domain.UploadFeedDataInterface
import rx.Single
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by Amit Tumkur on 29-05-2018.
 */
class UploadFeedDataSourceImpl:UploadFeedDataInterface {

    override fun uploadFeed(multiPartUploadFeedRequest: MultiPartUploadFeedRequest): Single<BasicResponse> {
        return RestClient.getApiService().uploadFeed(RestClient.getAuthHeaders(),
                multiPartUploadFeedRequest.multiPartList,
                multiPartUploadFeedRequest.description,
                multiPartUploadFeedRequest.emailId,
                multiPartUploadFeedRequest.tags,
                multiPartUploadFeedRequest.user_id,
                multiPartUploadFeedRequest.feed_type)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun updateFeed(multiPartUpdateFeedRequest: MultiPartUpdateFeedRequest): Single<BasicResponse> {
        return RestClient.getApiService().updateFeed(RestClient.getAuthHeaders(),
                multiPartUpdateFeedRequest.multiPartList,
                multiPartUpdateFeedRequest.description,
                multiPartUpdateFeedRequest.emailId,
                multiPartUpdateFeedRequest.tags,
                multiPartUpdateFeedRequest.user_id,
                multiPartUpdateFeedRequest.feed_type,
                multiPartUpdateFeedRequest.delete_img,
                multiPartUpdateFeedRequest.feed_id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }
}