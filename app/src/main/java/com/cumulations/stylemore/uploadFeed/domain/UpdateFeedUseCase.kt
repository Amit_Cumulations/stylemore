package com.cumulations.stylemore.uploadFeed.domain

import com.cumulations.stylemore.base.UseCase
import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse
import com.cumulations.stylemore.uploadFeed.data.model.MultiPartUpdateFeedRequest
import com.cumulations.stylemore.uploadFeed.data.model.MultiPartUploadFeedRequest
import com.cumulations.stylemore.uploadFeed.data.source.UploadFeedDataSourceImpl
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class UpdateFeedUseCase : UseCase<MultiPartUpdateFeedRequest, BasicResponse>() {
    private val uploadFeedDataInterface = UploadFeedDataSourceImpl()

    override fun buildUseCase(requestObj: MultiPartUpdateFeedRequest): Single<BasicResponse> {
        return uploadFeedDataInterface.updateFeed(requestObj)
    }
}
