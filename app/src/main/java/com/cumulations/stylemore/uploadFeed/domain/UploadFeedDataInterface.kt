package com.cumulations.stylemore.uploadFeed.domain

import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse
import com.cumulations.stylemore.uploadFeed.data.model.MultiPartUpdateFeedRequest
import com.cumulations.stylemore.uploadFeed.data.model.MultiPartUploadFeedRequest
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

interface UploadFeedDataInterface {
    fun uploadFeed(multiPartUploadFeedRequest: MultiPartUploadFeedRequest): Single<BasicResponse>
    fun updateFeed(multiPartUpdateFeedRequest: MultiPartUpdateFeedRequest): Single<BasicResponse>
}
