package com.cumulations.stylemore.uploadFeed.domain

import com.cumulations.stylemore.base.UseCase
import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse
import com.cumulations.stylemore.uploadFeed.data.model.MultiPartUploadFeedRequest
import com.cumulations.stylemore.uploadFeed.data.source.UploadFeedDataSourceImpl
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class UploadFeedUseCase : UseCase<MultiPartUploadFeedRequest, BasicResponse>() {
    private val uploadFeedDataInterface = UploadFeedDataSourceImpl()

    override fun buildUseCase(requestObj: MultiPartUploadFeedRequest): Single<BasicResponse> {
        return uploadFeedDataInterface.uploadFeed(requestObj)
    }
}
