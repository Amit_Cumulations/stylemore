package com.cumulations.stylemore.uploadFeed.presentation

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.View
import android.view.ViewGroup
import com.cumulations.stylemore.R
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper.get
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.data.model.TagsItem
import kotlinx.android.synthetic.main.dialog_add_tags.*
import java.util.*


/**
 * Created by Amit Tumkur on 05-06-2018.
 */
class AddTagsDialogFragment: DialogFragment(), View.OnClickListener,UpdateBrandDlgListener {

    private var selectedTags:LinkedList<TagsItem> = LinkedList()
    private lateinit var updateDialogTagsListener: UpdateDialogTagsListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        val dialog = Dialog(activity!!, R.style.CustomDialogTheme)
        dialog.setContentView(R.layout.dialog_add_tags)
        dialog.setCanceledOnTouchOutside(false)
        isCancelable = true
        dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        val userPref = SharedPreferenceHelper.customPrefs(activity!!, Constants.USER_PREF)
        val profPicUrl = userPref[Constants.USER_PROF_PIC,""]

        dialog.iv_tag_top.setOnClickListener(this)
        dialog.iv_tag_pants.setOnClickListener(this)
        dialog.iv_tag_dress.setOnClickListener(this)
        dialog.iv_tag_accessory.setOnClickListener(this)
        dialog.iv_tag_shoes.setOnClickListener(this)
        dialog.iv_tag_bag.setOnClickListener(this)
        dialog.iv_tag_skirt.setOnClickListener(this)
        dialog.iv_tag_watch.setOnClickListener(this)
        dialog.iv_tag_eyewear.setOnClickListener(this)
        dialog.iv_tag_other.setOnClickListener(this)

        dialog.btn_tag_done.setOnClickListener(this)

        return dialog
    }

    override fun onResume() {
        super.onResume()
        if (selectedTags.size>0){
            initTags()
        }
    }

    private fun initTags() {
        for (tagsItem in selectedTags){
            when(tagsItem.tag.toLowerCase()){
                "top"->{
                    dialog.iv_tag_top.isChecked = true
                    dialog.iv_tag_top.setImageResource(R.drawable.ic_white_top)
                    dialog.ll_tag_top.visibility = View.VISIBLE
                    dialog.et_tag_top.setText(tagsItem.brand)
                    dialog.et_tag_top.post { Runnable { dialog.et_tag_top.setSelection(dialog.et_tag_top.text.toString().length) } }
                }

                "pants"->{
                    dialog.iv_tag_pants.isChecked = true
                    dialog.iv_tag_pants.setImageResource(R.drawable.ic_white_pants)
                    dialog.ll_tag_pants.visibility = View.VISIBLE
                    dialog.et_tag_pants.setText(tagsItem.brand)
                    dialog.et_tag_pants.post { Runnable { dialog.et_tag_pants.setSelection(dialog.et_tag_pants.text.toString().length) } }
                }

                "dress"->{
                    dialog.iv_tag_dress.isChecked = true
                    dialog.iv_tag_dress.setImageResource(R.drawable.ic_white_dress)
                    dialog.ll_tag_dress.visibility = View.VISIBLE
                    dialog.et_tag_dress.setText(tagsItem.brand)
                }

                "accessory"->{
                    dialog.iv_tag_accessory.isChecked = true
                    dialog.iv_tag_accessory.setImageResource(R.drawable.ic_white_accessory)
                    dialog.ll_tag_accessory.visibility = View.VISIBLE
                    dialog.et_tag_accessory.setText(tagsItem.brand)
                }

                "shoes"->{
                    dialog.iv_tag_shoes.isChecked = true
                    dialog.iv_tag_shoes.setImageResource(R.drawable.ic_white_shoes)
                    dialog.ll_tag_shoes.visibility = View.VISIBLE
                    dialog.et_tag_shoes.setText(tagsItem.brand)
                }

                "bag"->{
                    dialog.iv_tag_bag.isChecked = true
                    dialog.iv_tag_bag.setImageResource(R.drawable.ic_white_bag)
                    dialog.ll_tag_bags.visibility = View.VISIBLE
                    dialog.et_tag_bag.setText(tagsItem.brand)
                }

                "skirt"->{
                    dialog.iv_tag_skirt.isChecked = true
                    dialog.iv_tag_skirt.setImageResource(R.drawable.ic_white_skirt)
                    dialog.ll_tag_skirt.visibility = View.VISIBLE
                    dialog.et_tag_skirt.setText(tagsItem.brand)
                }

                "watch"->{
                    dialog.iv_tag_watch.isChecked = true
                    dialog.iv_tag_watch.setImageResource(R.drawable.ic_white_watch)
                    dialog.ll_tag_watch.visibility = View.VISIBLE
                    dialog.et_tag_watch.setText(tagsItem.brand)
                }

                "eyewear"->{
                    dialog.iv_tag_eyewear.isChecked = true
                    dialog.iv_tag_eyewear.setImageResource(R.drawable.ic_white_eyewear)
                    dialog.ll_tag_eyewear.visibility = View.VISIBLE
                    dialog.et_tag_eyewear.setText(tagsItem.brand)
                }

                "other"->{
                    dialog.iv_tag_other.isChecked = true
                    dialog.iv_tag_other.setImageResource(R.drawable.ic_white_others)
                    dialog.ll_tag_other.visibility = View.VISIBLE
                    dialog.et_tag_other.setText(tagsItem.brand)
                }
            }
        }
    }

    fun setUpdateDialogTagListener(updateDialogTagsListener: UpdateDialogTagsListener){
        this.updateDialogTagsListener = updateDialogTagsListener
    }

    fun provideSelectedTags(selectedTagsList:LinkedList<TagsItem>){
        selectedTags = selectedTagsList
    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.iv_tag_top ->{
                if (dialog.iv_tag_top.isChecked){
                    dialog.iv_tag_top.isChecked = false
                    dialog.iv_tag_top.setImageResource(R.drawable.ic_red_top)

                    dialog.ll_tag_top.visibility = View.GONE
                } else{
                    dialog.iv_tag_top.isChecked = true
                    dialog.iv_tag_top.setImageResource(R.drawable.ic_white_top)
//                    dialog.ll_tag_top.visibility = View.VISIBLE
                    showEnterBrandDialog("Top")
                }
            }

            R.id.iv_tag_pants ->{
                if (dialog.iv_tag_pants.isChecked){
                    dialog.iv_tag_pants.isChecked = false
                    dialog.iv_tag_pants.setImageResource(R.drawable.ic_red_pants)

                    dialog.ll_tag_pants.visibility = View.GONE
                } else{
                    dialog.iv_tag_pants.isChecked = true
                    dialog.iv_tag_pants.setImageResource(R.drawable.ic_white_pants)
//                    dialog.ll_tag_pants.visibility = View.VISIBLE
                    showEnterBrandDialog("Pants")
                }
            }

            R.id.iv_tag_dress ->{
                if (dialog.iv_tag_dress.isChecked){
                    dialog.iv_tag_dress.isChecked = false
                    dialog.iv_tag_dress.setImageResource(R.drawable.ic_red_dress)

                    dialog.ll_tag_dress.visibility = View.GONE
                } else{
                    dialog.iv_tag_dress.isChecked = true
                    dialog.iv_tag_dress.setImageResource(R.drawable.ic_white_dress)
//                    dialog.ll_tag_dress.visibility = View.VISIBLE
                    showEnterBrandDialog("Dress")
                }
            }

            R.id.iv_tag_accessory ->{
                if (dialog.iv_tag_accessory.isChecked){
                    dialog.iv_tag_accessory.isChecked = false
                    dialog.iv_tag_accessory.setImageResource(R.drawable.ic_red_accessory)

                    dialog.ll_tag_accessory.visibility = View.GONE
                } else{
                    dialog.iv_tag_accessory.isChecked = true
                    dialog.iv_tag_accessory.setImageResource(R.drawable.ic_white_accessory)
//                    dialog.ll_tag_accessory.visibility = View.VISIBLE
                    showEnterBrandDialog("Accessory")
                }
            }

            R.id.iv_tag_shoes ->{
                if (dialog.iv_tag_shoes.isChecked){
                    dialog.iv_tag_shoes.isChecked = false
                    dialog.iv_tag_shoes.setImageResource(R.drawable.ic_red_shoes)

                    dialog.ll_tag_shoes.visibility = View.GONE
                } else{
                    dialog.iv_tag_shoes.isChecked = true
                    dialog.iv_tag_shoes.setImageResource(R.drawable.ic_white_shoes)
                    //                    dialog.ll_tag_accessory.visibility = View.VISIBLE
                    showEnterBrandDialog("Shoes")
                }
            }

            R.id.iv_tag_bag ->{
                if (dialog.iv_tag_bag.isChecked){
                    dialog.iv_tag_bag.isChecked = false
                    dialog.iv_tag_bag.setImageResource(R.drawable.ic_red_bag)

                    dialog.ll_tag_bags.visibility = View.GONE
                } else{
                    dialog.iv_tag_bag.isChecked = true
                    dialog.iv_tag_bag.setImageResource(R.drawable.ic_white_bag)
                    //                    dialog.ll_tag_accessory.visibility = View.VISIBLE
                    showEnterBrandDialog("Bag")
                }
            }

            R.id.iv_tag_skirt ->{
                if (dialog.iv_tag_skirt.isChecked){
                    dialog.iv_tag_skirt.isChecked = false
                    dialog.iv_tag_skirt.setImageResource(R.drawable.ic_red_skirt)

                    dialog.ll_tag_skirt.visibility = View.GONE
                } else{
                    dialog.iv_tag_skirt.isChecked = true
                    dialog.iv_tag_skirt.setImageResource(R.drawable.ic_white_skirt)
                    //                    dialog.ll_tag_accessory.visibility = View.VISIBLE
                    showEnterBrandDialog("Skirt")
                }
            }

            R.id.iv_tag_watch ->{
                if (dialog.iv_tag_watch.isChecked){
                    dialog.iv_tag_watch.isChecked = false
                    dialog.iv_tag_watch.setImageResource(R.drawable.ic_red_watch)

                    dialog.ll_tag_watch.visibility = View.GONE
                } else{
                    dialog.iv_tag_watch.isChecked = true
                    dialog.iv_tag_watch.setImageResource(R.drawable.ic_white_watch)
                    //                    dialog.ll_tag_accessory.visibility = View.VISIBLE
                    showEnterBrandDialog("Watch")
                }
            }

            R.id.iv_tag_eyewear ->{
                if (dialog.iv_tag_eyewear.isChecked){
                    dialog.iv_tag_eyewear.isChecked = false
                    dialog.iv_tag_eyewear.setImageResource(R.drawable.ic_red_eyewear)

                    dialog.ll_tag_eyewear.visibility = View.GONE
                } else{
                    dialog.iv_tag_eyewear.isChecked = true
                    dialog.iv_tag_eyewear.setImageResource(R.drawable.ic_white_eyewear)
                    //                    dialog.ll_tag_accessory.visibility = View.VISIBLE
                    showEnterBrandDialog("Eyewear")
                }
            }

            R.id.iv_tag_other ->{
                if (dialog.iv_tag_other.isChecked){
                    dialog.iv_tag_other.isChecked = false
                    dialog.iv_tag_other.setImageResource(R.drawable.ic_red_others)

                    dialog.ll_tag_other.visibility = View.GONE
                } else{
                    dialog.iv_tag_other.isChecked = true
                    dialog.iv_tag_other.setImageResource(R.drawable.ic_white_others)
                    //                    dialog.ll_tag_accessory.visibility = View.VISIBLE
                    showEnterBrandDialog("Other")
                }
            }

            R.id.btn_tag_done ->{
                Utils.closeKeyboard(activity!!,p0)
                if (validateSelectedTagsBrandName()){
                    updateDialogTagsListener.updateTags(selectedTags)
                    dismiss()
                }
            }
        }
    }

    private fun validateSelectedTagsBrandName():Boolean{
        dialog.et_tag_top.error = null
        dialog.et_tag_pants.error = null
        dialog.et_tag_dress.error = null
        dialog.et_tag_accessory.error = null
        dialog.et_tag_shoes.error = null
        dialog.et_tag_bag.error = null
        dialog.et_tag_skirt.error = null
        dialog.et_tag_watch.error = null
        dialog.et_tag_eyewear.error = null
        dialog.et_tag_other.error = null

        selectedTags.clear()

        if (dialog.iv_tag_top.isChecked){
            if (dialog.et_tag_top.text.isNullOrEmpty()){
                dialog.et_tag_top.error = "Enter brand name"
                dialog.et_tag_top.requestFocus()
                return false
            }

            val tagsItem = TagsItem("Top",dialog.et_tag_top.text.toString(),false)
            selectedTags.add(tagsItem)
        }

        if (dialog.iv_tag_pants.isChecked){
            if (dialog.et_tag_pants.text.isNullOrEmpty()){
                dialog.et_tag_pants.error = "Enter brand name"
                dialog.et_tag_pants.requestFocus()
                return false
            }

            val tagsItem = TagsItem("Pants",dialog.et_tag_pants.text.toString(),false)
            selectedTags.add(tagsItem)
        }

        if (dialog.iv_tag_dress.isChecked){
            if (dialog.et_tag_dress.text.isNullOrEmpty()){
                dialog.et_tag_dress.error = "Enter brand name"
                dialog.et_tag_dress.requestFocus()
                return false
            }

            val tagsItem = TagsItem("Dress",dialog.et_tag_dress.text.toString(),false)
            selectedTags.add(tagsItem)
        }

        if (dialog.iv_tag_accessory.isChecked){
            if (dialog.et_tag_accessory.text.isNullOrEmpty()){
                dialog.et_tag_accessory.error = "Enter brand name"
                dialog.et_tag_accessory.requestFocus()
                return false
            }

            val tagsItem = TagsItem("Accessory",dialog.et_tag_accessory.text.toString(),false)
            selectedTags.add(tagsItem)
        }

        if (dialog.iv_tag_shoes.isChecked){
            if (dialog.et_tag_shoes.text.isNullOrEmpty()){
                dialog.et_tag_shoes.error = "Enter brand name"
                dialog.et_tag_shoes.requestFocus()
                return false
            }

            val tagsItem = TagsItem("Shoes",dialog.et_tag_shoes.text.toString(),false)
            selectedTags.add(tagsItem)
        }

        if (dialog.iv_tag_bag.isChecked){
            if (dialog.et_tag_bag.text.isNullOrEmpty()){
                dialog.et_tag_bag.error = "Enter brand name"
                dialog.et_tag_bag.requestFocus()
                return false
            }

            val tagsItem = TagsItem("Bag",dialog.et_tag_bag.text.toString(),false)
            selectedTags.add(tagsItem)
        }

        if (dialog.iv_tag_skirt.isChecked){
            if (dialog.et_tag_skirt.text.isNullOrEmpty()){
                dialog.et_tag_skirt.error = "Enter brand name"
                dialog.et_tag_skirt.requestFocus()
                return false
            }

            val tagsItem = TagsItem("Skirt",dialog.et_tag_skirt.text.toString(),false)
            selectedTags.add(tagsItem)
        }

        if (dialog.iv_tag_watch.isChecked){
            if (dialog.et_tag_watch.text.isNullOrEmpty()){
                dialog.et_tag_watch.error = "Enter brand name"
                dialog.et_tag_watch.requestFocus()
                return false
            }

            val tagsItem = TagsItem("Watch",dialog.et_tag_watch.text.toString(),false)
            selectedTags.add(tagsItem)
        }

        if (dialog.iv_tag_eyewear.isChecked){
            if (dialog.et_tag_eyewear.text.isNullOrEmpty()){
                dialog.et_tag_eyewear.error = "Enter brand name"
                dialog.et_tag_eyewear.requestFocus()
                return false
            }

            val tagsItem = TagsItem("Eyewear",dialog.et_tag_eyewear.text.toString(),false)
            selectedTags.add(tagsItem)
        }

        if (dialog.iv_tag_other.isChecked){
            if (dialog.et_tag_other.text.isNullOrEmpty()){
                dialog.et_tag_other.error = "Enter brand name"
                dialog.et_tag_other.requestFocus()
                return false
            }

            val tagsItem = TagsItem("Other",dialog.et_tag_other.text.toString(),false)
            selectedTags.add(tagsItem)
        }

        return true
    }

    private fun showEnterBrandDialog(tag:String){
        val enterBrandNameDialogFragment = EnterBrandNameDialogFragment()
        val bundle = Bundle()
        bundle.putString(Constants.TAGS,tag)
        enterBrandNameDialogFragment.arguments = bundle
        enterBrandNameDialogFragment.show(activity!!.supportFragmentManager,EnterBrandNameDialogFragment::class.java.simpleName)
        enterBrandNameDialogFragment.setUpdateBrandDlgListener(this)
    }

    override fun updateBrand(tag: String?, brand: String?) {
            updateBrandEt(tag!!,brand)
    }

    private fun updateBrandEt(tag: String,brand: String?) {
            when(tag.toLowerCase()){
                "top"->{
                    if (brand == null){
                        dialog.iv_tag_top.isChecked = false
                        dialog.iv_tag_top.setImageResource(R.drawable.ic_red_top)
                        return
                    }
                    dialog.ll_tag_top.visibility = View.VISIBLE
                    dialog.et_tag_top.setText(brand)
                    dialog.et_tag_top.post { Runnable { dialog.et_tag_top.setSelection(dialog.et_tag_top.text.toString().length) } }
                }

                "pants"->{
                    if (brand == null){
                        dialog.iv_tag_pants.isChecked = false
                        dialog.iv_tag_pants.setImageResource(R.drawable.ic_red_pants)
                        return
                    }
                    dialog.ll_tag_pants.visibility = View.VISIBLE
                    dialog.et_tag_pants.setText(brand)
                    dialog.et_tag_pants.post { Runnable { dialog.et_tag_pants.setSelection(dialog.et_tag_pants.text.toString().length) } }
                }

                "dress"->{
                    if (brand == null){
                        dialog.iv_tag_dress.isChecked = false
                        dialog.iv_tag_dress.setImageResource(R.drawable.ic_red_dress)
                        return
                    }
                    dialog.ll_tag_dress.visibility = View.VISIBLE
                    dialog.et_tag_dress.setText(brand)
                    dialog.et_tag_dress.post { Runnable { dialog.et_tag_dress.setSelection(dialog.et_tag_dress.text.toString().length) } }
                }

                "accessory"->{
                    if (brand == null){
                        dialog.iv_tag_accessory.isChecked = false
                        dialog.iv_tag_accessory.setImageResource(R.drawable.ic_red_accessory)
                        return
                    }
                    dialog.ll_tag_accessory.visibility = View.VISIBLE
                    dialog.et_tag_accessory.setText(brand)
                    dialog.et_tag_accessory.post { Runnable { dialog.et_tag_accessory.setSelection(dialog.et_tag_accessory.text.toString().length) } }
                }

                "shoes"->{
                    if (brand == null){
                        dialog.iv_tag_shoes.isChecked = false
                        dialog.iv_tag_shoes.setImageResource(R.drawable.ic_red_shoes)
                        return
                    }
                    dialog.ll_tag_shoes.visibility = View.VISIBLE
                    dialog.et_tag_shoes.setText(brand)
                    dialog.et_tag_shoes.post { Runnable { dialog.et_tag_shoes.setSelection(dialog.et_tag_shoes.text.toString().length) } }
                }

                "bag"->{
                    if (brand == null){
                        dialog.iv_tag_bag.isChecked = false
                        dialog.iv_tag_bag.setImageResource(R.drawable.ic_red_bag)
                        return
                    }
                    dialog.ll_tag_bags.visibility = View.VISIBLE
                    dialog.et_tag_bag.setText(brand)
                    dialog.et_tag_bag.post { Runnable { dialog.et_tag_bag.setSelection(dialog.et_tag_bag.text.toString().length) } }
                }

                "skirt"->{
                    if (brand == null){
                        dialog.iv_tag_skirt.isChecked = false
                        dialog.iv_tag_skirt.setImageResource(R.drawable.ic_red_skirt)
                        return
                    }
                    dialog.ll_tag_skirt.visibility = View.VISIBLE
                    dialog.et_tag_skirt.setText(brand)
                    dialog.et_tag_skirt.post { Runnable { dialog.et_tag_skirt.setSelection(dialog.et_tag_skirt.text.toString().length) } }
                }

                "watch"->{
                    if (brand == null){
                        dialog.iv_tag_watch.isChecked = false
                        dialog.iv_tag_watch.setImageResource(R.drawable.ic_red_watch)
                        return
                    }
                    dialog.ll_tag_watch.visibility = View.VISIBLE
                    dialog.et_tag_watch.setText(brand)
                    dialog.et_tag_watch.post { Runnable { dialog.et_tag_watch.setSelection(dialog.et_tag_watch.text.toString().length) } }
                }

                "eyewear"->{
                    if (brand == null){
                        dialog.iv_tag_eyewear.isChecked = false
                        dialog.iv_tag_eyewear.setImageResource(R.drawable.ic_red_eyewear)
                        return
                    }
                    dialog.ll_tag_eyewear.visibility = View.VISIBLE
                    dialog.et_tag_eyewear.setText(brand)
                    dialog.et_tag_eyewear.post { Runnable { dialog.et_tag_eyewear.setSelection(dialog.et_tag_eyewear.text.toString().length) } }
                }

                "other"->{
                    if (brand == null){
                        dialog.iv_tag_other.isChecked = false
                        dialog.iv_tag_other.setImageResource(R.drawable.ic_red_others)
                        return
                    }
                    dialog.ll_tag_other.visibility = View.VISIBLE
                    dialog.et_tag_other.setText(brand)
                    dialog.et_tag_other.post { Runnable { dialog.et_tag_other.setSelection(dialog.et_tag_other.text.toString().length) } }
                }
            }
    }
}