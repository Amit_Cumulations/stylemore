package com.cumulations.stylemore.uploadFeed.presentation

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.ViewGroup
import com.cumulations.stylemore.R
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper.get
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.data.model.TagsItem
import kotlinx.android.synthetic.main.dialog_add_tags_v2.*
import java.util.*


/**
 * Created by Amit Tumkur on 05-06-2018.
 */
class AddTagsDialogFragmentV2: DialogFragment() {

    private var selectedTags: LinkedList<TagsItem> = LinkedList()
    private lateinit var updateDialogTagsListener: UpdateDialogTagsListener
    private lateinit var gridTagsAdapter: GridTagsAdapter
    private lateinit var editTagsBrandAdapter: EditTagsBrandAdapter

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        val dialog = Dialog(activity!!, R.style.CustomDialogTheme)
        dialog.setContentView(R.layout.dialog_add_tags_v2)
        dialog.setCanceledOnTouchOutside(true)
        dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        val userPref = SharedPreferenceHelper.customPrefs(activity!!, Constants.USER_PREF)
        val profPicUrl = userPref[Constants.USER_PROF_PIC,""]

        val verticalDecoration = DividerItemDecoration(
                activity,
                DividerItemDecoration.HORIZONTAL)
        verticalDecoration.setDrawable(ContextCompat.getDrawable(activity!!, R.drawable.vertical_divider)!!)

        val horizontalDecoration = DividerItemDecoration(
                activity,
                DividerItemDecoration.VERTICAL)
        horizontalDecoration.setDrawable(ContextCompat.getDrawable(activity!!, R.drawable.horizontal_divider)!!)

        (activity as UploadFeedActivity).gridTagsUpdateMap = Utils.getGridTagsMap()
        val numberOfColumns = 4
        gridTagsAdapter = GridTagsAdapter(activity!!, ArrayList(),this)
        dialog.rv_grid_tags_list.layoutManager = GridLayoutManager(activity!!, numberOfColumns)
        dialog.rv_grid_tags_list.adapter = gridTagsAdapter
        dialog.rv_grid_tags_list.addItemDecoration(verticalDecoration)
        dialog.rv_grid_tags_list.addItemDecoration(horizontalDecoration)

        val noOfColumns = 2
        editTagsBrandAdapter = EditTagsBrandAdapter(activity!!, LinkedList(),this)
        dialog.rv_brands_list.layoutManager = LinearLayoutManager(activity!!)
        dialog.rv_brands_list.adapter = editTagsBrandAdapter
//        dialog.rv_brands_list.addItemDecoration(verticalDecoration)
//        dialog.rv_brands_list.addItemDecoration(horizontalDecoration)

        if (selectedTags.size>0){
            loadSelectedGridTagsAndBrandFieldTags()
        } else
            gridTagsAdapter.updateTags(Utils.getTagsListFromMap((activity as UploadFeedActivity).gridTagsUpdateMap))

        dialog.btn_tag_done.setOnClickListener{
            /*for showing only selected tags in Upload Feed Screen*/
//            selectedTags.clear()
            validateSelectedTagsBrandName()
            selectedTags = Utils.getSelectedTagsListFromMap((activity as UploadFeedActivity).gridTagsUpdateMap)
            updateDialogTagsListener.updateTags(selectedTags)
//            dismiss()
        }

        return dialog
    }

    private fun loadSelectedGridTagsAndBrandFieldTags() {
        for (tagsItem in selectedTags){
            if ((activity as UploadFeedActivity).gridTagsUpdateMap.containsKey(tagsItem.tag)){
                (activity as UploadFeedActivity).gridTagsUpdateMap[tagsItem.tag] = tagsItem
            }
        }

        gridTagsAdapter.updateTags(Utils.getTagsListFromMap((activity as UploadFeedActivity).gridTagsUpdateMap))
        editTagsBrandAdapter.updateTagBrands(Utils.getSelectedTagsListFromMap((activity as UploadFeedActivity).gridTagsUpdateMap))
    }

    fun setUpdateDialogTagListener(updateDialogTagsListener: UpdateDialogTagsListener){
        this.updateDialogTagsListener = updateDialogTagsListener
    }

    fun updateTagsInMap(tagsItem: TagsItem){
        (activity as UploadFeedActivity).gridTagsUpdateMap[tagsItem.tag] = tagsItem
        editTagsBrandAdapter.updateTagBrands(Utils.getSelectedTagsListFromMap((activity as UploadFeedActivity).gridTagsUpdateMap))
    }

    fun updateEditedTagInMap(tagsItem: TagsItem){
        (activity as UploadFeedActivity).gridTagsUpdateMap[tagsItem.tag] = tagsItem
    }

    private fun validateSelectedTagsBrandName(){
        editTagsBrandAdapter.validateFields()
    }

    fun provideSelectedTags(selectedTags: LinkedList<TagsItem>) {
        this.selectedTags = selectedTags
    }
}