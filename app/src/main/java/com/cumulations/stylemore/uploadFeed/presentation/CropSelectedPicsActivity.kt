package com.cumulations.stylemore.uploadFeed.presentation

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.View
import android.view.ViewTreeObserver
import com.cumulations.stylemore.R
import com.cumulations.stylemore.base.nocropper.*
import com.cumulations.stylemore.base.pixLibrary.pix.Pix.IMAGE_RESULTS
import com.cumulations.stylemore.base.utils.BitmapHelperUtils
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.uploadFeed.data.model.CroppedPic
import kotlinx.android.synthetic.main.activity_crop_selected_pics.*
import kotlinx.android.synthetic.main.layout_full_screen_loader.*
import org.jetbrains.anko.toast
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class CropSelectedPicsActivity : AppCompatActivity() {
    private val TAG:String = CropSelectedPicsActivity::class.java.simpleName

    private lateinit var selectedCroppedPicsAdapter: SelectedCroppedPicsAdapter

    private var croppedPics: LinkedList<CroppedPic> = LinkedList()
//    private var isSnappedToCenter = true
    private var rotationCount = 0

    private var currentCroppedPicIndex: Int = -1
    var recyclerViewHeight:Int =0
    var croppedCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crop_selected_pics)

        val selectedPics = intent?.getSerializableExtra(Constants.SELECTED_PICS) as ArrayList<CroppedPic>

        for (croppedPic in selectedPics){
            croppedPics.add(croppedPic)
        }

        recyclerViewHeight = rv_selected_pics.height

        selectedCroppedPicsAdapter = SelectedCroppedPicsAdapter(this, croppedPics)
        val gridLayoutManager = GridLayoutManager(this,3)
//        horizontalLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        rv_selected_pics.layoutManager = gridLayoutManager
        rv_selected_pics.adapter = selectedCroppedPicsAdapter

        crv_selected_pic.setDebug(true)

        crv_selected_pic.setGridCallback(object : CropperView.GridCallback {
            override fun onGestureStarted(): Boolean {
                return true
            }

            override fun onGestureCompleted(): Boolean {
                return false
            }
        })

        /*Not setting maxzoom*/
        /*if (crv_selected_pic.width != 0) {
            crv_selected_pic.maxZoom = crv_selected_pic.width * 2 / 1280f
        } else {

            val vto = crv_selected_pic.viewTreeObserver
            vto.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    crv_selected_pic.viewTreeObserver.removeOnPreDrawListener(this)
                    crv_selected_pic.maxZoom = crv_selected_pic.width * 2 / 1280f
                    return true
                }
            })

        }*/

        iv_toggle_center.setOnClickListener {
            snapImage()
        }

        iv_rotate_image.setOnClickListener {
            rotateImage()
        }

        tv_done.setOnClickListener {
            showLoading()
            Handler().post {
                croppedCount = 0
                checkAndPrepareNotTouchedPics()
                for (pic in croppedPics){
                    cropScaledImageAsync(pic)
    //                    cropOriginalImageAsync(pic)
                }
            }
        }

        ib_back.setOnClickListener {
            onBackPressed()
        }
    }

    fun loadBitmap(clickedPicPos:Int) {

        currentCroppedPicIndex = clickedPicPos
        val currentCroppedPic = croppedPics[currentCroppedPicIndex]
        rotationCount = 0

        Log.i(TAG, "load image: ${currentCroppedPic?.fileUri}")

        if (currentCroppedPic?.originalBitmap == null){
            /*reset Image matrix*/
            val matrix = Matrix()
            matrix.reset()
            crv_selected_pic.mImageView.imageMatrix = matrix
            crv_selected_pic.mImageView.invalidate()

            currentCroppedPic?.originalBitmap = BitmapFactory.decodeFile(currentCroppedPic?.fileUri)
            currentCroppedPic?.scaledBitmap = get1280ScaledBitmap(currentCroppedPic?.originalBitmap!!)

            crv_selected_pic.setImageBitmap(currentCroppedPic?.scaledBitmap)
        } else{
            if (currentCroppedPic?.scaledBitmap!=null) {
                crv_selected_pic.replaceBitmap(currentCroppedPic?.scaledBitmap,currentCroppedPic?.matrix)
            }
        }
    }

    /*private fun prepareCropForOriginalImage(currentCroppedPic: CroppedPic): ScaledCropper? {
        val cropResult = crv_selected_pic.getCropInfo(currentCroppedPic)
        if (cropResult.cropInfo == null) {
            return null
        }

        val scale: Float
        val originalBitmap = currentCroppedPic?.originalBitmap
        val mScaledBitmap = currentCroppedPic?.scaledBitmap
        if (rotationCount % 2 == 0) {
            // same width and height
            scale = originalBitmap?.width!!.toFloat() / mScaledBitmap?.width!!
        } else {
            // width and height are interchanged
            scale = originalBitmap?.width!!.toFloat() / mScaledBitmap?.height!!
        }

        val cropInfo = cropResult.cropInfo.rotate90XTimes(mScaledBitmap.width, mScaledBitmap.height, rotationCount)
        return ScaledCropper(cropInfo, originalBitmap, scale)
    }*/

    private fun cropScaledImageAsync(croppedPic: CroppedPic?) {
        val state = crv_selected_pic.getCroppedBitmapAsync(croppedPic,object : CropperCallback() {
            override fun onCropped(bitmap: Bitmap?) {
                if (bitmap != null) {
                    croppedPic?.croppedBitmap = bitmap
                    try {
                        val croppedFile = BitmapUtils.getFile(croppedPic?.fileUri)
                        croppedPic?.croppedPicUri = croppedFile.absolutePath
                        BitmapHelperUtils.writeBitmapToFile(
                                bitmap,
                                croppedFile,
                                80)
                        Log.d("cropPic","success, ${croppedPic?.croppedPicUri}")
                        croppedCount++

                        if (croppedCount == croppedPics.size){
                            returnCroppedPics()
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onOutOfMemoryError() {
                Log.e("onOutOfMemoryError","")
            }
        })

        if (state == CropState.FAILURE_GESTURE_IN_PROCESS) {
            toast("unable to crop. Gesture in progress")
        }
    }

    /*private fun cropOriginalImageAsync(pic: CroppedPic?) {
        if (pic?.originalBitmap != null) {
            val scaledCropper = prepareCropForOriginalImage(pic) ?: return

            scaledCropper!!.crop(object : CropperCallback() {
                override fun onCropped(bitmap: Bitmap?) {
                    if (bitmap != null) {
                        pic?.croppedBitmap = bitmap
                        try {
                            val croppedFile = BitmapUtils.getFile(pic?.fileUri)
                            pic.croppedPicUri = croppedFile.absolutePath
                            BitmapHelperUtils.writeBitmapToFile(
                                    bitmap,
                                    croppedFile,
                                    80)
                            Log.d("cropPic","success, ${pic.croppedPicUri}")
                            croppedCount++

                            if (croppedCount == croppedPics.size){
                                returnCroppedPics()
                            }
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }

                    }
                }
            })
        }
    }*/

    private fun rotateImage() {
        val currentCroppedPic = croppedPics[currentCroppedPicIndex]
        if (currentCroppedPic?.scaledBitmap == null) {
            Log.e(TAG, "bitmap is not loaded yet")
            return
        }

        currentCroppedPic?.scaledBitmap = BitmapHelperUtils.rotateBitmap(currentCroppedPic?.scaledBitmap, 90f)
        crv_selected_pic.setImageBitmap(currentCroppedPic?.scaledBitmap)
        rotationCount++
    }

    private fun snapImage() {
        val currentCroppedPic = croppedPics[currentCroppedPicIndex]
        if (currentCroppedPic?.snappedToCentre) {
            crv_selected_pic.cropToCenter()
        } else {
            crv_selected_pic.fitToCenter()
        }

        currentCroppedPic.snappedToCentre = !currentCroppedPic.snappedToCentre
    }

    private fun get1280ScaledBitmap(originalBitmap: Bitmap): Bitmap{
        Log.i(TAG, "bitmap: " + originalBitmap.width + " " + originalBitmap.height)

        val maxP = Math.max(originalBitmap.width, originalBitmap.height)
        val scale1280 = maxP.toFloat() / 1280
        Log.i(TAG, "scaled: " + scale1280 + " - " + 1 / scale1280)

        return Bitmap.createScaledBitmap(originalBitmap, (originalBitmap.width / scale1280).toInt(),
                (originalBitmap.height / scale1280).toInt(), true)
    }

    fun updateCurrentPicMatrix(matrix: Matrix){
        croppedPics[currentCroppedPicIndex].matrix = matrix
    }

    private fun checkAndPrepareNotTouchedPics(){
        for (pic in croppedPics){
            if (pic.matrix == null || pic.scaledBitmap == null){
                /*Not selected yet
                * Make pics center cropped
                * */

                /*create necessary bitmaps*/
                pic?.originalBitmap = BitmapFactory.decodeFile(pic?.fileUri)
                pic?.scaledBitmap = get1280ScaledBitmap(pic?.originalBitmap!!)
                pic.matrix = crv_selected_pic.getCentreCropMatrix(pic./*originalBitmap*/scaledBitmap,crv_selected_pic.width)
                Log.d("checTouchedPics","matrix = ${pic.matrix}, ${pic.fileUri}")
            }
        }
    }

    fun showLoading() {
        if (isFinishing)
            return
        full_screen_loader_layout.visibility = View.VISIBLE
        Utils.showLoader(loader,null,this)
    }

    fun hideLoading() {
        if (isFinishing)
            return
        full_screen_loader_layout.visibility = View.GONE
        Utils.closeLoader(loader,null,this)
    }

    private fun returnCroppedPics() {

        runOnUiThread {
            hideLoading()
        }

        val picsList:ArrayList<String> = ArrayList()
        for (pic in croppedPics) {
            picsList.add(pic.croppedPicUri!!)
             Log.e("send cropped pic", "img ${pic.croppedPicUri}")
        }
        val resultIntent = Intent()
        resultIntent.putStringArrayListExtra(IMAGE_RESULTS, picsList)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
    }
}
