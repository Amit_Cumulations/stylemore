package com.cumulations.stylemore.uploadFeed.presentation

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.cumulations.stylemore.R
import com.cumulations.stylemore.feeds.data.model.TagsItem
import kotlinx.android.synthetic.main.grid_item_tags.view.*
import kotlinx.android.synthetic.main.list_item_et_brands.view.*
import org.w3c.dom.Text
import java.util.*

class EditTagsBrandAdapter(val context: Context,
                           var availableTags:LinkedList<TagsItem>,
                           val fragmentV2: AddTagsDialogFragmentV2) : RecyclerView.Adapter<EditTagsBrandAdapter.EditTagBrandsViewHolder>() {


    private var validate: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EditTagBrandsViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_et_brands, parent, false)
        return EditTagBrandsViewHolder(view)
    }

    override fun onBindViewHolder(holder: EditTagBrandsViewHolder, position: Int) {
        val tagsItem = availableTags[position]
        holder.bindTags(tagsItem,position)
    }

    override fun getItemCount(): Int {
        return availableTags.size
    }

    inner class EditTagBrandsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindTags(tagsItem: TagsItem?, clickedPos:Int){
            itemView.et_tag_brand.setText(tagsItem?.brand)

            itemView.et_tag_brand.addTextChangedListener(object : TextWatcher{
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                }

                override fun afterTextChanged(p0: Editable?) {
                    if (!p0.isNullOrEmpty()){
                        tagsItem?.brand = p0.toString()
                        fragmentV2.updateEditedTagInMap(tagsItem!!)
                    }
                }

            })


            if (validate) {
                if (itemView.et_tag_brand.text.isNullOrEmpty()) {
                    itemView.et_tag_brand.error = "Enter brand name"
                    itemView.et_tag_brand.requestFocus()
                } else {
                    itemView.et_tag_brand.error = null
                    itemView.et_tag_brand.clearFocus()
                    tagsItem?.brand = itemView.et_tag_brand.text.toString()
//                    availableTags[clickedPos] = tagsItem!!
                }
            }

            updateBrandIcons(itemView,tagsItem)
        }
    }

    fun updateBrandIcons(itemView: View, tagsItem: TagsItem?){
        when(tagsItem?.tag?.toLowerCase()){
            "top" -> {
                itemView.iv_brand_tag.setImageResource(R.drawable.ic_black_top)
            }

            "pants" -> {
                itemView.iv_brand_tag.setImageResource(R.drawable.ic_black_pants)
            }

            "dress" -> {
                itemView.iv_brand_tag.setImageResource(R.drawable.ic_black_dress)
            }

            "accessory" -> {
                itemView.iv_brand_tag.setImageResource(R.drawable.ic_black_accessory)
            }

            "shoes" -> {
                itemView.iv_brand_tag.setImageResource(R.drawable.ic_black_shoes)
            }

            "bag" -> {
                itemView.iv_brand_tag.setImageResource(R.drawable.ic_black_bag)
            }

            "skirt" -> {
                itemView.iv_brand_tag.setImageResource(R.drawable.ic_black_skirt)
            }

            "watch" -> {
                itemView.iv_brand_tag.setImageResource(R.drawable.ic_black_watch)
            }

            "eyewear" -> {
                itemView.iv_brand_tag.setImageResource(R.drawable.ic_black_eyewear)
            }

        }
    }

    fun validateFields(){
        validate = true
        notifyDataSetChanged()
    }

    fun updateTagBrands(availableTags: LinkedList<TagsItem>) {
        this.availableTags = availableTags
        notifyDataSetChanged()
    }
}
