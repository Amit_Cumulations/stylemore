package com.cumulations.stylemore.uploadFeed.presentation

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.ViewGroup
import android.view.WindowManager
import com.cumulations.stylemore.R
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper.get
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.data.model.TagsItem
import kotlinx.android.synthetic.main.dialog_add_tags_v2.*
import kotlinx.android.synthetic.main.dialog_enter_brand.*
import org.jetbrains.anko.toast
import java.util.*


/**
 * Created by Amit Tumkur on 05-06-2018.
 */
class EnterBrandNameDialogFragment: DialogFragment() {
    private lateinit var updateBrandDlgListener: UpdateBrandDlgListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        val dialog = Dialog(activity!!, R.style.EditDialogTheme)
        dialog.setContentView(R.layout.dialog_enter_brand)
        dialog.setCanceledOnTouchOutside(false)
        isCancelable = false
        dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        val tag = arguments?.getString(Constants.TAGS)
        dialog.dlg_tv_enter_brand.text = "Enter brand for $tag"

        // show soft keyboard
        dialog.dlg_tv_enter_brand.requestFocus()
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        dialog.dlg_btn_done.setOnClickListener{
            Utils.closeKeyboard(activity!!,it)
            if (dialog.dlg_et_brand_name.text.isNullOrEmpty()){
                activity!!.toast("Enter brand name")
            } else {
                updateBrandDlgListener.updateBrand(tag,dialog.dlg_et_brand_name.text.toString())
                dismiss()
            }
        }

        dialog.dlg_btn_cancel.setOnClickListener {
            Utils.closeKeyboard(activity!!,it)
            updateBrandDlgListener.updateBrand(tag,null)
            dismiss()
        }

        return dialog
    }

    fun setUpdateBrandDlgListener(updateBrandDlgListener: UpdateBrandDlgListener){
        this.updateBrandDlgListener = updateBrandDlgListener
    }
}