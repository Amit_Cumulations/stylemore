package com.cumulations.stylemore.uploadFeed.presentation

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cumulations.stylemore.R
import com.cumulations.stylemore.feeds.data.model.TagsItem
import kotlinx.android.synthetic.main.grid_item_tags.view.*
import java.util.*

class GridTagsAdapter(val context: Context,
                      var availableTags: ArrayList<TagsItem>,
                      val fragmentV2: AddTagsDialogFragmentV2) : RecyclerView.Adapter<GridTagsAdapter.GridTagsViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridTagsViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.grid_item_tags, parent, false)
        return GridTagsViewHolder(view)
    }

    override fun onBindViewHolder(holder: GridTagsViewHolder, position: Int) {
        val tagsItem = availableTags[position]
        holder.bindTags(tagsItem, position)
    }

    override fun getItemCount(): Int {
        return availableTags.size
    }

    inner class GridTagsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindTags(tagsItem: TagsItem?, clickedPos: Int) {
            itemView.tv_tag.text = tagsItem?.tag
            itemView.iv_tag.isChecked = tagsItem!!.isSelected
            updateTagIcons(itemView, tagsItem)

            itemView.iv_tag.setOnClickListener {
                itemView.iv_tag.isChecked = !itemView.iv_tag.isChecked
                tagsItem!!.isSelected = !tagsItem.isSelected
                updateTagIcons(itemView, tagsItem)
                fragmentV2.updateTagsInMap(tagsItem)
            }
        }
    }

    fun updateTagIcons(itemView: View, tagsItem: TagsItem?) {
        when (tagsItem?.tag?.toLowerCase()) {
            "top" -> {
                if (!tagsItem.isSelected)
                    itemView.iv_tag.setImageResource(R.drawable.ic_red_top)
                else itemView.iv_tag.setImageResource(R.drawable.ic_white_top)
            }

            "pants" -> {
                if (!tagsItem.isSelected)
                    itemView.iv_tag.setImageResource(R.drawable.ic_red_pants)
                else itemView.iv_tag.setImageResource(R.drawable.ic_white_pants)
            }

            "dress" -> {
                if (!tagsItem.isSelected)
                    itemView.iv_tag.setImageResource(R.drawable.ic_red_dress)
                else itemView.iv_tag.setImageResource(R.drawable.ic_white_dress)
            }

            "accessory" -> {
                if (!tagsItem.isSelected)
                    itemView.iv_tag.setImageResource(R.drawable.ic_red_accessory)
                else itemView.iv_tag.setImageResource(R.drawable.ic_white_accessory)
            }

            "shoes" -> {
                if (!tagsItem.isSelected)
                    itemView.iv_tag.setImageResource(R.drawable.ic_red_shoes)
                else itemView.iv_tag.setImageResource(R.drawable.ic_white_shoes)
            }

            "bag" -> {
                if (!tagsItem.isSelected)
                    itemView.iv_tag.setImageResource(R.drawable.ic_red_bag)
                else itemView.iv_tag.setImageResource(R.drawable.ic_white_bag)
            }

            "skirt" -> {
                if (!tagsItem.isSelected)
                    itemView.iv_tag.setImageResource(R.drawable.ic_red_skirt)
                else itemView.iv_tag.setImageResource(R.drawable.ic_white_skirt)
            }

            "watch" -> {
                if (!tagsItem.isSelected)
                    itemView.iv_tag.setImageResource(R.drawable.ic_red_watch)
                else itemView.iv_tag.setImageResource(R.drawable.ic_white_watch)
            }

            "eyewear" -> {
                if (!tagsItem.isSelected)
                    itemView.iv_tag.setImageResource(R.drawable.ic_red_eyewear)
                else itemView.iv_tag.setImageResource(R.drawable.ic_white_eyewear)
            }

            "other" -> {

            }

        }
    }

    fun updateTags(availableTags: ArrayList<TagsItem>) {
        this.availableTags = availableTags
        notifyDataSetChanged()
    }
}
