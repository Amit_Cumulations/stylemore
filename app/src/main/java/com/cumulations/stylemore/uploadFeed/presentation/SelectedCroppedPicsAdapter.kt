package com.cumulations.stylemore.uploadFeed.presentation


import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import com.cumulations.stylemore.R
import com.cumulations.stylemore.base.pixLibrary.utility.Utility
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.uploadFeed.data.model.CroppedPic
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.main_image.view.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class SelectedCroppedPicsAdapter(var context: Context,
                                 var picsList: LinkedList<CroppedPic>) : RecyclerView.Adapter<SelectedCroppedPicsAdapter.CroppedPicsViewHolder>() {

    internal var margin = 8
    internal var cardLayoutParams: FrameLayout.LayoutParams? = null
    internal var size = (Utils.getScreenWidthInDp(context as AppCompatActivity) / 3)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CroppedPicsViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.main_image, parent, false)
        return CroppedPicsViewHolder(v)
    }

    override fun onBindViewHolder(croppedPicsViewHolder: CroppedPicsViewHolder, position: Int) {
        val croppedPic = picsList[position]
        croppedPicsViewHolder.bindPics(croppedPic,position)
    }

    override fun getItemCount(): Int {
        return picsList.size
    }

    fun updatePics(picsList: LinkedList<CroppedPic>) {
        this.picsList = picsList
        notifyDataSetChanged()
    }

    inner class CroppedPicsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindPics(croppedPic: CroppedPic, clickedPos:Int){
            cardLayoutParams = FrameLayout.LayoutParams(size, size)
//            cardLayoutParams!!.setMargins(margin,margin,margin,margin)
                Picasso.get().load(File(croppedPic.fileUri))
                        .resize((size/1.5f).toInt(), (size/1.5f).toInt())
                        .centerCrop()
                        .placeholder(R.drawable.placeholder_large)
                        .into(itemView.sdv)

            itemView.cv_grid.layoutParams = cardLayoutParams
            itemView.selection.visibility = View.GONE
            itemView.sdv.setOnClickListener {
                (context as CropSelectedPicsActivity).loadBitmap(clickedPos)
            }

            if (clickedPos == 0)
                (context as CropSelectedPicsActivity).loadBitmap(clickedPos)
        }
    }
}
