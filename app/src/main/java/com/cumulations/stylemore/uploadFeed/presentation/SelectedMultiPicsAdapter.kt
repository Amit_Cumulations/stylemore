package com.cumulations.stylemore.uploadFeed.presentation


import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cumulations.stylemore.R
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.pixLibrary.utility.Utility
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_select_multi_pics.view.*
import java.io.File
import java.util.*

class SelectedMultiPicsAdapter(var context: Context,
                               var imagesList: ArrayList<String>) : RecyclerView.Adapter<SelectedMultiPicsAdapter.MultiPicsViewHolder>() {
    var deleteImagesList = ArrayList<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MultiPicsViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_item_select_multi_pics, parent, false)
        return MultiPicsViewHolder(v)
    }

    override fun onBindViewHolder(multiPicsViewHolder: MultiPicsViewHolder, position: Int) {
        val picUri = imagesList[position]
        multiPicsViewHolder.bindPics(picUri,position)
    }

    override fun getItemCount(): Int {
        return imagesList.size
    }

    fun updatePics(imagesList: ArrayList<String>) {
        this.imagesList = imagesList
        notifyDataSetChanged()
    }

    inner class MultiPicsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindPics(picUri:String,clickedPos:Int){
            val f = File(picUri)
            if (f.exists()) {
                val scaled = Utility.getScaledBitmap(240, Utility.getExcifCorrectedBitmap(f))
                if (scaled != null) {
                    Picasso.get().load(f)
//                            .resize(240, 240)
//                            .centerCrop()
                            .fit()
                            .placeholder(R.drawable.placeholder_large)
                            .into(itemView.iv_selected_pic)
                }
            } else {
                Picasso.get().load(StyleMoreApp.IMAGE_BASE_URL + imagesList[position])
//                        .resize(240, 240)
//                        .centerCrop()
                        .fit()
                        .placeholder(R.drawable.placeholder_large)
                        .into(itemView.iv_selected_pic)
            }

            itemView.iv_remove_pic.setOnClickListener {

                if (!picUri.contains("/storage/emulated")){
                    deleteImagesList.add(picUri)
                }
                imagesList.remove(picUri)
                notifyItemRemoved(clickedPos)
                notifyItemRangeChanged(clickedPos, imagesList.size)
                (context as UploadFeedActivity).updatePicsCount(itemCount.toString())
            }
        }
    }
}
