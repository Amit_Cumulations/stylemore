package com.cumulations.stylemore.uploadFeed.presentation

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.cumulations.stylemore.R
import com.cumulations.stylemore.feeds.data.model.TagsItem
import com.cumulations.stylemore.feeds.presentation.DialogFeedLikesAdapter
import kotlinx.android.synthetic.main.list_item_selected_tags.view.*

import java.io.File
import java.util.*

class SelectedTagsAdapter(val context: Context,
                          var selectedTags:LinkedList<TagsItem>) : RecyclerView.Adapter<SelectedTagsAdapter.SelectedTagsViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectedTagsViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_selected_tags, parent, false)
        return SelectedTagsViewHolder(view)
    }

    override fun onBindViewHolder(holder: SelectedTagsViewHolder, position: Int) {
        val tagsItem = selectedTags[position]
        holder.bindTags(tagsItem,position)
    }

    override fun getItemCount(): Int {
        return selectedTags.size
    }

    inner class SelectedTagsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindTags(tagsItem: TagsItem?, clickedPos:Int){

            when(tagsItem?.tag?.toLowerCase()){
                "top"->{
                    itemView.iv_selected_tag.setImageResource(R.drawable.ic_red_top)
                }

                "pants"->{
                    itemView.iv_selected_tag.setImageResource(R.drawable.ic_red_pants)
                }

                "dress"->{
                    itemView.iv_selected_tag.setImageResource(R.drawable.ic_red_dress)
                }

                "accessory"->{
                    itemView.iv_selected_tag.setImageResource(R.drawable.ic_red_accessory)
                }

                "shoes"->{
                    itemView.iv_selected_tag.setImageResource(R.drawable.ic_red_shoes)
                }

                "bag"->{
                    itemView.iv_selected_tag.setImageResource(R.drawable.ic_red_bag)
                }

                "skirt"->{
                    itemView.iv_selected_tag.setImageResource(R.drawable.ic_red_skirt)
                }

                "watch"->{
                    itemView.iv_selected_tag.setImageResource(R.drawable.ic_red_watch)
                }

                "eyewear"->{
                    itemView.iv_selected_tag.setImageResource(R.drawable.ic_red_eyewear)
                }

                "other"->{
                    itemView.iv_selected_tag.setImageResource(R.drawable.ic_red_others)
                }

            }

            itemView.tv_tag_brand_name.text = tagsItem?.brand
            itemView.tv_tag_brand_name.postDelayed({
                itemView.tv_tag_brand_name.isSelected = true
            },1500)

            itemView.iv_remove_tag.setOnClickListener {
                selectedTags.removeAt(clickedPos)
                notifyItemRemoved(clickedPos)
                notifyItemRangeChanged(clickedPos, selectedTags.size)
//                (context as UploadFeedActivity).updatePicsCount(itemCount.toString())
            }
        }
    }

    fun updateTags(selectedTags: LinkedList<TagsItem>) {
        this.selectedTags = selectedTags
        notifyDataSetChanged()
    }
}
