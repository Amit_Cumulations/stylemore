package com.cumulations.stylemore.uploadFeed.presentation

import com.cumulations.stylemore.feeds.data.model.TagsItem
import java.util.*

/**
 * Created by Amit Tumkur on 21-06-2018.
 */
interface UpdateBrandDlgListener {
    fun updateBrand(tag:String?,brand:String?)
}