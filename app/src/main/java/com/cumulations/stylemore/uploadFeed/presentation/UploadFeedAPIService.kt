package com.cumulations.stylemore.uploadFeed.presentation

import android.app.IntentService
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import com.cumulations.stylemore.R
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper.get
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.rest.RestClient
import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse
import com.cumulations.stylemore.uploadFeed.data.model.FeedData
import com.cumulations.stylemore.uploadFeed.data.model.MultiPartUpdateFeedRequest
import com.cumulations.stylemore.uploadFeed.data.model.MultiPartUploadFeedRequest
import com.cumulations.stylemore.uploadFeed.domain.UpdateFeedUseCase
import com.cumulations.stylemore.uploadFeed.domain.UploadFeedUseCase
import okhttp3.MultipartBody
import org.jetbrains.anko.toast
import java.io.File
import java.util.*


/**
 * Created by Amit Tumkur on 20-02-2018.
 */

class UploadFeedAPIService : IntentService("UploadFeedAPIService"),UploadFeedProgressUpdateView {
    private var notificationManager: NotificationManager? = null
    private var notificationBuilder: NotificationCompat.Builder? = null

    private var notificId:Int = 0
    private val smallIcon: Int
        get() = if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP)
            R.mipmap.ic_stat_launcher
        else
            R.mipmap.ic_launcher

    private var feedData: FeedData? = null

    override fun onHandleIntent(intent: Intent?) {
        buildNotification()

        feedData = intent?.getSerializableExtra(Constants.FEED_DATA) as FeedData?
        if (feedData!=null)
            sendPicsToServer(feedData)
        else
            hideLoading()
    }

    private fun sendPicsToServer(feedData: FeedData?) {
        val imagesMultiPartList = ArrayList<MultipartBody.Part>()
//        for (i in 0 until feedData!!.size) {
        if (feedData != null) {
            for (picPath in feedData.images) {
                val imageFile = File(picPath)
                val fileRequestBody = RestClient.createRequestBody(imageFile)
                val imageMultipart = MultipartBody.Part.createFormData(Constants.IMAGE, imageFile.name,fileRequestBody)
                imagesMultiPartList.add(imageMultipart)
            }

            val descriptionMultipartBody = MultipartBody.Part.createFormData(Constants.DESCRIPTION,feedData.description)

            val userPref = SharedPreferenceHelper.customPrefs(this,Constants.USER_PREF)
            val emailIdMultipartBody = MultipartBody.Part.createFormData(Constants.EMAIL,userPref[Constants.USER_EMAIL,""]!!)

            val tagsMultipartBody = MultipartBody.Part.createFormData(Constants.TAGS,feedData.tags)
            val userIdMultipartBody = MultipartBody.Part.createFormData(Constants.MP_USER_ID,StyleMoreApp.user_id)

            var feedTypeMultipartBody:MultipartBody.Part? = null
            if (feedData.feed_type!=null)
                feedTypeMultipartBody = MultipartBody.Part.createFormData(Constants.MP_FEED_TYPE,feedData.feed_type)

            val uploadFeedPresenter = UploadFeedPresenterImpl(this)
            if (feedData.feed_id == null){
                val multiPartUploadFeedRequest = MultiPartUploadFeedRequest(imagesMultiPartList,
                        descriptionMultipartBody,
                        emailIdMultipartBody,
                        tagsMultipartBody,
                        userIdMultipartBody,
                        feedTypeMultipartBody!!)
                uploadFeedPresenter.getUploadFeedResponse(UploadFeedUseCase(),multiPartUploadFeedRequest)
            } else /*if (feedData.delete_img!=null)*/{

                val feedIdMultipartBody = MultipartBody.Part.createFormData(Constants.MP_FEED_ID,feedData.feed_id)
                val deleteImgsMultipartBody = MultipartBody.Part.createFormData(Constants.MP_FEED_DELETE_IMGS,feedData.delete_img.toString())

                val multiPartUpdateFeedRequest = MultiPartUpdateFeedRequest(imagesMultiPartList,
                        descriptionMultipartBody,
                        emailIdMultipartBody,
                        tagsMultipartBody,
                        userIdMultipartBody,
                        feedTypeMultipartBody,
                        deleteImgsMultipartBody,
                        feedIdMultipartBody)
                uploadFeedPresenter.getUpdateFeedResponse(UpdateFeedUseCase(),multiPartUpdateFeedRequest)
            }
        }
    }

    private fun buildNotification() {
        if (notificationManager == null)
            notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (notificationBuilder == null)
            createNotificationBuilder(this)
    }

    private fun createNotificationBuilder(context: Context) {

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupFeedUploadNotificationChannel();
        }

        notificationBuilder = NotificationCompat.Builder(context,getString(R.string.upload_channel_id))
                .setSmallIcon(smallIcon)
                .setContentTitle("Feed Upload")
                .setContentText("Uploading feed in progress...")
                .setStyle(NotificationCompat.BigTextStyle()
                        .bigText("Uploading feed in progress..."))
                .setProgress(0,0,true)
                .setAutoCancel(true)
    }

    fun getNotificId():Int{
        return ++notificId
    }

    fun clearNotification(notificId: Int) {
//        notificationManager!!.cancel(notificId)
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupFeedUploadNotificationChannel() {
        val channelId = getString(R.string.upload_channel_id)

        if (Utils.isNotificationChannelEnabled(this, channelId))
            return

        val channelName = "Feed Upload"
        val channelDescription = "Notification to show feed upload progress."
        val notificationChannel: NotificationChannel
        notificationChannel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH)
        notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        notificationChannel.description = channelDescription
        notificationChannel.enableLights(true)
        notificationChannel.lightColor = R.color.insta_light_red
        notificationChannel.enableVibration(true)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val audioAttributes = AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION_EVENT)
                .build()
        notificationChannel.setSound(defaultSoundUri, audioAttributes)
        if (notificationManager != null) {
            notificationManager?.createNotificationChannel(notificationChannel)
        }
    }

    override fun showLoading() {
        if (feedData?.feed_id == null){
            Utils.showNotification(notificationManager,notificationBuilder,
                    "Feed Upload","Uploading feed in progress...",getNotificId())
        } else if (feedData?.feed_id!=null){
            Utils.showNotification(notificationManager,notificationBuilder,
                    "Feed Update","Updating feed in progress...",getNotificId())
        }
    }

    override fun hideLoading() {
        clearNotification(notificId)
    }

    override fun provideMPUploadResponse(basicResponse: BasicResponse) {
        if (basicResponse.status.equals("success",true)){
            notificationBuilder?.setProgress(0,0,false)
            if (feedData?.feed_id == null){
                notificationBuilder?.setProgress(0,0,false)
                Utils.showNotification(notificationManager,notificationBuilder,
                        "Feed Upload","Uploading feed completed",getNotificId())
                this.toast("Upload feed success")
            } else if (feedData?.feed_id!=null){
                Utils.showNotification(notificationManager,notificationBuilder,
                        "Feed Update","Updating feed completed",getNotificId())
                this.toast("Update feed success")
            }
        }
    }

    override fun showError(error: String) {
        notificationBuilder?.setProgress(0,0,false)
        if (feedData?.feed_id == null) {
            Utils.showNotification(notificationManager, notificationBuilder,
                    "Feed Upload", "Uploading feed failed $error", getNotificId())
        } else if (feedData?.feed_id != null) {
            Utils.showNotification(notificationManager, notificationBuilder,
                    "Feed Update", "Updating feed failed $error", getNotificId())
        }
    }
}
