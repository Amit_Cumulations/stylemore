package com.cumulations.stylemore.uploadFeed.presentation

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.cumulations.stylemore.R
import com.cumulations.stylemore.base.pixLibrary.pix.Pix
import com.cumulations.stylemore.base.pixLibrary.utility.PermUtil
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.data.model.ResultItem
import com.cumulations.stylemore.feeds.data.model.TagsItem
import com.cumulations.stylemore.uploadFeed.data.model.FeedData
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_upload_feed.*
import org.jetbrains.anko.toast
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import com.cumulations.stylemore.base.hashTagHelper.HashTagHelper
import com.cumulations.stylemore.feeds.presentation.FeedListFragment
import org.apache.commons.text.StringEscapeUtils


class UploadFeedActivity : AppCompatActivity(), View.OnClickListener, UpdateDialogTagsListener {

    private lateinit var selectedMultiPicsAdapter: SelectedMultiPicsAdapter
    private lateinit var selectedTagsAdapter: SelectedTagsAdapter
    private var selectedPics: ArrayList<String> = ArrayList()
    private var selectedTags: LinkedList<TagsItem> = LinkedList()

    private var onSaveInstanceStateCalled: Boolean = false
    public var gridTagsUpdateMap: LinkedHashMap<String, TagsItem> = LinkedHashMap()
    private var feedType: String? = "fashion"

    private var resultItem: ResultItem? = null
    var additionalSymbols = charArrayOf('_'/*, '$'*/)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_feed)

        val hashTagHelper = HashTagHelper.Creator.create(ContextCompat.getColor(this, R.color.insta_pink), null, additionalSymbols)
        hashTagHelper.handle(et_pics_caption)

        selectedMultiPicsAdapter = SelectedMultiPicsAdapter(this, ArrayList())
        val horizontalLayoutManager = LinearLayoutManager(this)
        horizontalLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        rv_selected_pics.layoutManager = horizontalLayoutManager
        rv_selected_pics.adapter = selectedMultiPicsAdapter

        val horizontalLayoutManager2 = LinearLayoutManager(this)
        selectedTagsAdapter = SelectedTagsAdapter(this, LinkedList())
        rv_selected_tags.layoutManager = horizontalLayoutManager2
        horizontalLayoutManager2.orientation = LinearLayoutManager.HORIZONTAL
        rv_selected_tags.adapter = selectedTagsAdapter

        iv_add_more_pics.setOnClickListener(this)
        iv_add_tag.setOnClickListener(this)
        btn_upload.setOnClickListener(this)
        ib_back.setOnClickListener(this)

        feedType = intent?.getStringExtra(Constants.MP_FEED_TYPE)
        resultItem = intent.getSerializableExtra(Constants.RESULT_ITEM) as ResultItem?

        if (resultItem != null) {
            initFeedData(resultItem!!)
        } else {
            iv_add_more_pics.performClick()
        }
    }

    private fun initFeedData(resultItem: ResultItem) {
        selectedPics = resultItem.pictures as ArrayList<String>
        selectedMultiPicsAdapter.updatePics(selectedPics)
        tv_selected_pics_count.text = selectedPics.size.toString() + "/10"

        selectedTags = resultItem.tags!!
        updateTags(selectedTags)

        val unescapedDescription = StringEscapeUtils.unescapeJava(resultItem.description)
        et_pics_caption.setText(unescapedDescription)
        et_pics_caption.post {
            et_pics_caption.setSelection(et_pics_caption.text.toString().length - 1)
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //Log.e("val", "requestCode ->  " + requestCode+"  resultCode "+resultCode);
        when (requestCode) {
            100 -> {
                if (resultCode == Activity.RESULT_OK) {
                    val pics = data?.getStringArrayListExtra(Pix.IMAGE_RESULTS) as ArrayList<String>
                    selectedPics.addAll(pics)
                    selectedMultiPicsAdapter.updatePics(selectedPics)
                    tv_selected_pics_count.text = selectedPics.size.toString() + "/10"
                } else if (resultCode == Activity.RESULT_CANCELED) {
//                    Utils.goToHomeScreen(this)
                    if (selectedPics.size == 0 && selectedTags.size == 0)
                        onBackPressed()
                }
            }
        }
    }

    /*Fix for gallery not opening after granting permissions*/
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS -> {
                val cameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED
                var writeExternalFile = false
                if (grantResults.size > 1) {
                    writeExternalFile = grantResults[1] == PackageManager.PERMISSION_GRANTED
                }
                var audio = false
                if (grantResults.size > 2) {
                    audio = grantResults[2] == PackageManager.PERMISSION_GRANTED
                }
                if (cameraPermission && writeExternalFile /*&& audio*/) {
                    iv_add_more_pics.performClick()
                } else {
                    toast("Enable camera and Write Storage permissions from app settings")
                }
            }
        }
    }

    fun updatePicsCount(size: String) {
        tv_selected_pics_count.text = "$size/10"
    }

    private var addTagsDialogFragment: AddTagsDialogFragment? = null

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.iv_add_more_pics -> {
                Pix.start(this@UploadFeedActivity, 100, 10)
            }

            R.id.iv_add_tag -> {
                /*val addTagsDialogFragmentV2 = AddTagsDialogFragmentV2()
                addTagsDialogFragmentV2.setUpdateDialogTagListener(this)
                if (selectedTags.size > 0) {
                    addTagsDialogFragmentV2.provideSelectedTags(selectedTags)
                }
                addTagsDialogFragmentV2.show(supportFragmentManager, AddTagsDialogFragmentV2::class.java.simpleName)*/
                addTagsDialogFragment = AddTagsDialogFragment()
                addTagsDialogFragment?.setUpdateDialogTagListener(this)
                addTagsDialogFragment?.show(supportFragmentManager, AddTagsDialogFragment::class.java.simpleName)
                if (selectedTags.size > 0) {
                    addTagsDialogFragment?.provideSelectedTags(selectedTags)
                }
            }

            R.id.ib_back -> {
                onBackPressed()
            }

            R.id.btn_upload -> {
                if (!Utils.isOnline()){
                    toast("Enable internet for upload")
                    return
                }

                et_pics_caption.error = null
                et_pics_caption.clearFocus()

                if (selectedPics.isEmpty()) {
                    toast("Add pics")
                    return
                }

                if(selectedPics.size>10){
                    toast("Only 10 images allowed to upload. Remove extra selected images to proceed.")
                    return
                }

                if (selectedTags.isEmpty()) {
                    toast("Add Tags")
                    return
                }

                if (et_pics_caption.text.isNullOrEmpty() || et_pics_caption.text.trim().isEmpty()) {
                    et_pics_caption.error = "Add Description"
                    et_pics_caption.requestFocus()
//                    toast("Add Description")
                    return
                }

                val tagsJSONArray = JSONArray()
                for (tagItem in selectedTags) {
                    val tagJSONObjectString = Gson().toJson(tagItem, TagsItem::class.java)
                    val tagJSONObject = JSONObject(tagJSONObjectString)
                    tagsJSONArray.put(tagJSONObject)
                }

                val deleteImgsJSONArray = JSONArray()
                for (picUri in selectedMultiPicsAdapter.deleteImagesList) {
                    deleteImgsJSONArray.put(picUri)
                }

                val localSelectedPics = ArrayList<String>()
                for (picUri in selectedPics) {
                    // show error, before viewing in the list
                    // checking path here is not a good thing
                    if (picUri.contains("/storage/")) {
                        localSelectedPics.add(picUri)
                    }
                }

                val etPicsCaptionString = et_pics_caption.text.toString()
                val unicodeCaptionString = StringEscapeUtils.escapeJava(etPicsCaptionString)
                val feedData = FeedData(localSelectedPics, unicodeCaptionString, tagsJSONArray.toString(),
                        feedType, resultItem?.feedId?.toString(), deleteImgsJSONArray.toString())

                startService(Intent(this@UploadFeedActivity, UploadFeedAPIService::class.java)
                        .putExtra(Constants.FEED_DATA, feedData))
                Utils.goToHomeScreen(this,FeedListFragment.FEED_TYPE_NEW)
            }
        }
    }

    override fun updateTags(selectedTags: LinkedList<TagsItem>) {
        this.selectedTags = selectedTags
        if (selectedTags.size > 0) {
            tv_selected_tags_no_view.visibility = View.GONE
            selectedTagsAdapter.updateTags(selectedTags)
            rv_selected_tags.visibility = View.VISIBLE
        } else {
            tv_selected_tags_no_view.visibility = View.VISIBLE
            rv_selected_tags.visibility = View.GONE
        }
    }

    /*Workaround for java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState*/
    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        onSaveInstanceStateCalled = true
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        onSaveInstanceStateCalled = false
    }
}
