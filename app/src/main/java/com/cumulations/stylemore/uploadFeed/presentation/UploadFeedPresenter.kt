package com.cumulations.stylemore.uploadFeed.presentation

import com.cumulations.stylemore.uploadFeed.data.model.MultiPartUpdateFeedRequest
import com.cumulations.stylemore.uploadFeed.data.model.MultiPartUploadFeedRequest
import com.cumulations.stylemore.uploadFeed.domain.UpdateFeedUseCase
import com.cumulations.stylemore.uploadFeed.domain.UploadFeedUseCase

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

interface UploadFeedPresenter {
    fun getUploadFeedResponse(uploadFeedUseCase: UploadFeedUseCase, multiPartUploadFeedRequest: MultiPartUploadFeedRequest)
    fun getUpdateFeedResponse(updateFeedUseCase: UpdateFeedUseCase, multiPartUpdateFeedRequest: MultiPartUpdateFeedRequest)
}
