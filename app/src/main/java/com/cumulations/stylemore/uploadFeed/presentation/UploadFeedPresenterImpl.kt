package com.cumulations.stylemore.uploadFeed.presentation

import android.util.Log
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.rest.RestClient
import com.cumulations.stylemore.uploadFeed.data.model.MultiPartUpdateFeedRequest
import com.cumulations.stylemore.uploadFeed.data.model.MultiPartUploadFeedRequest
import com.cumulations.stylemore.uploadFeed.domain.UpdateFeedUseCase
import com.cumulations.stylemore.uploadFeed.domain.UploadFeedUseCase
import retrofit2.adapter.rxjava.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class UploadFeedPresenterImpl(private val uploadFeedProgressUpdateView: UploadFeedProgressUpdateView) : UploadFeedPresenter {

    override fun getUploadFeedResponse(uploadFeedUseCase: UploadFeedUseCase, multiPartUploadFeedRequest: MultiPartUploadFeedRequest) {
        if (Utils.isOnline()){
            uploadFeedProgressUpdateView.showLoading()
            uploadFeedUseCase.execute(multiPartUploadFeedRequest)
                    .subscribe({
                        uploadFeedProgressUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            uploadFeedProgressUpdateView.provideMPUploadResponse(it)
                        } else if (it?.status.equals("error", true))
                            uploadFeedProgressUpdateView.showError(it.description)
                    }, {
                        uploadFeedProgressUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response()?.errorBody()
                                uploadFeedProgressUpdateView.showError(RestClient.getHttpErrorMessage(responseBody))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                uploadFeedProgressUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                uploadFeedProgressUpdateView.showError(it.message!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            uploadFeedProgressUpdateView.showError(e.message!!)
                        }
                    })

        } else
            uploadFeedProgressUpdateView.showError(Constants.NO_INTERNET)
    }

    override fun getUpdateFeedResponse(updateFeedUseCase: UpdateFeedUseCase, multiPartUpdateFeedRequest: MultiPartUpdateFeedRequest) {
        if (Utils.isOnline()){
            uploadFeedProgressUpdateView.showLoading()
            updateFeedUseCase.execute(multiPartUpdateFeedRequest)
                    .subscribe({
                        uploadFeedProgressUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            uploadFeedProgressUpdateView.provideMPUploadResponse(it)
                        } else if (it?.status.equals("error", true))
                            uploadFeedProgressUpdateView.showError(it.description)
                    }, {
                        uploadFeedProgressUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response()?.errorBody()
                                uploadFeedProgressUpdateView.showError(RestClient.getHttpErrorMessage(responseBody))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                uploadFeedProgressUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                uploadFeedProgressUpdateView.showError(it.message!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            uploadFeedProgressUpdateView.showError(e.message!!)
                        }
                    })

        } else
            uploadFeedProgressUpdateView.showError(Constants.NO_INTERNET)
    }
}
