package com.cumulations.stylemore.uploadFeed.presentation

import com.cumulations.stylemore.base.LoadingView
import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse

/**
 * Created by Amit Tumkur on 16-02-2018.
 */

interface UploadFeedProgressUpdateView : LoadingView {
    fun provideMPUploadResponse(basicResponse: BasicResponse)
    fun showError(error: String)
}
