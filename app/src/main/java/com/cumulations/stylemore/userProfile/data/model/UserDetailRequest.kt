package com.cumulations.stylemore.userProfile.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Amit Tumkur on 14-06-2018.
 */
data class UserDetailRequest(@SerializedName("user_id")
                             val user_id: String? = "",
                             @SerializedName("feed_id")
                             val feed_id: String = "",
                             @SerializedName("comment_id")
                             val comment_id: String = "")