package com.cumulations.stylemore.userProfile.data.model

import com.google.gson.annotations.SerializedName

data class UserFollowingResponse(@SerializedName("result")
                                 val result: List<UserFollowingResultItem>?,
                                 @SerializedName("status")
                                 val status: String = "",
                                 @SerializedName("following")
                                 val following: Boolean?,
                                 @SerializedName("description")
                                 val description: String = "")