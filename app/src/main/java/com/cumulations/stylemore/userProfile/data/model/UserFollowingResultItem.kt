package com.cumulations.stylemore.userProfile.data.model

import com.google.gson.annotations.SerializedName

data class UserFollowingResultItem(@SerializedName("user_id")
                                   val userId: Int = 0,
                                   @SerializedName("name")
                                   val name: String = "",
                                   @SerializedName("profile_pic")
                                   val profilePic: String = "",
                                   @SerializedName("following_me")
                                   var followingMe: Boolean? = false)