package com.cumulations.stylemore.userProfile.data.model

import com.google.gson.annotations.SerializedName

data class UserLikedResponse(@SerializedName("result")
                             val result: List<UserLikedResultItem>?,
                             @SerializedName("status")
                             val status: String = "",
                             @SerializedName("following")
                             val following: Boolean?,
                             @SerializedName("description")
                             val description: String = "")