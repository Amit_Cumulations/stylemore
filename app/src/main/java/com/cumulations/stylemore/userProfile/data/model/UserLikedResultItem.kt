package com.cumulations.stylemore.userProfile.data.model

import com.google.gson.annotations.SerializedName

data class UserLikedResultItem(@SerializedName("added_on")
                               val addedOn: String = "",
                               @SerializedName("pictures")
                               val pictures: List<String>?,
                               @SerializedName("feed_id")
                               val feedId: Int = 0)