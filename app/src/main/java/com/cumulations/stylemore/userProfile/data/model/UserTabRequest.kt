package com.cumulations.stylemore.userProfile.data.model

import com.google.gson.annotations.SerializedName

data class UserTabRequest(@SerializedName("tab")
                          val tab: String = "",
                          @SerializedName("user_id")
                          val userId: String = "",
                          @SerializedName("item_count")
                          val itemCount: Int = 0,
                          @SerializedName("index")
                          val index: Int = 0)