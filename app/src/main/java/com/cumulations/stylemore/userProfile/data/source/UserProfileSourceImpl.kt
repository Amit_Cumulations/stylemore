package com.cumulations.stylemore.userProfile.data.source

import com.cumulations.stylemore.rest.RestClient
import com.cumulations.stylemore.userProfile.data.model.UserFollowersResponse
import com.cumulations.stylemore.userProfile.data.model.UserFollowingResponse
import com.cumulations.stylemore.userProfile.data.model.UserLikedResponse
import com.cumulations.stylemore.userProfile.data.model.UserTabRequest
import com.cumulations.stylemore.userProfile.domain.UserProfileDataInterface
import rx.Single
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by Amit Tumkur on 29-05-2018.
 */
class UserProfileSourceImpl: UserProfileDataInterface {

    /*override fun getUserActivities(userDetailRequest: UserDetailRequest): Single<MockEarlierFeedsResponse> {
        return RestClient.getJSONGeneratorApiService().getUserActivities(RestClient.getAuthHeaders(),userDetailRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun getUserLikes(userDetailRequest: UserDetailRequest): Single<MockEarlierFeedsResponse> {
        return RestClient.getJSONGeneratorApiService().getUserLikes(RestClient.getAuthHeaders(),userDetailRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun getUserFollowers(userDetailRequest: UserDetailRequest): Single<FeedLikesResponse> {
        return RestClient.getJSONGeneratorApiService().getUserFollowers(RestClient.getAuthHeaders(),userDetailRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun getUserFollowing(userDetailRequest: UserDetailRequest): Single<FeedLikesResponse> {
        return RestClient.getJSONGeneratorApiService().getUserFollowing(RestClient.getAuthHeaders(),userDetailRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }*/

    override fun getUserUploads(userTabRequest: UserTabRequest): Single<UserLikedResponse> {
        return RestClient.getApiService().getUserTab12(RestClient.getAuthHeaders(),userTabRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun getUserLiked(userTabRequest: UserTabRequest): Single<UserLikedResponse> {
        return RestClient.getApiService().getUserTab12(RestClient.getAuthHeaders(),userTabRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun getUserFollowers(userTabRequest: UserTabRequest): Single<UserFollowersResponse> {
        return RestClient.getApiService().getUserFollowers(RestClient.getAuthHeaders(),userTabRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    override fun getUserFollowing(userTabRequest: UserTabRequest): Single<UserFollowingResponse> {
        return RestClient.getApiService().getUserFollowing(RestClient.getAuthHeaders(),userTabRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }
}