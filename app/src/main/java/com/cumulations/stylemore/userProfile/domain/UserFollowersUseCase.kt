package com.cumulations.stylemore.userProfile.domain

import com.cumulations.stylemore.base.UseCase
import com.cumulations.stylemore.userProfile.data.model.UserFollowersResponse
import com.cumulations.stylemore.userProfile.data.model.UserTabRequest
import com.cumulations.stylemore.userProfile.data.source.UserProfileSourceImpl
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class UserFollowersUseCase : UseCase<UserTabRequest, UserFollowersResponse>() {
    private val userProfileDataInterface = UserProfileSourceImpl()

    override fun buildUseCase(requestObj: UserTabRequest): Single<UserFollowersResponse> {
        return userProfileDataInterface.getUserFollowers(requestObj)
    }
}
