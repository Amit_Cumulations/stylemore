package com.cumulations.stylemore.userProfile.domain

import com.cumulations.stylemore.base.UseCase
import com.cumulations.stylemore.feeds.data.model.EarlierPostResponse
import com.cumulations.stylemore.feeds.data.model.FeedLikesResponse
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest
import com.cumulations.stylemore.userProfile.data.model.UserFollowingResponse
import com.cumulations.stylemore.userProfile.data.model.UserTabRequest
import com.cumulations.stylemore.userProfile.data.source.UserProfileSourceImpl
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class UserFollowingUseCase : UseCase<UserTabRequest, UserFollowingResponse>() {
    private val userProfileDataInterface = UserProfileSourceImpl()

    override fun buildUseCase(requestObj: UserTabRequest): Single<UserFollowingResponse> {
        return userProfileDataInterface.getUserFollowing(requestObj)
    }
}
