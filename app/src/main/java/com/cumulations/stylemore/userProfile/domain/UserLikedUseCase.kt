package com.cumulations.stylemore.userProfile.domain

import com.cumulations.stylemore.base.UseCase
import com.cumulations.stylemore.feeds.data.model.EarlierPostResponse
import com.cumulations.stylemore.feeds.data.model.MockEarlierFeedsResponse
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest
import com.cumulations.stylemore.userProfile.data.model.UserLikedResponse
import com.cumulations.stylemore.userProfile.data.model.UserTabRequest
import com.cumulations.stylemore.userProfile.data.source.UserProfileSourceImpl
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class UserLikedUseCase : UseCase<UserTabRequest, UserLikedResponse>() {
    private val userProfileDataInterface = UserProfileSourceImpl()

    override fun buildUseCase(requestObj: UserTabRequest): Single<UserLikedResponse> {
        return userProfileDataInterface.getUserLiked(requestObj)
    }
}
