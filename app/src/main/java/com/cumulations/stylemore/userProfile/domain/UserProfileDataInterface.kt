package com.cumulations.stylemore.userProfile.domain

import com.cumulations.stylemore.userProfile.data.model.UserFollowersResponse
import com.cumulations.stylemore.userProfile.data.model.UserFollowingResponse
import com.cumulations.stylemore.userProfile.data.model.UserLikedResponse
import com.cumulations.stylemore.userProfile.data.model.UserTabRequest
import rx.Single

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

interface UserProfileDataInterface {
    /*fun getUserActivities(userDetailRequest: UserDetailRequest): Single<MockEarlierFeedsResponse>
    fun getUserLikes(userDetailRequest: UserDetailRequest): Single<MockEarlierFeedsResponse>
    fun getUserFollowers(userDetailRequest: UserDetailRequest): Single<FeedLikesResponse>
    fun getUserFollowing(userDetailRequest: UserDetailRequest): Single<FeedLikesResponse>*/

    fun getUserUploads(userTabRequest: UserTabRequest): Single<UserLikedResponse>
    fun getUserLiked(userTabRequest: UserTabRequest): Single<UserLikedResponse>
    fun getUserFollowers(userTabRequest: UserTabRequest): Single<UserFollowersResponse>
    fun getUserFollowing(userTabRequest: UserTabRequest): Single<UserFollowingResponse>
}
