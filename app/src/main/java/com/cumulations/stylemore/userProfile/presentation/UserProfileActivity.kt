package com.cumulations.stylemore.userProfile.presentation

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.view.View
import com.cumulations.stylemore.R
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.firebase.FbNotification
import com.cumulations.stylemore.base.firebase.FirebaseSyncService
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper.get
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.domain.FollowUnfollowUseCase
import com.cumulations.stylemore.feeds.presentation.DialogBtnsListener
import com.cumulations.stylemore.feeds.presentation.FollowUserPresenterImpl
import com.cumulations.stylemore.feeds.presentation.FollowUserUiUpdateView
import com.cumulations.stylemore.feeds.presentation.TwoBtnDialogFragment
import com.cumulations.stylemore.login.presentation.SplashScreenActivity
import com.cumulations.stylemore.notifications.NotificationsActivity
import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest
import com.facebook.login.LoginManager
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_user_profile.*
import org.jetbrains.anko.toast


class UserProfileActivity : AppCompatActivity(), FollowUserUiUpdateView {
    private val tabTitles = arrayOf(
            "Uploads",
            "Liked",
            "Followers",
            "Following"
    )

    private var userName: String? = ""
    private var userPic: String? = ""
    private var userId: String? = ""
    private var userFollowing: Boolean = false
    private var followingClicked = false

    private lateinit var tabsViewPagerAdapter: TabsViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!Utils.isOnline()){
            toast(getString(R.string.no_internet_connection))
            finish()
            return
        }

        setContentView(R.layout.activity_user_profile)

        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        userId = intent?.getIntExtra(Constants.USER_ID, -1).toString()
        userName = intent?.getStringExtra(Constants.USER_FULLNAME)
        userPic = intent?.getStringExtra(Constants.USER_PROF_PIC)

        val userPref = SharedPreferenceHelper.customPrefs(this, Constants.USER_PREF)
        val prefUserId = userPref[Constants.USER_ID, ""]

        if (userId.isNullOrEmpty() || userId.equals("-1")) {
            ib_follow_user.visibility = View.GONE
            tv_title_toolbar.text = "My Profile"
            userId = prefUserId
        } else if (!prefUserId!!.isEmpty()) {
            if (userId!! == prefUserId) {
                iv_notifications.visibility = View.VISIBLE
                iv_logout.visibility = View.VISIBLE
                ib_follow_user.visibility = View.GONE
                tv_title_toolbar.text = "My Profile"
            } else {
                iv_notifications.visibility = View.GONE
                iv_logout.visibility = View.GONE
                ib_follow_user.visibility = View.VISIBLE
//                toggleFollowIcon()
                tv_title_toolbar.text = "User Profile"
            }
        }

        if (userName.isNullOrEmpty() && userPic.isNullOrEmpty()) {
            userPic = userPref[Constants.USER_PROF_PIC, ""]!!
            userName = userPref[Constants.USER_FULLNAME, ""]!!
        }

        Picasso.get().load(userPic)
                .placeholder(R.drawable.user_placeholder_square)
                .into(civ_profile_icon)
        tv_user_fullname.text = userName

        tabsViewPagerAdapter = TabsViewPagerAdapter(supportFragmentManager)
        tabsViewPager.adapter = tabsViewPagerAdapter
        tabsViewPager.offscreenPageLimit = 4
        fragment_tabs.setupWithViewPager(tabsViewPager)
        fragment_tabs.setTabTextColors(ContextCompat.getColor(this, R.color.slight_grey_text),
                ContextCompat.getColor(this, R.color.colorPrimary))

        tv_terms_conditions.setOnClickListener {
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://stylemore.in/terms-%26-conditions")))
            } catch (e:Exception){
                e.printStackTrace()
            }
        }

        ib_back.setOnClickListener {
            onBackPressed()
        }

        iv_notifications.setOnClickListener {
            startActivity(Intent(this, NotificationsActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK))
        }

        ib_follow_user.setOnClickListener {
            followingClicked = true
            followUnfollowUser()
        }

        iv_logout.setOnClickListener {

            val twoBtnDialogFragment = TwoBtnDialogFragment()
            val bundle = Bundle()
                bundle.putString(Constants.DLG_TITLE, "Do you want to logout?")
                bundle.putString(Constants.DLG_POS_BTN, "Logout")
            bundle.putString(Constants.DLG_NEG_BTN, "Cancel")
            twoBtnDialogFragment.arguments = bundle
            twoBtnDialogFragment.show(supportFragmentManager, TwoBtnDialogFragment::class.java.simpleName)
            twoBtnDialogFragment.setDialogBtnsListener(object : DialogBtnsListener {
                override fun onPositiveBtnClick(view: View) {
                    LoginManager.getInstance().logOut()
                    SharedPreferenceHelper.clearCustomPrefs(application, Constants.USER_PREF)
                    stopService(Intent(applicationContext, FirebaseSyncService::class.java))
                    FirebaseDatabase.getInstance().goOffline()
                    startActivity(Intent(this@UserProfileActivity, SplashScreenActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
                    finish()
                    twoBtnDialogFragment.dismiss()
//                    System.exit(0)
                }

                override fun onNegativeBtnClick(view: View) {
                    twoBtnDialogFragment.dismiss()
                }

            })
        }
    }

    override fun onStart() {
        super.onStart()
        if (isPaused)
            tabsViewPagerAdapter.refresh()
    }

    override fun onStop() {
        super.onStop()
        isPaused = true
    }

    private var isPaused: Boolean = false

    inner class TabsViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        private val items: HashMap<String, UserProfileDataFragment> = HashMap()

        override fun getItem(position: Int): UserProfileDataFragment {
            val userProfileDataFragment = UserProfileDataFragment()
            val bundle = Bundle()
            bundle.putString(Constants.USER_ID, userId!!)
            when (tabTitles[position]) {
                "Uploads" -> {
                    bundle.putInt(Constants.TAB_TYPE, UserProfileDataFragment.TYPE_ACTIVITIES)
                    if (items.containsKey("Uploads")) {
                        return items["Uploads"]!!
                    } else {
                        items["Uploads"] = userProfileDataFragment
                    }
                }
                "Liked" -> {
                    bundle.putInt(Constants.TAB_TYPE, UserProfileDataFragment.TYPE_LIKES)
                    if (items.containsKey("Liked")) {
                        return items["Liked"]!!
                    } else {
                        items["Liked"] = userProfileDataFragment
                    }
                }

                "Following" -> {
                    bundle.putInt(Constants.TAB_TYPE, UserProfileDataFragment.TYPE_FOLLOWING)
                    if (items.containsKey("Following")) {
                        return items["Following"]!!
                    } else {
                        items["Following"] = userProfileDataFragment
                    }
                }

                "Followers" -> {
                    bundle.putInt(Constants.TAB_TYPE, UserProfileDataFragment.TYPE_FOLLOWERS)
                    if (items.containsKey("Followers")) {
                        return items["Followers"]!!
                    } else {
                        items["Followers"] = userProfileDataFragment
                    }
                }
            }
            userProfileDataFragment.arguments = bundle
            return userProfileDataFragment
        }

        fun refresh() {
            val iterate = items.keys.iterator()
            while (iterate.hasNext()) {
                try {
                    val key = iterate.next()
                    items[key]!!.refresh()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        override fun getCount(): Int {
            return tabTitles.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return tabTitles[position]
        }
    }

    private fun toggleFollowIcon() {
        if (userFollowing) {
            ib_follow_user.setImageResource(R.drawable.ic_following_red)
        } else {
            ib_follow_user.setImageResource(R.drawable.ic_follow_black)
        }
    }

    fun followUnfollowUser() {
        val twoBtnDialogFragment = TwoBtnDialogFragment()
        val bundle = Bundle()
        if (userFollowing) {
            bundle.putString(Constants.DLG_TITLE, "Unfollow ${userName}?")
            bundle.putString(Constants.DLG_POS_BTN, "Unfollow")
        } else {
            bundle.putString(Constants.DLG_TITLE, "Follow ${userName}?")
            bundle.putString(Constants.DLG_POS_BTN, "Follow")
        }
        bundle.putString(Constants.DLG_NEG_BTN, "Cancel")
        twoBtnDialogFragment.arguments = bundle

        twoBtnDialogFragment.show(supportFragmentManager, TwoBtnDialogFragment::class.java.simpleName)

        twoBtnDialogFragment.setDialogBtnsListener(object : DialogBtnsListener {
            override fun onPositiveBtnClick(view: View) {
                val followUserPresenterImpl = FollowUserPresenterImpl(this@UserProfileActivity)
                val userDetailRequest = UserDetailRequest(userId, "")
                followUserPresenterImpl.getFollowResponse(FollowUnfollowUseCase(), userDetailRequest)
                twoBtnDialogFragment.dismiss()
            }

            override fun onNegativeBtnClick(view: View) {
                twoBtnDialogFragment.dismiss()
            }

        })
    }

    override fun provideFollowUserResult(basicResponse: BasicResponse) {
        if (basicResponse.result.equals("success", false)) {
            userFollowing = !userFollowing
            val userIdDbReference = FirebaseDatabase.getInstance().reference
                    .child("Notifications")
                    .child("${userId}")
            val timestamp = System.currentTimeMillis()
            if (userFollowing) {
                if (!StyleMoreApp.user_id.equals(userId)) {
                    val fbNotification = FbNotification(timestamp, "followed", Utils.getLoggedInActionUser(this),
                            -1, "NIL", timestamp, false, false)
                    fbNotification.actionToUserId = userId.toString()
                    Utils.pushActionToFirebaseDb(userIdDbReference, fbNotification)
                }
            } else {
                if (!StyleMoreApp.user_id.equals(userId)) {
                    val fbNotification = FbNotification(timestamp, "unfollowed", Utils.getLoggedInActionUser(this),
                            -1, "NIL", timestamp, false, false)
                    Utils.pushActionToFirebaseDb(userIdDbReference, fbNotification)
                }
            }
            toggleFollowIcon()
            tabsViewPagerAdapter.getItem(2).refresh()
        }
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun errorOccured(errorMsg: String) {
        toast(errorMsg)
    }

    fun updateFragment() {
        if (tabsViewPager.currentItem == 2) {
            tabsViewPagerAdapter.getItem(3).refresh()
        } else if (tabsViewPager.currentItem == 3) {
            tabsViewPagerAdapter.getItem(2).refresh()
            tabsViewPagerAdapter.getItem(3).refresh()

        }

    }

    fun updateFollowIcon(following: Boolean) {
        userFollowing = following
        toggleFollowIcon()
    }
}
