package com.cumulations.stylemore.userProfile.presentation

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cumulations.stylemore.R
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.SharedPreferenceHelper
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.login.presentation.SplashScreenActivity
import com.cumulations.stylemore.userProfile.data.model.UserFollowersResponse
import com.cumulations.stylemore.userProfile.data.model.UserFollowingResponse
import com.cumulations.stylemore.userProfile.data.model.UserLikedResponse
import com.cumulations.stylemore.userProfile.data.model.UserTabRequest
import com.cumulations.stylemore.userProfile.domain.UserFollowingUseCase
import com.cumulations.stylemore.userProfile.domain.UserLikedUseCase
import com.cumulations.stylemore.userProfile.domain.UserUploadsUseCase
import com.cumulations.stylemore.userProfile.presentation.adapters.UserFollowersAdapter
import com.cumulations.stylemore.userProfile.presentation.adapters.UserFollowingAdapter
import com.cumulations.stylemore.userProfile.presentation.adapters.UserUploadsAdapter
import kotlinx.android.synthetic.main.fragment_title_recyclerview_only.*
import kotlinx.android.synthetic.main.layout_full_screen_loader.*
import org.jetbrains.anko.toast
import java.util.*

/**
 * Created by Amit Tumkur on 14-06-2018.
 */
class UserProfileDataFragment : Fragment(), UserProfileUiUpdateView {

    companion object {
        const val TYPE_ACTIVITIES = 11
        const val TYPE_LIKES = 12
        const val TYPE_FOLLOWERS = 13
        const val TYPE_FOLLOWING = 14
    }

    private var tabType: Int = TYPE_ACTIVITIES
    private lateinit var userUploadsAdapter: UserUploadsAdapter
    private lateinit var userFollowersAdapter: UserFollowersAdapter
    private lateinit var userFollowingAdapter: UserFollowingAdapter

    private var currentPageCount = 0
    private var isLastPage = false
    private var isPageLoading = false

    private lateinit var listLayoutManager: LinearLayoutManager
    private lateinit var gridLayoutManager: GridLayoutManager

    private var pageSize = 10

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_title_recyclerview_only, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabType = arguments?.getInt(Constants.TAB_TYPE) ?: TYPE_ACTIVITIES

        if (Utils.getScreenHeightInPixel(activity as Activity)>1920){
            pageSize = 15
        }

        initAdapterType()
    }

    private fun initAdapterType() {
        tv_title.text = ""
        when (tabType) {
            TYPE_LIKES, TYPE_ACTIVITIES -> {
                setUpGridAdapter()
            }

            TYPE_FOLLOWERS, TYPE_FOLLOWING -> {
                setUpListAdapter()
            }
        }

        rv_fragment_list.addOnScrollListener(recyclerViewOnScrollListener)

        swipe_refresh_layout.setOnRefreshListener {

            if (!Utils.isOnline()){
                activity?.toast(Constants.NO_INTERNET)
                swipe_refresh_layout.isRefreshing = false
                return@setOnRefreshListener
            }

            when (tabType) {
                TYPE_LIKES -> {
                    userUploadsAdapter.userLikedList?.clear()
                    userUploadsAdapter.notifyDataSetChanged()
                }

                TYPE_ACTIVITIES -> {
                    userUploadsAdapter.userLikedList?.clear()
                    userUploadsAdapter.notifyDataSetChanged()
                }

                TYPE_FOLLOWERS -> {
                    userFollowingAdapter.userFollowingList?.clear()
                    userFollowingAdapter.notifyDataSetChanged()
                }

                TYPE_FOLLOWING -> {
                    userFollowingAdapter.userFollowingList?.clear()
                    userFollowingAdapter.notifyDataSetChanged()
                }
            }

            currentPageCount = 0
            isLastPage = false

            loadNextUserData()
        }

        swipe_refresh_layout.setColorSchemeColors(
                ContextCompat.getColor(activity!!, R.color.insta_light_red),
                ContextCompat.getColor(activity!!, R.color.insta_purple),
                ContextCompat.getColor(activity!!, R.color.insta_orange))

        loadNextUserData()
    }

    private fun setUpGridAdapter() {
        val numberOfColumns = 3
        userUploadsAdapter = UserUploadsAdapter(activity!!, ArrayList())
        gridLayoutManager = GridLayoutManager(activity!!, numberOfColumns)
        rv_fragment_list.layoutManager = gridLayoutManager
        rv_fragment_list.adapter = userUploadsAdapter
    }

    private fun setUpListAdapter() {
        userFollowingAdapter = UserFollowingAdapter(activity!!, ArrayList())
        listLayoutManager = LinearLayoutManager(this.activity!!)
        rv_fragment_list.layoutManager = listLayoutManager
        rv_fragment_list.itemAnimator = DefaultItemAnimator()
        rv_fragment_list.adapter = userFollowingAdapter
    }

    private fun loadNextUserData() {
        val userProfilePresenter = UserProfilePresenterImpl(this)
        val userId = arguments?.getString(Constants.USER_ID)

        currentPageCount++

        when (tabType) {
            TYPE_ACTIVITIES -> {
                val userTabRequest = UserTabRequest("activity", userId!!, pageSize, currentPageCount)
                userProfilePresenter.getUserUploadsResponse(UserUploadsUseCase(), userTabRequest)
            }

            TYPE_LIKES -> {
                val userTabRequest = UserTabRequest("liked", userId!!, pageSize, currentPageCount)
                userProfilePresenter.getUserLikedResponse(UserLikedUseCase(), userTabRequest)
            }

            TYPE_FOLLOWING -> {
                val userTabRequest = UserTabRequest("following", userId!!, pageSize, currentPageCount)
                userProfilePresenter.getUserFollowingResponse(UserFollowingUseCase(), userTabRequest)
            }

            TYPE_FOLLOWERS -> {
                val userTabRequest = UserTabRequest("followers", userId!!, pageSize, currentPageCount)
                userProfilePresenter.getUserFollowingResponse(UserFollowingUseCase(), userTabRequest)
            }
        }
    }

    override fun showLoading() {
        if (activity == null || activity!!.isFinishing)
            return
        isPageLoading = true
        full_screen_loader_layout.visibility = View.VISIBLE
        Utils.showLoader(loader, null, this.activity!!)
    }

    override fun hideLoading() {
        if (activity == null || activity!!.isFinishing)
            return
        isPageLoading = false
        full_screen_loader_layout.visibility = View.GONE
        Utils.closeLoader(loader, null, this.activity!!)
    }

    override fun provideUserUploads(userLikedResponse: UserLikedResponse) {
        if (activity == null || activity?.isFinishing!!)
            return
        tv_no_data.visibility = View.GONE
        rv_fragment_list.visibility = View.VISIBLE
        swipe_refresh_layout.isRefreshing = false
        if (userLikedResponse.result!!.size < pageSize) {
            isLastPage = true
        }

        userUploadsAdapter.updateList(userLikedResponse.result)
        if (isLastPage)
            tv_title.text = "Total ${userUploadsAdapter.userLikedList?.size} uploads"
        else
            tv_title.text = "Last ${userUploadsAdapter.userLikedList?.size} uploads"
    }

    override fun provideUserLiked(userLikedResponse: UserLikedResponse) {
        if (activity == null || activity?.isFinishing!!)
            return
        tv_no_data.visibility = View.GONE
        rv_fragment_list.visibility = View.VISIBLE
        swipe_refresh_layout.isRefreshing = false

        if (userLikedResponse.following!=null){
            (activity as UserProfileActivity).updateFollowIcon(userLikedResponse.following)
        }

        if (userLikedResponse.result!!.size < pageSize) {
            isLastPage = true
        }
        userUploadsAdapter.updateList(userLikedResponse.result)

        if (isLastPage)
            tv_title.text = "Total ${userUploadsAdapter.userLikedList?.size} styles liked"
        else
            tv_title.text = "Last ${userUploadsAdapter.userLikedList?.size} styles liked"
    }

    override fun provideUserFollowers(userFollowersResponse: UserFollowersResponse) {
        if (activity == null || activity?.isFinishing!!)
            return
        tv_no_data.visibility = View.GONE
        rv_fragment_list.visibility = View.VISIBLE
        swipe_refresh_layout.isRefreshing = false

        if (userFollowersResponse.following!=null){
            (activity as UserProfileActivity).updateFollowIcon(userFollowersResponse.following)
        }

        if (userFollowersResponse.result!!.size < pageSize) {
            isLastPage = true
        }
        userFollowersAdapter.updateList(userFollowersResponse?.result)
        tv_title.text = "${userFollowingAdapter.userFollowingList?.size} followers"

        if (isLastPage)
            tv_title.text = "Total ${userFollowingAdapter.userFollowingList?.size} followers"
        else
            tv_title.text = "Last ${userFollowingAdapter.userFollowingList?.size} followers"
    }

    override fun provideUserFollowing(userFollowingResponse: UserFollowingResponse) {
        if (activity == null || activity?.isFinishing!!)
            return
        tv_no_data.visibility = View.GONE
        rv_fragment_list.visibility = View.VISIBLE
        swipe_refresh_layout.isRefreshing = false

        if (userFollowingResponse.following!=null){
            (activity as UserProfileActivity).updateFollowIcon(userFollowingResponse.following)
        }

        if (userFollowingResponse.result!!.size < pageSize) {
            isLastPage = true
        }
        userFollowingAdapter.updateList(userFollowingResponse.result)

        when (tabType) {
            TYPE_FOLLOWERS -> {
                if (isLastPage)
                    tv_title.text = "Total ${userFollowingAdapter.userFollowingList?.size} " +
                            if (userFollowingAdapter.userFollowingList?.size!! <= 1) "follower" else "followers"
                else
                    tv_title.text = "Last ${userFollowingAdapter.userFollowingList?.size} " +
                            if (userFollowingAdapter.userFollowingList?.size!! <= 1) "follower" else "followers"

                /*Commenting since this solution won't work is logged in userid isn't present in 1st pagination*/
                /*var isFollowing = false
                userFollowingResponse.result.forEach {
                    if (StyleMoreApp.user_id == it?.userId.toString()) {
                        isFollowing = true
                        return@forEach
                    }
                }
                (activity as UserProfileActivity).updateFollowIcon(isFollowing)*/
            }

            TYPE_FOLLOWING -> {
                if (isLastPage)
                    tv_title.text = "Total ${userFollowingAdapter.userFollowingList?.size} following"
                else
                    tv_title.text = "Last ${userFollowingAdapter.userFollowingList?.size} following"
            }
        }
    }

    override fun showError(error: String) {
        if (activity?.isFinishing!!)
            return

        swipe_refresh_layout.isRefreshing = false

        if (error == Constants.INVALID_AUTH_KEY) {
            activity?.toast(error)
            SharedPreferenceHelper.clearCustomPrefs(activity!!, Constants.USER_PREF)
            activity!!.startActivity(Intent(activity, SplashScreenActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
            activity!!.finish()
        } else if (error == Constants.NO_DATA_FOUND) {
//            isLastPage = true
            if (currentPageCount ==  1) {
                when (tabType) {
                    TYPE_ACTIVITIES -> {
                        if (userUploadsAdapter.itemCount == 0) {
                            tv_no_data.text = "0 uploads"
                            tv_no_data.visibility = View.VISIBLE
                            rv_fragment_list.visibility = View.GONE
                        }
                    }

                    TYPE_LIKES -> {
                        if (userUploadsAdapter.itemCount == 0) {
                            tv_no_data.text = "0 styles liked"
                            tv_no_data.visibility = View.VISIBLE
                            rv_fragment_list.visibility = View.GONE
                        }
                    }

                    TYPE_FOLLOWERS -> {
                        if (userFollowingAdapter.itemCount == 0) {
                            tv_no_data.text = "0 followers"
                            tv_no_data.visibility = View.VISIBLE
                            rv_fragment_list.visibility = View.GONE

                            /*Hack to update following (this user) icon of currently logged in User*/
                            if (activity!=null)
                                (activity as UserProfileActivity).updateFollowIcon(false)
                        }
                    }

                    TYPE_FOLLOWING -> {
                        if (userFollowingAdapter.itemCount == 0) {
                            tv_no_data.text = "0 following"
                            tv_no_data.visibility = View.VISIBLE
                            rv_fragment_list.visibility = View.GONE
                        }
                    }
                }
            } else if (!isPageLoading){
                isLastPage = true
                when(tabType){
                    TYPE_ACTIVITIES, TYPE_LIKES->{
                        activity?.toast("No further feeds")
                    }
                    TYPE_FOLLOWERS->{
                        activity?.toast("No further followers")
                    }

                    TYPE_FOLLOWING->{
                        activity?.toast("No further following")
                    }
                }
            }
        } /*else {
            activity?.toast(error)
            if (currentPageCount>0)
                currentPageCount--
        }*/
    }

    fun refresh() {
        currentPageCount = 0
        isPageLoading = false
        isLastPage = false
        initAdapterType()
    }

    private val recyclerViewOnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if (dy > 0) {
                var visibleItemCount = -1
                var totalItemCount = -1
                var firstVisibleItemPosition = -1

                if (isPageLoading || isLastPage || swipe_refresh_layout.isRefreshing) {
                    return
                }

                when (tabType) {

                    TYPE_ACTIVITIES -> {
                        visibleItemCount = gridLayoutManager.childCount
                        totalItemCount = gridLayoutManager.itemCount
                        firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition()

                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                                && totalItemCount >= pageSize) {
                            loadNextUserData()
                        }
                    }

                    TYPE_LIKES -> {
                        visibleItemCount = gridLayoutManager.childCount
                        totalItemCount = gridLayoutManager.itemCount
                        firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition()
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                                && totalItemCount >= pageSize) {
                            loadNextUserData()
                        }
                    }

                    TYPE_FOLLOWERS -> {
                        visibleItemCount = listLayoutManager.childCount
                        totalItemCount = listLayoutManager.itemCount
                        firstVisibleItemPosition = listLayoutManager.findFirstVisibleItemPosition()
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                                && totalItemCount >= pageSize) {
                            loadNextUserData()
                        }
                    }

                    TYPE_FOLLOWING -> {
                        visibleItemCount = listLayoutManager.childCount
                        totalItemCount = listLayoutManager.itemCount
                        firstVisibleItemPosition = listLayoutManager.findFirstVisibleItemPosition()
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                                && totalItemCount >= pageSize) {
                            loadNextUserData()
                        }
                    }
                }
            }
        }
    }

    /*override fun onStop() {
        super.onStop()
        rv_fragment_list.removeOnScrollListener(recyclerViewOnScrollListener)
        currentPageCount = 0
        isPageLoading = false
        isLastPage = false
    }*/

    override fun onDestroyView() {
        super.onDestroyView()
        rv_fragment_list.removeOnScrollListener(recyclerViewOnScrollListener)
        currentPageCount = 0
        isPageLoading = false
        isLastPage = false
    }
}