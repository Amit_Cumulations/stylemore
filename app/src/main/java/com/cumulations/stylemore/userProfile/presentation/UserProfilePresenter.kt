package com.cumulations.stylemore.userProfile.presentation

import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest
import com.cumulations.stylemore.userProfile.data.model.UserTabRequest
import com.cumulations.stylemore.userProfile.domain.UserUploadsUseCase
import com.cumulations.stylemore.userProfile.domain.UserFollowersUseCase
import com.cumulations.stylemore.userProfile.domain.UserFollowingUseCase
import com.cumulations.stylemore.userProfile.domain.UserLikedUseCase

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

interface UserProfilePresenter {
    fun getUserUploadsResponse(userUploadsUseCase: UserUploadsUseCase, userTabRequest: UserTabRequest)
    fun getUserLikedResponse(userLikedUseCase: UserLikedUseCase, userTabRequest: UserTabRequest)
    fun getUserFollowersResponse(userFollowersUseCase: UserFollowersUseCase, userTabRequest: UserTabRequest)
    fun getUserFollowingResponse(userFollowingUseCase: UserFollowingUseCase, userTabRequest: UserTabRequest)
}
