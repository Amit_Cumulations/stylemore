package com.cumulations.stylemore.userProfile.presentation

import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.rest.RestClient
import com.cumulations.stylemore.userProfile.data.model.UserTabRequest
import com.cumulations.stylemore.userProfile.domain.UserUploadsUseCase
import com.cumulations.stylemore.userProfile.domain.UserFollowersUseCase
import com.cumulations.stylemore.userProfile.domain.UserFollowingUseCase
import com.cumulations.stylemore.userProfile.domain.UserLikedUseCase
import retrofit2.adapter.rxjava.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


/**
 * Created by Amit Tumkur on 19-02-2018.
 */

class UserProfilePresenterImpl(var userProfileUiUpdateView: UserProfileUiUpdateView) : UserProfilePresenter {

    override fun getUserUploadsResponse(userUploadsUseCase: UserUploadsUseCase, userTabRequest: UserTabRequest) {
        if (Utils.isOnline()){
            userProfileUiUpdateView.showLoading()
            userUploadsUseCase.execute(userTabRequest)
                    .subscribe({
                        userProfileUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            userProfileUiUpdateView.provideUserUploads(it!!)
                        } else if (it?.status.equals("error", true))
                            userProfileUiUpdateView.showError(it?.description!!)
                    }, {
                        userProfileUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                userProfileUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                userProfileUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                if (it.localizedMessage != null)
                                    userProfileUiUpdateView.showError(it.localizedMessage!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (e.localizedMessage!=null)
                                userProfileUiUpdateView.showError(e.localizedMessage!!)
                        }
                    })

        } else
            userProfileUiUpdateView.showError(Constants.NO_INTERNET)
    }

    override fun getUserLikedResponse(userLikedUseCase: UserLikedUseCase, userTabRequest: UserTabRequest) {
        if (Utils.isOnline()){
            userProfileUiUpdateView.showLoading()
            userLikedUseCase.execute(userTabRequest)
                    .subscribe({
                        userProfileUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            userProfileUiUpdateView.provideUserLiked(it!!)
                        } else if (it?.status.equals("error", true))
                            userProfileUiUpdateView.showError(it?.description!!)
                    }, {
                        userProfileUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                userProfileUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                userProfileUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                if (it.localizedMessage != null)
                                    userProfileUiUpdateView.showError(it.localizedMessage!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (e.localizedMessage!=null)
                                userProfileUiUpdateView.showError(e.localizedMessage!!)
                        }
                    })

        } else
            userProfileUiUpdateView.showError(Constants.NO_INTERNET)
    }

    override fun getUserFollowersResponse(userFollowersUseCase: UserFollowersUseCase, userTabRequest: UserTabRequest) {
        if (Utils.isOnline()){
            userProfileUiUpdateView.showLoading()
            userFollowersUseCase.execute(userTabRequest)
                    .subscribe({
                        userProfileUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            userProfileUiUpdateView.provideUserFollowers(it!!)
                        } else if (it?.status.equals("error", true))
                            userProfileUiUpdateView.showError(it?.description!!)
                    }, {
                        userProfileUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                userProfileUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                userProfileUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                if (it.localizedMessage != null)
                                    userProfileUiUpdateView.showError(it.localizedMessage!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (e.localizedMessage!=null)
                                userProfileUiUpdateView.showError(e.localizedMessage!!)
                        }
                    })

        } else
            userProfileUiUpdateView.showError(Constants.NO_INTERNET)
    }

    override fun getUserFollowingResponse(userFollowingUseCase: UserFollowingUseCase, userTabRequest: UserTabRequest) {
        if (Utils.isOnline()){
            userProfileUiUpdateView.showLoading()
            userFollowingUseCase.execute(userTabRequest)
                    .subscribe({
                        userProfileUiUpdateView.hideLoading()
                        if (it?.status.equals("success", true)) {
                            userProfileUiUpdateView.provideUserFollowing(it!!)
                        } else if (it?.status.equals("error", true))
                            userProfileUiUpdateView.showError(it?.description!!)
                    }, {
                        userProfileUiUpdateView.hideLoading()
                        try {
                            it.printStackTrace()
                            if (it is HttpException) {
                                val responseBody = it.response().errorBody()
                                userProfileUiUpdateView.showError(RestClient.getHttpErrorMessage(responseBody!!))
                            } else if (it is ConnectException || it is SocketTimeoutException
                                    || it is UnknownHostException) {
                                userProfileUiUpdateView.showError(Constants.NETWORK_ERROR)
                            } else {
                                if (it.localizedMessage != null)
                                    userProfileUiUpdateView.showError(it.localizedMessage!!)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                            if (e.localizedMessage!=null)
                                userProfileUiUpdateView.showError(e.localizedMessage!!)
                        }
                    })

        } else
            userProfileUiUpdateView.showError(Constants.NO_INTERNET)
    }

}
