package com.cumulations.stylemore.userProfile.presentation

import com.cumulations.stylemore.base.LoadingView
import com.cumulations.stylemore.feeds.data.model.FeedLikesResponse
import com.cumulations.stylemore.feeds.data.model.MockEarlierFeedsResponse
import com.cumulations.stylemore.userProfile.data.model.UserFollowersResponse
import com.cumulations.stylemore.userProfile.data.model.UserFollowingResponse
import com.cumulations.stylemore.userProfile.data.model.UserLikedResponse

/**
 * Created by Amit Tumkur on 16-02-2018.
 */

interface UserProfileUiUpdateView : LoadingView {
    /*fun provideUserUploads(mockEarlierFeedsResponse: MockEarlierFeedsResponse)
    fun provideUserLiked(mockEarlierFeedsResponse: MockEarlierFeedsResponse)
    fun provideUserFollowers(feedLikesResponse: FeedLikesResponse)
    fun provideUserFollowing(feedLikesResponse: FeedLikesResponse)*/

    fun provideUserUploads(userLikedResponse: UserLikedResponse)
    fun provideUserLiked(userLikedResponse: UserLikedResponse)
    fun provideUserFollowers(userFollowersResponse: UserFollowersResponse)
    fun provideUserFollowing(userFollowingResponse: UserFollowingResponse)
    fun showError(error: String)
}
