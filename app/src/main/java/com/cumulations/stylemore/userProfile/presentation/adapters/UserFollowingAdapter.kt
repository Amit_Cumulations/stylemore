package com.cumulations.stylemore.userProfile.presentation.adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cumulations.stylemore.R
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.firebase.FbNotification
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.domain.FollowUnfollowUseCase
import com.cumulations.stylemore.feeds.presentation.DialogBtnsListener
import com.cumulations.stylemore.feeds.presentation.FollowUserPresenterImpl
import com.cumulations.stylemore.feeds.presentation.FollowUserUiUpdateView
import com.cumulations.stylemore.feeds.presentation.TwoBtnDialogFragment
import com.cumulations.stylemore.uploadFeed.data.model.BasicResponse
import com.cumulations.stylemore.userProfile.data.model.UserDetailRequest
import com.cumulations.stylemore.userProfile.data.model.UserFollowingResultItem
import com.cumulations.stylemore.userProfile.presentation.UserProfileActivity
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_likes.view.*
import org.jetbrains.anko.toast

class UserFollowingAdapter(val context: Context,
                           var userFollowingList: MutableList<UserFollowingResultItem>?) : RecyclerView.Adapter<UserFollowingAdapter.UserFollowingItemViewHolder>(),
        FollowUserUiUpdateView {

    private var clickedPos = -1

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): UserFollowingItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_likes, parent, false)
        return UserFollowingItemViewHolder(view)
    }


    override fun onBindViewHolder(userFollowingItemViewHolder: UserFollowingItemViewHolder, position: Int) {
        val feedLikesRecordsItem = userFollowingList?.get(position)
        userFollowingItemViewHolder.bindUserFollowing(feedLikesRecordsItem,position)
    }

    override fun getItemCount(): Int {
        return userFollowingList?.size!!
    }

    inner class UserFollowingItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindUserFollowing(userFollowingResultItem: UserFollowingResultItem?, position: Int) {
            Picasso.get().load(userFollowingResultItem?.profilePic)
                    .placeholder(R.drawable.user_placeholder_square)
                    .resize(80, 80)
                    .centerCrop()
                    .into(itemView.civ_user_icon)

            itemView.tv_user_name.text = userFollowingResultItem?.name

            if (StyleMoreApp.user_id == userFollowingResultItem?.userId.toString()){
                itemView.iv_following.visibility = View.GONE
            } else {
                itemView.iv_following.visibility = View.VISIBLE
                if (userFollowingResultItem?.followingMe!!) {
                    itemView.iv_following.setImageResource(R.drawable.ic_following_red)
                } else
                    itemView.iv_following.setImageResource(R.drawable.ic_follow_black)
            }

            itemView.ll_user_name.setOnClickListener {
                context.startActivity(Intent(context, UserProfileActivity::class.java)
                        .putExtra(Constants.USER_ID,userFollowingResultItem?.userId)
                        .putExtra(Constants.USER_FULLNAME,userFollowingResultItem?.name)
                        .putExtra(Constants.USER_FOLLOWING,userFollowingResultItem?.followingMe)
                        .putExtra(Constants.USER_PROF_PIC,userFollowingResultItem?.profilePic))
//                (context as UserProfileActivity).finish()
            }

            itemView.iv_following.setOnClickListener {
                if (userFollowingResultItem != null) {
                    clickedPos = position
                    followUnfollowUser(userFollowingResultItem)
                }
            }
        }
    }

    fun updateList(userFollowingList: List<UserFollowingResultItem>?) {
        this.userFollowingList?.addAll(userFollowingList!!)
        notifyDataSetChanged()
    }

    fun followUnfollowUser(userFollowingResultItem: UserFollowingResultItem){

        val twoBtnDialogFragment = TwoBtnDialogFragment()
        val bundle = Bundle()
        if (userFollowingResultItem.followingMe!!) {
            bundle.putString(Constants.DLG_TITLE, "Unfollow user ${userFollowingResultItem.name}?")
            bundle.putString(Constants.DLG_POS_BTN, "Unfollow")
        } else{
            bundle.putString(Constants.DLG_TITLE, "Follow user ${userFollowingResultItem.name}?")
            bundle.putString(Constants.DLG_POS_BTN, "Follow")
        }
        bundle.putString(Constants.DLG_NEG_BTN,"Cancel")
        twoBtnDialogFragment.arguments = bundle

        twoBtnDialogFragment.show((context as AppCompatActivity).supportFragmentManager, TwoBtnDialogFragment::class.java.simpleName)

        twoBtnDialogFragment.setDialogBtnsListener(object : DialogBtnsListener {
            override fun onPositiveBtnClick(view: View) {

                val followUserPresenterImpl = FollowUserPresenterImpl(this@UserFollowingAdapter)
                val userDetailRequest = UserDetailRequest(userFollowingResultItem.userId.toString(),"")
                followUserPresenterImpl.getFollowResponse(FollowUnfollowUseCase(),userDetailRequest)
                twoBtnDialogFragment.dismiss()

                userFollowingResultItem.followingMe = !userFollowingResultItem.followingMe!!
                notifyItemChanged(clickedPos)
            }

            override fun onNegativeBtnClick(view: View) {
                twoBtnDialogFragment.dismiss()
            }

        })
    }

    override fun provideFollowUserResult(basicResponse: BasicResponse) {
        if (userFollowingList?.size!! == 0)
            return
        val userFollowingResultItem = userFollowingList?.get(clickedPos)
        if (basicResponse.result.equals("success",false)){
            val userIdDbReference = FirebaseDatabase.getInstance().reference
                    .child("Notifications")
                    .child("${userFollowingResultItem?.userId}")
            val timestamp = System.currentTimeMillis()
            if (userFollowingResultItem?.followingMe!!){
                /*push to fb db*/
                if (!StyleMoreApp.user_id.equals(userFollowingResultItem.userId.toString())) {
                    val fbNotification = FbNotification(timestamp,"followed", Utils.getLoggedInActionUser(context),
                            -1,"NIL",timestamp,false, false)
                    fbNotification.actionToUserId = userFollowingResultItem.userId.toString()
                    Utils.pushActionToFirebaseDb(userIdDbReference, fbNotification)
                }
            } else {
                if (!StyleMoreApp.user_id.equals(userFollowingResultItem.userId.toString())) {
                    val fbNotification = FbNotification(timestamp,"unfollowed", Utils.getLoggedInActionUser(context),
                            -1,"NIL",timestamp,false, false)
                    Utils.pushActionToFirebaseDb(userIdDbReference, fbNotification)
                }
            }
        }
        (context as UserProfileActivity).updateFragment()

    }

    override fun errorOccured(errorMsg: String) {
        context.toast(errorMsg)
        val resultItem = userFollowingList?.get(clickedPos)
        resultItem?.followingMe = !resultItem?.followingMe!!
        notifyItemChanged(clickedPos)
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }
}



