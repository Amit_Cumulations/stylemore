package com.cumulations.stylemore.userProfile.presentation.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.cumulations.stylemore.R
import com.cumulations.stylemore.StyleMoreApp
import com.cumulations.stylemore.base.utils.Constants
import com.cumulations.stylemore.base.utils.Utils
import com.cumulations.stylemore.feeds.presentation.FeedDetailsActivity
import com.cumulations.stylemore.userProfile.data.model.UserLikedResultItem
import com.cumulations.stylemore.userProfile.presentation.UserProfileActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.grid_item_image.view.*

class UserUploadsAdapter(val context: Context,
                         var userLikedList: MutableList<UserLikedResultItem>?) : RecyclerView.Adapter<UserUploadsAdapter.UserActivitiesItemViewHolder>() {
    internal var cardLayoutParams: FrameLayout.LayoutParams? = null
    internal var size = (Utils.getScreenWidthInDp(context as AppCompatActivity) / 3)

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): UserActivitiesItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.grid_item_image, parent, false)
        return UserActivitiesItemViewHolder(view)
    }


    override fun onBindViewHolder(userActivitiesItemViewHolder: UserActivitiesItemViewHolder, position: Int) {
        val earlierPostsRecordsItem = userLikedList!![position]
        userActivitiesItemViewHolder.bindUserActivity(earlierPostsRecordsItem)
    }

    override fun getItemCount(): Int {
        return userLikedList?.size!!
    }

    inner class UserActivitiesItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindUserActivity(userLikedResultItem: UserLikedResultItem?){
            cardLayoutParams = FrameLayout.LayoutParams(size,size)

            itemView.cv_parent_layout.layoutParams = cardLayoutParams
            if (userLikedResultItem?.pictures!!.isNotEmpty()) {
                Picasso.get().load(StyleMoreApp.IMAGE_BASE_URL+userLikedResultItem.pictures.get(0))
//                    .memoryPolicy(MemoryPolicy.NO_CACHE)
//                    .networkPolicy(NetworkPolicy.NO_CACHE)
                        .placeholder(R.drawable.placeholder_large)
                        .resize((size/1.5f).toInt(), (size/1.5f).toInt())
//                        .fit()
                        .centerCrop()
                        .into(itemView.iv_feed_image)
            }

            itemView.cv_parent_layout.setOnClickListener {
                val intent = Intent(context, FeedDetailsActivity::class.java)
                intent.putExtra(Constants.FEED_ID,userLikedResultItem.feedId)
                context.startActivity(intent)
//                (context as UserProfileActivity).finish()
            }
        }
    }

    fun updateList(userLikedList: List<UserLikedResultItem>?){
        this.userLikedList?.addAll(userLikedList!!)
        notifyDataSetChanged()
    }

}



